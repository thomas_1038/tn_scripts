import math
import numpy as np
from addmol.f2cc2f import Frac2Cart
f2c = Frac2Cart()

def make_coor_layerbylayer(coor_dir, mat_lat, ind_z, range_surf):
    arr_coor_dir = np.array(coor_dir)

    # Sort the atomic coordinate by z-axis
    coor_dir_rev = arr_coor_dir[np.argsort(arr_coor_dir[:,ind_z])[::-1]].tolist()
    #print(len(coor_dir_rev), "MAX")

    lat_c = math.sqrt(float(mat_lat[ind_z][0])**2 + float(mat_lat[ind_z][1])**2 + float(mat_lat[ind_z][2])**2)
    zmax = float(coor_dir_rev[0][ind_z]) * lat_c

    # Separate the atomic coordinates of the model by layer
    coor_eachlayer = []; index_eachlayer = []
    a = []; b = []; i = 1
    for coor in coor_dir_rev:
        #print(i)
        z = float(coor[ind_z]) * lat_c; diff_z = abs(zmax - z)
        index = coor_dir.index(coor)
        coor = list(map(float, coor))

        # The atoms belong the same layer when the diffirence between them is within the set value
        if i == len(coor_dir_rev):
            #print(i, "END")
            a.append(coor); coor_eachlayer.append(a)
            b.append(index); index_eachlayer.append(b)

        else: 
            if diff_z < range_surf:
                #print(i, "a")
                a.append(coor); b.append(index); i += 1

            else:
                #print(i, "b")
                coor_eachlayer.append(a); index_eachlayer.append(b)
                zmax = z; a = []; a.append(coor); b = []; b.append(index)
                i += 1

    return coor_eachlayer, index_eachlayer

def get_surfatoms(coor_dir, mat_lat, range_surf, surf_roughness):
    arr_coor_dir = np.array(coor_dir)

    # Sort the atomic coordinate by z-axis
    coor_dir_rev = arr_coor_dir[np.argsort(arr_coor_dir[:,2])[::-1]].tolist()

    lat_c = math.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)
    zmax = float(coor_dir_rev[0][2]) * lat_c

    # Separate the atomic coordinates of the model by layer
    coor_eachlayer = []; index_eachlayer = []
    a = []; b = []; i = 1
    for coor in coor_dir_rev:
        z = float(coor[2]) * lat_c; diff_z = abs(zmax - z)
        index = coor_dir.index(coor)
        coor = list(map(float, coor))

        # The atoms belong the same layer when the diffirence between them is within the set value
        if diff_z < range_surf:
            if i == len(coor_dir):
                a.append(coor); coor_eachlayer.append(a)
                b.append(index); index_eachlayer.append(b)
            else: a.append(coor); b.append(index); i += 1

        elif diff_z > range_surf:
            coor_eachlayer.append(a); index_eachlayer.append(b)
            zmax = z; a = []; a.append(coor); b = []; b.append(index)
            i += 1

    surfatoms_dir = coor_eachlayer[0][::-1]
    index_surf = index_eachlayer[0][::-1]
    surfatoms_car = f2c.frac2cart(mat_lat, surfatoms_dir)

    zs_toplayer = [x[2] * lat_c for x in surfatoms_dir]
    dis_in_surfatoms = [abs(float(max(zs_toplayer)) - float(x)) for x in zs_toplayer]

    ind_rough = 0
    for x in dis_in_surfatoms:
        if x <= surf_roughness: continue
        else: ind_rough = 1; break

    if ind_rough == 0: Param_rough = "S"
    else: Param_rough = "R"

    return surfatoms_car, surfatoms_dir, index_surf, Param_rough
# END