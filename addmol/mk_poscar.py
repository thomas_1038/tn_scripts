import os #, shutil
import numpy as np
import addmol.readpos as rp
from addmol import helper, Frac2Cart
f2c = Frac2Cart()

def mk_pos_adline(coor_ad_dir, atomposition_dir_new, list_SD, Mode, labelofel, list_Ename, numofel, list_Num_adatom, eachlabels_el, mat_lat_new, flagS):
    poslines = []; Ltot = ""

    L_label = "  ".join([str(x) for x in list_Ename]) + "  " +  "  ".join([str(x) for x in labelofel])
    L_num = "  ".join([str(x) for x in list_Num_adatom]) + "  " + "  ".join([str(x) for x in numofel])
    L_TTT = "T T T"
    L_forpos = "1.0\n"
    L_forpos += "  ".join([str(x) for x in mat_lat_new[0]]) + "\n"
    L_forpos += "  ".join([str(x) for x in mat_lat_new[1]]) + "\n"
    L_forpos += "  ".join([str(x) for x in mat_lat_new[2]]) + "\n"
    L_forpos += L_label + "\n" + L_num + "\n"
    if flagS == 1: L_forpos += "Selective" + "\n"
    L_forpos += "Direct" + "\n"

    num_line = 1
    for coor in coor_ad_dir:
        L_forpos_inroop = str(num_line) + "\n" + L_forpos; line_ad = ""

        if Mode in ["a"]:
            #shift_x = 0.5 - float(coor[0][0]) ; shift_y = 0.5 - float(coor[0][1])
            #coorX_shift = float(coor[0][0]) + float(shift_x); coorY_shift = float(coor[0][1]) + float(shift_y)
            #if coorX_shift > 1: coorX_shift = coorX_shift - 1
            #elif coorX_shift < 0: coorX_shift = coorX_shift + 1
            #if coorY_shift > 1: coorY_shift = coorY_shift - 1
            #elif coorY_shift < 0: coorY_shift = coorY_shift + 1
            #line_ad += str(coorX_shift) + "  " + str(coorY_shift) + "  " + str(coor[0][2]) + "  "

            line_ad += str(coor[0][0]) + "  " + str(coor[0][1]) + "  " + str(coor[0][2]) + "  "
            if flagS == 1: line_ad += L_TTT + " "
            line_ad += "!" + str(list_Ename[0]) + "\n"

        elif Mode in ["d", "p"]:
            #Gx_point = (float(coor[0][0]) + float(coor[1][0]))/2 ; Gy_point = (float(coor[0][1]) + float(coor[1][1]))/2
            #shift_x = 0.5 - Gx_point ; shift_y = 0.5 - Gy_point

            num_ad = 0; num_pos = 0; list_Num_adatom_forpos = []
            for n in list_Num_adatom:
                num_pos += n
                list_Num_adatom_forpos.append(num_pos)

            for x in coor:
                #coorX_shift = float(x[0]) + float(shift_x); coorY_shift = float(x[1]) + float(shift_y)
                #if coorX_shift > 1: coorX_shift = coorX_shift - 1
                #elif coorX_shift < 0: coorX_shift = coorX_shift + 1
                #if coorY_shift > 1: coorY_shift = coorY_shift - 1
                #elif coorY_shift < 0: coorY_shift = coorY_shift + 1
                #line_ad += str(coorX_shift) + "  " + str(coorY_shift) + "  " + str(x[2]) + "  "

                line_ad += str(x[0]) + "  " + str(x[1]) + "  " + str(x[2]) + "  "
                if flagS == 1: line_ad += L_TTT  + " "

                if len(list_Ename) == 1: line_ad += "!" + str(list_Ename[0]) + "\n"
                elif len(list_Ename) > 1:
                    i = 0
                    for n in list_Num_adatom_forpos:
                        if num_ad < n: break
                        i += 1
                    line_ad += "!" + str(list_Ename[i]) + "\n"
                num_ad += 1

        i = 0
        for coor_sup in atomposition_dir_new:
            line = ""
            #coorX_atm_shift = coor_sup[0] + shift_x; coorY_atm_shift = coor_sup[1] + shift_y
            #if coorX_atm_shift > 1: coorX_atm_shift = coorX_atm_shift - 1
            #elif coorX_atm_shift < 0: coorX_atm_shift = coorX_atm_shift + 1
            #if coorY_atm_shift > 1: coorY_atm_shift = coorY_atm_shift - 1
            #elif coorY_atm_shift < 0: coorY_atm_shift = coorY_atm_shift + 1
            #line += str(coorX_atm_shift) + "  " + str(coorY_atm_shift) + "  " + str(coor_sup[2])  + "  "

            line += str(coor_sup[0]) + "  " + str(coor_sup[1]) + "  " + str(coor_sup[2])  + "  "
            if flagS == 1: line += "  ".join(list_SD[i])
            line += "  " + eachlabels_el[i] + "\n"
            L_forpos_inroop += line
            i += 1

        L_forpos_inroop += line_ad; poslines.append(L_forpos_inroop)
        Ltot += L_forpos_inroop; num_line += 1
    return poslines, Ltot

def mk_poss_and_dir(path, name_dir, ch, poslines, Ltot):
    path_dir = path + "/" + name_dir; os.mkdir(path_dir)

    for i in range(0, len(poslines)):
        if i in range(0, 9): POSCAR_new = path_dir + "/POSCAR_" + ch + "0" + str(i+1) + ".vasp"
        else: POSCAR_new = path_dir + "/POSCAR_" + ch + str(i+1) + ".vasp"
        with open(POSCAR_new, "w") as fp: fp.write(poslines[i])

    with open(path_dir + "/" + name_dir + "_POSCARS", "w") as fpt: fpt.write(Ltot)

"""
def mkpos_allsites(coor_sites, Name, labelofel,list_Ename, numofel, list_Num_adatom, mat_lat_new):
    L_label = " ".join([str(x) for x in labelofel]) + " " + " ".join([str(x) for x in list_Ename])
    L_num = "  ".join([str(x) for x in numofel]) + " " + " ".join([str(x) for x in list_Num_adatom])
    L_num2 = ""
    for num_at in atomnum:
        L_num2 += num_at + " "

        L_forpos1 = L_label + "\n" + "1.0" + "\n"
        L_forpos1 += "  ".join([str(x) for x in mat_lat_new[0]]) + "\n"
        L_forpos1 += "  ".join([str(x) for x in mat_lat_new[1]]) + "\n"
        L_forpos1 += "  ".join([str(x) for x in mat_lat_new[2]]) + "\n"
        L_forpos1 += L_label + "\n" + L_num + "\n"

        if flag_selective >= 0:
            L_forpos1 += "Selective" + "\n"
        L_forpos1 += "Cartesian" + "\n"

        for atmposi in atomposition:
            line_atmps = ""
            for cmp_atmps in atmposi:
                line_atmps += str(cmp_atmps) + "  "
            line_atmps += "\n"
            L_forpos1 += line_atmps

        line_adatmps = ""
        for cmp_adatmps in coor_sites:
            line_adatmps += str(cmp_adatmps[0]) + "  " + str(cmp_adatmps[1]) + " " + str(cmp_adatmps[2]) + " !X \n"

        L_forpos1 += line_adatmps
        POSCAR_new = path + "/POSCAR_" + Name + ".vasp"
        f_pos = open(POSCAR_new, "w")
        f_pos.write(L_forpos1)
        f_pos.close()
"""

def mk_poss(path_poscars):
    pathlist = path_poscars.split("/")
    cur_dir1 = pathlist[-1]
    cur_dir2 = pathlist[-2]
    poscars = sorted([x for x in os.listdir(path_poscars) if "vasp" in x])
    Lp = ""
    for p in poscars:
        with open(path_poscars + "/" + p, "r") as fc: poslines = fc.readlines()

        flagS = 0
        if "S" in poslines[7]: flagS = 1
        sum_num_atoms = sum([int(x) for x in helper.conv_line(poslines[6])])
        for i in range(0, len(poslines)):
            if i == 0: Lp += p.replace(".vasp","") + "\n"; continue
            if i == sum_num_atoms + 8 + flagS: break
            Lp += poslines[i]

    if cur_dir1 == "CONTCARs" or cur_dir1 == "POSCARs":
        with open(path_poscars + "/" + cur_dir2 +  "_POSCARS","w") as fp: fp.write(Lp)
    else:
        with open(path_poscars + "/" + cur_dir2 + "_" + cur_dir1 + "_POSCARS","w") as fp: fp.write(Lp)

def mk_pos(nowpath, filename, firstlines, mat_lat, labelofel, numofel, list_labelofel, flagS, coor_car, list_SD, line_add=""):
    Lpos = str(filename) + "\n1.0\n"
    for line in mat_lat: Lpos += "  ".join(map(str, line)) + "\n"
    Lpos += " ".join(labelofel) + "\n" + " ".join(numofel) + "\n"
    if flagS == 1: Lpos += firstlines[-2] + "\n" + firstlines[-1] + "\n"
    else: Lpos += firstlines[-1] + "\n"
    #Lpos += "Cartesian\n"
    #print(coor_car); sys.exit()
    coor_car_x = helper.adjust_len([str(x) for x in ["{:.10f}".format(float(coor[0])) for coor in coor_car]])
    coor_car_y = helper.adjust_len([str(x) for x in ["{:.10f}".format(float(coor[1])) for coor in coor_car]])
    coor_car_z = helper.adjust_len([str(x) for x in ["{:.10f}".format(float(coor[2])) for coor in coor_car]])

    for i in range(0, len(coor_car_x)):
        Lpos += str(coor_car_x[i]) + " " + str(coor_car_y[i]) + " " + str(coor_car_z[i]) + " "
        if flagS == 1:
            Lpos += " ".join(list_SD[i]) #str(list_SD[i])
        Lpos += " " + str(list_labelofel[i]) + "\n"
    if not line_add == "": Lpos += line_add + "\n"
    with open(nowpath + "/" + filename, "w") as fp: fp.write(Lpos)

def trans_coor_in_pos(filename, path, lab_in):
    flagFC = 1
    x_basis = 0.5; y_basis = 0.5 #; z_basis = 0.5
    coor_basis_in = [float(x_basis),float(y_basis)] #,float(z_basis)]
    #coor_name_in = [str(x_basis), str(y_basis)] #, str(z_basis)]

    with open(path + "/"+ filename) as fp: poslines = fp.readlines()
    labelofel = rp.get_labelofel(poslines); numofel = rp.get_numofel(poslines); sumatomnum = rp.get_sumofnumofel(poslines)
    list_labelofel = rp.mk_labelofel(labelofel, numofel)[0]
    firstlines, flagS = rp.get_firstlines(poslines)
    mat_lat = rp.get_matrix(poslines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
    arr_coor_dir = np.array(coor_dir)
    if flagFC == 0: firstlines[-1] = "Direct"
    else: firstlines[-1] = "Cartesian"
    coor_obj = np.array(coor_dir[list_labelofel.index("!"+lab_in)])
    coor_basis = np.array(coor_basis_in + [coor_obj[2]])
    #x_name, y_name, z_name = coor_name_in[0], coor_name_in[1], "def"

    coor_new_tot = []
    for c in arr_coor_dir:
        coor_new = c - coor_obj + coor_basis; i = 0
        for x in coor_new:
            if x < 0: coor_new[i] = x + 1
            elif x >= 1: coor_new[i] = x - 1
            i += 1
        coor_new_tot.append(coor_new.tolist())
    if flagFC == 1: coor_new_tot = f2c.frac2cart(mat_lat, coor_new_tot)
    if flagS == 1:
        list_tot = np.concatenate([np.array(coor_new_tot), np.array(list_SD)], axis=1)
        list_tot = np.concatenate([list_tot, np.array([[str(x)] for x in list_labelofel])], axis=1)
    else: list_tot = np.concatenate([np.array(coor_new_tot), np.array([[str(x)] for x in list_labelofel])], axis=1)
    a = firstlines + ["  ".join(x) for x in list_tot.tolist()]
    with open(path + "/" + filename, "w") as fpn: fpn.write("\n".join(a) + "\n")
# END: script
