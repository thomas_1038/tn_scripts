#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, math #shutil, sys, re, glob
import numpy as np

import addmol.question as q
import addmol.readpos as rp
import addmol.mk_coorad as mkca
import addmol.helper as hl
from addmol import calc
from addmol import Frac2Cart, GetCandidateAdSite, Screening

f2c = Frac2Cart()
getca = GetCandidateAdSite()
scr = Screening()

################# Parameter ###################
dis_AdtoSup_df = 2.5
d_AS_o = dis_AdtoSup_df
d_AS_b = dis_AdtoSup_df
d_AS_h = dis_AdtoSup_df
Param_rough = "R"

bond_H2     = 0.75020  #H2
bond_N2     = 1.11290  #N2
bond_O2     = 1.23420  #O2
bond_F2     = 1.42404  #F2
bond_CO     = 1.14309  #CO
bond_CO2    = 1.17647  #CO2
bond_OH_H2O = 0.97192  #H2O
bond_HH_H2O = 1.53697  #H2O
angle_H2O   = 75.5     #H2O
bond_NH_NH2 = 1.02     #NH2
bond_HH_NH2 = 1.64     #NH2
angle_NH2   = 75.5     #NH2
bond_CC_C2H2 = 1.20750 #C2H2
bond_CH_C2H2 = 1.07015 #C2H2
bond_NH_NH3  = 1.02135 #NH3
bond_HH_NH3 =  1.63713 #NH3
bond_CH_CH4 = 1.09589
bond_HH_CH4 = 1.78958

dis_len = 1.5
###############################################

path = os.getcwd()

print("\n### START PROGRAM ###\n")
"""
if os.path.exists(path + "/input.dat"):
    with open(path + "/input.dat") as fi: inputlines = fi.readlines()

    for line in inputlines:
        if "filename" in line and "#" not in line: filename = hl.conv_line(line)[2]
        elif "kind_mol" in line and "#" not in line: kind_mol = hl.conv_line(line)[2]
        elif "Ename" in line and "#" not in line:
            list_Ename = []
            for n in enumerate(i, hl.conv_line(line)):
                if i == 0 or i == 1: pass
                else: list_Ename.append(n)
        elif "Num_adatom" in line and "#" not in line:
            list_Num_adatom = []
            for n in enumerate(i, hl.conv_line(line)):
                if i == 0 or i == 1: pass
                else: Num_adatom.append(n)
        elif "Mode" in line and "#" not in line: Mode = hl.conv_line(line)[2
"""

filename = q.Q_file(os.getcwd())
d_AS_o, d_AS_b, d_AS_h = q.Q_d_AS(dis_AdtoSup_df)
range_surf = q.Q_range_surf()
kind_mol, list_Ename, list_Num_adatom, Mode, Shape, len_mol, angle_mol, Q_dismode = q.Q_Mode()

"""
#filename = "POSCAR.vasp"
filename = q.Q_file()
kind_mol = "H"
list_Ename = ["H"]
list_Num_adatom = [1]
Mode = "a"
Shape = "N"
len_mol = [bond_H2]
len_mol = [bond_CH_CH4, bond_HH_CH4]
angle_mol = []
Q_dismode = "n"
"""

adsite = q.Q_adsite(); Num_adatom = sum(list_Num_adatom)

# Read POSCAR/CONTCAR/SPOSCAR
with open(filename, "r") as f: contlines = f.readlines()

# Get the atomic species, the atomic number, the total number of atoms from POSCAR
labelofel = rp.get_labelofel(contlines)
numofel = rp.get_numofel(contlines)
sumatomnum = rp.get_sumofnumofel(contlines)

# Make the atomic tags at the far right of INCAR
list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel, numofel)

print("Support: " + " ".join([str(x) for x in labelofel]))
print("Num: " + " ".join([str(x) for x in numofel]))

# Get the several lines starting from POSCAR, lattice vector, atomic coordinates
# POSCAR's beginning few lines (up to Direct / Cartesian)
firstlines, flagS = rp.get_firstlines(contlines)
# Lattice vector
mat_lat = rp.get_matrix(contlines)
coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
arr_coor_car = np.array(coor_car)
arr_coor_dir = np.array(coor_dir)
list_num_SD = rp.mk_listnumSD(list_SD)

print("Num_fix: " + " ".join([str(x) for x in list_num_SD]) + "\n")

# Cross product of each lattice vector and its unit vector, a, b vector
veca = np.array(mat_lat[0]); vecb = np.array(mat_lat[1])
cross_ab = np.cross(veca, vecb)
veca_uni = calc.uni_vec(veca); vecb_uni = calc.uni_vec(vecb)
cross_ab_uni = calc.uni_vec(cross_ab)
#rota_120_uni = calc.rotation(cross_ab_uni, veca_uni, 120)[0]
#rota_240_uni = calc.rotation(cross_ab_uni, veca_uni, 240)[0]
lat_c = math.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)

# Make the coordinate of the adsorbed molecule
list_adsites = []
label = "_" + kind_mol
if "ontop" in adsite:
    Qlab = input("Please input label of atom you want to check, i.e., N1 or the specific coordinate like [a,b,c]: ")
    if "[" in Qlab: coor = np.array([float(x) for x in Qlab.replace("[","").replace("]","").split(",")]); label += "_self"
    else: coor = coor_car[list_labelofel.index("!"+Qlab)] + cross_ab_uni * d_AS_o; label += Qlab

    list_adsites.append(coor.tolist())
    if adsite in ["ontop_endon", "oe"]:
        coor_ad = mkca.mk_endon_mol(list_adsites, mat_lat, veca_uni, vecb_uni, cross_ab_uni, kind_mol, Num_adatom, Shape, len_mol, angle_mol)
        label += "_oe"

    elif adsite in ["ontop_sideon", "os"]:
        coor_ad = mkca.mk_sideon_mol_op(list_adsites, mat_lat, veca_uni, vecb_uni, Num_adatom, Shape, len_mol, dis_len)
        coor_ad = [coor_ad[0][0]]
        label += "_os"

elif "bridge" in adsite:
    Qlab1 = input("Please input label of atom you want to check, i.e., N1 or the specific coordinate like [a,b,c]: ")
    if "[" in Qlab1:
        coor1_car = np.array([float(x) for x in Qlab1.replace("[","").replace("]","").split(",")]); label += "s1"
    else:
        coor1_car = np.array(coor_car[list_labelofel.index("!"+Qlab1)]); label += Qlab1
    coor1_dir = f2c.cart2frac(mat_lat, [coor1_car])[0]

    Qlab2 = input("Please input label of atom you want to check, i.e., N1 or the specific coordinate like [a,b,c]: ")
    if "[" in Qlab2:
        coor2_car = np.array([float(x) for x in Qlab2.replace("[","").replace("]","").split(",")]); label += "s2"
    else:
        coor2_car = np.array(coor_car[list_labelofel.index("!"+Qlab2)]); label += Qlab2
    coor2_dir = f2c.cart2frac(mat_lat, [coor2_car])[0]

    #Qlab1 = input("Please input label of atom you want to check, i.e., N1: ")
    #Qlab2 = input("Please input label of atom you want to check, i.e., N1: ")
    #label += Qlab1 + Qlab2
    #coor1_car = np.array(coor_car[list_labelofel.index("!"+Qlab1)])
    #coor2_car = np.array(coor_car[list_labelofel.index("!"+Qlab2)])
    #coor1_dir = np.array(coor_dir[list_labelofel.index("!"+Qlab1)])
    #coor2_dir = np.array(coor_dir[list_labelofel.index("!"+Qlab2)])
    index_ad = [[list_labelofel.index("!"+Qlab1)+1, list_labelofel.index("!"+Qlab2)+1]]

    M_point = (coor1_car + coor2_car)/2
    coor1_z = coor1_dir[2] * lat_c ; coor2_z = coor2_dir[2] * lat_c
    if coor1_z >= coor2_z: vec_bridge = coor1_z - M_point
    elif coor1_z < coor2_z: vec_bridge = coor2_car - M_point

    vec_adatom = calc.uni_vec(np.cross(np.cross(vec_bridge, cross_ab + M_point), vec_bridge)) * d_AS_b
    list = vec_adatom + M_point; list_adsites.append(list.tolist())

    if adsite in ["bridge_endon", "be"]:
        coor_ad = mkca.mk_endon_mol(list_adsites, mat_lat, veca_uni, vecb_uni, cross_ab_uni, kind_mol, Num_adatom, Shape, len_mol, angle_mol)
        label += "_be"

    elif adsite in ["bridge_sideon", "bs"]:
        coor_ad = mkca.mk_sideon_mol(list_adsites, index_ad, arr_coor_car, mat_lat, cross_ab_uni, Param_rough, d_AS_b, kind_mol, Num_adatom, Shape, len_mol, angle_mol, dis_len)
        coor_ad = [coor_ad[0][0]]
        label += "_bs"

elif "hollow" in adsite:
    Qlab1 = input("Please input label of atom you want to check, i.e., N1: ")
    Qlab2 = input("Please input label of atom you want to check, i.e., N1: ")
    Qlab3 = input("Please input label of atom you want to check, i.e., N1: ")
    coor1_car = np.array(coor_car[list_labelofel.index("!"+Qlab1)])
    coor2_car = np.array(coor_car[list_labelofel.index("!"+Qlab2)])
    coor3_car = np.array(coor_car[list_labelofel.index("!"+Qlab3)])
    coor1_dir = np.array(coor_dir[list_labelofel.index("!"+Qlab1)])
    coor2_dir = np.array(coor_dir[list_labelofel.index("!"+Qlab2)])
    coor3_dir = np.array(coor_dir[list_labelofel.index("!"+Qlab3)])
    label += Qlab1 + Qlab2 + Qlab3
    index_ad = [[list_labelofel.index("!"+Qlab1), list_labelofel.index("!"+Qlab2), list_labelofel.index("!"+Qlab3)]]

    vec_nn1 = coor2_car - coor1_car; vec_nn2 = coor3_car - coor1_car
    G_point = (coor1_car + coor2_car + coor3_car)/3
    vec_adatom = calc.uni_vec(np.cross(vec_nn1, vec_nn2) + G_point) * d_AS_h
    list_adsites.append(vec_adatom + G_point)

    coor_ad = mkca.mk_endon_mol(list_adsites, mat_lat, veca_uni, vecb_uni, cross_ab_uni, kind_mol, Num_adatom, Shape, len_mol, angle_mol)
    label += "_he"

coor_ad = f2c.frac2cart(mat_lat, coor_ad[0])

# Make POSCAR with adsorbed molecule
list_Ename_forpos = []
iE = 0
for numE in list_Num_adatom:
    i = 1
    while i < numE + 1: list_Ename_forpos.append(list_Ename[iE] + str(i)); i += 1
    iE += 1

#L_label = "  ".join([str(x) for x in labelofel]) + "  " + "  ".join([str(x) for x in list_Ename])
#L_num = "  ".join([str(x) for x in numofel]) + "  " + "  ".join([str(x) for x in list_Num_adatom])
L_label = "  ".join([str(x) for x in list_Ename]) + "  " +  "  ".join([str(x) for x in labelofel])
L_num = "  ".join([str(x) for x in list_Num_adatom]) + "  " + "  ".join([str(x) for x in numofel])
Lpos =  L_label + "\n" + "1.0" + "\n"
Lpos += "  ".join([str(x) for x in mat_lat[0]]) + "\n"
Lpos += "  ".join([str(x) for x in mat_lat[1]]) + "\n"
Lpos += "  ".join([str(x) for x in mat_lat[2]]) + "\n"
Lpos += L_label + "\n" + L_num + "\n"
if flagS == 1: Lpos += "Selective" + "\n"
Lpos += "Cartesian" + "\n"

num2 = 0
for coor in coor_ad: Lpos += "  ".join([str(x) for x in coor]) + " T T T !" + str(list_Ename_forpos[num2]) + "\n"; num2 += 1

num1 = 0
for coor in coor_car:
    Lpos += "  ".join([str(x) for x in coor])
    if flagS == 1: Lpos += "  " + "  ".join([str(x) for x in list_SD[num1]])
    Lpos += "  " + list_labelofel[num1] + "\n"
    num1 += 1

with open(path + "/" + filename.replace(".vasp","") + label + ".vasp", "w") as fp: fp.write(Lpos)

print("\n### END PROGRAM ###\n")
# END
