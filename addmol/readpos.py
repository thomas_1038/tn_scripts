#import sys
from addmol import helper
from addmol import Frac2Cart
import numpy as np
f2c = Frac2Cart()
#import numpy as np

def get_labelofel(poslines):
    labelofel = helper.conv_line(poslines[5])
    return labelofel

def get_numofel(poslines):
    numofel = helper.conv_line(poslines[6])
    return numofel

def get_sumofnumofel(poslines):
    sumofnumofel = sum([int(x) for x in helper.conv_line(poslines[6])])
    return sumofnumofel

def mk_labelofel(labelofel, numofel):
    num1 = 0
    eachlabels_el = []; eachlabels_el_nn = []; dict_labelofel = {}
    num0 = 1
    while num1 < len(labelofel):
        num2 = 0
        while num2 < int(numofel[num1]):
            eachlabels_el.append("!" + labelofel[num1] + str(num2+1))
            eachlabels_el_nn.append(labelofel[num1])
            dict_labelofel.update({num0:labelofel[num1]+str(num2+1)})
            num0 += 1; num2 += 1
        num1 += 1
    return eachlabels_el, eachlabels_el_nn, dict_labelofel

def get_firstlines(poslines):
    flagS = 0
    if "S" in poslines[7]: flagS = 1

    num0 = 0; firstposlines = []
    while num0 < 7 + flagS:
        L = "  ".join(helper.conv_line(poslines[num0]))
        firstposlines.append(L)
        num0 += 1
    firstposlines.append("Cartesian")
    return firstposlines, flagS

def get_matrix(poslines):
    num1 = 2; mat_lat = []
    while num1 < 5:
        mat_lat.append(list(map(float, helper.conv_line(poslines[num1]))))
        num1 += 1
    return mat_lat

def get_coordinate(poslines, sumatomnum, mat_lat):
    flagS = 0
    if "S" in poslines[7]: flagS = 1

    flagCD = 0 # 0 means Direct
    if "C" in poslines[7 + flagS]: flagCD = 1 # 1 means Cartesian

    num2 = 8 + flagS
    coor_tot = []; list_SD = []
    while num2 < 8 + flagS + sumatomnum:
        coor = helper.conv_line(poslines[num2])
        coor_tot.append(list(map(float, [coor[0], coor[1], coor[2]])))
        if flagS == 1: list_SD.append([coor[3], coor[4], coor[5]])
        num2 += 1

    if flagCD == 0:
        coor_dir = coor_tot
        coor_car = f2c.frac2cart(mat_lat, coor_tot)
    else:
        coor_dir = f2c.cart2frac(mat_lat, coor_tot)
        coor_car = coor_tot

    return coor_car, coor_dir, list_SD

def mk_listnumSD(list_SD):
    num_ft = 0; num_tag = 0; list_num_SD =[]
    while num_tag < len(list_SD):
        if num_ft == 0:
            if list_SD[num_tag][0] == "F": num_ft += 1
            else: pass
        else:
            if list_SD[num_tag][0] == "F": num_ft += 1
            elif list_SD[num_tag][0] == "T" and list_SD[num_tag-1][0] == "F":
                list_num_SD.append(num_ft)
                num_ft = 0
            else: pass
        num_tag += 1
    return list_num_SD

def get_coordinate_eachel(coor, eachlabels_el_nn):
    coor_eachel = []; coor_ee_app = coor_eachel.append
    label_eachel = []; label_ee_app = label_eachel.append
    ac = [] #; ac.append = ac.append
    al = [] #; al_app = al.append
    for i, x in enumerate(coor):
        if i == 0: 
            ac.append(x); al.append(eachlabels_el_nn[i])
        elif i == len(coor) -1:
            if eachlabels_el_nn[i] == eachlabels_el_nn[i-1]:
                ac.append(x); coor_ee_app(ac)
                al.append(eachlabels_el_nn[i]); label_ee_app(al)
            else:
                coor_ee_app(ac); coor_ee_app([x])
                label_ee_app(al); label_ee_app([eachlabels_el_nn[i]])
        else:
            if eachlabels_el_nn[i] == eachlabels_el_nn[i-1]:
                ac.append(x); al.append(eachlabels_el_nn[i])
            else:
#                print("T")
                coor_ee_app(ac); ac = []; ac.append(x)
                label_ee_app(al); al = []; al.append(eachlabels_el_nn[i])

    return coor_eachel, label_eachel

def calc_surface_area(mat_lat):
    a = np.array(mat_lat[0])
    b = np.array(mat_lat[1])
    c = np.cross(a,b)
    S = np.linalg.norm(c)
    return S
