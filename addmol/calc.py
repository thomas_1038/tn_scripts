import numpy as np
import math
from functools import reduce

def uni_vec(x):
    uni_x = x/np.linalg.norm(x)
    return uni_x

def angle(vec1, vec2):
    dot_v12 = np.dot(vec1, vec2)
    norm_v1 = np.linalg.norm(vec1)
    norm_v2 = np.linalg.norm(vec2)
    cos = dot_v12/(norm_v1*norm_v2)
    theta = float(np.arccos(cos) * 180 / np.pi)
    return theta

def rotation(vec_rot, degx, degy, degz):
    Cx = np.cos(np.radians(degx)); Sx = np.sin(np.radians(degx))
    Cy = np.cos(np.radians(degy)); Sy = np.sin(np.radians(degy))
    Cz = np.cos(np.radians(degz)); Sz = np.sin(np.radians(degz))

    # z-axis
    Rotz = np.matrix([[Cz, -1 * Sz, 0], [Sz, Cz, 0], [0, 0, 1]])
    Roty = np.matrix([[Cy, 0, Sy], [0, 1, 0], [-1 * Sy, 0, Cy]])
    Rotx = np.matrix([[1, 0, 0], [0, Cx, -1 * Sx], [0, Sx, Cx]])

    def rot(v1, R):
        vec_ini = np.array(v1)
        vec_fin= np.array(np.dot(R, vec_ini).tolist()[0])
        vec_fin = [round(x, 8) for x in vec_fin]
        return vec_fin

    #vec_rot_fin = rot(rot(rot(np.array(vec_rot), Rotx), Roty), Rotz)
    vec_rot_fin = rot(np.array(vec_rot), Rotx)
    return vec_rot_fin

def gcd(*numbers):
    return reduce(math.gcd, numbers)

def gcd_list(numbers):
    return reduce(math.gcd, numbers)
# END