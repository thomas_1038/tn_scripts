import numpy as np
import math
from addmol.f2cc2f import Frac2Cart
from addmol import calc

f2c = Frac2Cart()

def mk_endon_mol(coor_ad_car, mat_lat_new, veca_uni, vecb_uni, cross_ab_uni, kind_mol, Num_adatom, Shape, len_mol, angle_mol):
    coor_ad_car_new_ini = []
    arr_coor_ad_car = np.array(coor_ad_car)

    for coor_ad in arr_coor_ad_car:
        if Num_adatom == 1:
            list = [coor_ad.tolist()]
            coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

        if Num_adatom == 2:
            vec_endon_ad = np.array(cross_ab_uni * float(len_mol[0]))
            list = [coor_ad, np.array(coor_ad + vec_endon_ad).tolist()]
            coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

        elif Num_adatom == 3 and Shape == "L":
            vec_endon_ad = np.array(cross_ab_uni * float(len_mol[0]))
            list = [np.array(coor_ad + vec_endon_ad).tolist(),
                    coor_ad, np.array(coor_ad + 2 * vec_endon_ad).tolist()]
            coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

        elif Num_adatom == 3 and Shape == "N":
            if kind_mol in ["H2O","h2o"]:
                bond_OH_H2O = float(len_mol[0])
                bond_HH_H2O = float(len_mol[1])
                angle_H2O = float(angle_mol[0])

                #O adsorption
                r = np.radians(angle_H2O/2)
                length_OH_sin = float(len_mol[0]) * np.sin(r)
                length_OH_cos = bond_OH_H2O * np.cos(r)
                vec_var = np.array(cross_ab_uni * float(length_OH_sin))
                list2a_ini = [np.array(coor_ad.tolist()),
                              np.array(coor_ad + vec_var - veca_uni * float(length_OH_cos)).tolist(),
                              np.array(coor_ad + vec_var + veca_uni * float(length_OH_cos)).tolist()]
                coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list2a_ini))

                """
                list2a_fin = [np.array(coor_ad.tolist()),
                              np.array(coor_ad + vec_var - veca_uni*float(length_OH_cos)).tolist(),
                              np.array(coor_ad + veca_uni * float(length_OH_cos + dis_len)).tolist()]
                """

                list2b_ini = [np.array(coor_ad.tolist()),
                              np.array(coor_ad + vec_var - vecb_uni * float(length_OH_cos)).tolist(),
                              np.array(coor_ad + vec_var + vecb_uni * float(length_OH_cos)).tolist()]
                coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list2b_ini))

                """
                list2b_fin = [np.array(coor_ad.tolist()),
                              np.array(coor_ad + vec_var - vecb_uni * float(length_OH_cos)).tolist()
                              np.array(coor_ad + vecb_uni * float(length_OH_cos + dis_len)).tolist()]
                """

                #H adsorption
                vec_endon_ad  = np.array(cross_ab_uni * bond_OH_H2O)
                vec_endon_ad2 = np.array(cross_ab_uni * bond_HH_H2O)
                deg_rot = angle_H2O/2
                vec_rot = calc.rotation(veca_uni, vec_endon_ad, deg_rot)
                list1 = [np.array(coor_ad + vec_rot[0]).tolist(),
                         coor_ad.tolist(),
                         np.array(coor_ad + vec_endon_ad2).tolist()]
                coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list1))

        elif Num_adatom == 4 and Shape == "L":
            vec_endon_ad = np.array(cross_ab_uni * float(len_mol[0]))
            vec_endon_ad2 = np.array(cross_ab_uni*float(len_mol[1]))
            list = [np.array(coor_ad + vec_endon_ad2).tolist(),
                    np.array(coor_ad + vec_endon_ad2 + vec_endon_ad).tolist(),
                    coor_ad,
                    np.array(coor_ad + 2*vec_endon_ad2 + vec_endon_ad).tolist()]
            coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

        elif Num_adatom == 4 and Shape == "N":
            if kind_mol in ["NH3","nh3"]:
                vecb = calc.uni_vec(np.cross(veca_uni, cross_ab_uni))
                vec_endon_var = np.array(cross_ab_uni * float(len_mol[2]))
                vec_hor1a = np.array(veca_uni * float(len_mol[1])/math.sqrt(3))
                vec_hor2a = -1.0 * vec_hor1a / 2.0
                vec_hor2b = np.array(vecb * float(len_mol[1]/2))
                list = [np.array(coor_ad).tolist(),
                        np.array(coor_ad + vec_endon_var + vec_hor1a).tolist(),
                        np.array(coor_ad + vec_endon_var + vec_hor2a + vec_hor2b).tolist(),
                        np.array(coor_ad + vec_endon_var + vec_hor2a - vec_hor2b).tolist()]
                coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

        elif Num_adatom == 5 and Shape == "N":
            if kind_mol in ["CH4","ch4"]:
                bond_CH = float(len_mol[0]); bond_HH = float(len_mol[1])
                height_tetHH = math.sqrt(6)/3 * bond_HH; height_CH3 = height_tetHH - bond_CH

                vec_Cz = cross_ab_uni * bond_CH
                vec_Hz = cross_ab_uni * height_tetHH
                vec_Hx = veca_uni * 1/2 * bond_HH
                vec_Hy = vecb_uni * math.sqrt(3)/6 * bond_HH
                list = [np.array(coor_ad + vec_Cz).tolist(),
                        np.array(coor_ad).tolist(),
                        np.array(coor_ad + vec_Hz + 2 * vec_Hy).tolist(),
                        np.array(coor_ad + vec_Hz - 1 * vec_Hy + vec_Hx).tolist(),
                        np.array(coor_ad + vec_Hz - 1 * vec_Hy - vec_Hx).tolist()]
                coor_ad_car_new_ini.append(f2c.cart2frac(mat_lat_new, list))

    return coor_ad_car_new_ini

def mk_sideon_mol(coor_ad_car, index_ad, arr_coor_car, mat_lat_new, cross_ab_uni, Param_rough, d_AS_b, kind_mol, Num_adatom, Shape, len_mol, angle_mol, dis_len):
    num_s = 0
    coor_ad_car_ini_new = [] ; coor_ad_car_fin_new = []
    cx = float(mat_lat_new[2][0]) ; cy = float(mat_lat_new[2][1]) ; cz = float(mat_lat_new[2][2])
    lat_c = math.sqrt(cx**2 + cy**2 + cz**2)
    for site in coor_ad_car:
        site_br1 = arr_coor_car[index_ad[num_s][0]-1]
        site_br2 = arr_coor_car[index_ad[num_s][1]-1]
        site_br0 = (site_br1 + site_br2)/2
        site_br1_z = site_br1[2] * lat_c
        site_br2_z = site_br2[2] * lat_c
        site_br0_z = site_br0[2] * lat_c

        if site_br1_z >= site_br2_z: vec_bridge = site_br1 - site_br0
        elif site_br1_z < site_br2_z: vec_bridge = site_br2 - site_br0
        if Param_rough == "S":
            vec_adatom_uni = calc.uni_vec(cross_ab_uni + site_br0)
            vec_adatom = cross_ab_uni * d_AS_b
        elif Param_rough == "R":
            #vec_adatom_uni = calc.uni_vec(np.cross(np.cross(vec_bridge, cross_ab_uni + site_br0), vec_bridge))
            vec_adatom_uni = calc.uni_vec(np.cross(np.cross(vec_bridge, cross_ab_uni), vec_bridge))
            vec_adatom = vec_adatom_uni * d_AS_b

        if Num_adatom == 2:
            # initial site
            vec1_uni_ini = calc.uni_vec(site_br1 - site_br0) * (float(len_mol[0])/2)
            vec2_uni_ini = calc.uni_vec(site_br2 - site_br0) * (float(len_mol[0])/2)
            site_ad1_ini = np.array(site_br0 + vec1_uni_ini + vec_adatom).tolist()
            site_ad2_ini = np.array(site_br0 + vec2_uni_ini + vec_adatom).tolist()
            list_ini = [site_ad1_ini, site_ad2_ini]
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini))

            # final site
            vec1_uni_fin = calc.uni_vec(site_br1 - site_br0) * ((float(len_mol[0]) + dis_len)/2)
            vec2_uni_fin = calc.uni_vec(site_br2 - site_br0) * ((float(len_mol[0]) + dis_len)/2)
            site_ad1_fin = np.array(site_br0 + vec1_uni_fin + vec_adatom).tolist()
            site_ad2_fin = np.array(site_br0 + vec2_uni_fin + vec_adatom).tolist()
            list_fin = [site_ad1_fin, site_ad2_fin]
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin))

        elif Num_adatom == 3 and Shape == "L":
            # initial site
            vec1_uni_ini = calc.uni_vec(site_br1 - site_br0) * (float(len_mol[0])/2)
            vec2_uni_ini = calc.uni_vec(site_br2 - site_br0) * (float(len_mol[0])/2)
            vec3_1_ini = vec1_uni_ini * 2 ;  vec3_2_ini = vec2_uni_ini * 2
            site_ad1_ini = np.array(site_br0 + vec1_uni_ini + vec_adatom).tolist()
            site_ad2_ini = np.array(site_br0 + vec2_uni_ini + vec_adatom).tolist()
            site_ad3_1_ini = np.array(site_ad1_ini + vec3_1_ini).tolist()
            site_ad3_2_ini = np.array(site_ad2_ini + vec3_2_ini).tolist()
            list_ini1 = [site_ad1_ini, site_ad2_ini, site_ad3_1_ini]
            list_ini2 = [site_ad2_ini, site_ad1_ini, site_ad3_2_ini]
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini1))
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini2))

            # final site
            vec1_uni_fin = calc.uni_vec(site_br1 - site_br0) * ((float(len_mol[0]) + dis_len)/2)
            vec2_uni_fin = calc.uni_vec(site_br2 - site_br0) * ((float(len_mol[0]) + dis_len)/2)
            vec3_1_fin = calc.uni_vec(site_br1 - site_br0) * float(len_mol[0])
            vec3_2_fin = calc.uni_vec(site_br2 - site_br0) * float(len_mol[0])
            site_ad1_fin = np.array(site_br0 + vec1_uni_fin + vec_adatom).tolist()
            site_ad2_fin = np.array(site_br0 + vec2_uni_fin + vec_adatom).tolist()
            site_ad3_1_fin = np.array(site_ad1_fin + vec3_1_fin).tolist()
            site_ad3_2_fin = np.array(site_ad2_fin + vec3_2_fin).tolist()
            list_fin1 = [site_ad1_fin, site_ad2_fin, site_ad3_1_fin]
            list_fin2 = [site_ad2_fin, site_ad1_fin, site_ad3_2_fin]
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin1))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin2))

        elif Num_adatom == 3 and Shape == "N":
            if kind_mol in ["H2O","h2o"]:
                bond_OH_H2O = float(len_mol[0])
                bond_HH_H2O = float(len_mol[1])
                angle_H2O = float(angle_mol[0])

                # initial site
                # O-H adsorption
                vec1_uni_ini = calc.uni_vec(site_br1-site_br0) * (float(bond_OH_H2O)/2)
                vec2_uni_ini = calc.uni_vec(site_br2-site_br0) * (float(bond_OH_H2O)/2)
                r = np.radians(angle_H2O)
                length_OH_sin = bond_OH_H2O * np.sin(r)
                length_OH_cos = bond_OH_H2O * np.cos(r)
                vec_var = np.array(cross_ab_uni * float(length_OH_sin))
                vec3_1_ini = calc.uni_vec(site_br1-site_br0) * float(length_OH_cos)
                vec3_2_ini = calc.uni_vec(site_br2-site_br0) * float(length_OH_cos)
                site_ad1_ini = np.array(site_br0 + vec1_uni_ini + vec_adatom).tolist()
                site_ad2_ini = np.array(site_br0 + vec2_uni_ini + vec_adatom).tolist()
                site_ad3_1_ini = np.array(site_ad1_ini + vec_var + vec3_1_ini).tolist()
                site_ad3_2_ini = np.array(site_ad2_ini + vec_var + vec3_2_ini).tolist()
                list_ini1 = [site_ad1_ini, site_ad2_ini, site_ad3_1_ini]
                list_ini2 = [site_ad2_ini, site_ad1_ini, site_ad3_2_ini]
                coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini1))
                coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini2))

                # H-H adsorption
                vec1_uni_ini2 = calc.uni_vec(site_br1-site_br0) * (float(bond_HH_H2O)/2)
                vec2_uni_ini2 = calc.uni_vec(site_br2-site_br0) * (float(bond_HH_H2O)/2)
                r = np.radians(angle_H2O/2)
                length_OH_sin = bond_OH_H2O * np.sin(r)
                vec_var_ini2 = np.array(cross_ab_uni * float(length_OH_sin))
                site_ad1_ini2 = np.array(site_br0 + vec1_uni_ini2 + vec_adatom).tolist()
                site_ad2_ini2 = np.array(site_br0 + vec2_uni_ini2 + vec_adatom).tolist()
                site_ad3_ini2 = np.array(site_br0 + vec_var_ini2 + vec_adatom).tolist()
                list_ini3 = [site_ad3_ini2, site_ad1_ini2, site_ad2_ini2]
                coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini3))

                # final site
                # O-H adsorption
                vec1_uni_fin = calc.uni_vec(site_br1-site_br0) * ((float(bond_OH_H2O) + dis_len)/2)
                vec2_uni_fin = calc.uni_vec(site_br2-site_br0) * ((float(bond_OH_H2O) + dis_len)/2)
                r = np.radians(angle_H2O)
                length_OH_sin = bond_OH_H2O * np.sin(r)
                length_OH_cos = bond_OH_H2O * np.cos(r)
                vec_var = np.array(cross_ab_uni * float(length_OH_sin))
                vec3_1_fin = calc.uni_vec(site_br1-site_br0) * float(length_OH_cos)
                vec3_2_fin = calc.uni_vec(site_br2-site_br0) * float(length_OH_cos)
                site_ad1_fin = np.array(site_br0 + vec1_uni_ini + vec_adatom).tolist()
                site_ad2_fin = np.array(site_br0 + vec2_uni_ini + vec_adatom).tolist()
                site_ad3_1_fin = np.array(site_ad1_fin + vec_var + vec3_1_fin).tolist()
                site_ad3_2_fin = np.array(site_ad2_fin + vec_var + vec3_2_fin).tolist()
                list_fin1 = [site_ad1_fin, site_ad2_fin, site_ad3_1_fin]
                list_fin2 = [site_ad2_fin, site_ad1_fin, site_ad3_2_fin]
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin1))
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin2))

                # H-H adsorption
                vec1_uni_fin2 = calc.uni_vec(site_br1-site_br0) * ((float(bond_HH_H2O) + dis_len)/2)
                vec2_uni_fin2 = calc.uni_vec(site_br2-site_br0) * ((float(bond_HH_H2O) + dis_len)/2)
                vec_var_fin2  = np.array(cross_ab_uni * float(bond_OH_H2O))
                site_ad1_fin2 = np.array(site_br0 + vec1_uni_fin2 + vec_adatom).tolist()
                site_ad2_fin2 = np.array(site_br0 + vec2_uni_fin2 + vec_adatom).tolist()
                site_ad3_fin2 = np.array(site_br1 + vec_var_fin2).tolist()
                site_ad4_fin2 = np.array(site_br2 + vec_var_fin2).tolist()
                list_fin3 = [site_ad3_fin2, site_ad1_fin2, site_ad2_fin2]
                list_fin4 = [site_ad4_fin2, site_ad1_fin2, site_ad2_fin2]
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin3))
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin4))

        elif Num_adatom == 4 and Shape == "L":
            # initial site
            vec1_uni_ini = calc.uni_vec(site_br1-site_br0) * (float(len_mol[0])/2)
            vec2_uni_ini = calc.uni_vec(site_br2-site_br0) * (float(len_mol[0])/2)
            vec3_uni_ini = calc.uni_vec(site_br1-site_br0) * float(len_mol[1])
            vec4_uni_ini = calc.uni_vec(site_br2-site_br0) * float(len_mol[1])
            site_ad1_ini = np.array(site_br0 + vec1_uni_ini + vec_adatom).tolist()
            site_ad2_ini = np.array(site_br0 + vec2_uni_ini + vec_adatom).tolist()
            site_ad3_ini = np.array(site_ad1_ini + vec3_uni_ini).tolist()
            site_ad4_ini = np.array(site_ad2_ini + vec4_uni_ini).tolist()
            list_ini = [site_ad1_ini, site_ad2_ini, site_ad3_ini, site_ad4_ini]
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini))

            # final site
            vec1_uni_fin = calc.uni_vec(site_br1-site_br0) * ((float(len_mol[0]) + dis_len)/2)
            vec2_uni_fin = calc.uni_vec(site_br2-site_br0) * ((float(len_mol[0]) + dis_len)/2)
            vec3_uni_fin = calc.uni_vec(site_br1-site_br0) * float(len_mol[1])
            vec4_uni_fin = calc.uni_vec(site_br2-site_br0) * float(len_mol[1])
            site_ad1_fin = np.array(site_br0 + vec1_uni_fin + vec_adatom).tolist()
            site_ad2_fin = np.array(site_br0 + vec2_uni_fin + vec_adatom).tolist()
            site_ad3_fin = np.array(site_ad1_fin + vec3_uni_fin).tolist()
            site_ad4_fin = np.array(site_ad2_fin + vec4_uni_fin).tolist()
            list_fin = [site_ad1_fin, site_ad2_fin, site_ad3_fin, site_ad4_fin]
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin))

        elif Num_adatom == 4 and Shape == "N":
            if kind_mol in ["NH3","nh3"]:
                # basic parameter
                bond_NH_NH3 = float(len_mol[0])
                bond_HH_NH3 = float(len_mol[1])

                lenA = float(bond_NH_NH3)
                lenB = math.sqrt(3)/2
                lenC = math.sqrt(bond_NH_NH3**2 - bond_HH_NH3**2/4)
                cosA = (lenB**2 + lenC**2 - lenA**2)/(2 * lenB * lenC)
                cosB = (lenC**2 + lenA**2 - lenB**2)/(2 * lenC * lenA)
                cosC = (lenA**2 + lenB**2 - lenC**2)/(2 * lenA * lenB)
                radA = np.arccos(cosA) ; radB = np.arccos(cosB) ; radC = np.arccos(cosC)
                len_var = np.sin(radA+radC) * lenC ; len_hor = np.cos(radA+radC) * lenC

                vec1_uni = calc.uni_vec(site_br1 - site_br0)
                vec2_uni = calc.uni_vec(site_br2 - site_br0)
                vec_var = np.array(vec_adatom_uni * float(len_var))
                vec_rot1_1 = calc.rotation(vec_adatom_uni, vec1_uni, 90) ; vec_rot1_2 = calc.rotation(vec_adatom_uni, vec1_uni, -90)
                vec_rot2_1 = calc.rotation(vec_adatom_uni, vec2_uni, 90) ; vec_rot2_2 = calc.rotation(vec_adatom_uni, vec2_uni, -90)
                vec_hor1_1 = np.array(calc.uni_vec(vec_rot1_1[0])) ; vec_hor1_2 = np.array(calc.uni_vec(vec_rot1_2[0]))
                vec_hor2_1 = np.array(calc.uni_vec(vec_rot2_1[0])) ; vec_hor2_2 = np.array(calc.uni_vec(vec_rot2_2[0]))

                # initial site
                site_ad1_ini = np.array(site_br0 + vec1_uni * float(bond_NH_NH3)/2 + vec_adatom).tolist()
                site_ad2_ini = np.array(site_br0 + vec2_uni * float(bond_NH_NH3)/2 + vec_adatom).tolist()
                site_ad3_11_ini = np.array(site_ad1_ini + vec_var + -1*vec1_uni * len_hor + float(bond_HH_NH3/2) * vec_hor1_1).tolist()
                site_ad3_12_ini = np.array(site_ad1_ini + vec_var + -1*vec1_uni * len_hor + float(bond_HH_NH3/2) * vec_hor1_2).tolist()
                site_ad3_21_ini = np.array(site_ad2_ini + vec_var + -1*vec2_uni * len_hor + float(bond_HH_NH3/2) * vec_hor2_1).tolist()
                site_ad3_22_ini = np.array(site_ad2_ini + vec_var + -1*vec2_uni * len_hor + float(bond_HH_NH3/2) * vec_hor2_2).tolist()
                list_ini1 = [site_ad1_ini, site_ad2_ini, site_ad3_11_ini, site_ad3_12_ini]
                list_ini2 = [site_ad2_ini, site_ad1_ini, site_ad3_21_ini, site_ad3_22_ini]
                coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini1))
                coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, list_ini2))

                # final site
                site_ad1_fin = np.array(site_br0 + vec1_uni * float(bond_NH_NH3 + dis_len)/2 + vec_adatom)
                site_ad2_fin = np.array(site_br0 + vec2_uni * float(bond_NH_NH3 + dis_len)/2 + vec_adatom)
                site_ad3_11_fin = np.array(site_ad1_fin + vec_var + vec1_uni * float(len_hor) + float(bond_HH_NH3)/2 * vec_hor1_1[0])
                site_ad3_12_fin = np.array(site_ad1_fin + vec_var + vec1_uni * float(len_hor) + float(bond_HH_NH3)/2 * vec_hor1_2[0])
                site_ad3_21_fin = np.array(site_ad1_fin + vec_var + vec1_uni * float(len_hor) + float(bond_HH_NH3)/2 * vec_hor2_1[0])
                site_ad3_22_fin = np.array(site_ad1_fin + vec_var + vec1_uni * float(len_hor) + float(bond_HH_NH3)/2 * vec_hor2_2[0])
                list_fin1 = [site_ad1_fin, site_ad2_fin, site_ad3_11_fin, site_ad3_12_fin]
                list_fin2 = [site_ad2_fin, site_ad1_fin, site_ad3_21_fin, site_ad3_22_fin]
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin1))
                coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, list_fin2))

        num_s += 1
    return coor_ad_car_ini_new, coor_ad_car_fin_new

def mk_sideon_mol_op(coor_ad_car, mat_lat_new, veca_uni, vecb_uni, Num_adatom, Shape, len_mol, dis_len):
    coor_ad_car_ini_new = [] ; coor_ad_car_fin_new = []
    arr_coor_ad_car = np.array(coor_ad_car)
    for coor_ad in arr_coor_ad_car:
        if Num_adatom == 2:
            # site along with vecA
            lista_ini = [np.array(coor_ad - veca_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist()]
            lista_fin = [np.array(coor_ad - veca_uni * (float(len_mol[0]) + dis_len)/2).tolist(),
                         np.array(coor_ad + veca_uni * (float(len_mol[0]) + dis_len)/2).tolist()]
            # site along with vecB
            listb_ini = [np.array(coor_ad - vecb_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist()]
            listb_fin = [np.array(coor_ad - vecb_uni * (float(len_mol[0]) + dis_len)/2).tolist(),
                         np.array(coor_ad + vecb_uni * (float(len_mol[0]) + dis_len)/2).tolist()]

            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, lista_ini))
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, listb_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, lista_fin))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, listb_fin))

        elif Num_adatom == 3 and Shape == "L":
            #site along with vecA
            # site along with vecA1
            lista1_ini = [np.array(coor_ad - veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - veca_uni * float(len_mol[0] * 3/2)).tolist()]
            lista1_fin = [np.array(coor_ad - veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + veca_uni * float(len_mol[0]/2 + dis_len)).tolist(),
                          np.array(coor_ad - veca_uni * float(len_mol[0] * 3/2 + dis_len)).tolist()]
            # site along with vecA2
            lista2_ini = [np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + veca_uni * float(len_mol[0] * 3/2)).tolist()]
            lista2_fin = [np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - veca_uni * float(len_mol[0]/2 + dis_len)).tolist(),
                          np.array(coor_ad + veca_uni * float(len_mol[0] * 3/2 + dis_len)).tolist()]

            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, lista1_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, lista1_fin))
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, lista2_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, lista2_fin))

            # site along with vecB
            # site along with vecB1
            listb1_ini = [np.array(coor_ad - vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - vecb_uni * float(len_mol[0] * 3/2)).tolist()]
            listb1_fin = [np.array(coor_ad - vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + vecb_uni * float(len_mol[0]/2 + dis_len)).tolist(),
                          np.array(coor_ad - vecb_uni * float(len_mol[0] * 3/2 + dis_len)).tolist()]
            # site along with vecB2
            listb2_ini = [np.array(coor_ad + vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad + vecb_uni * float(len_mol[0] * 3/2)).tolist()]
            listb2_fin = [np.array(coor_ad + vecb_uni * float(len_mol[0]/2)).tolist(),
                          np.array(coor_ad - vecb_uni * float(len_mol[0]/2 + dis_len)).tolist(),
                          np.array(coor_ad + vecb_uni * float(len_mol[0] * 3/2 + dis_len)).tolist()]

            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, listb1_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, listb1_fin))
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, listb2_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, listb2_fin))

        #elif Num_adatom == 3 and Shape == "N":
            #if kind_mol in ["H2O","h2o"]:
                #rotation of vector
                #r = np.radians(angle_H2O/2)
                #S = np.sin(r)
                #C = np.cos(r)
                #length_OH_sin = bond_OH_H2O*S
                #length_OH_cos = bond_OH_H2O*C
                #vec_var = np.array(cross_ab_uni*float(length_OH_sin))

                #site along with vecA
                #lista_ini.append(np.array(coor_ad.tolist()))
                #lista_ini.append(np.array(coor_ad + vec_var - veca_uni*float(length_OH_cos)).tolist())
                #lista_ini.append(np.array(coor_ad + vec_var + veca_uni*float(length_OH_cos)).tolist())

                #lista_fin.append(np.array(coor_ad.tolist()))
                #lista_fin.append(np.array(coor_ad + vec_var - veca_uni*float(length_OH_cos)).tolist())
                #lista_fin.append(np.array(coor_ad + veca_uni*float(length_OH_cos + dis_len)).tolist())

                #site along with vecB
                #listb_ini.append(np.array(coor_ad.tolist()))
                #listb_ini.append(np.array(coor_ad + vec_var - vecb_uni*float(length_OH_cos)).tolist())
                #listb_ini.append(np.array(coor_ad + vec_var + vecb_uni*float(length_OH_cos)).tolist())

                #listb_fin.append(np.array(coor_ad.tolist()))
                #listb_fin.append(np.array(coor_ad + vec_var - vecb_uni*float(length_OH_cos)).tolist())
                #listb_fin.append(np.array(coor_ad + vecb_uni*float(length_OH_cos + dis_len)).tolist())

        elif Num_adatom == 4 and Shape == "L":
            #site along with vecA
            lista_ini = [np.array(coor_ad - veca_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad - veca_uni * float(len_mol[0]/2 + len_mol[1])).tolist,
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2 + len_mol[1])).tolist()]
            lista_fin = [np.array(coor_ad - veca_uni * float(len_mol[0]/2 + dis_len/2)).tolist(),
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2 + dis_len/2)).tolist(),
                         np.array(coor_ad - veca_uni * float(len_mol[0]/2 + len_mol[1] + dis_len/2)).tolist(),
                         np.array(coor_ad + veca_uni * float(len_mol[0]/2 + len_mol[1] + dis_len/2)).tolist()]

            #site along with vecB
            listb_ini = [np.array(coor_ad - vecb_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad + vecb_uni * float(len_mol[0]/2)).tolist(),
                         np.array(coor_ad - vecb_uni * float(len_mol[0]/2 + len_mol[1])).tolist(),
                         np.array(coor_ad + vecb_uni * float(len_mol[0]/2 + len_mol[1])).tolist()]
            listb_fin = [np.array(coor_ad - vecb_uni * float(len_mol[0] + dis_len/2)).tolist(),
                         np.array(coor_ad + vecb_uni * float(len_mol[0] + dis_len/2)).tolist(),
                         np.array(coor_ad - vecb_uni * float(len_mol[0] + len_mol[1] + dis_len/2)).tolist(),
                         np.array(coor_ad + vecb_uni * float(len_mol[0] + len_mol[1] + dis_len/2)).tolist()]

            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, lista_ini))
            coor_ad_car_ini_new.append(f2c.cart2frac(mat_lat_new, listb_ini))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, lista_fin))
            coor_ad_car_fin_new.append(f2c.cart2frac(mat_lat_new, listb_fin))

    return coor_ad_car_ini_new, coor_ad_car_fin_new
# END