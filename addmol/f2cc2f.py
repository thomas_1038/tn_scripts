class Frac2Cart:
  def det3(self, mat):
    A00_11_22 = mat[0][0]*mat[1][1]*mat[2][2]
    A01_12_20 = mat[0][1]*mat[1][2]*mat[2][0]
    A02_10_21 = mat[0][2]*mat[1][0]*mat[2][1]
    A02_11_20 = mat[0][2]*mat[1][1]*mat[2][0]
    A01_10_22 = mat[0][1]*mat[1][0]*mat[2][2]
    A00_12_21 = mat[0][0]*mat[1][2]*mat[2][1]
    det = A00_11_22 + A01_12_20 + A02_10_21 - A02_11_20 - A01_10_22 - A00_12_21
    return det

  def frac2cart(self, cellParam, fracCoords):
    cartCoords = []
    for i in fracCoords:
        xPos = i[0]*cellParam[0][0] + i[1]*cellParam[1][0] + i[2]*cellParam[2][0]
        yPos = i[0]*cellParam[0][1] + i[1]*cellParam[1][1] + i[2]*cellParam[2][1]
        zPos = i[0]*cellParam[0][2] + i[1]*cellParam[1][2] + i[2]*cellParam[2][2]
        cartCoords.append([xPos, yPos, zPos])
    return cartCoords

  def cart2frac(self, cellParam, cartCoords):
    latCnt = [x[:] for x in [[None]*3]*3]
    for a in range(3):
        for b in range(3):
            latCnt[a][b] = cellParam[b][a]

    fracCoords = []
    detLatCnt = self.det3(latCnt)
    for i in cartCoords:
        aPos = (self.det3([[i[0], latCnt[0][1], latCnt[0][2]], [i[1], latCnt[1][1], latCnt[1][2]], [i[2], latCnt[2][1], latCnt[2][2]]])) / detLatCnt
        bPos = (self.det3([[latCnt[0][0], i[0], latCnt[0][2]], [latCnt[1][0], i[1], latCnt[1][2]], [latCnt[2][0], i[2], latCnt[2][2]]])) / detLatCnt
        cPos = (self.det3([[latCnt[0][0], latCnt[0][1], i[0]], [latCnt[1][0], latCnt[1][1], i[1]], [latCnt[2][0], latCnt[2][1], i[2]]])) / detLatCnt
        fracCoords.append([aPos, bPos, cPos])
    return fracCoords
# END