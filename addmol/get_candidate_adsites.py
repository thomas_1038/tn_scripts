import math
import numpy as np
import addmol.readpos as rp
from addmol import calc, Frac2Cart
f2c = Frac2Cart()

class GetCandidateAdSite:
    def get_dis_bestN(self, distance, N, tolerance_dis):
        dis0 = float(distance[0])
        dis0_plus = dis0 + float(tolerance_dis)

        dis_bestN = []; dis_sum = []
        for dis in distance:
            if dis0 <= dis <= dis0_plus: dis_sum.append(dis)

            elif dis0 < dis:
                dis_bestN.append(dis_sum)
                if len(dis_bestN) < N:
                    dis0 = dis; dis0_plus = dis + float(tolerance_dis)
                    dis_sum = [dis]
                else: break
        return dis_bestN

    def get_NN2atoms(self, Coordinates, XYZ0, tolerance_dis):
        dis_NN = [np.linalg.norm(XYZ - XYZ0) for XYZ in Coordinates]
        index_dis_NN = np.array(dis_NN).argsort().tolist(); del index_dis_NN[0]

        # Make the list of NN1 and NN2
        dis_NN = sorted(dis_NN)
        dis_NN_b2 = self.get_dis_bestN(dis_NN, 3, tolerance_dis); del dis_NN_b2[0]
        N_ind2 = sum([len(x) for x in dis_NN_b2])
        index_b2 = []; index_b2_app = index_b2.append; idx = 0
        while idx < N_ind2: index_b2_app(index_dis_NN[idx]); idx += 1

        # Make the list of NN1
        dis_NN_b1 = self.get_dis_bestN(dis_NN, 2, tolerance_dis); del dis_NN_b1[0]
        N_ind1 = sum([len(x) for x in dis_NN_b1])
        index_b1 = []; index_b1_app = index_b1.append; idx = 0
        while idx < N_ind1: index_b1_app(index_dis_NN[idx]); idx += 1

        return dis_NN_b2, index_b2, index_b1

    def get_candidates_op(self, arr_surfatoms_car, cross_ab_norm, d_AS_o):
        dis_ad = cross_ab_norm * d_AS_o
        op_coor_ad_car = [x + dis_ad for x in arr_surfatoms_car]
        return op_coor_ad_car

    def get_candidates_br(self, arr_surfatoms_car, surfatoms_dir, lat_c, cross_ab, tolerance_dis, d_AS_b, Param_rough, dis_min_AS):
        br_coor_ad_car = []; index_br0 = []
        br_coor_ad_car_app = br_coor_ad_car.append; index_br0_app = index_br0.append

#        num_br1 = 0
        for num_br1, coor in enumerate(arr_surfatoms_car):
            coor0 = float(surfatoms_dir[num_br1][2]) * lat_c
            index_b2 = self.get_NN2atoms(arr_surfatoms_car, coor, tolerance_dis)[1]
            list_coorN = [arr_surfatoms_car[x] for x in index_b2]

#            num_br2 = 0
            for num_br2, coorN in enumerate(list_coorN):
                list_index_br = [num_br1, index_b2[num_br2]]
                M_point = (coor + coorN)/2

                # Make vector_br
                coor_z = float(surfatoms_dir[index_b2[num_br2]][2]) * lat_c
                if coor_z >= coor0: vec_br = coorN - M_point
                elif coor_z < coor0: vec_br = coor - M_point

                if vec_br.tolist() == [0, 0, 0]: continue
                else:
                    if Param_rough == "S": vec_adatom = calc.uni_vec(cross_ab + M_point) * d_AS_b
                    elif Param_rough == "R": vec_adatom = calc.uni_vec(np.cross(np.cross(vec_br, cross_ab + M_point), vec_br)) * d_AS_b

                    coor_br = vec_adatom + M_point
                    dis_AS = min([np.linalg.norm(coor - coor_br) for coor in arr_surfatoms_car])
                    if dis_AS > dis_min_AS: br_coor_ad_car_app(coor_br.tolist()); index_br0_app(list_index_br)

#                num_br2 += 1
#            num_br1 += 1
        return br_coor_ad_car, index_br0

    def get_candidates_hl(self, arr_surfatoms_car, surfatoms_dir, lat_c, cross_ab, tolerance_dis, d_AS_h, Param_rough, dis_min_AS):
        hl_coor_ad_car = []; index_hl0 = []
        hl_coor_ad_car_app = hl_coor_ad_car.append; index_hl0_app = index_hl0.append

#        num_hl1 = 0
        for num_hl1, coor in enumerate(arr_surfatoms_car):
            index_b2 = self.get_NN2atoms(arr_surfatoms_car, coor, tolerance_dis)[1]
            list_coor_NN1 = [arr_surfatoms_car[x].tolist() for x in index_b2]

#            num_hl2 = 0
            for num_hl2, coor_NN1 in enumerate(list_coor_NN1):
                coor_hl_nn1 = np.array(coor_NN1)
                index_b2_nn = self.get_NN2atoms(arr_surfatoms_car, coor_hl_nn1, tolerance_dis)[1]
                list_coor_NN2 = [arr_surfatoms_car[x].tolist() for x in index_b2_nn]
                list_coor_NN12 = [x for x in list_coor_NN2 if x in list_coor_NN1 and x != coor.tolist() and x != coor_hl_nn1.tolist()]
                index_b12_nn = [arr_surfatoms_car.tolist().index(x) for x in list_coor_NN12]

#                if coor.tolist() in list_coor_NN12: list_coor_NN12.remove(coor.tolist())
#                if coor_hl_nn1.tolist() in list_coor_NN12: list_coor_NN12.remove(coor_hl_nn1.tolist())
#                num_hl3 = 0
                for num_hl3, coor_NN12 in enumerate(list_coor_NN12):
                    # this line is mistaken
                    list_index_hl = [num_hl1, index_b2[num_hl2], index_b12_nn[num_hl3]]
                    coor_hl_nn2 = np.array(coor_NN12)

                    vec_nn1 = coor_hl_nn1 - coor; vec_nn2 = coor_hl_nn2 - coor
                    angle_hl1 = calc.angle(coor_hl_nn1 - coor, coor_hl_nn2 - coor)
                    angle_hl2 = calc.angle(coor - coor_hl_nn1, coor_hl_nn2 - coor_hl_nn1)
                    angle_hl3 = calc.angle(coor - coor_hl_nn2, coor_hl_nn1 - coor_hl_nn2)

                    if (angle_hl1 > 90.0) or (angle_hl2 > 90.0) or (angle_hl3 > 90.0): continue
                    else:
                        G_point = (coor + coor_hl_nn1 + coor_hl_nn2)/3
                        if Param_rough == "S": vec_adatom = calc.uni_vec(cross_ab + G_point) * d_AS_h
                        elif Param_rough == "R": vec_adatom = calc.uni_vec(np.cross(vec_nn1, vec_nn2) + G_point) * d_AS_h

                        coor_hl = vec_adatom + G_point
                        dis_AS = min([np.linalg.norm(coor - coor_hl) for coor in arr_surfatoms_car])
                        if dis_AS > dis_min_AS: hl_coor_ad_car_app(coor_hl.tolist()); index_hl0_app(list_index_hl)
#                    num_hl3 += 1
#                num_hl2 += 1
#            num_hl1 += 1
        return hl_coor_ad_car, index_hl0

    def rm_samesites(self, coor_ad_car, surfatoms_car, index_coor_ad):
        coor_ad_car1 = []; index_coor_ad1 = []
        coor_ad_car1_app = coor_ad_car1.append; index_coor_ad1_app = index_coor_ad1.append
#        num1 = 0
        for num1, x1 in enumerate(coor_ad_car):
            if x1 not in coor_ad_car1: coor_ad_car1_app(x1); index_coor_ad1_app(num1)
#            num1 += 1
        list_index1 = [index_coor_ad[x] for x in index_coor_ad1]

        coor_ad_car2 = []; index_coor_ad2 = []
        coor_ad_car2_app = coor_ad_car2.append; index_coor_ad2_app = index_coor_ad2.append
#        num2 = 0
        for num2, x2 in enumerate(coor_ad_car1):
            for y in surfatoms_car:
                if x2 not in surfatoms_car and x2 not in coor_ad_car2: coor_ad_car2_app(x2); index_coor_ad2_app(num2)
#            num2 += 1
        list_index2 = [list_index1[x] for x in index_coor_ad2]
        return coor_ad_car2, list_index2

    def get_adsites(self, coor_dir, surfatoms_dir, list_index2, dict_labelofel):
        L = ""; candidates_adsite = []
        for x in list_index2:
            list = []
            for i in range(0, len(x)):
                S = int(coor_dir.index(surfatoms_dir[x[i]])) + 1
                list.append(S); L += str(dict_labelofel[S]) + "  "
            else: L += "\n"
            candidates_adsite.append(list)

        return L, candidates_adsite

    def mk_3x3x3_coor(self, arr_coor_dir, mat_lat, eachlabels_el_nn):
        A = np.mgrid[-1:1.1, -1:1.1, -1:1.1].reshape(3, -1).T
        coor3x3x3_dir = []
        append_ad = coor3x3x3_dir.append
        for vec in A:
            arr_coor33_dir = arr_coor_dir + vec
            for x in arr_coor33_dir: append_ad(x)
#                if abs(x[0]) < range_lp and abs(x[1]) < range_lp and abs(x[2]) < range_lp: append_ad(x)

        arr_coor3x3x3_dir = np.array(coor3x3x3_dir)
        arr_coor3x3x3_car = np.array(f2c.frac2cart(mat_lat, arr_coor3x3x3_dir.tolist()))
        x = 0
        list_labelofel_3x3x3 = []
        while x < len(A):
            list_labelofel_3x3x3 += eachlabels_el_nn
            x += 1
        return arr_coor3x3x3_car, arr_coor3x3x3_dir, list_labelofel_3x3x3

    def mk_each_3x3x3coor(self, coor_eachel_dir, mat_lat, eachlabels_el_nn):
        arr_coor333_eachel_car = []; arr_coor333_eachel_dir = []; eachlabels_el_333 = []
        for l in coor_eachel_dir:
            coor333_car, coor333_dir, eachlabel_el_333 = self.mk_3x3x3_coor(np.array(l), mat_lat, eachlabels_el_nn)
            arr_coor333_eachel_car.append(np.array(coor333_car))
            arr_coor333_eachel_dir.append(np.array(coor333_dir))
            eachlabels_el_333.append(eachlabel_el_333)
        return arr_coor333_eachel_car, arr_coor333_eachel_dir, eachlabels_el_333

    """
    def mk_coor3x3x3_eachel(self, arr_coor333_car, arr_coor333_dir, eachlabels_el_333):
        eachlabels_el_333_argsrt = np.array(eachlabels_el_333).argsort()
        coor333_car_srt = [arr_coor333_car[i] for i in eachlabels_el_333_argsrt]
        coor333_dir_srt = [arr_coor333_dir[i] for i in eachlabels_el_333_argsrt]

        def mk_coor3x3x3_eachel_indef(coor333_srt):
            coor333_tot = []; ac = []
#            label333_tot = []; al = []
            for i, x in enumerate(coor333_srt):
                if i == 0:
                    ac.append(coor333_srt[i])
                elif i == len(coor333_tot) -1:
                    if eachlabels_el_333_argsrt[i] == eachlabels_el_333_argsrt[i]:
                        ac.append(coor333_srt[i]); coor333_tot.append(ac)
                    else:
                        coor333_tot.append(ac)
                        coor333_tot.append([coor333_srt[i]])
                else:
                    if eachlabels_el_333_argsrt[i] == eachlabels_el_333_argsrt[i]:
                        ac.append(coor333_srt[i])
                    else:
                        coor333_tot.append(ac)
                        ac = []; ac.append(coor333_srt[i])

            return coor333_tot

        coor333_eachel_car = mk_coor3x3x3_eachel_indef(coor333_car_srt)
        coor333_eachel_dir = mk_coor3x3x3_eachel_indef(coor333_dir_srt)

        return coor333_eachel_car, coor333_eachel_dir, eachlabels_el_333_argsrt
    """

    def rm_adsites_nearatom(self, arr_coor_ad_car, arr_coor3x3x3_car, len_min):
        coor_ad_car_new = []
        for coor_ad in arr_coor_ad_car:
            diss_adtosf = sorted([np.linalg.norm(coor_ad - x) for x in arr_coor3x3x3_car])
            if min(diss_adtosf) > len_min: coor_ad_car_new.append(coor_ad)
        return coor_ad_car_new

    def get_info_adsites(self, arr_coor_ad_car, arr_coor3x3x3_car, sumatomnum, N, len_min):
        sum_info = [] #; sum_list = []
        for coor_ad in arr_coor_ad_car:
            diss_adtosf = [np.linalg.norm(coor_ad - x) for x in arr_coor3x3x3_car]
            #coor_adtosf = [x for x in arr_coor3x3x3_car]
            #arr_coor_adtosf = np.array(coor_adtosf)
            index_dis_adtosf = np.array(diss_adtosf).argsort()
            diss_adtosf = sorted(diss_adtosf)

            # Make distance list of adatom and surface atoms in support (Best3)
            diss_min = diss_adtosf[0:N]
            indexs_min = []; A = index_dis_adtosf[0:N]
            for i in A: indexs_min.append(np.mod(i, int(sumatomnum))+1)

            #labels_min = [list_labelofel_3x3x3[i-1] for i in indexs_min]
            #sum_ind_labels = [labelofel.index(l) for l in labels_min]

            sum_info.append(diss_min) # + sum_ind_labels)
            #sum_info.append([diss_min, labels_min, indexs_min, coor_ad.tolist()])

        return sum_info

    def get_dissmin(self, contlines, lab_in, N):
        labelofel = rp.get_labelofel(contlines)
        numofel = rp.get_numofel(contlines)
        sumatomnum = rp.get_sumofnumofel(contlines)
        mat_lat = rp.get_matrix(contlines)
        eachlabels_el, eachlabels_el_nn, dict_labelofel = rp.mk_labelofel(labelofel, numofel)
        coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
        indexs_lab_in = []
        for i, l in enumerate(eachlabels_el):
            if l in "!" + lab_in: indexs_lab_in.append(i)

        pos_basis = coor_car[indexs_lab_in[-1]]
        arr_pos_basis = np.array(pos_basis)

        numofel_gcd = np.array(list(map(int,numofel[:-1])))/(calc.gcd_list(list(map(int,numofel[:-1]))))
        num_thr_surounded_ad = np.array((float(N)/np.sum(numofel_gcd)) * numofel_gcd)
        num_thr_surounded_ad = np.ceil(num_thr_surounded_ad)
        eachlabels_el_rmad = eachlabels_el_nn[:-1]
        coor_eachel_dir, label_eachel = rp.get_coordinate_eachel(coor_dir[:-1], eachlabels_el_rmad)
        arr_coor333_eachel_car, arr_coor333_eachel_dir, eachlabels_el_333 = self.mk_each_3x3x3coor(coor_eachel_dir, mat_lat, eachlabels_el_rmad)

        """
        labelで指定された原子の座標を取り除く
        """

        #arr_coor_dir = np.array(coor_dir) #; arr_coor_car = np.array(coor_car)
        #list_num_SD = rp.mk_listnumSD(list_SD)

        """
        A = np.mgrid[-1:1.1, -1:1.1, -1:1.1].reshape(3, -1).T
        coor_3x3x3_dir = []; coor_3x3x3_dir_ap = coor_3x3x3_dir.append
        for vec in A:
            coor_3x3_dir = arr_coor_dir + vec
            for p in coor_3x3_dir: coor_3x3x3_dir_ap(p)

        arr_coor_3x3x3_dir = np.array(coor_3x3x3_dir); coor_3x3x3_dir = arr_coor_3x3x3_dir.tolist()
        arr_coor_3x3x3_car = np.array(f2c.frac2cart(mat_lat, coor_3x3x3_dir))
        #; coor_3x3x3_car = arr_coor_3x3x3_car.tolist()
        """

        for i, coor333_eachel in enumerate(arr_coor333_eachel_car):
            Ni = int(num_thr_surounded_ad[i])
            diss = [np.linalg.norm(pos_basis - p) for p in coor333_eachel]

            index_dis_sort = np.array(diss).argsort()
            dis_sort = np.array(sorted(diss))
            diss_min = dis_sort[:Ni]

            coors_min = [coor333_eachel[i1] for i1 in index_dis_sort[1:Ni+1]]
            uni_vecs_min = [(coor - arr_pos_basis)/diss_min[i2] for i2, coor in enumerate(coors_min)]

            indexs_min = [np.mod(i3, int(len(coor333_eachel))) for i3 in index_dis_sort[1:Ni+1]]
            labels_min = [eachlabels_el_333[i][i4].replace("!","") for i4 in indexs_min]

            if i == 0:
                diss_min_tot = diss_min; uni_vecs_min_tot = uni_vecs_min; labels_min_tot = labels_min
            else:
                diss_min_tot = np.append(diss_min_tot, diss_min)
                uni_vecs_min_tot = np.append(uni_vecs_min_tot, uni_vecs_min)
                labels_min_tot = np.append(labels_min_tot, labels_min)

        return diss_min_tot, labels_min_tot, uni_vecs_min_tot


        """
        diss = [np.linalg.norm(pos_basis - p) for p in arr_coor_3x3x3_car]
        index_dis_sort = np.array(diss).argsort()
        dis_sort = sorted(diss); diss_min = dis_sort[1:N+1]
        coors_min = [arr_coor_3x3x3_car[i] for i in index_dis_sort[1:N+1]]
        uni_vecs_min = [(coor - arr_pos_basis)/diss_min[i] for i, coor in enumerate(coors_min)]

        indexs_min = [np.mod(i, int(sumatomnum)) for i in index_dis_sort[1:N+1]]
        labels_min = [eachlabels_el[l].replace("!","") for l in indexs_min]
        """

    def mk_labels_adsite(self, index_ad, eachlabels_el, index_surf, path, ch):
        labels_ad_site = []; Lpairad = ""
        for l in index_ad:
            Pair_ad = []
            for x in l:
                Pair_ad.append(eachlabels_el[index_surf[x]].replace("!",""))
                Lpairad += eachlabels_el[index_surf[x]].replace("!","") + " "
#                if x == l[-1]: Pair_ad += eachlabels_el[index_surf[x]].replace("!","")
#                else: Pair_ad +=  eachlabels_el[index_surf[x]].replace("!","") + "-"
            labels_ad_site.append(Pair_ad)
            Lpairad += "\n"

        with open(path + "/labels_" + str(ch) + "_sites.dat", "w") as f: f.write(Lpairad)
        return labels_ad_site
# END