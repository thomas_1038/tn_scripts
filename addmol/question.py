import math, sys, os
from addmol import helper

########## Bond length and angle of molecules ###########
bond_H2     = 0.75020  #H2
bond_N2     = 1.11290  #N2
bond_O2     = 1.23420  #O2
bond_F2     = 1.42404  #F2
bond_CO     = 1.14309  #CO
bond_CO2    = 1.17647  #CO2
bond_OH_H2O = 0.97192  #H2O
bond_HH_H2O = 1.53697  #H2O
angle_H2O   = 75.5     #H2O
bond_CC_C2H2 = 1.20750 #C2H2
bond_CH_C2H2 = 1.07015 #C2H2
bond_NH_NH3  = 1.02135 #NH3
bond_HH_NH3 =  1.63713 #NH3
bond_CH_CH4 = 1.09589
bond_HH_CH4 = 1.78958
dis_len = 1.5
###############################################

def Q_sd():
    Q_sd = input("\nDo you check Selective Dynamics TAG in POSCAR? [y,N]: ")
    if Q_sd in ["y", "Y"]: pass
    else: print("\nYou should change Selective Dynamics TAG in POSCAR! BYE!\n"); sys.exit()

def Q_file(nowpath):
    vfiles = [x for x in os.listdir(nowpath) if ".vasp" in x]
    print("Vasp files are here: " + " ".join(vfiles))
    Q_filename = input("Please input the file name; POSCAR or CONTCAR or SPOSCAR; [p, c, s] or some file name: ")
    if Q_filename in ["p"]: filename = "POSCAR"
    elif Q_filename in ["c"]: filename = "CONTCAR"
    elif Q_filename in ["s"]: filename = "SPOSCAR"
    else: filename = str(Q_filename)
    return filename

def Q_d_AS(dis_AdtoSup_df):
    Q_d_AS =input("Please input the distance between support and molecule.\ndefault values(ontop: " + str(dis_AdtoSup_df) + "Ang, bridge: " + str(dis_AdtoSup_df) + "Ang, hollow: " + str(dis_AdtoSup_df) +"Ang) or by hand [d, h]: ")
    if Q_d_AS in ["d"]:
        d_AS_o = dis_AdtoSup_df; d_AS_b = dis_AdtoSup_df; d_AS_h = dis_AdtoSup_df

    elif Q_d_AS in ["h"]:
        Q_d_AS_ontop = input("Please input the distance between support and molecule-\"ontop\"; default value is " + str(dis_AdtoSup_df) + "Ang, [d, num]: ")
        if Q_d_AS_ontop in ["d"]: d_AS_o = dis_AdtoSup_df
        else: d_AS_o = float(Q_d_AS_ontop)

        Q_d_AS_bridge = input("Please input the distance between support and molecule-\"bridge\"; default value is " + str(dis_AdtoSup_df) + "Ang, [d, num]: ")
        if Q_d_AS_bridge in ["d"]: d_AS_b = dis_AdtoSup_df
        else: d_AS_b = float(Q_d_AS_bridge)

        Q_d_AS_hollow = input("Please input the distance between support and molecule-\"hollow\"; default value is " + str(dis_AdtoSup_df) + "Ang, [d, num]: ")
        if Q_d_AS_hollow in ["d"]: d_AS_h = dis_AdtoSup_df
        else: d_AS_h = float(Q_d_AS_hollow)
    return d_AS_o, d_AS_b, d_AS_h

def Q_range_surf():
    Q_range_surf = input("Please input the surface range; defalt value is 1.0Ang, [d, num]: ")
    if Q_range_surf in ["d"]: range_surf = 1.0
    else: range_surf = float(Q_range_surf)
    return range_surf

def Q_Mode():
    Mode = input("Please input the adsorption mode, ad-atom(1) or diatomic-molecule(2) or polyatomic-molecule; [a, d, p]: ")
    if Mode in ["a"]:
        Elementname = input("Please input the name of element of adsorbed atom / molecule, i.e., Ru: ")
        list_Ename = [Elementname]
        list_Num_adatom = [1]
        len_mol = []
        angle_mol = []
        Q_kind_mol = ""
        Q_dismode = ""
        Shape = "L"
    elif Mode in ["d"]:
        Q_dismode = input("Do you use dissociation mode? (This mode can make initial and final model of molecule clreavage.) [y/N]: ")
        Q_kind_mol = input("Please input the kind of molecules, [H2,h2 / O2,o2 / N2,n2 / F2, f2 / CO, co] or distance you want, i.e., 2: ")
        Shape = "L"
        angle_mol = []
        if Q_kind_mol in ["H2", "h2"]:   list_Ename = ["H"]; list_Num_adatom = [2]; len_mol = [bond_H2]
        elif Q_kind_mol in ["O2", "o2"]: list_Ename = ["O"]; list_Num_adatom = [2]; len_mol = [bond_O2]
        elif Q_kind_mol in ["N2", "n2"]: list_Ename = ["N"]; list_Num_adatom = [2]; len_mol = [bond_N2]
        elif Q_kind_mol in ["F2", "f2"]: list_Ename = ["F"] ; list_Num_adatom = [2]; len_mol = [bond_F2]
        elif Q_kind_mol in ["CO", "co"]: list_Ename = ["C", "O"] ; list_Num_adatom = [1, 1]; len_mol = [bond_CO]
        else:
            Elementname = input("Please input the name of element of adsorbed atom or molecule, i.e., Ru or H O: ")
            len_mol = [float(Q_kind_mol)]
            list_Ename = helper.conv_line(Elementname)
            list_Num_adatom = [1, 1]

    elif Mode in ["p"]:
        Q_dismode = input("Do you use dissociation mode? (This mode can make initial and final model of molecule cleavage.) [y/N]: ")
        Q_kind_mol = input("Please input bond length of molecule, [CO2, co2 / H2O, h2o / C2H2, c2h2 / NH3, nh3 / CH4, ch4 ] or number you want, i.e., 2: ")
        if Q_kind_mol in ["CO2", "co2"]:
            Num_adatom = 3; list_Num_adatom = [1, 2]
            list_Ename = ["C", "O"]
            len_mol = [bond_CO2]
            angle_mol = []
            Shape = "L" #Linear
        if Q_kind_mol in ["H2O", "h2o"]:
            Num_adatom = 3 ; list_Num_adatom = [1, 2]
            list_Ename = ["O", "H"]
            len_mol = [bond_OH_H2O, bond_HH_H2O]
            angle_mol = [angle_H2O]
            Shape = "N" #Nonlinear
        elif Q_kind_mol in ["C2H2", "c2h2"]:
            Num_adatom = 4 ; list_Num_adatom = [2, 2]
            list_Ename = ["C", "H"]
            len_mol = [bond_CC_C2H2, bond_CH_C2H2]
            angle_mol = []
            Shape = "L"
        elif Q_kind_mol in ["NH3", "nh3"]:
            Num_adatom = 4 ; list_Num_adatom = [1, 3]
            list_Ename = ["N", "H"]
            len_mol = [bond_NH_NH3, bond_HH_NH3, math.sqrt(float(bond_NH_NH3)**2 -float(bond_HH_NH3)**2/3)]
            angle_mol = []
            Shape = "N"
        elif Q_kind_mol in ["CH4", "ch4"]:
            Num_adatom = 5 ; list_Num_adatom = [1, 4]
            list_Ename = ["C", "H"]
            len_mol = [bond_CH_CH4, bond_HH_CH4]
            angle_mol = []
            Shape = "N"

    return Q_kind_mol, list_Ename, list_Num_adatom, Mode, Shape, len_mol, angle_mol, Q_dismode

def Q_adsite():
    Q_adsite = input("Please input the adsorption site you want to get, [ontop_endon, oe / ontop_sideon, os / bridge_endon, be / bridge_sideon, bs / hollow_endon, he] : ")
    if Q_adsite in ["ontop_endon", "oe"]: adsite = "ontop_endon"
    elif Q_adsite in ["ontop_sideon", "os"]: adsite = "ontop_sideon"
    elif Q_adsite in ["bridge_endon", "be"]: adsite = "bridge_endon"
    elif Q_adsite in ["bridge_sideon", "bs"]: adsite = "bridge_sideon"
    elif Q_adsite in ["hollow_endon", "he"]: adsite = "hollow_endon"
    return adsite

def Q_pltshow():
    pltshow = input("Do you want to show the dendgram of the adsorption sites, [y,N]: ")
    if pltshow in ["y", "Y"]: ind_plt = 1
    else: ind_plt = 0
    return ind_plt

def Q_length_min():
    Q_lenmin = input("Please input the minimum distance between add-X and surface atom; default value is 1.5 Ang: ")
    if "d" in Q_lenmin: len_min = 1.5
    else: len_min = float(Q_lenmin)
    return len_min
# END