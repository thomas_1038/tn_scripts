import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#from sklearn.cluster import KMeans
#from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster #, inconsistent
from addmol import Frac2Cart
f2c = Frac2Cart()

class Screening:
    # Dendrogram
    def dendro(self, df, path_info, name_site, threshold, ind_plt=1):
        # Ward x Euclidean distance
        linkage_result = linkage(df, method='ward', metric='euclidean')

        # Incosistent cofficient
        #inconsistent_result = inconsistent(linkage_result)
        #df_inconsistent = pd.DataFrame(inconsistent_result)
        #df_inconsistent.to_csv(path + "/" + name_site + "_incns.dat", sep="\t")

        # Visualization of hierarchical clustering
        plt.figure(num=None, facecolor='w', edgecolor='k')
        dendrogram(linkage_result, color_threshold=threshold)

        # Get the clustering result
        clusters = fcluster(linkage_result, threshold, criterion='distance')
        df_clusters = pd.DataFrame(clusters)
        df_clusters.to_csv(path_info + "/" + name_site +"_clst.dat", sep="\t")

        plt.savefig(path_info  +"/" + name_site + "_dendrogram.png", dpi=300, facecolor="None", edgecolor="None")
        if ind_plt == 1: plt.show()
        return clusters

    # Clustering by K-means
    def kmeans(self, list):
        model = KMeans(n_clusters=2).fit(list)

        # Reduce dimensions with PCA
        pca = PCA(n_components=2)
        model_r = pca.fit_transform(list)

        # Visualize the results on the scatter plot
        plt.figure()
        for (i, label) in enumerate(model.labels_):
            if label == 0: plt.scatter(model_r[i, 0], model_r[i, 1], c='red')
            elif label == 1: plt.scatter(model_r[i, 0], model_r[i, 1], c='blue')
        plt.show()

    # DBSCAN
    def dbscan(self, df, path_info, name_site, ind_plt):
        dbs_tot = []
        if ind_plt == 1:
            for eps in np.arange(0.1, 1.5, 0.1):
                for minPts in range(1, 20, 2):
                    dbscan = DBSCAN(eps=eps,min_samples=minPts).fit(df)
                    y_dbscan = dbscan.labels_
                    # The number of the outlier
                    outlier = len(np.where(y_dbscan ==-1)[0])
                    # The number of the cluster
                    num_clst = int(np.max(y_dbscan)) + 1
                    # The number of the cluser1 and cluster2
                    num_plt1 = len(np.where(y_dbscan ==0)[0])
                    num_plt2 = len(np.where(y_dbscan ==1)[0])
                    dbs_tot.append([eps, minPts, outlier, num_clst, num_plt1, num_plt2])

            df_clusters = pd.DataFrame(dbs_tot)
            df_clusters.columns = ["eps", "minPts", "outlier", "Nc", "Np1", "Np2"]
            df_clusters.to_csv(path_info + "/" + name_site +"_clst.dat", sep=",")
            return df_clusters

        else:
            dbscan = DBSCAN()
            clusters = dbscan.fit_predict(df)
            df_clusters = pd.DataFrame(clusters)
            df_clusters.to_csv(path_info + "/" + name_site +"_clst.dat", sep="\t")
            print(clusters)
            return clusters

    # Create the clustering result
    def mk_ad_clst(self, adsite, ad_df, ad_label, eachlabels_el, name, threshhold, path_info, ind_plt):
        ad_clst = self.dendro(ad_df, path_info, name, threshhold, ind_plt)
        #ad_clst = self.dbscan(ad_df, path_info, name)
        ad_clst_srt = sorted(ad_clst); ad_clst_argsrt = np.array(ad_clst).argsort()
        ad_label_srt = [ad_label[i] for i in ad_clst_argsrt]
#        print(ad_label_srt); sys.exit()
        ad_index_srt = [[eachlabels_el.index("!"+x) for x in l] for l in ad_label_srt]
        max_clst = np.max(ad_clst)

        ad_clst = []; ad_label_clst = []; ad_index_clst = []
        for i1 in range(1, max_clst + 1):
            a = [adsite[ad_clst_argsrt[i]] for i in range(0, len(ad_clst_argsrt)) if i1 == int(ad_clst_srt[i])]
#            b = [ad_clst_argsrt[i] for i in range(0, len(ad_clst_argsrt)) if i1 == int(ad_clst_srt[i])]
            b = [ad_label_srt[i] for i in range(0, len(ad_clst_argsrt)) if i1 == int(ad_clst_srt[i])]

            c = []
            for i in range(0, len(ad_clst_argsrt)):
                if i1 == int(ad_clst_srt[i]):
                    cc = []
                    for n1 in ad_index_srt[i]: 
                        cc.append(n1 + 1)
                    c.append(cc)
            #c = [ad_index_srt[i]  for i in range(0, len(ad_clst_argsrt)) if i1 == int(ad_clst_srt[i])]
            #print(c)
            ad_clst.append(a); ad_label_clst.append(b); ad_index_clst.append(c)
        return ad_clst, ad_label_clst, ad_index_clst
# END