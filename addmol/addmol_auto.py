#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, math, shutil, sys #, sys, re,  glob
import numpy as np
import pandas as pd

import addmol.question as q
import addmol.readpos as rp
import addmol.get_surfatoms as gs
import addmol.mk_coorad as mkca
import addmol.mk_poscar as mkp
import addmol.helper as hl
from addmol import calc, Frac2Cart, GetCandidateAdSite, Screening
f2c = Frac2Cart(); getca = GetCandidateAdSite(); scr = Screening()

######## Best parameter (2017/10/31) #########
# Threshhold (Used by making candidates of adsorption sites)
thr_dis = 0.1
surf_roughness = 0.1
range_surf = 1.0

# Threshhold (Used by screening candidates of adsorption sites)
range_lp = 2  #supercell
dis_min_AS = 1.7

dis_AdtoSup_df = 2.5
d_AS_o = dis_AdtoSup_df
d_AS_b = dis_AdtoSup_df
d_AS_h = dis_AdtoSup_df

num_clst = 20 #The number of atoms which are located around the ad-site.
coeff_for_clust = 0.2
thr_clst = num_clst * coeff_for_clust

thr_clst_op = thr_clst
thr_clst_br = thr_clst
thr_clst_hl = thr_clst

length_min = 1.5

###############################################

########## Bond length and angle of molecules ###########
bond_H2     = 0.75020  #H2
bond_N2     = 1.11290  #N2
bond_O2     = 1.23420  #O2
bond_F2     = 1.42404  #F2
bond_CO     = 1.14309  #CO
bond_CO2    = 1.17647  #CO2
bond_OH_H2O = 0.97192  #H2O
bond_HH_H2O = 1.53697  #H2O
angle_H2O   = 75.5     #H2O
bond_CC_C2H2 = 1.20750 #C2H2
bond_CH_C2H2 = 1.07015 #C2H2
bond_NH_NH3  = 1.02135 #NH3
bond_HH_NH3 =  1.63713 #NH3
dis_len = 1.5
###############################################

print("\n### START PROGRAM ###\n")

path = os.getcwd()
path_input = path + "/input.dat"

if os.path.exists(path_input):
    with open(path_input) as fip: inputlines = fip.readlines()
    inputlines = [l.replace("=","") for l in inputlines]

    for il in inputlines:
        if "filename" in il:
            filename = hl.conv_line(il)[-1]
        elif "thr_dis" in il:
            thr_dis = float(hl.conv_line(il)[-1])
        elif "surf_roughness" in il:
            surf_roughness = float(hl.conv_line(il)[-1])
        elif "range_surf" in il:
            range_surf = float(hl.conv_line(il)[-1])
        elif "range_lp" in il:
            range_lp = float(hl.conv_line(il)[-1])
        elif "length_min" in il:
            length_min = float(hl.conv_line(il)[-1])
        elif "dis_min_AS" in il:
            dis_min_AS = float(hl.conv_line(il)[-1])
        elif "d_AS_o" in il:
            d_AS_o = float(hl.conv_line(il)[-1])
        elif "d_AS_b" in il:
            d_AS_b = float(hl.conv_line(il)[-1])
        elif "d_AS_h" in il:
            d_AS_h = float(hl.conv_line(il)[-1])
        elif "kind_mol" in il:
            kind_mol = str(hl.conv_line(il)[-1])
        elif "Ename" in il:
            list_Ename = hl.conv_line(il)[1:]
        elif "Num_adatom" in il:
            list_Num_adatom =[int(x) for x in hl.conv_line(il)[1:]]
            Num_adatom = sum(list_Num_adatom)
        elif "Mode" in il:
            Mode = str(hl.conv_line(il)[-1])
        elif "Shape" in il:
            Shape = str(hl.conv_line(il)[-1])
        elif "len_mol" in il:
            len_mol = [float(x) for x in hl.conv_line(il)[1:]]
        elif "angle_mol" in il:
            if len(hl.conv_line(il)) == 1:
                angle_mol = []
            else:
                angle_mol = [float(x) for x in hl.conv_line(il)[1:]]
        elif "dismode" in il:
            dismode = str(hl.conv_line(il)[-1])
        elif "num_clst" in il:
            num_clst = int(hl.conv_line(il)[-1])
        elif "coeff_for_clust" in il:
            coeff_for_clust = float(hl.conv_line(il)[-1])
        elif "thr_clst_op" in il:
            thr_clst_op = float(hl.conv_line(il)[-1])
        elif "thr_clst_br" in il:
            thr_clst_br = float(hl.conv_line(il)[-1])
        elif "thr_clst_hl" in il:
            thr_clst_hl = float(hl.conv_line(il)[-1])
        elif "ind_plot" in il:
            ind_plt = int(hl.conv_line(il)[-1])

else:
    # Questions
    filename = q.Q_file(os.getcwd())
    d_AS_o, d_AS_b, d_AS_h = q.Q_d_AS(dis_AdtoSup_df)
    length_min = q.Q_length_min()
    range_surf = q.Q_range_surf()
    kind_mol, list_Ename, list_Num_adatom, Mode, Shape, len_mol, angle_mol, dismode = q.Q_Mode()
    ind_plt = q.Q_pltshow()
    Num_adatom = sum(list_Num_adatom)

Li = ""
Li += "filename = " + filename + "\n\n"

Li += "# Threshhold (Used by making candidates of adsorption sites)\n"
Li += "thr_dis = " + str(thr_dis) + "\n"
Li += "surf_roughness = " + str(surf_roughness) + "\n"
Li += "range_surf = " + str(range_surf) + "\n\n"

Li += "# Threshhold (Used by screening candidates of adsorption sites)" + "\n"
Li += "range_lp = " + str(range_lp) + "\n"  #supercell
Li += "length_min = " + str(length_min) + "\n"
Li += "dis_min_AS = " + str(dis_min_AS) + "\n"
Li += "d_AS_o = " + str(d_AS_o) + "\n"
Li += "d_AS_b = " + str(d_AS_b) + "\n"
Li += "d_AS_h = " + str(d_AS_h) + "\n\n"

Li += "# Information about adsorbate" + "\n"
Li += "kind_mol = " + str(kind_mol) + "\n"
Li += "Ename = " + " ".join(list_Ename) + "\n"
Li += "Num_adatom = " + " ".join([str(x) for x in list_Num_adatom]) + "\n"
Li += "Mode = " + str(Mode) + "\n"
Li += "Shape = " + str(Shape) + "\n"
Li += "len_mol = " + " ".join([str(x) for x in len_mol]) + "\n"
Li += "angle_mol = " + " ".join([str(x) for x in angle_mol]) + "\n"
Li += "dismode = " + str(dismode) + "\n\n"

Li += "# Clustering" + "\n"
Li += "num_clst = " + str(num_clst) + "\n"
Li += "coeff_for_clst = " + str(coeff_for_clust) + "\n"
Li += "thr_clst_op = " + str(thr_clst_op) + "\n"
Li += "thr_clst_br = " + str(thr_clst_br) + "\n"
Li += "thr_clst_hl = " + str(thr_clst_hl) + "\n"
Li += "ind_plot = " + str(ind_plt) + "\n\n"

"""
kind_mol = ["Au"]
list_Ename = ["Au"]
list_Num_adatom = [1]
Num_adatom = sum(list_Num_adatom)
Mode = "a"
Shape = ""
len_mol = [bond_N2]
angle_mol = [0]
dismode = "n"
ind_plt = 1
"""

num = 0
while True:
    if num in range(0,10):
        path_dir = path + "/0" + str(num)
    else:
        path_dir = path + "/" + str(num)

    if os.path.exists(path_dir):
        num += 1
    else: break

os.mkdir(path_dir)
with open(path_dir + "/input.dat", "w") as fi: fi.write(Li)
#dirname = "N" + str(N) + "_thr" + str(thr_clst) + "_rangeS" + str(range_surf) + "_disOp" + str(d_AS_o) + "_disBr" + str(d_AS_b) + "_disHl" + str(d_AS_h) + "_lenmin" + str(length_min)
#path_dir = path + "/" + dirname
#if not os.path.exists(path_dir): os.mkdir(path_dir)

path_info = path_dir + "/info_files"
if not os.path.exists(path_info): os.mkdir(path_info)

# Read POSCAR/CONTCAR/SPOSCAR
with open(filename, "r") as f: contlines = f.readlines()

# Get the atomic species, the atomic number, the total number of atoms from POSCAR
labelofel = rp.get_labelofel(contlines)
numofel = rp.get_numofel(contlines)
sumatomnum = rp.get_sumofnumofel(contlines)

# Make the atomic tags at the far right of INCAR
eachlabels_el, eachlabels_el_nn, dict_labelofel = rp.mk_labelofel(labelofel, numofel)

#
numofel_gcd = np.array(list(map(int,numofel)))/(calc.gcd_list(list(map(int,numofel))))
num_thr_surounded_ad = np.array(math.ceil(num_clst/np.sum(numofel_gcd)) * numofel_gcd).tolist()

"""
デバッグ
print(eachlabels_el, eachlabels_el_nn, dict_labelofel)
sys.exit()
"""

print("Support: " + " ".join([str(x) for x in labelofel]))
print("Num: " + " ".join([str(x) for x in numofel]))

# Get the several lines starting from POSCAR, lattice vector, atomic coordinates
# POSCAR's beginning few lines (up to Direct / Cartesian)
firstlines, flagS = rp.get_firstlines(contlines)
# lattice vector
mat_lat = rp.get_matrix(contlines)
coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
arr_coor_car = np.array(coor_car); arr_coor_dir = np.array(coor_dir)
list_num_SD = rp.mk_listnumSD(list_SD)
coor_eachel_dir, label_eachel = rp.get_coordinate_eachel(coor_dir, eachlabels_el_nn)

print("Num_fix: " + " ".join([str(x) for x in list_num_SD]) + "\n")

# Cross product of each lattice vector and its unit vector, a, b vector
veca = np.array(mat_lat[0]); vecb = np.array(mat_lat[1])
cross_ab = np.cross(veca, vecb)
veca_uni = calc.uni_vec(veca); vecb_uni = calc.uni_vec(vecb)
cross_ab_uni = calc.uni_vec(cross_ab)
#rota_120_uni = calc.rotation(cross_ab_uni, veca_uni, 120)[0]
#rota_240_uni = calc.rotation(cross_ab_uni, veca_uni, 240)[0]
lat_c = math.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)

# Get the atomic coordinates of several surface layers
print("Getting surface atoms ...")
surfatoms_car, surfatoms_dir, index_surf, Param_rough = gs.get_surfatoms(coor_dir, mat_lat, range_surf, surf_roughness)
arr_surfatoms_dir = np.array(surfatoms_dir); arr_surfatoms_car = np.array(surfatoms_car)
#print(index_surf)

if Param_rough == "S": print("Parameter of roughness: Smooth")
else: print("Parameter of roughness: Rough")
print("Number of atoms in surface: " + str(len(arr_surfatoms_car)) + "\n")

# Make the candidates for the ontop sites
print("Making the candidates of ontop sites ... ")
op_ad_car = getca.get_candidates_op(arr_surfatoms_car, cross_ab_uni, d_AS_o)
index_op = [[i] for i in range(0, len(index_surf))]
arr_op_ad_car = np.array(op_ad_car)
print("Pre-ontop site: " + str(len(op_ad_car)) + "\n")

# Make the candidates for the bridge sites
print("Making the candidates of bridge sites ... ")
br_ad0, index_br0 = getca.get_candidates_br(arr_surfatoms_car, surfatoms_dir, lat_c, cross_ab, thr_dis, d_AS_b, Param_rough, dis_min_AS)
br_ad_car, index_br = getca.rm_samesites(br_ad0, surfatoms_car, index_br0)
arr_br_ad_car = np.array(br_ad_car)
#L_br, index_br_sum = getca.get_adsites(coor_dir, surfatoms_dir, index_br0, dict_labelofel)
print("Pre-bridge site: " + str(len(br_ad_car)) + "\n")

# Make the candidates for the hollow site
print("Making the candidates of hollow sites ... ")
hl_ad0, index_hl0 = getca.get_candidates_hl(arr_surfatoms_car, surfatoms_dir, lat_c, cross_ab, thr_dis, d_AS_h, Param_rough, dis_min_AS)
hl_ad_car, index_hl = getca.rm_samesites(hl_ad0, surfatoms_car, index_hl0)
arr_hl_ad_car = np.array(hl_ad_car)
#L_hl, index_hl_sum = getca.get_adsites(coor_dir, surfatoms_dir, index_hl, dict_labelofel)
print("Pre-hollow site: " + str(len(hl_ad_car)) + "\n")

# Make 3x3x3 coordinates to the original atomic coordinates
arr_coor333_car, arr_coor333_dir, eachlabels_el_333 = getca.mk_3x3x3_coor(arr_coor_dir, mat_lat, eachlabels_el_nn)
arr_coor333_eachel_car, arr_coor333_eachel_dir, eachlabels_el_333 = getca.mk_each_3x3x3coor(coor_eachel_dir, mat_lat, eachlabels_el_nn)

if len(op_ad_car) > 0:
    op_ad_car = getca.rm_adsites_nearatom(arr_op_ad_car, arr_coor333_car, length_min)
    arr_op_ad_car = np.array(op_ad_car)
if len(br_ad_car) > 0:
    br_ad_car = getca.rm_adsites_nearatom(arr_br_ad_car, arr_coor333_car, length_min)
    arr_br_ad_car = np.array(br_ad_car)
if len(hl_ad_car) > 0:
    hl_ad_car = getca.rm_adsites_nearatom(arr_hl_ad_car, arr_coor333_car, length_min)
    arr_hl_ad_car = np.array(hl_ad_car)

# Make the file describing the index of atoms constituting the adsorption site
# with open(path_info + "/sum_br_sites.dat","w") as f: f.write(L_br)
#with open(path_info + "/sum_hl_sites.dat","w") as f: f.write(L_hl)

def mk_pos_adtot(ch, ad_car, coor_car, path, firstlines, mat_lat, labelofel, numofel, flagS, list_SD):
    coor_car_tot = coor_car + ad_car
    labelofel_new = labelofel + ["X"]
    numofel_new = numofel + [str(len(ad_car))]
    eachlabels_el_ad = rp.mk_labelofel(labelofel_new, numofel_new)[0]

    list_SD_new = list_SD
    for i in range(0, len(ad_car)): list_SD_new.append(["T","T","T"])
    if len(ad_car) > 0:
        mkp.mk_pos(path_info, "POSCAR_" + str(ch) + "_tot.vasp", firstlines, mat_lat, labelofel_new,
                   numofel_new, eachlabels_el_ad, flagS, coor_car_tot, list_SD_new)

mk_pos_adtot("op", op_ad_car, coor_car, path_dir, firstlines, mat_lat, labelofel, numofel, flagS, list_SD)
mk_pos_adtot("br", br_ad_car, coor_car, path_dir, firstlines, mat_lat, labelofel, numofel, flagS, list_SD)
mk_pos_adtot("hl", hl_ad_car, coor_car, path_dir, firstlines, mat_lat, labelofel, numofel, flagS, list_SD)
if len(op_ad_car) > 0: labels_op_site = getca.mk_labels_adsite(index_op, eachlabels_el, index_surf, path_info, "op")
if len(br_ad_car) > 0: labels_br_site = getca.mk_labels_adsite(index_br, eachlabels_el, index_surf, path_info, "br")
if len(hl_ad_car) > 0: labels_hl_site = getca.mk_labels_adsite(index_hl, eachlabels_el, index_surf, path_info, "hl")

"""
arr_coor333_eachel_car = []; arr_coor333_eachel_dir = []; eachlabels_el_333 = []
for l in coor_eachel_dir:
    coor333_car, coor333_dir, eachlabel_el_333 = getca.mk_3x3x3_coor(np.array(l), mat_lat, range_lp, eachlabels_el_nn)
    arr_coor333_eachel_car.append(np.array(coor333_car))
    arr_coor333_eachel_dir.append(np.array(coor333_dir))
    eachlabels_el_333.append(eachlabel_el_333)
"""

# Screening for the candidates of the adsorption sites group by hierarchical clustering
print("Getting the information of adsorption sites ...")
def mk_clst_adsite(ad_car, ad_label, eachlabels_el, ch, thr_clst, num_ad, length_min):
    if num_ad == 0: return [], []
#    ad_info = getca.get_info_adsites(ad_car, arr_coor333_car, sumatomnum, N, length_min)

    for i, l in enumerate(arr_coor333_eachel_car):
        ad_info_eachel = getca.get_info_adsites(ad_car, l, sumatomnum, int(num_thr_surounded_ad[i]), length_min)
        if i == 0: ad_info = np.array(ad_info_eachel)
        else: ad_info = np.c_[ad_info, np.array(ad_info_eachel)]

    ad_df = pd.DataFrame(ad_info)
    ad_df.to_csv(path_info + "/" + ch + "_info.dat", sep="\t")
    ad_clst, ad_label_clst, ad_index_clst = scr.mk_ad_clst(ad_car, ad_df, ad_label, eachlabels_el, ch, thr_clst, path_info, ind_plt)
    return ad_clst, ad_label_clst, ad_index_clst

if len(op_ad_car) > 0:
    op_clst, op_label_clst, op_index_clst = mk_clst_adsite(arr_op_ad_car, labels_op_site, eachlabels_el, "op", thr_clst_op, len(op_ad_car), length_min)
if len(br_ad_car) > 0:
    br_clst, br_label_clst, br_index_clst = mk_clst_adsite(arr_br_ad_car, labels_br_site, eachlabels_el, "br", thr_clst_br, len(br_ad_car), length_min)
if len(hl_ad_car) > 0:
    hl_clst, hl_label_clst, hl_index_clst = mk_clst_adsite(arr_hl_ad_car, labels_hl_site, eachlabels_el, "hl", thr_clst_hl, len(hl_ad_car), length_min)

# Make the list and POSCARS of the clustered adsorption site groups
if not os.path.exists(path_dir + "/clst"): os.mkdir(path_dir + "/clst")
def mk_poss_clst(name, ch, ad_clst, ad_label_clst, ad_index_clst, num_ad):
    if num_ad == 0: return [], []
    path_clstdir = path_dir + "/clst/" + name + "_clst"
    if os.path.exists(path_clstdir): shutil.rmtree(path_clstdir)
    os.mkdir(path_clstdir)

    ad_coor_car_selected = []; ad_coor_dir_selected = []; indexs_selected = []; labels_selected = []
    for i in range(0, len(ad_clst)):
        ad_coor_car_selected.append(ad_clst[i][0])
        ad_eachclst = f2c.cart2frac(mat_lat, ad_clst[i]); ad_coor_dir_selected.append(ad_eachclst[0])
        ad_eachclst = [[x] for x in ad_eachclst]
        indexs_selected.append(ad_index_clst[i][0])
        labels_selected.append(ad_label_clst[i][0])

        poslines, Ltot = mkp.mk_pos_adline(ad_eachclst, coor_dir, list_SD, "a", labelofel, ["X"], numofel, [1], eachlabels_el, mat_lat, flagS)
        if i in range(0, 9): mkp.mk_poss_and_dir(path_clstdir, name + "0" + str(i+1), ch, poslines, Ltot)
        else: mkp.mk_poss_and_dir(path_clstdir, name + str(i+1), ch, poslines, Ltot)

    else:
        sumlines, Lsum = mkp.mk_pos_adline([[x] for x in ad_coor_dir_selected], coor_dir, list_SD, "a", labelofel, ["X"], numofel, [1], eachlabels_el, mat_lat, flagS)
        mkp.mk_poss_and_dir(path_clstdir, name + "_sum", ch, sumlines, Lsum)

    return ad_coor_car_selected, labels_selected, indexs_selected

if len(op_ad_car) > 0:
    op_selected, op_label_selected, op_index_selected = mk_poss_clst("ontop", "op", op_clst,  op_label_clst, op_index_clst, len(op_ad_car))
    print("\nontop site: " + str(len(op_selected)))
    print("label of ontop site:", op_label_selected)
if len(br_ad_car) > 0:
    br_selected, br_label_selected, br_index_selected = mk_poss_clst("bridge", "br", br_clst, br_label_clst, br_index_clst, len(br_ad_car))
    print("bridge site: " + str(len(br_selected)))
    print("label of bridge site:", br_label_selected, br_index_selected)

if len(hl_ad_car) > 0:
    hl_selected, hl_label_selected, hl_index_selected = mk_poss_clst("hollow", "hl", hl_clst, hl_label_clst, hl_index_clst, len(hl_ad_car))
    print("hollow site: " + str(len(hl_selected)))
    print("label of hollow site:", hl_label_selected, hl_index_selected)

# New lattice Parameter
cx = float(mat_lat[2][0]); cy = float(mat_lat[2][1]); cz = float(mat_lat[2][2])
lc = math.sqrt(cx**2 + cy**2 + cz**2)
n_for_cvec = (lc + float(d_AS_o))/lc
cx_new = cx * n_for_cvec ; cy_new = cy * n_for_cvec ; cz_new = cz * n_for_cvec
mat_lat_new = [[mat_lat[0][0], mat_lat[0][1], mat_lat[0][2]],
               [mat_lat[1][0], mat_lat[1][1], mat_lat[1][2]],
               [cx_new, cy_new, cz_new]]
coor_dir_new = f2c.cart2frac(mat_lat_new, arr_coor_car.tolist())

# Make the coordinates of the adsorbed molecules
print("\nMaking the coordinates of adsorped atoms or molecules ...")
def mk_ad_end(ad_selected, num_ad):
    if num_ad == 0: return []
    ad_end = mkca.mk_endon_mol(ad_selected, mat_lat_new, veca_uni, vecb_uni, cross_ab_uni, kind_mol, Num_adatom, Shape, len_mol, angle_mol)
    return ad_end

def mk_br_side(ad_selected, ad_index_selected, num_ad):
    if num_ad == 0: return [], []
    ad_side_ini, ad_side_fin = mkca.mk_sideon_mol(ad_selected, ad_index_selected, arr_coor_car, mat_lat_new, cross_ab_uni, Param_rough, d_AS_b, kind_mol, Num_adatom, Shape, len_mol, angle_mol, dis_len)
    return ad_side_ini, ad_side_fin

def mk_op_side(ad_selected, num_ad):
    if num_ad == 0: return [], []
    if Shape != "L": return [], []
    elif Shape == "L":
        ad_side_ini, ad_side_fin = mkca.mk_sideon_mol_op(ad_selected, mat_lat_new, veca_uni, vecb_uni, Num_adatom, Shape, len_mol, dis_len)
        return ad_side_ini, ad_side_fin

if len(op_ad_car) > 0: op_ad_end = mk_ad_end(op_selected, len(op_ad_car))
if len(br_ad_car) > 0: br_ad_end = mk_ad_end(br_selected, len(br_ad_car))
if len(hl_ad_car) > 0: hl_ad_end = mk_ad_end(hl_selected, len(hl_ad_car))

if Mode in ["d", "p"]:
    br_ad_side_ini, br_ad_side_fin = mk_br_side(br_selected, br_index_selected, len(br_ad_car))
    op_ad_side_ini, op_ad_side_fin = mk_op_side(op_selected, len(op_ad_car))

# Make POSCARs with adsorbed molecule and and thier directories
print("\nMaking POSCARs in each directory ...")
def mk_poss_in_addir(coor_ads, name_dir, ch, num_ad):
    if num_ad == 0: return
    poslines, Ltot = mkp.mk_pos_adline(coor_ads, coor_dir_new, list_SD, Mode, labelofel, list_Ename, numofel, list_Num_adatom, eachlabels_el, mat_lat_new, flagS)
    mkp.mk_poss_and_dir(path_dir, name_dir, ch, poslines, Ltot)

if len(op_ad_car) > 0: mk_poss_in_addir(op_ad_end, "ontop_endon", "oe", len(op_ad_car))
if len(br_ad_car) > 0: mk_poss_in_addir(br_ad_end, "bridge_endon", "br", len(br_ad_car))
if len(hl_ad_car) > 0: mk_poss_in_addir(hl_ad_end, "hollow_endon", "hl", len(hl_ad_car))
if Mode == "d" or Mode == "p":
    mk_poss_in_addir(br_ad_side_ini, "bridge_sideon_ini", "bsi", len(br_ad_car))
    if dismode in ["y", "Y"]: mk_poss_in_addir(br_ad_side_ini, "bridge_sideon_ini", "bsi", len(br_ad_car))

    if Shape == "L":
        mk_poss_in_addir(op_ad_side_ini, "ontop_sideon_ini", "osi", len(op_ad_car))
        if dismode in ["y", "Y"]: mk_poss_in_addir(op_ad_side_fin, "ontop_sideon_fin", "osf", len(op_ad_car))

print("\n### END PROGRAM ###\n")
# END
