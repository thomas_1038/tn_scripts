#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# make .sh file

import os
import shutil
import sys
from stat import *

path = os.getcwd()
jobname = input("Please input the .sh file of siesta: ")

L = "#!/bin/sh" + "\n"
L += "#PBS -l nodes=1:ppn=16" + "\n"
L += "cd $PBS_O_WORKDIR"+"\n"
L += "NPROCS=`wc -l <$PBS_NODEFILE`" + "\n"
L += "export MKL_NUM_THREADS=1" + "\n"
L += "\n"
L += "/usr/local/mpich2-1.0.8_intel-13.0.1.117/bin/mpiexec -np 16 /home/nakao/siesta-3.1/Obj/siesta < input.fdf > std.out\n"

#shlines = [a1,a2,a3,a4,a5,a6,a7]
path_job = path + "/" + str(jobname)+ ".sh"
f = open(path_job,"w")
f.write(L)
f.close()
os.chmod(path_job, S_IXUSR | S_IRUSR | S_IWUSR )

print("\n" + str(jobname)+ ".sh has been made!")
