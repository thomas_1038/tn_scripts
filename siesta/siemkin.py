#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# make input file of siesta
# 2015/06/23 ver0.0
# 2017/03/08 ver0.1

import shutil
import sys
import os

path = os.getcwd()
inputname = path + "/input.fdf"
shutil.copy("/home/nakao/siesta-3.1/Examples/example.fdf", inputname)

print("\ninput.fdf has been made!")


