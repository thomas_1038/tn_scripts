#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/03/20 ver1.51
#2015/04/08 ver1.511

import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib.cm as cm

argv = sys.argv
argc = len(argv)
num1 = 1

while num1 < argc:
    if num1 == 1:
        dat = str(argv[num1])
        data = np.genfromtxt(dat,dtype=None)
        x = data[:,0]
        ycp = data[:,3]
        yicp = data[:,4]
        p, = plt.plot(x,ycp,'k',label = dat)
        icplabel = "ICOHP_" + str(dat)
        ax = plt.twinx()
        ax.plot(x,yicp,'k',label = icplabel)
        num1 += 1

    elif num1 > 1:
        dat = str(argv[num1])
        data = np.genfromtxt(dat,dtype=None)
        x = data[:,0]
        ycp = data[:,3]
        yicp = data[:,4]
        p, = plt.plot(x,ycp,color=cm.spectral(float(num1)/argc),label = dat)
        icplabel = "ICOHP_" + str(dat)
        ax = plt.twinx()
        ax.plot(x,yicp,color=cm.spectral(float(num1)/argc),label = icplabel)
        num1 += 1

    else:
        sys.exit()

plt.title('Electronic Density of States',fontsize=20)
plt.xlabel('DOS [state/eV]',fontsize=18)
plt.ylabel('Energy [eV]',fontsize=18)
plt.xticks(fontsize = 15)
#plt.xticks(np.arange(-8,8,1))
plt.yticks(fontsize = 15)
plt.minorticks_on
plt.grid(True)
plt.legend(loc="upper left",prop={'size':10})
plt.show()
