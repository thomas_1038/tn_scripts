#!/bin/sh
#PBS -l nodes=1:ppn=16
cd $PBS_O_WORKDIR
NPROCS=`wc -l <$PBS_NODEFILE`
export MKL_NUM_THREADS=1

/usr/local/mpich2-1.0.8_intel-13.0.1.117/bin/mpiexec -np 16 /home/nakao/siesta-3.1/Obj/siesta < input.fdf > std.out
