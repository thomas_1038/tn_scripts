#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 2015/08/26 ver1.00

import os
import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib.cm as cm

argv = sys.argv
path = os.getcwd()

filename = input("Please input the file name: ")
path_file = path + "/"+ filename

#read .dat file
#dat = str(argv[1])
f = open(path_file)
lines = f.readlines()
f.close()

#remove \n
numlist1 = []
for line in lines:
     xy = line.replace('\n',' ')
     numlist1.append(xy)

while numlist1.count(" ") > 0:
     numlist1.remove(" ")

#remove "    "
numlist2 = []
for line in numlist1:
     splitline = line.split(" ")
     numlist2.append(splitline)

#remove ""
lastnum = len(numlist2)
i1 = 0
numlist3 =[]
while i1 < lastnum :
     list = numlist2[i1]
     while list.count("") > 0:
          list.remove("")
     numlist3.append(list)
     i1 += 1

#defite energylist & doslist
energylist = []
doslist = []
i2 = 0

#make energylist & doslist
while i2 < lastnum:
     energy = numlist3[i2][0]
     dos = float(numlist3[i2][1]) + float(numlist3[i2][2])
     energylist.append(energy)
     doslist.append(dos)
     i2 += 1

l = len(energylist)
S = 0
i3 = 0  
i4 = 1

while i4 < lastnum:
     x1 = float(energylist[i3])
     x2 = float(energylist[i4])
     y1 = float(doslist[i3])
     y2 = float(doslist[i4])
    
     if x1 < float(argv[2]): 
          S += 0
          print(x1)
          print(S)
          i3 += 1
          i4 += 1


     elif x1 > float(argv[2]) and x2 <= float(argv[3]):
          s = 0.5*(x2-x1)*(y1+y2)
          print(x1)
          print(x2)
          print(S)
          S += s
          i3 += 1
          i4 += 1

     else:
         S += 0
         i3 += 1
         i4 += 1

print(S)
