#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/04/09 ver1.00
#This script 
#usage: script num1 num2
#num1>num2

import os
import shutil
import numpy as np
import sys
import glob
import math

argv = sys.argv
argc = len(argv)
num1 = 1

while num1 < argc: 
#read dos file
    dat = str(argv[num1])
    data = np.genfromtxt(dat,dtype=None)

#split x and y axis
    x = data[:,0]
    yup = data[:,1]
    ydown = data[:,2]
    ydown = -1 * ydown

#rename dos file
    dname = dat + "_before"
    os.rename(dat,dname)

#write x and y value on output file 
    output = dat 
    num2 = 0
    f = open(output,'w')
    while num2 < len(x):
        line = str(x[num2]) + "           " + str(yup[num2]) + "            " + str(ydown[num2]) + "\n"
        f.write(line)
        num2 += 1
    f.close()   

    num1 += 1

#judge whether backup directory exists
num3 = 1
while True:
    direcname1 = "backup" + str(num3) 
    flag2 = os.path.exists(direcname1)
    if flag2 == True:
        num3 +=1
    else:
        break
os.mkdir(direcname1)

#move axisfile to "axischange" directory 
bplist = glob.glob('*_before*')
for bpfile in bplist:     
    shutil.move(bpfile,direcname1)

letter1 = direcname1 + " directory has been made!"
print(letter1)
#End
