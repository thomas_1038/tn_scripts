#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/04/09 ver1.00
#This script 
#usage: script num1 num2
#num1>num2

import os
import shutil
import numpy as np
import sys
import glob
import math

print("\n# SIESTA_CONVERT_COOP_AND_COHP #")
Q_spin = input("Spin polarized or Not, [y/N]: ") 

path = os.getcwd()
argv = sys.argv
argc = len(argv)
num1 = 1

if Q_spin in ["y", "Y"]:
	# Loop: Start
	while num1 < argc: 	
		# read dos file
		dat = str(argv[num1])
		data = np.genfromtxt(dat,dtype=None)

		# split x and y axis
		x = data[:,0]
		list_cohpu = data[:,1]
		list_cohpd = data[:,2]
		list_cohpu_a = []
		list_cohpu_b = []
		list_cohpd_a = []
		list_cohpd_b = []
		list_cohpsum = []
		list_cohpsum_a = []
		list_cohpsum_b = []

		# make cohpsum(COHP up + COHP ddown)
		# cohpsum list : Start making 
		i = 0
		while i < len(list_cohpu):
			if float(list_cohpu[i]) > 0:
				cohpu_a = list_cohpu[i]
				list_cohpu_a.append(cohpu_a)
				cohpu_b = 0
				list_cohpu_b.append(cohpu_b)
			elif float(list_cohpu[i]) < 0:
				cohpu_a = 0
				list_cohpu_a.append(cohpu_a)
				cohpu_b = list_cohpu[i]
				list_cohpu_b.append(cohpu_b)
			elif float(list_cohpu[i]) == 0:
				cohpu_a = 0
				list_cohpu_a.append(cohpu_a)
				cohpu_b = 0
				list_cohpu_b.append(cohpu_b)

			if float(list_cohpd[i]) > 0:
				cohpd_a = list_cohpd[i]
				list_cohpd_a.append(cohpd_a)
				cohpd_b = 0
				list_cohpd_b.append(cohpd_b)
			elif float(list_cohpd[i]) < 0:
				cohpd_a = 0
				list_cohpd_a.append(cohpd_a)
				cohpd_b = list_cohpd[i]
				list_cohpd_b.append(cohpd_b)
			elif float(list_cohpd[i]) == 0:
				cohpd_a = 0
				list_cohpd_a.append(cohpd_a)
				cohpd_b = 0
				list_cohpd_b.append(cohpd_b)

			cohpsum = float(list_cohpu[i]) + float(list_cohpd[i])
			list_cohpsum.append(cohpsum)
        
			if cohpsum > 0:
				cohpsum_a = cohpsum
				list_cohpsum_a.append(cohpsum_a)
				cohpsum_b = 0
				list_cohpsum_b.append(cohpsum_b)
			elif cohpsum < 0:
				cohpsum_a = 0
				list_cohpsum_a.append(cohpsum_a)
				cohpsum_b = cohpsum
				list_cohpsum_b.append(cohpsum_b)
			elif cohpsum == 0:
				cohpsum_a = 0
				list_cohpsum_a.append(cohpsum_a)
				cohpsum_b = 0
				list_cohpsum_b.append(cohpsum_b)
				
			i += 1
		# cohpsum list : Finish making
 
		# make icohp(ICOHP)
		# icohp list : Start making 
		icohpu = 0
		icohpd = 0
		icohptot = 0

		list_icohpu = []
		list_icohpd = []
		list_icohptot = []

		l = 0
		diff_en = abs(float(x[0])-float(x[1]))
		while l < len(list_cohpu)-1:
			Su = (float(list_cohpu[l]) + float(list_cohpu[l+1]))*diff_en/2
			Sd = (float(list_cohpd[l]) + float(list_cohpd[l+1]))*diff_en/2
        
			icohpu += Su
			list_icohpu.append(icohpu)
			icohpd += Sd
			list_icohpd.append(icohpd)
			icohptot += Su + Sd
			list_icohptot.append(icohptot)

			l += 1
		# icohp list : Finish making
    
		list_icohpu.append("0")
		list_icohpd.append("0")
		list_icohptot.append("0")

		# rename dos file
		dname = dat + "_before"
		os.rename(dat,dname)

		# write x and y value on output file 
		# output(num1) file : Start making
		L = "Energy   COHP_up   COHP_down   COHP_sum  iCOHP_up   iCOHP_down   iCOHP_sum " + "\n"
		num2 = 0
		while num2 < len(x):
			L += str(x[num2]) + "   " + str(list_cohpu[num2]) + "   " + str(list_cohpd[num2]) + "   " + str(list_cohpsum[num2]) + "   " + str(list_icohpu[num2]) + "   " + str(list_icohpd[num2]) + "   " + str(list_icohptot[num2]) + "\n"
			num2 += 1

		output = path + "/" + dat + ".dat"
		f = open(output,'w')
		f.write(L)
		f.close()

		L_fg = "Energy   COHP_up_bonding    COHP_up_anti-bonding   COHP_down_bonding   COHP_down_anti-bonding   COHP_sum_bonding  COHP_sum_anti-bonding   iCOHP_up   iCOHP_down  iCOHP_sum " + "\n"
		num3 = 0
		while num3 < len(x):
			L_fg += str(x[num3]) + "   " + str(list_cohpu_b[num3]) + "   " + str(list_cohpu_a[num3]) + "   " + str(list_cohpd_b[num3]) + "   " + str(list_cohpd_a[num3]) + "   " + str(list_cohpsum_b[num3]) + "   " + str(list_cohpsum_a[num3])+ "   " + str(list_icohpu[num3]) + "   " + str(list_icohpd[num3]) + "   " + str(list_icohptot[num3]) + "\n"
			num3 += 1

		output_graph = path + "/" + dat + "_forgraph.dat"
		f = open(output_graph,'w')
		f.write(L_fg)
		f.close()
  
		num1 += 1
	# Loop End

elif Q_spin in ["n", "N"]:
	# Loop: Start
	while num1 < argc: 
		# read dos file
		dat = str(argv[num1])
		data = np.genfromtxt(dat,dtype=None)

		# split x and y axis
		x = data[:,0]
		list_cohpu = data[:,1]
		list_cohpu_a = []
		list_cohpu_b = []

		# cohp list : Start making 
		i = 0
		while i < len(list_cohpu):
			if float(list_cohpu[i]) > 0:
				cohpu_a = list_cohpu[i]
				list_cohpu_a.append(cohpu_a)
				cohpu_b = 0
				list_cohpu_b.append(cohpu_b)
			elif float(list_cohpu[i]) < 0:
				cohpu_a = 0
				list_cohpu_a.append(cohpu_a)
				cohpu_b = list_cohpu[i]
				list_cohpu_b.append(cohpu_b)
			elif float(list_cohpu[i]) == 0:
				cohpu_a = 0
				list_cohpu_a.append(cohpu_a)
				cohpu_b = 0
				list_cohpu_b.append(cohpu_b)
        				
			i += 1
		# cohp list : Finish making
 
		# make icohp(ICOHP)
	        # icohp list : Start making 
		icohpu = 0
		list_icohpu = []

		l = 0
		diff_en = abs(float(x[0])-float(x[1]))
		while l < len(list_cohpu)-1:
			Su = (float(list_cohpu[l]) + float(list_cohpu[l+1]))*diff_en/2
			icohpu += Su
			list_icohpu.append(icohpu)

			l += 1
		# icohp list : Finish making
		list_icohpu.append("0")

		# rename dos file 
		dname = dat + "_before"
		os.rename(dat,dname)

		# write x and y value on output file 
		# output(num1) file : Start making
		L = "Energy   COHP   iCOHP" + "\n"
		num2 = 0
		while num2 < len(x):
			L += str(x[num2]) + "   " + str(list_cohpu[num2]) + "   "  + str(list_icohpu[num2]) + "\n"
			num2 += 1

		output = path + "/" + dat + ".dat"
		f = open(output,'w')
		f.write(L)
		f.close()

		L_fg = "Energy   COHP_bonding    COHP_anti-bonding    iCOHP" + "\n"
		num3 = 0
		while num3 < len(x):
			L_fg += str(x[num3]) + "   " + str(list_cohpu_b[num3]) + "   " + str(list_cohpu_a[num3]) + "   " + str(list_icohpu[num3]) + "\n"
			num3 += 1

		output_graph = path + "/" + dat + "_forgraph.dat"
		f = open(output_graph,'w')
		f.write(L_fg)
		f.close()
  
		num1 += 1
	# Loop: End

# Judge whether axischange directory exists
num = 1
while True:
    direcname1 = "backup" + str(num) 
    flag2 = os.path.exists(direcname1)
    if flag2 == True:
        num +=1
    else:
        break
os.mkdir(direcname1)

#move bpfile to backup directory 
bplist = glob.glob('*_before*')
for bpfile in bplist:     
    shutil.move(bpfile,direcname1)

print(direcname1 + " directory has been made!")
# End
