#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2016/02/01 ver1.00
#This script 
#usage: script file1 file2 ~ fileN

#libraries
import os
import shutil
import numpy as np
import sys
import glob
import math
import subprocess
from subprocess import*

print("\n# SIESTA_CHANGE_ENERGY_BASED_ON_FERMI_ENERGY #")
Q_spin = input("Spin polarized or Not, [y/N]: ")

# Definition
path = os.getcwd()
argv = sys.argv
argc = len(argv)
num1 = 1

if len(argv) == 0:
    print("You should INPUT the name of files you want to change!!")
    sys.exit()

# get fermi level
files = os.listdir(path)
eigenfile = [x for x in files if ".EIG" in x]
eigenfilename = path + "/" + str(eigenfile[0])

## read .EIG file
f = open(eigenfilename)
eigenlines = f.readlines()
f.close()

## get fermi level                                                                         
fermi = eigenlines[0].replace('\n','')
fermi = fermi.split(" ")
while fermi.count("") > 0:
    fermi.remove("")

fermilevel = float(fermi[0])

# judge whether axischange directory exists                                                                                                              
num2 = 1
while True:
    direcname = "ChgEn" + str(num2)
    direcpath = path + "/" + direcname
    flag = os.path.exists(direcpath)
    if flag == True:
        num2 += 1
    else:
        break
os.mkdir(direcpath)

# shift energy axis
argc = len(argv)
num1 = 1
# Start:while
while num1 < argc:
    ## read dos file
    gap = 0 - fermilevel
    dat = str(argv[num1])
    data = np.genfromtxt(dat,dtype=None)

    ## split x and y axis
    ## chage x axis
    x = data[:,0]
    yup = data[:,1]
    if Q_spin in ["y", "Y"]:
        ydown = data[:,2]
    x += gap

    ## write x and y value on output file 
    L = ""
    num2 = 0
    while num2 < len(x):
        if Q_spin in ["y", "Y"]:
            L += str(x[num2]) + "         " + str(yup[num2]) + "          " + str(ydown[num2]) + "\n"
        elif Q_spin in ["n", "N"]:
            L += str(x[num2]) + "         " + str(yup[num2]) + "\n"
        num2 += 1

    output = direcpath + "/" + dat
    f = open(output,'w')
    f.write(L)
    f.close()
    num1 += 1
# End:while
print(str(direcname) + " directory has been made!")

files2 = os.listdir(direcpath)
cohpfiles = [x for x in files2 if ".cohp" in x]
coopfiles = [x for x in files2 if ".coop" in x]
dosfiles = [x for x in files2 if "dos" in x]
cohpcoopfiles = cohpfiles + coopfiles

os.chdir(direcpath)
# change COHP & COOP files
print("\n## CHANGE: COOP & COHP ##")
if len(cohpcoopfiles) == 0:
    print("There is no cohp & coop files.")
else:
    Lcohpcoop = ""
    for L in cohpcoopfiles:
        Lcohpcoop += L + " "

    print(Lcohpcoop)
    subprocess.call("sieconvcoop.py " + Lcohpcoop, shell=True)
    print("cohp & coop files have been converted!")

# change PDOS files
print("\n## CHANGE: PDOS ##")
if len(dosfiles) == 0:
    print("There is no dos & pdos files.")
else:
    Ldos = ""
    for L in dosfiles:
        Ldos += L + " "
        
    print(Ldos)
    subprocess.call("sieconvdos.py " +  Ldos, shell=True)
    print("dos & pdos files have been converted!")

# End

