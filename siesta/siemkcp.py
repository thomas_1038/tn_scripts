#!/usr/bin/env python3                                                               
# -*- coding: utf-8 -*-  

#2015/08/12 version 1.0
#This script make backup directory.
#usage:script

import os
import shutil
import sys
import glob

path = os.getcwd()
path_coop = path + "/COOP"
path_pdos = path + "/PDOS"

file = input("Please input the file name of HSX and WFSX: ")

#
cflag = os.path.exists(path_coop)
if cflag == True:
    print("COOP directory already exists.")
else:
    os.mkdir(path_coop)
    print("New COOP directory has been made!")

wflag = os.path.exists(path_pdos)
if cflag == True:
    print("PDOS directory already exists.")
else:
    os.mkdir(path_pdos)
    print("New PDOS directory has been made!")

#copy HSX and WFSX file to COOP and PDOS.
hsxfile = str(file) + ".HSX"
wfsxfile = str(file) + ".WFSX"
eigfile = str(file) + ".EIG"

path_hsx = path + "/" + hsxfile
path_wfsx = path + "/" + wfsxfile
path_eig = path + "/" + eigfile 

shutil.copy(path_hsx,path_coop)
shutil.copy(path_hsx,path_pdos)
shutil.copy(path_wfsx,path_coop)
shutil.copy(path_wfsx,path_pdos)
shutil.copy(path_eig,path_pdos)
shutil.copy(path_eig,path_coop)

#make mpr file
c1 = str(file) +"\n"
c2 = "COOP" + "\n"
c3 = "!Curve Label| ex) N2s-N2px" + "\n"
c4 = "!orb1 spec| ex) N(atom name) or N_2s(atom orbital) or 2(atom Num)" + "\n"
c5 = "!distance range| ex) 0 2(Ang)" + "\n"
c6 = "!orb2 spec| ex) N(atom name) or N_2s(atom orbital) or 2(atom Num)"
cooplines = [c1,c2,c3,c4,c5,c6]

cpmprname = path_coop + "/coop.mpr"
cp = open(cpmprname,"w")
for line in cooplines:
        cp.write(str(line))

p1 = str(file) + "\n"
p2 = "DOS" + "\n"
p3 = "!Curve Label" + "\n"
p4 = "!orb1 spec| ex) N(atom name) or N_2s(atom orbital) or 2(atom Num)" + "\n"
pdoslines = [p1,p2,p3,p4]

pdsmprname = path_pdos + "/pdos.mpr"
pd = open(pdsmprname,"w")
for line in pdoslines:
        pd.write(str(line))
