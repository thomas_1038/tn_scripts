#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess
import numpy as np

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
filename = input("Please input the name of .mpr file, e.g., coop: ")
path_mprfile = path + "/" + filename + ".mpr"

f = open(path_mprfile)
mprlines = f.readlines()
f.close()
list_mpr = [conv_line(x) for x in mprlines]
tag_mpr = str(list_mpr[2][0])

SMEAR = input("PLease set value of smearing parameter (default 0.5 eV): ")
NPTS = input("Please set number of sampling points (default 200): ")
Min_e = input("Please set lower bound of energy range, e.g., -20: ")
Max_e = input("Please set upper bound of energy range, e.g., 10: ")
print("\n")

mprop = "~/siesta-3.1/Util/COOP/mprop" + " -s " + SMEAR + " -n " + NPTS + " -m " + Min_e + " -M " + Max_e + " " + filename
subprocess.call(mprop,shell=True)
siechgen = "siechgen.py " + filename + "." + tag_mpr + ".cohp " +  filename + "." + tag_mpr + ".coop" 
subprocess.call(siechgen,shell=True)

path_dir_old = path + "/ChgEn1"
path_dir_new = path + "/ChgEn_" + tag_mpr + "_s" + SMEAR + "n" + NPTS + "from" + Min_e + "to" + Max_e
os.rename(path_dir_old, path_dir_new)

print("Done!")
