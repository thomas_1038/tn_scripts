#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#make input file of siesta
#2015/06/23 ver1.0

import shutil
import sys

argv = sys.argv
argc = len(argv)
num1 = 1

while num1 < argc:
    potplace = "/home/nakao/siesta-3.1/PP/GGA/" + argv[num1] +".psf"
    shutil.copy(potplace,"./")
    num1 += 1

