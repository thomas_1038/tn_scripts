#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/03/20 ver1.51
#2015/04/08 ver1.511

import numpy as np
import matplotlib.pyplot as plt
import sys
import matplotlib.cm as cm

argv = sys.argv
argc = len(argv) 
num1 = 1

while num1 < argc:
#    if num1 == 1:
#        dat = str(argv[num1])
#        data = np.genfromtxt(dat,dtype=None)
#        x = data[:,0]
#        yup = data[:,1]
#        ydown = data[:,2]
#        p, = plt.plot(x,yup,'k',label = dat)
#        p, = plt.plot(x,ydown,'k')
#        num1 += 1

    if num1 >= 1:
        dat = str(argv[num1])
        data = np.genfromtxt(dat,dtype=None)
        x = data[:,0]
        yup = data[:,1]
        ydown = data[:,2]
        p, = plt.plot(x,yup,color=cm.spectral(float(num1)/argc),label = dat)
        p, = plt.plot(x,ydown,color=cm.spectral(float(num1)/argc))
        num1 += 1

    else:
        sys.exit()

plt.title('Electronic Density of States',fontsize=20)
plt.xlabel('DOS [state/eV]',fontsize=18)
plt.ylabel('Energy [eV]',fontsize=18)
plt.xticks(fontsize = 15)
#plt.xticks(np.arange(-8,8,1))
plt.yticks(fontsize = 15)
plt.minorticks_on
plt.grid(True)
plt.legend(loc="upper left",prop={'size':10})
plt.show()
