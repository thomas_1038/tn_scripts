#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

myname = os.path.basename(__file__)

"""
def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line
"""

path = os.getcwd()
for curdir, dirs, files in os.walk(path):
    for file in files: 
        if myname in file: continue
        elif ".py" in file and ".pyc" not in file:
            sub.call("2to3 -w -n " + curdir + "/" + file, shell=True)

print("\nDone!\n")
