#! /usr/bin/env python3                                                                           
# -*- coding: utf-8 -*-
#This script 

import sys
import os

argv = sys.argv
PATH = os.getcwd()
v2mpath = PATH + "/v2m.xyz.rev"

vflag = os.path.exists(v2mpath)
if vflag == False:
    print("You should use MOLDEN and MOL.exe, before using this script")
    sys.exit()

#read v2m.xyz.rev
f = open("v2m.xyz.rev")
v2mlines = f.readlines()
f.close()

#read POSCAR
f = open("POSCAR")
poslines = f.readlines()
f.close()

#count Atom number
atomnum = poslines[6].replace('\n','')
atomnum = atomnum.split(" ")
while atomnum.count("") > 0:
    atomnum.remove("")

lastatom = 0
for num in atomnum:
    lastatom += int(num)

#make v2m atom position matrix
num0 = 2
lastnum = int(num0) + lastatom
v2matomposition = []
while num0 < lastnum:
    line = v2mlines[num0]
    line = line.replace('\n','')
    line = line.replace('\r','')
    line = line.replace('\t',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    v2matomposition.append(line)
    num0 += 1

#If POSCAR include "Selective dynamics"
flag = poslines[7].find("Selective")
if flag >= 0:
    #make upper line of POSCAR_car                                              
    num0 = 0
    firstposcarlines = []
    while num0 < 8:
        line = poslines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)

    #make atom position matrix
    num1 = 9
    lastnum = int(num1) + lastatom
    posatomposition = []
    while num1 < lastnum:
        line = poslines[num1]
        line = line.replace('\n','')
        line = line.replace('\r','')
        line = line.replace('\t',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        posatomposition.append(line)
        num1 += 1

    #make CONTCAR_car lines
    num3 = 0
    newposposition = []
    while num3 < len(posatomposition):
        v2mx = v2matomposition[num3][1]
        v2my = v2matomposition[num3][2]
        v2mz = v2matomposition[num3][3]
        torfx = str(posatomposition[num3][3])
        torfy = str(posatomposition[num3][4])
        torfz = str(posatomposition[num3][5])

        line = str(v2mx) + "       " + str(v2my) + "       " + str(v2mz) + "       " + torfx + "       " + torfy + "       " + torfz 
        newposposition.append(line)
        num3 += 1

    #
    newposlines = firstposcarlines + newposposition

    #write POSCAR_new
    poscar = open("POSCAR_new",'w')
    for line in newposlines:
        L = line + "\n"
        poscar.write(L)
    poscar.close()
    
    print("POSCAR_new has been made!")

else:
    #make upper line of POSCAR_car                                                                                       
    num0 = 0
    firstposcarlines = []
    while num0 < 6:
        line = poslines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)

    #make atom position matrix                                                                                           
   # num1 = 8
   # lastnum = int(num1) + lastatom
   # posatomposition = []
   # while num1 < lastnum:
   #     line = poslines[num1]
   #     line = line.replace('\n','')
   #     line = line.replace('\r','')
   #     line = line.replace('\t',' ')
   #     line = line.split(" ")
   #     while line.count("") > 0:
   #         line.remove("")
   #     posatomposition.append(line)
   #     num1 += 1

    #make CONTCAR_car lines                                                                                              
    num3 = 0
    newposposition = []
    while num3 < len(posatomposition):
        v2mx = v2matomposition[num3][1]
        v2my = v2matomposition[num3][2]
        v2mz = v2matomposition[num3][3]

        line = str(v2mx) + "       " + str(v2my) + "       " + str(v2mz)
        newposposition.append(line)
        num3 += 1

    #                                                                                                                    
    newposlines = firstposcarlines + newposposition

    #write POSCAR_new                                                                                                    
    poscar = open("POSCAR_new",'w')
    for line in newposlines:
        L = line + "\n"
        poscar.write(L)
    poscar.close()

    print("POSCAR_new has been made!")

