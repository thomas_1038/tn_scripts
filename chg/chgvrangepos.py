#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, math #, shutil, threading, glob, re
#import subprocess as sub
import numpy as np
import addmol.readpos as rp
import addmol.mk_poscar as mp

def cat_help():
    Lh =  "\n-h: show HELP\n\n"
    Lh += "-r: You can input the vacuum range after -r.\n"
    Lh += "-n: You can input the filename after -n.\n\n"
    print(Lh); sys.exit()

argv = sys.argv
path = os.getcwd()

if len(argv) > 1 and argv[1] == "-h": cat_help()
if len(argv) > 1:
    if "-r" in argv: range_vac_df = float(argv[argv.index("-r") + 1])
    if "-n" in argv: filename = argv[argv.index("-n") + 1]

elif len(argv) == 1:
    print("\nDefault vaccum range: 20.5")
    Qv = input("\nPlease input the vacuum range, default=20.5, [d or sepecific value, e.g., 15]: ")
    if Qv == "d": range_vac_df = 20.5
    else: range_vac_df = float(Qv)

    filename = input("Please input the file name: ")

# read POSCAR
with open(path + "/" + filename) as f: contlines = f.readlines()

# POSCARから原子種、原子数、合計原子数を抽出
labelofel = rp.get_labelofel(contlines)
numofel = rp.get_numofel(contlines)
sumatomnum = rp.get_sumofnumofel(contlines)

# INCARの一番右側につける原子タグ作成
list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel, numofel)

# POSCARから始めの数行、格子ベクトル、原子座標を抽出
firstlines, flagS = rp.get_firstlines(contlines) #POSCARの始め数行(Direct/Cartesianまで)
mat_lat = rp.get_matrix(contlines) #格子ベクトル
mat_lat_old = mat_lat
coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
arr_coor_car = np.array(coor_car)
arr_coor_dir = np.array(coor_dir)
list_num_SD = rp.mk_listnumSD(list_SD)
lat_c = math.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)

coor_dir_zaxis = [float(coor[2]) for coor in coor_dir]
range_slab = lat_c*(max(coor_dir_zaxis) - min(coor_dir_zaxis))
range_vac = lat_c - range_slab
#range_vac = float(mat_lat[2][2]) - max(coor_dir_zaxis) + min(coor_dir_zaxis)
diff_range = range_vac_df - range_vac
k = (lat_c + diff_range)/lat_c
mat_lat_c_new = [float(mat_lat[2][0])*k, float(mat_lat[2][1])*k, float(mat_lat[2][2])*k]
mat_lat_new = [mat_lat[0], mat_lat[1], mat_lat_c_new]
mp.mk_pos(path, filename.replace(".vasp","") + "_vac" + str(range_vac_df) + ".vasp", firstlines, mat_lat_new, labelofel, numofel, list_labelofel, flagS, coor_car, list_SD)

a = [[str(y) for y in x] for x in mat_lat_old]
b = [[str(y) for y in x] for x in mat_lat_new]

L = "\nVac: " + str(range_vac) + "\n"
L += "\nOld lattice vector\n"
L += "  ".join(a[0]) + "\n" + "  ".join(a[1]) + "\n" + "  ".join(a[2])
L += "\n\nNew lattice vector\n"
L += "  ".join(b[0]) + "\n" + "  ".join(b[1]) + "\n" + "  ".join(b[2]) + "\n"

if __name__ == "__main__": print(L)
# END: Program