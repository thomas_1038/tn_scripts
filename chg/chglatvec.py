#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
import numpy as np
import addmol.readpos as rp

def chg_latvec(nowpath):
    path_pos = nowpath + "/POSCAR"
    with open(path_pos) as fp: poslines = fp.readlines()
    M = np.array(rp.get_matrix(poslines))

    Q, R = np.linalg.qr(M)
    #l, P = np.linalg.eig(M)
    #Pi = np.linalg.inv(P)
    #print(P, Pi)

    print(Q)
    #print(M_new)

if __name__ == "__main__":
    chg_latvec(os.getcwd())
    print("Done!")
# END