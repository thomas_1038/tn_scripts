#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess
import numpy as np
import re

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
files = [x for x in os.listdir(path)]
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) == True]

p1 = r"[0-9]_[0-9]_[0-9]"
p2 = r"-[0-9]_[0-9]_[0-9]"
p3 = r"[0-9]_-[0-9]_[0-9]"
p4 = r"[0-9]_[0-9]_-[0-9]"
p5 = r"-[0-9]_-[0-9]_[0-9]"
p6 = r"[0-9]_-[0-9]_-[0-9]"
p7 = r"-[0-9]_[0-9]_-[0-9]"
p8 = r"-[0-9]_-[0-9]_-[0-9]"

dirs_surf = []
for dir in dirs:
    if re.match(p1,dir) or re.match(p2,dir) or re.match(p3,dir) or re.match(p4,dir) or re.match(p5,dir) or re.match(p6,dir) or re.match(p7,dir) or re.match(p8,dir):
        dirs_surf.append(dir)
dirs_surf.sort()

for dir in dirs_surf:
    path_dir = path + "/" + dir
    poscars_surf =  [x for x in os.listdir(path_dir) if dir.find("POSCAR")]
    for pos in poscars_surf:
        os.rename(path_dir + "/" + pos, path_dir + "/" + pos + ".vasp")

print("Done!")
