#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def change_old_dir_names(nowpath, keyword):
    #def conv_line(line):
    #    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    #    line = line.split(" ")
    #    while line.count("") > 0: line.remove("")
    #    return line

    vdirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" + x) and keyword in x])
    print(" ".join(vdirs))

    for vdr in vdirs:
        i = 0
        path_dir = nowpath + "/" + vdr
        dir_precM = sorted([x for x in os.listdir(path_dir) if os.path.isdir(path_dir + "/" + x) and "PrecM" in x and "_PrenM" not in x])
        dir_precN = sorted([x for x in os.listdir(path_dir) if os.path.isdir(path_dir + "/" + x) and "PrecN" in x and "_PrecN" not in x])
        dirs_precA = sorted([x for x in os.listdir(path_dir) if os.path.isdir(path_dir + "/" + x) and "PrecA" in x and "_PrecA" not in x])

        if len(dir_precM) > 0:
            os.rename(path_dir + "/" + dir_precM[0], path_dir + "/" + str(i) + "_" + dir_precM[0])
            i += 1

        if len(dir_precN) > 0:
            os.rename(path_dir + "/" + dir_precN[0], path_dir + "/" + str(i) + "_" + dir_precN[0])
            i += 1

        for adr in dirs_precA:
            os.rename(path_dir + "/" + adr, path_dir + "/" + str(i) + "_" + adr)
            i += 1

if __name__ == "__main__":
    keyword = input("Please input the directory name: ")
    change_old_dir_names(os.getcwd(), keyword)
    print("Done!")
