#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/12/10 version 1.00
#usage: script

import os
import os.path
import shutil
import sys
import math

filename = input("Please input the name of PARCHG you want to change: ")

path = os.getcwd()
path_PARCHG = path + "/" + str(filename)

#read CONTCAR_car_l
f = open(path_PARCHG)
lines_PARCHG = f.readlines()
f.close()

#count Atom number
line_natom = lines_PARCHG[6].replace('\n','')
line_natom = line_natom.split(" ")
while line_natom.count("") > 0: line_natom.remove("")

natom = 0
for num in line_natom: natom += int(num)

#make fix word matrix
num1 = 10 + natom
dataline_ChargeDensity = []
#print lines_PARCHG[num1]

while num1 < len(lines_PARCHG):
	line = lines_PARCHG[num1]
	line = line.replace('\n','')
	line = line.replace('\r','')
	line = line.replace('\t',' ')
	line = line.split(" ")
	while line.count("") > 0: line.remove("")
	dataline_ChargeDensity.extend(line)
	num1 += 1

#print len(dataline_ChargeDensity)

num = 0
lines_ChargeDensity_forFile = []

amari = len(dataline_ChargeDensity)%5
fnum = len(dataline_ChargeDensity) - amari

while num < len(dataline_ChargeDensity):
	if num < fnum:
		num1 = num +1
		num2 = num +2
		num3 = num +3
		num4 = num +4
		line = " " + str(dataline_ChargeDensity[num]) + "  " + str(dataline_ChargeDensity[num1]) + "  " + str(dataline_ChargeDensity[num2]) + "  " + str(dataline_ChargeDensity[num3]) + "  " + str(dataline_ChargeDensity[num4]) 
		lines_ChargeDensity_forFile.append(line)
		num += 5
	
	else:
		num1 = fnum +1
		num2 = fnum +2
		num3 = fnum +3
		num4 = fnum +4
		
		if amari == 1: line = " " + str(dataline_ChargeDensity[fnum]) 
		elif amari == 2: line = " " + str(dataline_ChargeDensity[fnum]) + "  " + str(dataline_ChargeDensity[num1]) 
		elif amari == 3: line = " " + str(dataline_ChargeDensity[fnum]) + "  " + str(dataline_ChargeDensity[num1]) + "  " + str(dataline_ChargeDensity[num2]) 
		elif amari == 4: line = " " + str(dataline_ChargeDensity[fnum]) + "  " + str(dataline_ChargeDensity[num1]) + "  " + str(dataline_ChargeDensity[num2]) + "  " + str(dataline_ChargeDensity[num3]) 
			
		lines_ChargeDensity_forFile.append(line)
		break


#L = ""
#for data in data_CD:
#	L += str(data) + " "

#print data_CD
newfile = filename + "_cov"
f = open(newfile,'w')

num1 = 0
fnum1 = 10 + natom
while num1 < fnum1:
	line = lines_PARCHG[num1]
	f.write(line)
	num1 += 1

num2 = 0
while num2 < len(lines_ChargeDensity_forFile):
	line = str(lines_ChargeDensity_forFile[num2]) + "\n"
	f.write(line)
	num2 += 1

f.close()

L = newfile + " has been maked!"
print(L)
