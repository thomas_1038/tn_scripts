#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#This script
#2017/10/23

import os, sys, math, shutil
import numpy as np
import addmol.helper as hl
import addmol.readpos as rp
import addmol.get_surfatoms as gsa
import addmol.mk_poscar as mkp
from addmol import Frac2Cart
f2c = Frac2Cart()

def sort_pos(nowpath, filename, num_fixatom, range_surf=1):
    path_file = nowpath + "/" + filename
    with open(path_file) as f: pclines = f.readlines()

    labelofel = rp.get_labelofel(pclines)
    numofel = rp.get_numofel(pclines)
    sum_atomnum  = rp.get_sumofnumofel(pclines)
    eachlabels_el, eachlabels_el_nn, dict_labelofel = rp.mk_labelofel(labelofel, numofel)
    firstlines, flagS = rp.get_firstlines(pclines)
    mat_lat = rp.get_matrix(pclines)
    coor_dir = rp.get_coordinate(pclines, sum_atomnum, mat_lat)[1]
    coor_eachel_dir, label_eachel = rp.get_coordinate_eachel(coor_dir, eachlabels_el_nn)
    #for l in coor_eachel_dir:
    #    print(len(l))

    list_SD = []
    for i1, num_e in enumerate(numofel):
        for i2 in range(0, int(num_e)):
            if i2 < int(num_fixatom[i1]): list_SD.append(["F","F","F"])
            else: list_SD.append(["T","T","T"])

    coor_ee_dir_srt = []
    for coor_ee in coor_eachel_dir:
        coor_ee_lbl = reversed(gsa.make_coor_layerbylayer(coor_ee, mat_lat, 2, range_surf)[0])
        
        #for a in coor_ee_lbl:
        #    print(len(a))

        for coor_ee_eachlayer in coor_ee_lbl:
            coor_ee_eachlayer_y = [coor[1] for coor in coor_ee_eachlayer]
            coor_ee_eachlayer_y_argsrt = np.array(coor_ee_eachlayer_y).argsort()
            coor_ee_eachlayer_y_srt = sorted(coor_ee_eachlayer_y)

            coor_ee_eachlayer_srt = []
            for i1, ind1 in enumerate(coor_ee_eachlayer_y_argsrt):
                coor_ee_eachlayer_srt.append([coor_ee_eachlayer[ind1][0], coor_ee_eachlayer_y_srt[i1], coor_ee_eachlayer[ind1][2]])
            #print(len(coor_ee_eachlayer_srt))
            coor_ee_dir_srt += coor_ee_eachlayer_srt

            """
            coor_ee_eachlayer_lbl = gsa.make_coor_layerbylayer(coor_ee_eachlayer_srt, mat_lat, 0, 1)[0]
            print(len(coor_ee_eachlayer_lbl)); sys.exit()
            for coor_ee_eachline in coor_ee_eachlayer_lbl:
                coor_ee_eachline_x = [coor[0] for coor in coor_ee_eachline]
                coor_ee_eachline_x_argsrt = np.array(coor_ee_eachline_x).argsort()
                coor_ee_eachline_x_srt = sorted(coor_ee_eachline_x)

                coor_ee_eachline_srt = []
                for i2, ind2 in enumerate(coor_ee_eachline_x_argsrt):
                    coor_ee_eachline_srt.append([coor_ee_eachline_x_srt[i2], coor_ee_eachline[ind2][1],  coor_ee_eachline[ind2][2]])
            """

    #print(len(coor_ee_dir_srt)); print(len(eachlabels_el))
    coor_ee_car_srt = f2c.frac2cart(mat_lat, coor_ee_dir_srt)
    mkp.mk_pos(nowpath, filename.replace(".vasp","") + "_srt.vasp", firstlines, mat_lat, labelofel, numofel, eachlabels_el, flagS, coor_ee_car_srt, list_SD)

if __name__ == "__main__":
    filename = input("Please input the file name, i.e., [ POSCAR,p / CONTCAR,c / SPOSCAR, s] or FILENAME (e.g., POSCAR1): ")
    if filename in ["POSCAR", "p", "P"]: filename = "POSCAR"
    elif filename in ["CONTCAR", "c", "C"]: filename = "CONTCAR"
    elif filename in ["SPOSCAR", "s", "S"]: filename = "SPOSCAR"

    with open(os.getcwd() + "/" + filename) as fpc: pclines = fpc.readlines()
    labelofel = rp.get_labelofel(pclines)
    numofel = rp.get_numofel(pclines)

    print(" ".join(labelofel) + "\n" + " ".join(numofel) + "\n")
    num_fixatom = input("Please input the number of fix atoms of " + " ".join(labelofel) + ", e.g., 12 12 12: ")
    num_fixatom = [int(x) for x in num_fixatom.split(" ")]

    # Write CONTCAR_car
    #filename = str(file.replace(".vasp", "")) + "_srt.vasp"
    sort_pos(os.getcwd(), filename, num_fixatom)

    print(str(filename) + " has been made!")
# END: script