#! /usr/bin/env python3                                                                           
# -*- coding: utf-8 -*-
#This script 

import sys

Lfile = input("Please input the file name: ")

#read CONTCAR
f = open(Lfile)
contlines = f.readlines()
f.close()

#for line in contlines:
#    flag = line.find("Cartesian")
#    if flag >= 0:
#        print "You need not use this script!"
#        sys.exit()

#read Atom label                                                              
atomlabel = contlines[5].replace('\n','')
atomlabel = atomlabel.replace('^M','')
atomlabel = atomlabel.split(" ")
while atomlabel.count("") > 0:
    atomlabel.remove("")

#count Atom number
atomnum = contlines[6].replace('\n','')
atomnum = atomnum.replace('^M','')
atomnum = atomnum.split(" ")
while atomnum.count("") > 0:
    atomnum.remove("")

lastatom = 0
for num in atomnum:
    lastatom += int(num)

num1 = 0
atomlabellist = []
while num1 < len(atomlabel):
    num2 = 0
    while num2 < int(atomnum[num1]):
        num3 = num2 + 1
        L = "!" + atomlabel[num1]  #str(num3)
        atomlabellist.append(L)
        num2 += 1
    num1 += 1

#print atomlabellist

#If 
flag = contlines[7].find("Selective")
if flag >= 0:
    #make upper line of POSCAR_car
    num0 = 0
    firstposcarlines = []
    while num0 < 8:
        line = contlines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)
    
    #make lattice vecor matrix
    num1 = 2
    latticevector =[]
    while num1 < 5: 
        line = contlines[num1].replace('\n','')
        line = line.replace('^M','')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        latticevector.append(line)
        num1 += 1
    
    #make atom position matrix
    #num2 = 9
    #lastnum = int(num2) + lastatom
    #diratomposition = []
    #while num2 < lastnum:
        #line = contlines[num2]
        #if line == "\n":
        #    break
        #line = line.replace('\n','')
        #line = line.replace('^M','')
        #line = line.split(" ")
        #while line.count("") > 0:
            #line.remove("")
        #diratomposition.append(line)
        #num2 += 1
    
    #difinition lattice vector
    #latax = float(latticevector[0][0])
    #latay = float(latticevector[0][1])
    #lataz = float(latticevector[0][2])
    #latbx = float(latticevector[1][0])
    #latby = float(latticevector[1][1])
    #latbz = float(latticevector[1][2])
    #latcx = float(latticevector[2][0])
    #latcy = float(latticevector[2][1])
    #latcz = float(latticevector[2][2])
    
    #make cartesian atom position matrix
    num3 = 0
    caratomposition = []

    while num3 < len(diratomposition):
        line = []
        posa = float(diratomposition[num3][0])
        posb = float(diratomposition[num3][1])
        posc = float(diratomposition[num3][2])
        dirafix = str(diratomposition[num3][3])
        dirbfix = str(diratomposition[num3][4])
        dircfix = str(diratomposition[num3][5])
        atomlabel = str(atomlabellist[num3])

        line.append(posa)
        line.append(posb)
        line.append(posc)
        line.append(dirafix)
        line.append(dirbfix)
        line.append(dircfix)
        line.append(atomlabel)

        caratomposition.append(line)
        num3 += 1

    numatom = 1
    for i in range(len(caratomposition)):
        if str(caratomposition[numatom][6]) == str(caratomposition[numatom-1][6]): 
            for j in range(len(caratomposition)-1, i, -1):
                if caratomposition[j][2] < caratomposition[j-1][2]:
                    caratomposition[j][2], caratomposition[j-1][2] = caratomposition[j-1][2], caratomposition[j][2]
            numatom += 1
        elif numatom == len(caratomposition)+1:
            for j in range(len(caratomposition)-1, i, -1):
                if caratomposition[j][2] < caratomposition[j-1][2]:
                    caratomposition[j][2], caratomposition[j-1][2] = caratomposition[j-1][2], caratomposition[j][2]

    carpositionlines = []
    for line in caratomposition:
        L = str(caratomposition[line][0]) + " " + str(caratomposition[line][1]) + " " + str(caratomposition[line][2]) + " " + str(caratomposition[line][3]) + " " + str(caratomposition[line][4]) + " " + str(caratomposition[line][5]) + " " + str(caratomposition[line][6]) 
        carpositionlines.append(L)
                                                                                                                                                                                                           
    #make CONTCAR_car lines
    contcarlines = firstposcarlines + caratomposition
    
    #write CONTCAR_car
    filename = str(Lfile) + "_shift0.5A.vasp"
    contcar = open(filename,'w')
    for line in contcarlines:
        L = line + "\n"
        contcar.write(L)
    contcar.close()

    Lprint = str(filename) + " has been made!"
    print(Lprint)

#If                                                                                                                   
else:
    #make upper line of POSCAR_car                                                                                    
    num0 = 0
    firstposcarlines = []
    while num0 < 7:
        line = contlines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)

    #make lattice vecor matrix                                                                                        
    num1 = 2
    latticevector =[]
    while num1 < 5:
        line = contlines[num1].replace('\n','')
        line = line.replace('^M','')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        latticevector.append(line)
        num1 += 1

    #make atom position matrix                                                                                        
    num2 = 8
    lastnum = int(num2) + lastatom
    diratomposition = []
    while num2 < lastnum:
        line = contlines[num2]
        if line == "\n":
            break
        line = line.replace('\n','')
        line = line.replace('^M','')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        diratomposition.append(line)
        num2 += 1
    #difinition lattice vector                                                                                        
    latax = float(latticevector[0][0])
    latay = float(latticevector[0][1])
    lataz = float(latticevector[0][2])
    latbx = float(latticevector[1][0])
    latby = float(latticevector[1][1])
    latbz = float(latticevector[1][2])
    latcx = float(latticevector[2][0])
    latcy = float(latticevector[2][1])
    latcz = float(latticevector[2][2])

    #make cartesian atom position matrix                                                                              
    num3 = 0
    caratomposition = []
    while num3 < len(diratomposition):
       dira = float(diratomposition[num3][0])
       dirb = float(diratomposition[num3][1])
       dirc = float(diratomposition[num3][2])

       carx = float(latax*dira + latbx*dirb + latcx*dirc)
       cary = float(latay*dira + latby*dirb + latcy*dirc)
       carz = float(lataz*dira + latbz*dirb + latcz*dirc)
       atomlabel = str(atomlabellist[num3])

       line = str(carx) + "        " + str(cary) + "        " + str(carz) + "        " + atomlabel 
       caratomposition.append(line)
       num3 += 1

    #make CONTCAR_car lines                                                                                            
    contcarlines = firstposcarlines + caratomposition

    #write CONTCAR_car                                                        
    filename = str(Lfile) + "_CAR"
    contcar = open(filename,'w')
    for line in contcarlines:
        L = line + "\n"
        contcar.write(L)
    contcar.close()

    Lprint = str(filename) + " has been made!"
    print(Lprint)
