#! /usr/bin/env python3                                                                                
# -*- coding: utf-8 -*-  

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

myname = os.path.basename(__file__)
chgwrd_bf = "/usr/bin/env python"
chgwrd_af = "/usr/bin/env python3"

"""
def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line
"""

path = os.getcwd()
for curdir, dirs, files in os.walk(path):
    if "backup" in curdir: continue

    for file in files: 
        if myname in file: continue
        elif ".py" in file and ".pyc" not in file:
            with open(curdir + "/" + file) as fp: pylines = fp.readlines()
            ind = 0
            for i in range(0, len(pylines)):
                if chgwrd_bf in pylines[i]: 
                    pylines[i] =  pylines[i].replace(chgwrd_bf,chgwrd_af)
                    if ind == 0: print(str(file)); ind += 1

            with open(curdir + "/" + file,"w") as fp: fp.write("".join(pylines))

print("\nDone!\n")
