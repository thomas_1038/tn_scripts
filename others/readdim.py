#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import addmol.helper as hl

def read_dimcar(nowpath):
    step = "--"; force = "-----"; torque = "-----"; curvature = "-----"; angle = "-----"
    path_dim = nowpath + "/DIMCAR"
    with open(path_dim) as fd: dimlines = fd.readlines()

    if len(dimlines) > 1:
        dimline = hl.conv_line(dimlines[-1])
        step = dimline[0]
        force = "%.2f" % float(dimline[1])
        torque = "%.2f" % float(dimline[2])
        curvature = "%.2f" % float(dimline[4])
        angle = "%.2f" % float(dimline[5])

    return step, str(force), str(torque), str(curvature), str(angle)
# END