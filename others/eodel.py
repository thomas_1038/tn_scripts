#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/07/23 version 1.0
#2015/08/12 version 1.01
#This script deleate the error and output directories.
#usage: script

import os
import os.path
import shutil

path = os.getcwd()
patherr = path + "/error"
pathout = path + "/output"

#make error file
eflag = os.path.exists(patherr)

if eflag == False:
    os.mkdir(patherr)
    #print "New error directory has been made!"

files = os.listdir('./')
errorfiles1 = [x for x in files if ".sh.e" in x]
errorfiles2 = [x for x in files if "OTHERS.e" in x]
errorfiles3 = [x for x in files if "lobsterin.e" in x]

for errorfile1 in errorfiles1:
    shutil.move(errorfile1,patherr)
for errorfile2 in errorfiles2:
    shutil.move(errorfile2,patherr)
for errorfile3 in errorfiles3:
    shutil.move(errorfile3,patherr)

#make output file
oflag = os.path.exists(pathout)

if oflag == False:
    os.mkdir(pathout)
   # print "New output directory has benn made!"

files = os.listdir('./')
outputfiles1 = [x for x in files if ".sh.o" in x]
outputfiles2 = [x for x in files if "OTHERS.o" in x]
outputfiles3 = [x for x in files if "lobsterin.o" in x]

for outputfile1 in outputfiles1:
    shutil.move(outputfile1,pathout)
for outputfile2 in outputfiles2:
    shutil.move(outputfile2,pathout)
for outputfile3 in outputfiles3:
    shutil.move(outputfile3,pathout)

#remove error and output files
fe = os.path.exists(patherr)
if fe == True:
    shutil.rmtree(patherr)

fo = os.path.exists(pathout)
if fo == True:
    shutil.rmtree(pathout)

print("All error and output files are removed!")

