#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import subprocess as sub

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
#files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
#dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

sub.call("ps aux > ps-aux.out", shell=True)
with open(path + "/ps-aux.out") as fpa: palines = fpa.readlines()
palines = [conv_line(l) for l in palines]

Lnum = ""
for paline in palines:
    if len(paline) > 12:
        if "emacs" in paline[10] or "emacs" in paline[11] or "emacs" in paline[12]: 
            Lnum += " " + str(paline[1]); print("emacs1")
        elif "dbus-daemon" in paline[10]:
            Lnum += " " + str(paline[1]); print("dbus-daemon")
    else:
        if "emacs" in paline[10]: Lnum += " " + str(paline[1]); print("emacs2")
    
#print("kill -KILL" + Lnum)
sub.call("kill -KILL" + Lnum, shell=True)
if os.path.exists(path + "/ps-aux.out"): os.remove(path + "/ps-aux.out") 
print("\nAll remained processes have benn killed!")
