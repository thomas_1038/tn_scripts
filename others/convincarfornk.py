#! /usr/bin/env python3                                                            
# -*- coding: utf-8 -*-                                                        
#2015/07/03 ver1.0
#usage:script A1 A2 ... Af (A1~Af:Pseudopotential name)
#This script makes POTCAR.

import os
import os.path
import shutil
import sys
import threading

argv = sys.argv
if len(argv) > 1:
    if "--n" in argv:
        index_n = argv.index("--n")
        npar = str(argv[index_n +1])
    if "--k" in argv:
        index_k = argv.index("--k")
        kpar = str(argv[index_k +1])

elif len(argv) == 1:
    npar = input("Please input NPAR value: ")
    kpar = input("Please input KPAR value: ")

path = os.getcwd()
path_INCAR = path + "/INCAR"
f_in = open(path_INCAR)
incarlines = f_in.readlines()
f_in.close()

num = 0
while num < len(incarlines):
    flag_NPAR = incarlines[num].find("NPAR =")
    flag_KPAR = incarlines[num].find("KPAR =")
    
    if flag_NPAR >= 0:
        value = "NPAR = " + npar + "\n"
        incarlines[num] = value
    elif flag_KPAR >= 0:
        value = "KPAR = " + kpar + "\n"
        incarlines[num] = value
    num += 1
            
# change INCAR
f_in = open(path_INCAR, "w")
for line in incarlines:
    f_in.write(line)
f_in.close()

print("Done!")
