#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#ver1.0
#This script deleate files which contains "~".

import os, glob

files = glob.glob('*~*')
for file in files: os.remove(file)
