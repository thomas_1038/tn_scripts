#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys

def mk_poscars(nowpath):
    path_xdat = nowpath + "/XDATCAR"
    if not os.path.exists(path_xdat):
        print("You should prepare XDATCAR! BYE!"); sys.exit()

    with open(path_xdat) as fx: xdatlines = fx.readlines()

    path_dir_xdat = nowpath + "/xdat"
    if not os.path.exists(path_dir_xdat): os.mkdir(path_dir_xdat)

    L_first = xdatlines[1] + xdatlines[2] + xdatlines[3] + xdatlines[4] + xdatlines[5] + xdatlines[6]
    Ltot = ""; eachPOSCAR = []; checker = 0
    for i, l in enumerate(xdatlines):
        if i < 7: pass
        else:
            if "Direct configuration=" in l:
                num_it = l.replace("Direct configuration=","").replace(" ","").replace("\n","")
                if int(num_it) != 1 and checker > 0: eachPOSCAR.append(Li)
                print(num_it)

                num_it = l.replace("Direct configuration=","").replace(" ","").replace("\n","")
                Ltot += str(num_it+"\n") + L_first + "Direct\n"
                Li = str(num_it+"\n") + L_first + "Direct\n"
                checker += 1
            else: Ltot += l; Li += l
    else: eachPOSCAR.append(Li)

    with open(nowpath+"/xdatPOSCARS","w") as fps: fps.write(Ltot)

    for i, L in enumerate(eachPOSCAR):
        if i+1 in [n for n in range(0,10)]: filename = "POSCAR00" + str(i+1) + ".vasp"
        elif i+1 in [n for n in range(10,100)]: filename = "POSCAR0" + str(i+1) + ".vasp"
        else: filename = "POSCAR" + str(i+1) + ".vasp"
        with open(path_dir_xdat + "/" + filename, "w") as fp: fp.write(L)

if __name__ == "__main__":
    mk_poscars(os.getcwd()); print("\nDONE!\n")
# END: script" + filename
