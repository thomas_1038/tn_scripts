#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#This script
#2017/10/23

import sys, os, math, shutil
import addmol.helper as hl
import numpy as np

def f2cc2f(nowpath, filename, type_trans):
    def det3(mat):
      return ((mat[0][0]*mat[1][1]*mat[2][2]) + (mat[0][1]*mat[1][2]*mat[2][0]) + (mat[0][2]*mat[1][0]*mat[2][1]) - (mat[0][2]*mat[1][1]*mat[2][0]) - (mat[0][1]*mat[1][0]*mat[2][2]) - (mat[0][0]*mat[1][2]*mat[2][1]))
    def frac2cart(cellParam, fracCoords):
      cartCoords = []
      for i in fracCoords:
        xPos = i[0]*cellParam[0][0] + i[1]*cellParam[1][0] + i[2]*cellParam[2][0]
        yPos = i[0]*cellParam[0][1] + i[1]*cellParam[1][1] + i[2]*cellParam[2][1]
        zPos = i[0]*cellParam[0][2] + i[1]*cellParam[1][2] + i[2]*cellParam[2][2]
        cartCoords.append([xPos, yPos, zPos])
      return cartCoords
    def cart2frac(cellParam, cartCoords):
      latCnt = [x[:] for x in [[None]*3]*3]
      for a in range(3):
          for b in range(3):
              latCnt[a][b] = cellParam[b][a]

      fracCoords = []
      detLatCnt = det3(latCnt)
      for i in cartCoords:
        aPos = (det3([[i[0], latCnt[0][1], latCnt[0][2]], [i[1], latCnt[1][1], latCnt[1][2]], [i[2], latCnt[2][1], latCnt[2][2]]])) / detLatCnt
        bPos = (det3([[latCnt[0][0], i[0], latCnt[0][2]], [latCnt[1][0], i[1], latCnt[1][2]], [latCnt[2][0], i[2], latCnt[2][2]]])) / detLatCnt
        cPos = (det3([[latCnt[0][0], latCnt[0][1], i[0]], [latCnt[1][0], latCnt[1][1], i[1]], [latCnt[2][0], latCnt[2][1], i[2]]])) / detLatCnt
        fracCoords.append([aPos, bPos, cPos])
      return fracCoords

    if not os.path.exists(nowpath + "/" + filename):
        print("You should prepare " + filename + " here! BYE!"); sys.exit()

    with open(nowpath + "/" + filename) as fp: pclines = fp.readlines()
    flagS = 0 ; flagD = 0 ; flagC = 0 ; line_dc = ""
    if "S" in pclines[7]: flagS = 1
    if "D" in pclines[7+flagS]: flagD = 1 ; line_dc = "Direct"
    elif "C" in pclines[7+flagS]: flagC = 1 ; line_dc = "Cartesian"
    if __name__ == "__main__": print("\n" + line_dc)

    # Read Atom label
    atomlabel = hl.conv_line(pclines[5]); atomnum = hl.conv_line(pclines[6])
    Sum_numatom = sum([int(x) for x in hl.conv_line(pclines[6])])

    # Make label
    n1 = 0 ; atomlabellist = []
    while n1 < len(atomlabel):
        n2 = 0
        while n2 < int(atomnum[n1]): atomlabellist.append("!" + atomlabel[n1] + str(n2+1)); n2 += 1
        n1 += 1

    firstlines = [] ; mat_lat =[] ; coor = [] ; DynamicsLabels = []
    # Make upper line of POSCAR_car
    n0 = 0
    while n0 < 7 + flagS: firstlines.append("  ".join(hl.conv_line(pclines[n0]))); n0 += 1

    # Make lattice vecor matrix
    n1 = 2
    while n1 < 5: mat_lat.append(list(map(float, hl.conv_line(pclines[n1])))) ; n1 += 1

    # Make atom position matrix
    n2 = 8 + flagS
    while n2 < 8 + flagS + Sum_numatom:
        line = hl.conv_line(pclines[n2])
        coor.append(list(map(float, [line[0], line[1], line[2]])))
        if flagS == 1: DynamicsLabels.append([line[3],line[4],line[5]])
        n2 += 1

    if type_trans == "fc" and flagD == 1:
        coor = frac2cart(mat_lat, coor); line_DorC = "Cartesian"
    elif type_trans == "cf" and flagC == 1:
        coor = cart2frac(mat_lat, coor); line_DorC ="Direct"
    else: line_DorC = line_dc

    for i1, l in enumerate(coor):
        for i2, x in enumerate(l): coor[i1][i2] = str("{:.10f}".format(x))

    list_tot_row = []
    for i in range(0, len(coor[0])): list_tot_row.append(hl.adjust_len([str(x[i]) for x in coor]))
    if flagS == 1:
        for i in range(0, len(DynamicsLabels[0])): list_tot_row.append(hl.adjust_len([str(x[i]) for x in DynamicsLabels]))
    list_tot_row.append(hl.adjust_len(atomlabellist))

    Lpos = str(firstlines[0]) + "\n1.0\n"
    Lpos += str(firstlines[2]) + "\n" + str(firstlines[3]) + "\n" + str(firstlines[4]) + "\n" + str(firstlines[5]) + "\n" + str(firstlines[6]) +"\n"
    if flagS == 1: Lpos += "Selective\n"
    Lpos += line_DorC + "\n"

    i2 = 0 ; spc = " "
    for x in list_tot_row[0]:
        i1 = 0
        for y in list_tot_row: Lpos += str(list_tot_row[i1][i2]) + spc; i1 += 1
        Lpos += "\n"; i2 += 1

    return Lpos

if __name__ == "__main__":
    argv = sys.argv
    nowpath = os.getcwd()

    if len(argv) == 1:
        filename = input("Please input the file name, i.e., [ POSCAR,p / CONTCAR,c / SPOSCAR, s] or FILENAME (e.g., POSCAR1): ")
        if filename in ["POSCAR", "p", "P"]: filename = "POSCAR"
        elif filename in ["CONTCAR", "c", "C"]: filename = "CONTCAR"
        elif filename in ["SPOSCAR", "s", "S"]: filename = "SPOSCAR"

    elif len(argv) > 1:
        if argv[1] == "p": filename = "POSCAR"
        elif argv[1] == "c": filename = "CONTCAR"
        else: filename = str(argv[1])

    if len(argv) > 2:
        if argv[2] == "fc": type_trans = "fc"
        elif argv[2] == "cf": type_trans = "cf"
        else: type_trans = str(argv[2])
    else: type_trans = input("Please input conversion mode, [Frac to Car: fc; Car to Frac: cf; Win to Linux: wl]: ")

    Lpos = f2cc2f(nowpath, filename, type_trans)
    if len(argv) < 4:
        filename_new = str(filename).replace(".vasp","") + "_" + type_trans + ".vasp"
        with open(nowpath + "/" + filename_new,'w') as fpc: fpc.write(Lpos)
    elif len(argv) == 4 and argv[3] == "p":
        filename_new = "POSCAR"
        with open(nowpath + "/" + filename_new,'w') as fpc: fpc.write(Lpos)
    elif len(argv) == 4 and argv[3] == "c":
        filename_new = "CONTCAR"
        with open(nowpath + "/" + filename_new,'w') as fpc: fpc.write(Lpos)
    elif len(argv) == 4 and argv[3] == "n":
        filename_new = str(filename)
        with open(nowpath + "/" + filename_new,'w') as fpc: fpc.write(Lpos)
    print(filename_new + " has been made!\n")

# END script