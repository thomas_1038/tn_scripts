#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os #, sys, shutil, threading, glob, re
#import subprocess as sub
import numpy as np
import addmol.helper as hl
import addmol.readpos as rp
import addmol.calc as clc
import addmol.mk_poscar as mkp
from addmol.f2cc2f import Frac2Cart
f2c = Frac2Cart()

################################ INPUT ZONE ################################
coortype_in = "c"
lab_in = "Ca14"
#lab_in = "Ca77"
x_basis = 0.5
y_basis = 0.5
z_basis = 0.5

coor_basis_in = [float(x_basis),float(y_basis),float(z_basis)]
coor_name_in = [str(x_basis), str(y_basis), str(z_basis)]
################################ INPUT ZONE ################################

def trans_coor_in_pos(nowpath, filename, flag_inp, coortype, lab_in, coor_basis_in, coor_name_in):
    #path = os.getcwd()
    #filename = q.Q_file()
    #filename = "POSCAR"

    #Q_inp = input("Do you use the inputted parameter or not? [Y/n]: ")
    #if Q_inp in ["y","Y"]: flag_inp = 1
    #else: flag_inp = 0

    #if flag_inp == 0: Q_coortype = input("Please input coordinate type you want to use [frac, f/ cart, c]: ")
    #else: Q_coortype = coortype_in
    if coortype in ["frac", "f"]: flagFC = 0 # 0 means fractional
    else: flagFC = 1 # 1 means cartesian

    with open(nowpath + "/"+ filename) as fp: poslines = fp.readlines()
    labelofel = rp.get_labelofel(poslines); numofel = rp.get_numofel(poslines); sumatomnum = rp.get_sumofnumofel(poslines)
    list_labelofel = rp.mk_labelofel(labelofel, numofel)[0]
    firstlines, flagS = rp.get_firstlines(poslines)
    mat_lat = rp.get_matrix(poslines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
    #arr_coor_car = np.array(coor_car)
    arr_coor_dir = np.array(coor_dir)
    #list_num_SD = rp.mk_listnumSD(list_SD)

    if flagFC == 0: firstlines[-1] = "Direct"
    else: firstlines[-1] = "Cartesian"

    if flag_inp == 0: Qlab = input("Please input label of atom you want to make it basis, i.e., N1: ")
    else: Qlab = lab_in
    coor_obj = np.array(coor_dir[list_labelofel.index("!"+Qlab)])

    def Q_coor(ch, coor):
        L = "Please input the \""+ch+"\"-coordinate where you want to put the basis atom by \"fractional\",\ni.e., 0.5 or [n, N] (\"n\" means \"Use same value\") : "
        Q1 = input(L)
        if Q1 in ["n", "N"] and ch == "x": coor = float(coor[0]); name = "def"
        elif Q1 in ["n", "N"] and ch == "y": coor = float(coor[1]); name = "def"
        elif Q1 in ["n", "N"] and ch == "z": coor = float(coor[2]); name = "def"
        else: coor = float(Q1); name = str(coor)
        return coor, name

    if flag_inp == 0:
        x_shift, x_name = Q_coor("x", coor_obj); y_shift, y_name = Q_coor("y", coor_obj); z_shift, z_name = Q_coor("z", coor_obj)
        coor_basis = np.array([x_shift, y_shift, z_shift])
    else:
        coor_basis = np.array(coor_basis_in)
        x_name, y_name, z_name = coor_name_in[0], coor_name_in[1], coor_name_in[2]

    coor_new_tot = []
    for c in arr_coor_dir:
        coor_new = c - coor_obj + coor_basis; i = 0
        for x in coor_new:
            if x < 0: coor_new[i] = x + 1
            elif x >= 1: coor_new[i] = x - 1
            i += 1
        coor_new_tot.append(coor_new.tolist())

    if flagFC == 1: coor_new_tot = f2c.frac2cart(mat_lat, coor_new_tot)

    list_labelofel = hl.adjust_len(list_labelofel)
    coor_new_tot = np.array([hl.adjust_len([str(x) for x in l]) for l in np.array(coor_new_tot).T.tolist()]).T.tolist()

    """
    if flagS == 1:
        list_tot = np.concatenate([np.array(coor_new_tot), np.array(list_SD)], axis=1)
        list_tot = np.concatenate([list_tot, np.array([[str(x)] for x in list_labelofel])], axis=1)
    else: list_tot = np.concatenate([np.array(coor_new_tot), np.array([[str(x)] for x in list_labelofel])], axis=1)
    list_tot = firstlines + ["  ".join(x) for x  in list_tot.tolist()]
    #list_tot_diff = ["  ".join(x) for x in np.concatenate([coor_diff, np.array([[str(x)] for x in list_labelofel])], axis=1).tolist()]
    """

    newfile = filename.replace(".vasp", "") + "_" + Qlab + "_x" + str(x_name) + "y" + str(y_name) + "z" + str(z_name) + ".vasp"
    mkp.mk_pos(nowpath, newfile, firstlines, mat_lat, labelofel, numofel, list_labelofel, flagS, coor_new_tot, list_SD, line_add="")
    #with open(path + "/" + newfile, "w") as fpn: fpn.write("\n".join(list_tot))
    #return newfile

def rot_coor_in_pos(nowpath, filename, coortype, degx, degy, degz):
    #if flag_inp == 0: Q_coortype = input("Please input coordinate type you want to use [frac, f/ cart, c]: ")
    #else: Q_coortype = coortype_in
    if coortype in ["frac", "f"]: flagFC = 0 # 0 means fractional
    else: flagFC = 1 # 1 means cartesian

    with open(nowpath + "/"+ filename) as fp: poslines = fp.readlines()
    labelofel = rp.get_labelofel(poslines); numofel = rp.get_numofel(poslines); sumatomnum = rp.get_sumofnumofel(poslines)
    list_labelofel = rp.mk_labelofel(labelofel, numofel)[0]
    firstlines, flagS = rp.get_firstlines(poslines)
    mat_lat = rp.get_matrix(poslines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
    #arr_coor_car = np.array(coor_car)
    arr_coor_dir = np.array(coor_dir)
    #list_num_SD = rp.mk_listnumSD(list_SD)

    if flagFC == 0: firstlines[-1] = "Direct"
    else: firstlines[-1] = "Cartesian"

    mat_lat_new = []
    for ml in mat_lat:
        mat_lat_new.append(clc.rotation(ml, degx, degy, degz))

    coor_new_tot = []
    for c in arr_coor_dir:
        coor_new_tot.append(clc.rotation(c, degx, degy, degz))

    if flagFC == 1: coor_new_tot = f2c.frac2cart(mat_lat, coor_new_tot)

    list_labelofel = hl.adjust_len(list_labelofel)
    coor_new_tot = np.array([hl.adjust_len([str(x) for x in l]) for l in np.array(coor_new_tot).T.tolist()]).T.tolist()

    """
    if flagS == 1:
        list_tot = np.concatenate([np.array(coor_new_tot), np.array(list_SD)], axis=1)
        list_tot = np.concatenate([list_tot, np.array([[str(x)] for x in list_labelofel])], axis=1)
    else: list_tot = np.concatenate([np.array(coor_new_tot), np.array([[str(x)] for x in list_labelofel])], axis=1)
    list_tot = firstlines + ["  ".join(x) for x  in list_tot.tolist()]
    #list_tot_diff = ["  ".join(x) for x in np.concatenate([coor_diff, np.array([[str(x)] for x in list_labelofel])], axis=1).tolist()]
    """

    newfile = filename.replace(".vasp", "") + "_dx" + str(degx) + "dy" + str(degy) + "dz" + str(degz) + ".vasp"
    mkp.mk_pos(nowpath, newfile, firstlines, mat_lat_new, labelofel, numofel, list_labelofel, flagS, coor_new_tot, list_SD, line_add="")

    #with open(nowpath + "/" + newfile, "w") as fpn: fpn.write("\n".join(list_tot))
    #return newfile

if __name__ == "__main__":
    trans_or_rot = input("Please input the mode; trans or rot, [t/r]: ")
    #Q_inp = input("Do you use the inputted parameter or not? [Y/n]: ")
    #if Q_inp in ["y","Y"]: flag_inp = 1
    #else: flag_inp = 0
    filename = input("Please input the filename: ")
    coortype = input("Please input coordinate type you want to use [frac, f/ cart, c]: ")

    if trans_or_rot == "t":
        trans_coor_in_pos(os.getcwd(), filename, 0, coortype, lab_in, coor_basis_in, coor_name_in)

    elif trans_or_rot == "r":
        degx = float(input("Please input the rotation angle along x-axis: "))
        degy = float(input("Please input the rotation angle along y-axis: "))
        degz = float(input("Please input the rotation angle along z-axis: "))
        rot_coor_in_pos(os.getcwd(), filename, coortype, degx, degy, degz)

    print("\nDone!\n")

# END: script