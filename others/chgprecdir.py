#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

commands = ["-h","-d","-all","-wav","-chg","-xml","-pm","-pn","-pcg","-out"]
rm_files = ["CHG","IBZKPT","pic.err","pic.out","PCDAT","REPORT"]
rm_files_precM = ["DOSCAR","EIGENVAL","PROCAR","vasprun.xml","CHG","IBZKPT","pic.err","pic.out","PCDAT","REPORT"]
rm_files_precN = ["DOSCAR","EIGENVAL","PROCAR","vasprun.xml","CHG","IBZKPT","pic.err","pic.out","PCDAT","REPORT"]
rm_files_ldos = ["POTCAR","XDATCAR","CHGCAR","DOSCAR","EIGENVAL","PROCAR","vasprun.xml","CHG","CONTCAR","IBZKPT","pic.err","pic.out","PCDAT","REPORT"]

argv = sys.argv

"""
if len(argv) == 1:
    print("\nYou should input the command after script name! You can see the commands by inputting -h (HELP). BYE!\n")
    sys.exit()
else:
    for i in range(1,len(argv)):
        if argv[i] not in commands: print("\n  You input UNregistered command. BYE!\n"); sys.exit()

if len(argv) > 1 and "-h" in argv:
    Lhelp =  "\n    -h: show HELP"
    Lhelp += "\n    -d: defalut mode: keep CHGCAR, DOSCAR, EIGENVAL, PROCAR and vasprun.xml in the most accurate calc. directory, and keep WAVECAR in LDOS"
    Lhelp += "\n    -all: remove all the files instead of necessary files."
    Lhelp += "\n    -wav: romove WAVECAR"
    Lhelp += "\n    -out: remove OUTCAR"
    Lhelp += "\n    -chg: remove CHGCAR in the most accurate calc. directory"
    Lhelp += "\n    -xml: remove vasprun.xml in the most accurate calc. directory"
    Lhelp += "\n    -pm:  remove files only in PrecM"
    Lhelp += "\n    -pcg: remove PARCHG in LDOS"
    Lhelp += "\n"
    print(Lhelp); sys.exit()

elif len(argv) > 1 and "-d" in argv: pass
elif len(argv) > 1 and "-all" in argv:
    rm_files = ["CHGCAR","DOSCAR","EIGENVAL","PROCAR","vasprun.xml","WAVECAR","CHG","IBZKPT","pic.err","pic.out","PCDAT","REPORT"]
    rm_files_ldos = ["PARCHG","OUTCAR","POTCAR","XDATCAR","CHGCAR","DOSCAR","EIGENVAL","PROCAR","vasprun.xml","CHG","CONTCAR","IBZKPT","OSZICAR","pic.err","pic.out","PCDAT","REPORT"]

if len(argv) > 1 and "-wav" in argv: 
    rm_files.append("WAVECAR"); rm_files_ldos.append("WAVECAR")
    rm_files_precM.append("WAVECAR"); rm_files_precN.append("WAVECAR")
if len(argv) > 1 and "-out" in argv: 
    rm_files.append("OUTCAR"); rm_files_ldos.append("OUTCAR"); rm_files_precM.append("OUTCAR")
if len(argv) > 1 and "-chg" in argv: 
    rm_files.append("CHGCAR"); rm_files_precM.append("CHGCAR"); rm_files_precN.append("CHGCAR")
if len(argv) > 1 and "-xml" in argv: 
    rm_files.append("vasprun.xml"); rm_files_precM.append("vasprun.xml")
"""

path = os.getcwd()
for curdir, dirs, files in os.walk(path):
    for file in files:
        if file in rm_files_precM and "PrecM" in curdir: print(curdir + "/" + file); os.remove(curdir + "/" + file)
else: print("\n  Done!\n"); sys.exit()

"""
for curdir, dirs, files in os.walk(path):
    for file in files:
        if "UNK" in file: 
            if "-unk" in argv: print(curdir + "/" + file); os.remove(curdir + "/" + file)
        elif "LDOS" in curdir:
            if file in rm_files_ldos: print(curdir + "/" + file); os.remove(curdir + "/" + file)
            elif len(argv) > 1 and "-pcg" in argv and "PARCHG" in file: curdir + "/" + file; os.remove(curdir + "/" + file)
        elif file in rm_files and "LDOS" not in curdir: print(curdir + "/" + file); os.remove(curdir + "/" + file)
        elif file in rm_files_precM and "PrecM" in curdir: print(curdir + "/" + file); os.remove(curdir + "/" + file)
"""

print("\n  Done!\n")
#END: Program
