#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import os.path
import shutil
import sys

argv = sys.argv
if len(argv) == 1:
    print("You should input some comannds. Please input -h or -help if you want to know the commands. bye!")
    sys.exit()

command = str(argv[1])
if command == "-h" or command == "-help":
    l = "-q or -qsub: qsub ~.sh in each directories."
    l += "-as or -autostop: make autostop in each directories." + "\n"
    print(l)
    sys.exit()

path = os.getcwd()
list_cwd = os.listdir(path)
direcs_hkl = [x for x in list_cwd if os.path.isdir(x)]
index_re = direcs_hkl.index("Relax")
del direcs_hkl[index_re]
print(direcs_hkl)

for direc in direcs_hkl:
    path_direc = path + "/" + direc
    list_direc = os.listdir(path_direc)
    direcs_in_hkl = [x for x in list_direc if "Relax" in x]
    print(direcs_in_hkl)

    for relaxdirec in direcs_in_hkl:
        Lp = relaxdirec + " in " + direc
        print(Lp)

        path_direc_relax = path_direc + "/" + relaxdirec
        
        if command == "-qsub" or command == "-q":
            os.chdir(path_direc_relax)
            autorun = "nohup ./calcautorelax_n.py > log.dat &"
            os.system(autorun)
                    
        elif command == "-as" or command == "-autostop":
            #os.chdir(path_direc_relax)
            path_autostop = path_direc_relax + "/autostop"
            f = open(path_autostop, "w")
            f.write("autostop")
            f.close()
