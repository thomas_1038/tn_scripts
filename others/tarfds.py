#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

argv = sys.argv; del argv[0]
argv =[x.replace("/","") for x in argv]

Q_zip = input("Please input the mode [zip, z/ unzip, u]: ")
if Q_zip in ["zip", "z"]: 
    code = "zcvf"
    for dr in argv: sub.call("tar " + code + " " +  dr + ".tar.gz " + dr, shell=True)

elif Q_zip in ["unzip", "u"]: 
    code = "zxvf"
    for dr in argv: sub.call("tar " + code + " " +  dr, shell=True)

else: print("Please input [zip, z/ unzip, u]! BYE!"); sys.exit()


print("\nDone!\n")
# END
