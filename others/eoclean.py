#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#This script make error and output directorise where error and output shfiles are located.
#and move error and output shfiles to directories.

import os, re, glob, shutil

def clean_eo():
    path = os.getcwd()
    path_eodir = path + "/eo_files"
    if os.path.exists(path_eodir) == False: os.mkdir(path_eodir)

    #move error shfiles to directory.
    #eofiles = glob.glob(r"[0-9][0-9][0-9]")
    eofiles = [x for x in os.listdir(path) if os.path.isfile(x) and re.findall(r"[0-9]{2,9}",x) and (".e" in x or ".o" in x)]
    for eof in eofiles: shutil.copy(path+"/"+eof, path_eodir); os.remove(path+"/"+eof)

if __name__ == "__main__": clean_eo(); print("\nDone!\n")
