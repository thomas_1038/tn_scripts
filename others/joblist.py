#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import os.path
import sys
import shutil
import threading
import re
import datetime
import subprocess

### Definition of functions ###
def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line
### End ###

################## INPUT ZONE ##################
Nnodes = 12
LT = 300
list_name = "c-list"
path_log = "/home/nakao/qstat-log"
list_rm = ["POSCARs","tmp","models"]
vautorun = "qsub job.sh"
################## INPUT ZONE ##################

path = os.getcwd()
path_clist = path + "/" + list_name
if os.path.exists(path_clist) == False:
    list_dir = [fd for fd in os.listdir(path) if os.path.isdir(path+"/"+fd) == True]
    for x in list_rm:
        if x in list_dir:
            list_dir.remove(x)

    list_dir.sort()
    f_c = open(path_clist,"w")
    for direc in list_dir:
        f_c.write(direc + " Q\n")
    f_c.close()

def autocheck():
    print("now looping")
    os.chdir(path_log)
    qsdir = "qsdir.py > qsdir.dat"
    subprocess.call(qsdir,shell=True)
    
    path_qsdir = path_log + "/qsdir.dat"
    flag_qsdir = os.path.exists(path_qsdir)
    if flag_qsdir == False:
        print("qsdir.dat does not exsit!")
        sys.exit()
        
    f = open(path_qsdir)
    qsdirlines = f.readlines()
    f.close()

    #make Job list
    list_Job = []
    num = 0
    while num < len(qsdirlines):
        flag = qsdirlines[num].find(".accel")
        if flag >= 0:
            line = qsdirlines[num].replace("\n","")
            line = line.split(" ")
            while line.count("") > 0:
                line.remove("")
            list_Job.append(line)
            num += 1
        else:
            num += 1
 
    print(list_Job)

    numQ = 0
    numR = 0
    for job in list_Job:
        print(job)
        if str(job[2]) == "nakao": 
            if str(job[1]) == "Q":
                numQ += 1
            elif str(job[1]) == "R":
                numR += 1
        
    if numR < int(Nnodes): #and numQ == 0:
        numJ = int(Nnodes) - numR

        f_c = open(path_clist)
        clines = f_c.readlines()
        f_c.close()

        numJc = 0
        clines = [conv_line(cline) for cline in clines]
        for cline in clines:
            if cline[1] == "Q":
                path_dir = path + "/" + cline[0] 
                
                os.chdir(path_dir)
                subprocess.call(vautorun,shell=True)
                cline[1] = "R"
                
                numJc += 1
                if numJc == numJ:
                    break

        f_c = open(path_clist,"w")
        for cline in clines:
            f_c.write(cline[0] + " " + cline[1] + "\n")
        f_c.close()
                
    t_ho = threading.Timer(LT,autocheck)
    t_ho.start()

t_ho = threading.Thread(target=autocheck)
t_ho.start()
