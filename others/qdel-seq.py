#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#This script change Initial POSCAR to POSCAR_ini and CONTCAR to POSCAR.
#usage:script

import os
import sys
import shutil

Ni = input("Please input the first job number: ")
Nf = input("Please input the final job number: ")
choice = input("Please input the machine name, [ven, accel, ties]: ")
if choice in ["v", "ven"]:
    machine = "ven"
elif choice in ["a", "accel"]:
    machine = "accel"
elif choice in ["t", "ties"]:
    machine = "ties"
else:
    machine = ""
    #print("Your input machine name is wrong. Please reinput.")

L = "qdel "
for i in range(int(Ni), int(Nf)):
    L += str(i) + " "
#L = "for f in `seq " + str(Ni) + " " + str(Nf) + "`; do qdel $f." + '\"' + str(machine) + '\"; done'
#os.system(L)
print(L)
