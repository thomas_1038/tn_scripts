#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

print("Done!")
