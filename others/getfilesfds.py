#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

stdout = "std.out"
ind_iter = "F="
ind_req = "reached required accuracy - stopping structural energy minimisation"
ind_wav = "writing wavefunctions"

rm_dirs = ["models","POSCARs","tmp","origin","CONTCARs","scripts","candidates","miss","miss1","miss2",\
        "Baders","MAXFORCEs","OUTCARs","PARCHGs","stdouts","vaspruns"]
needfiles = ["BADERCHG","MAXFORCE","OUTCAR","std.out","vasprun.xml"]

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
Baders = path + "/Baders"
Maxforces = path + "/MAXFORCEs"
OUTCARs = path + "/OUTCARs"
PARCHGs = path + "/PARCHGs"
stdouts = path + "/stdouts"
vaspruns = path + "/vaspruns"
path_dirs = [Baders, Maxforces, OUTCARs, stdouts, vaspruns]

for dr in path_dirs:
    if os.path.exists(dr) == False: os.mkdir(dr)

dirs_adcalc = sorted([x for x in os.listdir(path) if os.path.isdir(x) and x not in rm_dirs])

for dr in dirs_adcalc:
    #print dr
    path_dir = path + "/" + dr; path_calc = path_dir + "/calc"
    if os.path.exists(path_calc) == False: continue

    # Read vauto-input file
    path_ilist = path_dir + "/vauto-input"
    if  os.path.exists(path_ilist) == True:
        with open(path_ilist) as fi: ilines = fi.readlines()
    else:
        if __name__ == "__main__": print("\nvauto-input doesn't exist!\n")
        continue

    # Make vauto-input list
    clist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0 and [val for val in conv_line(line) if val != "#"][0] in ["M","N","A"] and conv_line(line)[1] != "A"]
    clist = [[x for x in list if x != "#"] for list in clist]

    for num_prec, rd in enumerate(clist):
        if str(rd[8])=="F":
            dir_prec = str(num_prec) + "_Prec"+str(rd[0])+"cut"+str(rd[1])+"k"+str(rd[2])+"x"+str(rd[3])+"x"+str(rd[4])+"Ed"+str(rd[7])
        elif str(rd[8])=="T":
            dir_prec = str(num_prec) + "_Prec"+str(rd[0])+"cut"+str(rd[1])+"k"+str(rd[2])+"x"+str(rd[3])+"x"+str(rd[4])+"Ed"+ str(rd[7])+"Fd"+str(rd[9])

        print(dr + "/" + dir_prec)
        path_dir_in_re = path_dir + "/" + dir_prec
        path_bader = path_dir_in_re + "/bader/BADERCHG"
        path_maxforce = path_dir_in_re + "MAXFORCE"
        path_outcar = path_dir_in_re + "/OUTCAR"
        path_stdout = path_dir_in_re + "/" + stdout
        path_vasprun = path_dir_in_re + "/vasprun.xml"
        path_files = [path_bader, path_maxforce, path_outcar, path_vasprun, path_stdout]

        # Read std.out #
        if os.path.exists(path_stdout):
            with open(path_stdout) as fs: stdline = fs.read()
            iter = stdline.count(ind_iter); flag_req = stdline.find(ind_req); flag_wav = stdline.find(ind_wav)
            if flag_req >= 0 and iter > 0: sign_re = "R" + str(iter)
            elif flag_wav >= 0 and iter > 0: sign_re = "W" + str(iter)
            elif iter > 0: sign_re = "N" + str(iter)

        for i in range(0,len(path_files)):
            if os.path.exists(path_files[i]):
                if len(os.listdir(path_dirs[i])) == 0:
                    shutil.copy(path_files[i], path_dirs[i] + "/" + needfiles[i] + "_" + dr + dir_prec + "_" + sign_re)
                else:
                    for x in os.listdir(path_dirs[i]):
                        if dr in x and dir_prec in x: os.remove(path_x)
                    shutil.copy(path_files[i], path_dirs[i] + "/" + needfiles[i] + "_" + dr + dir_prec + "_" + sign_re)

"""
for curdir, dirs, files in os.walk(path):
    for file in files:
        if file in rm_files_ldos and "LDOS" in curdir: print curdir + "/" + file; os.remove(curdir + "/" + file)
        elif file in rm_files and "LDOS" not in curdir: print curdir + "/" + file; os.remove(curdir + "/" + file)
        elif file in rm_files_precM and "PrecM" in curdir: print curdir + "/" + file; os.remove(curdir + "/" + file)
"""

print("\n  Done!\n")
#END: Program
