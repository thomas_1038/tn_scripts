#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import smtplib
from email.mime.text import MIMEText
from email.utils import formatdate

FROM_ADDRESS = 'thomas.1038.regi@gmail.com'
MY_PASSWORD = '$google-Kyogoku05$'
TO_ADDRESS = 't.nakao@mces.titech.ac.jp'
BCC = ""
#BCC = 'receiver2@test.net'
SUBJECT = 'test'
BODY = 'python'

def create_message(body, subject, from_addr=FROM_ADDRESS, to_addr=TO_ADDRESS, bcc_addrs=BCC):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = from_addr
    msg['To'] = to_addr
    msg['Bcc'] = bcc_addrs
    msg['Date'] = formatdate()
    return msg

def send(msg, from_addr=FROM_ADDRESS, mypass=MY_PASSWORD, to_addrs=TO_ADDRESS):
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.ehlo()
    smtpobj.login(from_addr, mypass)
    smtpobj.sendmail(from_addr, to_addrs, msg.as_string())
    smtpobj.close()

if __name__ == '__main__':
    subject = SUBJECT
    body = BODY

    msg = create_message(body, subject)
    send(msg)

