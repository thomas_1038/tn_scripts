#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#This script change Initial POSCAR to POSCAR_ini and CONTCAR to POSCAR.
#usage:script

import os, sys, shutil

def chg_contpos(nowpath):
    def chg_pos2con(path_ini, path_pos, path_cont, name):
        if not os.path.exists(path_ini):
            if os.path.exists(path_cont):
                os.rename(path_pos, path_ini); shutil.copy(path_cont, path_pos)
            else: shutil.copy(path_pos, path_ini)
            print("\n" + name + " has changed POSCAR!\n")

        else:
            if os.path.exists(path_cont):
                i = 1
                while True:
                    if i < 10: path_pre = nowpath +  "/POSCAR_pre0" + str(i) + ".vasp"
                    else: path_pre = nowpath +  "/POSCAR_pre" + str(i) + ".vasp"

                    if os.path.exists(path_pre): i += 1
                    else: break
                os.rename(path_pos, path_pre); shutil.copy(path_cont, path_pos)
                print("\nPOSCAR has changed POSCAR_pre, and " + name + " has changed POSCAR!\n")

            else: print("\n" + name + " does not exist! BYE!\n"); sys.exit()

    #If Initital POSCAR does not exsits in corrent direcrory.
    path_ini = nowpath +  "/POSCAR_ini.vasp"
    path_pos = nowpath +  "/POSCAR"
    path_cont = nowpath +  "/CONTCAR"
    path_cent = nowpath +  "/CENTCAR"

    if os.path.exists(path_cent): chg_pos2con(path_ini, path_pos, path_cent, "CENTCAR")
    elif os.path.exists(path_cont): chg_pos2con(path_ini, path_pos, path_cont, "CONTCAR")
    else: print("CONTCAR and CENCAR do not exist! BYE!"); sys.exit()
if __name__ == "__main__": chg_contpos(os.getcwd())
# END: program