#! /usr/bin/env python3                                                                                                        
# -*- coding: utf-8 -*-                                                                                                  
#usage: script   

import os
import sys
import shutil

path = os.getcwd()
argv = sys.argv 

#get directory list
list = os.listdir(path)
direcs = [x for x in list if os.path.isdir(x)]
direcs.sort()
direcs.remove("calc")

#make kp-directories

total_list = []
for direcname in direcs:        
    #difinition
    direcpath = path + "/" + str(direcname)
    KPOINTS = direcpath + "/KPOINTS"
    INCAR = direcpath + "/INCAR"
    stdout = direcpath + "/std.out"
    POSCAR = direcpath + "/POSCAR"

    list = []
        
    #read INCAR
    flag_incar = os.path.exists(INCAR)
    if flag_incar == True:
        f = open(INCAR)
        incarlines = f.readlines()
        f.close()
    
    #get cutoff energy
    num = 0
    while num < len(incarlines):
        flag_cutoff = incarlines[num].find("ENCUT =")
        if flag_cutoff >= 0:
            line = incarlines[num]
            line = line.replace('\n','')
            line = line.replace('\r','')
            line = line.replace('\t',' ')
            line = line.split(" ")
            while line.count("") > 0:
                line.remove("")
            num += 1
        else:
            num += 1
            
    list.append(line[2])
        
    #read KPOINTS
    f = open(KPOINTS)
    kplines = f.readlines()
    f.close()
    line  = kplines[3]
    line = line.replace('\n','')
    line = line.replace('\r','')
    line = line.replace('\t',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")

    list.append(line)

    #read std.out
    f = open(stdout)
    stdlines = f.readlines()
    f.close()
    
    num1 = 0
    list_toten = []
    while num1 < len(stdlines):
        if stdlines[num1].find("E0=") >= 0:
            line = stdlines[num1]
            line = line.replace('\n','')
            line = line.replace('\r','')
            line = line.replace('\t',' ')
            line = line.split(" ")
            while line.count("") > 0:
                line.remove("")
            list_toten.append(line)
            num1 += 1
        else:
            num1 += 1

    if int(len(list_toten)) > 0:
        fnum = len(list_toten) -1
        Etot = list_toten[fnum][4]
        list.append(Etot)

    else:
        list.append("0")

    #read POSCAR
    f = open(POSCAR)
    poslines= f.readlines()
    f.close()
    
    line  = poslines[6]
    line = line.replace('\n','')
    line = line.replace('\r','')
    line = line.replace('\t',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
        
    sum_atom = 0
    for atom in line:
        sum_atom += float(atom)

    Eatom = float(Etot)/float(sum_atom)
    list.append(Eatom)

    total_list.append(list)

#print total_list[0][1][0]

flag = True
while flag:
    flag = False
    for i in range(len(total_list)-1):
        if int(total_list[i][0]) == int(total_list[i+1][0]):
            if int(total_list[i][1][0]) > int(total_list[i+1][1][0]):
                total_list[i],total_list[i+1] = total_list[i+1],total_list[i]
                flag = True

#print total_list

file = path + "/TotalandAtomEnergy"
f = open(file, 'w')

Lf = "cutoff[eV]   kpoints   Etot[eV]   Eatom[eV/atom]" + "\n"  
f.write(Lf)

for line in total_list:
    cutoff = str(line[0]) + "  " 
    kp = str(line[1][0]) + "x" + str(line[1][1]) + "x" +str(line[1][2]) + "  " 
    tot = str(line[2]) + "  "
    atom = str(line[3]) + "\n"
    L = cutoff + kp + tot + atom
    f.write(L)
f.close()

print("TotalandAtomEnergy has been made!")
