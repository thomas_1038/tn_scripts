#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
from . import getmaxforce as gm
#import numpy as np

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
path_ilist = path + "/vauto-input"

# read vauto-input file
if os.path.exists(path_ilist) == True:
    f = open(path_ilist)
    ilines = f.readlines()
    f.close()
else:
    print("vauto-input does not exist.")
    sys.exit()

# make c-list list
clist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0 and [val for val in conv_line(line) if val != "#"][0] in ["M","N","A"] and conv_line(line)[1] != "A"]
clist = [[x for x in list if x != "#"] for list in clist]

list_tot = []
for num_prec, direc in enumerate(clist):
    list = []
    if str(direc[8]) == "F":
        direcname = str(num_prec) + "_Prec" + str(direc[0]) + "cut" + str(direc[1]) + "k" + str(direc[2]) +"x"+ str(direc[3])+ "x" +  str(direc[4]) + "Ed"+ str(direc[7])
    if str(direc[8]) == "T":
        direcname = str(num_prec) + "_Prec" + str(direc[0]) + "cut" + str(direc[1]) + "k" + str(direc[2]) +"x"+ str(direc[3])+ "x" +  str(direc[4]) + "Ed"+ str(direc[7]) + "Fd" + str(direc[9])

    list.append(direcname)
    path_direc = path + "/" + direcname
    path_stdout = path_direc + "/std.out"
    path_outcar = path_direc + "/OUTCAR"

    # Read OUTCAR
    list_toten = []
    if os.path.exists(path_outcar) == True:
        f = open(path_outcar)
        outlines = f.readlines()
        f.close()

        num_o = 0
        while num_o < len(outlines):
            if outlines[num_o].find("FREE ENERGIE OF THE ION-ELECTRON SYSTEM") >= 0:
                num_o += 4
                toten = outlines[num_o]
                toten = toten.replace('\n','').replace('\r','').replace('\t',' ')
                toten = toten.split(" ")
                list_toten.append(toten[-1])
            else:
                num_o += 1

    if os.path.exists(path_stdout) == False:
        print("std.out does not exist!")
        list.append("Nn ")
        list.append("--------")

    elif os.path.exists(path_stdout) == True:
        f = open(path_stdout)
        stdlines = f.readlines()
        f.close()

        num_bad = 0
        num_dav = 0
        for stdline in stdlines:
            flag_req  = stdline.find("reached required accuracy - stopping structural energy minimisation")
            flag_wav  = stdline.find("writing wavefunctions")
            flag_bad1 = stdline.find("VERY BAD NEWS! internal error in subroutine SGRCON")
            flag_bad2 = stdline.find("VERY BAD NEWS! internal error in subroutine IBZKPT")
            flag_dav = stdline.find("DAV:   1")
            if len(list_toten) > 0:
                Etot = round(float(list_toten[-1]), 6)
                if flag_bad1 >= 0 or flag_bad2 >= 0:
                    num_bad += 1
                if flag_dav >= 0:
                    num_dav += 1

                if flag_req >= 0 and len(list_toten) > 0:
                    if len(list_toten) <10:
                        num_scf = str(len(list_toten)) + " "
                    elif len(list_toten) >= 10:
                        num_scf = str(len(list_toten))
                    list.append("R"+num_scf)
                    list.append('%06.6f' % Etot)
                    break

                elif flag_wav >= 0 and len(list_toten) > 0:
                    if len(list_toten) <10:
                        num_scf = str(len(list_toten)) + " "
                    elif len(list_toten) >= 10:
                        num_scf = str(len(list_toten))
                    list.append("W"+num_scf)
                    list.append('%06.6f' % Etot)
                    break

        else:
            if len(list_toten) > 0:
                if len(list_toten) <10:
                    num_scf = str(len(list_toten)) + " "
                elif len(list_toten) >= 10:
                    num_scf = str(len(list_toten))
                    list.append("N"+num_scf)
                    list.append('%06.6f' % Etot)
            elif len(list_toten) == 0 and num_bad > 0 and num_dav == 0:
                list.append("IBE")
                list.append("--------")
            else:
                list.append("N0 ")
                list.append("--------")
    # End: Read OUTCAR

    #os.chdir(path_direc)
    gm.get_maxforce(path_direc)
    path_maxforce = path_direc+"/MAXFORCE"

    if os.path.exists(path_maxforce):
        fm = open(path_maxforce)
        maxflines = fm.readlines()
        fm.close()

        if len(maxflines) == 0: force = "--------"; atom = "----"
        else:
            force = conv_line(maxflines[-1])[2]
            atom = conv_line(maxflines[-1])[3]

    else:
        force = "--------"
        atom = "----"

    list.append(force)
    list.append(atom)
    list_tot.append(list)
    # End: for

L = ""
for line in list_tot:
    for l in line:
        L += str(l) + "  "
    else:
        L += "\n"

path_energy = path + "/TotalEnergy.dat"
f = open(path_energy,'w')
f.write(L)
f.close()

print("\n" + L)
# End Program

