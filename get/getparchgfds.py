#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

rm_dirs = ["models","POSCARs","tmp","origin","CONTCARs","scripts","candidates","miss","miss1","miss2","PARCHGs"]

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
path_dir = path + "/PARCHGs"
if os.path.exists(path_dir) == False: os.mkdir(path_dir)

dirs_adcalc = sorted([x for x in os.listdir(path) if os.path.isdir(x) and x not in rm_dirs])

for dr in dirs_adcalc:
    path_addir = path + "/" + dr
    for curdir, dirs, files in os.walk(path_addir):
        for file in files:
            if "PARCHG" in file and "LDOS" in curdir:
                list_curdir = curdir.split("/")
                name_site = list_curdir[-3]
                name_ldosdir = list_curdir[-1].replace("LDOS-from","f").replace("to","t")
                newname = "PARCHG_" + name_site + "_" + name_ldosdir + ".vasp"
                print(curdir + "/" + file); shutil.move(curdir + "/" + file, path_dir+"/" + newname)

print("\n  Done!\n")
#END: Program
