#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

list_exp = ["CONTCARs","POSCARs","tmp","miss","test","models","sum_Eads","backup","BestCONTCARs"]

path = os.getcwd()
dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) if x not in list_exp])

Ls = ""
for dr in dirs:
    print(dr)
    path_dir = path + "/" + dr
    os.chdir(path_dir)
    sub.call("getaden.py",shell=True)

    sumeads = [x for x in os.listdir(path_dir) if os.path.isfile(path_dir+"/"+x) if "Sum_Eads" in x][0]
    with open(path_dir + "/" + sumeads) as fs: sumelines = fs.readlines()

    Ls +=  "".join(sumelines[1:]) + "\n"

with open(path + "/Sum_Eads.dat", "w") as fst: fst.write(Ls)
print(Ls)

print("Done!")
