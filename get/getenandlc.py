#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script
#2017/05/25 add checkfunction of the wavefunction convergence.

import os, sys, math, shutil

def get_enandlatcon(nowpath):
    def conv_line(line):
        line = line.replace('\n',' ')
        line = line.replace('\r',' ')
        line = line.replace('\t',' ')
        line = line.replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        return line

    #get directory list
    direcs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x)])
    direcs.remove("calc")

    argv = sys.argv
    if len(argv) == 1: poslabel = 6
    else: poslabel = int(argv[1])

    total_list = []
    for direcname in direcs:
        print(direcname)
        #difinition
        direcpath = nowpath + "/" + str(direcname)
        KPOINTS = direcpath + "/KPOINTS"
        INCAR = direcpath + "/INCAR"
        stdout1 = direcpath + "/std.out"
        stdout2 = direcpath + "/stdout"
        POSCAR = direcpath + "/POSCAR"
        CONTCAR = direcpath + "/CONTCAR"
        OUTCAR  = direcpath + "/OUTCAR"
        list = []

        #read POSCAR
        flag_pos = os.path.exists(POSCAR)
        if flag_pos == True:
            f = open(POSCAR)
            poslines= f.readlines()
            f.close()

            sum_atom = 0
            for atom in conv_line(poslines[poslabel]):
                sum_atom += float(atom)

        # Read INCAR
        flag_incar = os.path.exists(INCAR)
        if flag_incar == True:
            f = open(INCAR)
            incarlines = f.readlines()
            f.close()

            for incarline in incarlines:
                flag_cutoff = incarline.find("ENCUT =")
                if flag_cutoff >= 0:
                    encut = conv_line(incarline)
            list.append(encut[2])
        else:
            list.append("---")

        # Read KPOINTS
        flag_kp = os.path.exists(KPOINTS)
        if flag_kp == True:
            f = open(KPOINTS)
            kplines = f.readlines()
            f.close()
            list.append(conv_line(kplines[3]))
        else:
            list.append("---")

        # Read OUTCAR
        list_toten = []
        flag_out = os.path.exists(OUTCAR)
        if flag_out == True:
            f = open(OUTCAR)
            outlines = f.readlines()
            f.close()

            num_o = 0
            while num_o < len(outlines):
                if outlines[num_o].find("FREE ENERGIE OF THE ION-ELECTRON SYSTEM") >= 0:
                    num_o += 4
                    toten = outlines[num_o]
                    toten = toten.replace('\n','')
                    toten = toten.replace('\r','')
                    toten = toten.replace('\t',' ')
                    toten = toten.split(" ")
                    list_toten.append(toten[-1])
                else:
                    num_o += 1

        # Read std.out
        flag_std1 = os.path.exists(stdout1)
        if flag_std1 == True:
            f = open(stdout1)
            stdlines = f.readlines()
            f.close()

            #list_toten = []
            #while stdline in stdlines:
            #    if stdlines[num1].find("E0=") >= 0:
            #        list_toten.append(conv_line(stdline))

        elif flag_std1 == False:
            flag_std2 = os.path.exists(stdout2)
            if flag_std2 == True:
                f = open(stdout2)
                stdlines = f.readlines()
                f.close()

                #list_toten = []
                #for stdline in stdlines:
                #    if stdline.find("E0=") >= 0:
                #        list_toten.append(conv_line(stdline))


        # Check the wave fuction convergence
        for stdline in stdlines:
            flag_req = stdline.find("reached required accuracy - stopping structural energy minimisation")
            flag_wav = stdline.find("writing wavefunctions")
            if int(len(list_toten)) > 0:
                Etot = float(list_toten[-1])
                Eatom = float(Etot)/float(sum_atom)

            if flag_req >= 0 and int(len(list_toten)) > 0:
                list.append("R") #R means "reached required accuracy - stopping structural energy minimisation" in std.out.
                list.append('%04.4f' % Etot)
                list.append('%04.4f' % Eatom)
                break

            elif flag_wav >= 0 and int(len(list_toten)) > 0:
                #Etot = list_toten[-1]
                #Eatom = float(Etot)/float(sum_atom)
                list.append("W") #W means "writing wavefunctions" in std.out.
                list.append('%04.4f' % Etot)
                list.append('%04.4f' % Eatom)
                break

        else:
            if int(len(list_toten)) > 0:
                #Etot = list_toten[-1]
                #Eatom = float(Etot)/float(sum_atom)
                list.append("N") #N means no message in std.out.
                list.append('%04.4f' % Etot)
                list.append('%04.4f' % Eatom)

            else:
                list.append("N") #N means no message in std.out.
                list.append('%04.4f' % 0.000)
                list.append('%04.4f' % 0.000)

        # Caculate the lattice vector of POSCAR
        if flag_pos == True:
            line_pa  = conv_line(poslines[2])
            line_pb  = conv_line(poslines[3])
            line_pc  = conv_line(poslines[4])
            lc_a_pos = math.sqrt(float(line_pa[0])**2 + float(line_pa[1])**2 + float(line_pa[2])**2)
            lc_b_pos = math.sqrt(float(line_pb[0])**2 + float(line_pb[1])**2 + float(line_pb[2])**2)
            lc_c_pos = math.sqrt(float(line_pc[0])**2 + float(line_pc[1])**2 + float(line_pc[2])**2)
            list.append('%03.3f' % lc_a_pos)
            list.append('%03.3f' % lc_b_pos)
            list.append('%03.3f' % lc_c_pos)

        else:
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)

        # Read CONTCAR
        flag_cont = os.path.exists(CONTCAR)
        if flag_cont == True:
            f = open(CONTCAR)
            contlines= f.readlines()
            f.close()

            if len(contlines) > 0:
                line_ca  = conv_line(contlines[2])
                line_cb  = conv_line(contlines[3])
                line_cc  = conv_line(contlines[4])
                lc_a_cont = math.sqrt(float(line_ca[0])**2 + float(line_ca[1])**2 + float(line_ca[2])**2)
                lc_b_cont = math.sqrt(float(line_cb[0])**2 + float(line_cb[1])**2 + float(line_cb[2])**2)
                lc_c_cont = math.sqrt(float(line_cc[0])**2 + float(line_cc[1])**2 + float(line_cc[2])**2)
                list.append('%03.3f' % lc_a_cont)
                list.append('%03.3f' % lc_b_cont)
                list.append('%03.3f' % lc_c_cont)
                ratio_a = ((float(lc_a_cont)-float(lc_a_pos))/float(lc_a_pos))*100
                ratio_b = ((float(lc_b_cont)-float(lc_b_pos))/float(lc_b_pos))*100
                ratio_c = ((float(lc_c_cont)-float(lc_c_pos))/float(lc_c_pos))*100
                list.append('%03.3f' % ratio_a)
                list.append('%03.3f' % ratio_b)
                list.append('%03.3f' % ratio_c)

            else:
                list.append('%03.3f' % 0.000)
                list.append('%03.3f' % 0.000)
                list.append('%03.3f' % 0.000)
                list.append('%03.3f' % 0.000)
                list.append('%03.3f' % 0.000)
                list.append('%03.3f' % 0.000)

        else:
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)
            list.append('%03.3f' % 0.000)

        total_list.append(list)

    # Bubble sort of kpoints in total_list
    flag = True
    while flag:
        flag = False
        for i in range(len(total_list)-1):
            if int(total_list[i][0]) == int(total_list[i+1][0]):
                if int(total_list[i][1][0]) > int(total_list[i+1][1][0]):
                    total_list[i],total_list[i+1] = total_list[i+1],total_list[i]
                    flag = True

    # Print total_list[0]
    Lf = "cutoff[eV]   kpoints [R,W,N]   Etot[eV]   Eatom[eV/atom]  pos-a[Ang]  pos-b[Ang]  pos-c[Ang]   con-a[Ang]  con-b[Ang]  con-c[Ang]   a[%]  b[%]  c[%]" + "\n"
    num = 0
    for line in total_list:
        cutoff = str(line[0]) + "  "
        kp = str(line[1][0]) + "x" + str(line[1][1]) + "x" +str(line[1][2]) + "  "
        jud_e = str(line[2]) + "  "
        tot = str(line[3]) + "  "
        atom = str(line[4]) + "  "
        lc_p = str(line[5]) + "  " + str(line[6]) + "  " + str(line[7]) + "  "
        lc_c = str(line[8]) +"  " + str(line[9]) +"  " + str(line[10]) + "  "
        lc_r = str(line[11]) + "  " + str(line[12]) + "  " + str(line[13]) + "\n"
        Lf += cutoff + kp + jud_e + tot + atom + lc_p + lc_c + lc_r
        #f.write(L)
        num += 1
    #f.close()

    file = nowpath + "/Sum_TOTENandLATCON.dat"
    with open(file, 'w') as f: f.write(Lf)

    print("Sum_TOTENandLATCON.dat has been made!")

if __name__ == "__main__":
    get_enandlatcon(os.getcwd())

# END