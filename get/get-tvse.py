#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

stdout = path + "/std.out"
with open(stdout) as fs: stdlines = fs.readlines()

temps = [int(conv_line(l)[2].replace(".","")) for l in stdlines if "T=" in l]
energies = [conv_line(l)[4] for l in stdlines if "T=" in l]
maxt = max(temps)

L = "MAX-T = " + str(maxt) + "\n\n"
print(L)
for i, t in enumerate(temps):
    L += str(i+1) + " " + str(t) + " " + energies[i] + "\n"

with open(path + "/TvsE.out", "w") as ft: ft.write(L)
print("Done!")
