#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, datetime
#import subprocess as sub
import get.getmaxforce as gm
import addmol.readpos as rp
import addmol.helper as hl

def get_surfen(nowpath):
    """
    L_Etot = "------------" ; L_Esurf = "-----" ; L_F = "-----"
    list_SumEn = []
    #dirs_exp = ["tmp", "test", "miss"]
    dirs_exp = ["models","models2","tmp","origin","POSCARs","CONTCARs","scripts","miss1","miss2","miss","PARCHGs","sum_Esurf", "test"]
    slabdirs = sorted([dr for dr in os.listdir(path_slab) if os.path.isdir(path_slab + "/" + dr) and dr not in dirs_exp and "_" in dr])

    path_energybulk = path_slab + "/Ebulk.dat"
    with open(path_energybulk) as feb: eblines = feb.readlines()

    Ebulk_atom = float([hl.conv_line(x) for x in eblines][1][0])
    #print(Ebulk_atom); sys.exit()

    for slabdir in slabdirs:
        print(slabdir)
        path_slabdir = path_slab + "/" + slabdir
        modeldirs = sorted([dr for dr in os.listdir(path_slabdir) if os.path.isdir(path_slabdir + "/" + dr) and dr not in dirs_exp and "_" in dr])

        for modeldir in modeldirs:
            print(modeldir)
            list_En = [] ; append_En = list_En.append ; append_En(modeldir)
            path_modeldir = path_slabdir + "/" + modeldir

            # read vauto-input
            path_ilist = path_modeldir + "/vauto-input"
            if os.path.exists(path_ilist):
                with open(path_ilist, "r") as fi: ilines = fi.readlines()
            else:
                if __name__ == "__main__": print("vauto-input doesn't exist!\n"); continue

            # Calculate the surface area and the number of atoms from POSCAR in calc
            path_calc = path_modeldir + "/calc"
            path_pos_in_calc = path_calc + "/POSCAR"
            with open(path_pos_in_calc) as fp: poslines = fp.readlines()
            mat_lat = rp.get_matrix(poslines)
            S = rp.calc_surface_area(mat_lat)
            N = rp.get_sumofnumofel(poslines)

            # Make vauto-input list
            clist = [hl.conv_line(line) for line in ilines if len(hl.conv_line(line)) > 0 and [val for val in hl.conv_line(line) if val != "#"][0] in ["M","N","A"] and hl.conv_line(line)[1] != "A"]
            clist = [[x for x in l if x != "#"] for l in clist]

            # Get energy from each accuracy calc. #
            #list_tot = []
            for num_prec, x in enumerate(clist):
                if str(x[8]) == "F":
                    dirname = str(num_prec)+"_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])
                elif str(x[8]) == "T":
                    dirname = str(num_prec)+"_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])+"Fd"+str(x[9])

                append_En(dirname)  #a = [] ; a.append(dirname)
                path_dir_in_re = path_modeldir + "/" + dirname
                path_stdout = path_dir_in_re + "/std.out"
                path_outcar = path_dir_in_re + "/OUTCAR"

                # Make MAXFORCE
                if os.path.exists(path_outcar) and os.path.exists(path_stdout): gm.get_maxforce(path_dir_in_re)
                path_maxforce = path_dir_in_re + "/MAXFORCE"
                if os.path.exists(path_maxforce):
                    with open(path_maxforce,"r") as fm: maxflines = fm.readlines()
                    if len(maxflines) == 0: iter = 0; Etot_forout = L_Etot; F_forout = L_F; atom = "----"; Esurf_forout = L_Esurf
                    else:
                        iter = int(hl.conv_line(maxflines[-1])[0])
                        Etot =  float(hl.conv_line(maxflines[-1])[1]); Etot_forout = "%.6f" % Etot
                        F = float(hl.conv_line(maxflines[-1])[2]);     F_forout = "%.3f" % F
                        Esurf = (Etot - Ebulk_atom*N)/(2*S) ;          Esurf_forout = "%.2f" % Esurf
                        atom = hl.conv_line(maxflines[-1])[3]
                else: iter = 0; Etot_forout = L_Etot; F_forout = L_F; atom = "----"; Esurf_forout = L_Esurf
                if not os.path.exists(path_stdout):
                    if __name__ == "__main__": print(dirname + ": std.out does not exist!")
                    sign_iter = "Nn"
                else:
                    with open(path_stdout,"r") as fs: stdline = fs.read()
                    flag_req  = stdline.find("reached required accuracy - stopping structural energy minimisation")
                    flag_wav  = stdline.find("writing wavefunctions")
                    flag_bad1 = stdline.find("VERY BAD NEWS! internal error in subroutine SGRCON:")
                    flag_bad2 = stdline.find("VERY BAD NEWS! internal error in subroutine IBZKPT:")
                    flag_dav  = stdline.find("DAV:   1")
                    if flag_req >= 0 and iter > 0: sign_iter = "R" + str(iter)
                    elif flag_wav >= 0 and iter > 0: sign_iter = "W" + str(iter)
                    else:
                        if iter > 0: sign_iter = "N" + str(iter)
                        elif iter == 0 and flag_bad1 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                        elif iter == 0 and flag_bad2 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                        else: sign_iter = "N" + str(iter)
                list_En += [sign_iter, Etot_forout, F_forout, atom, Esurf_forout]
                #a += [sign_iter, Etot_forout, F_forout, atom, Esurf_forout]
                #list_tot.append(a)
            list_SumEn.append(list_En)

    list_row = []
    max_list_Sum = max([len(x) for x in list_SumEn])
    for i in range(len(list_SumEn)):
        if len(list_SumEn[i]) < max_list_Sum:
            exlist = ["--" for val in range(max_list_Sum - len(list_SumEn[i]))]
            list_SumEn[i].extend(exlist)
    for i in range(max_list_Sum): list_row.append([row[i] for row in list_SumEn])
    for row in list_row:
        max_x = max([len(x) for x in row])
        i = 0
        for x in row:
            if len(x) < max_x:
                while len(x) < max_x: x += " "
            row[i] = x ; i += 1

    ### Make Sum_Eads.dat ###
    L = ""
    for i2 in range(len(list_row[0])):
        for i1 in range(len(list_row)): L += str(list_row[i1][i2]) + " "
        else: L += "\n"
    with open(path_slab + "/Sum_Ensurf.dat", "w") as fse: fse.write(L)

    return L
    """

    ##################################   INPUT ZONE   ##################################
    list_rm = ["models","models2","tmp","origin","POSCARs","CONTCARs","scripts","miss1","miss2","miss","PARCHGs","sum_Esurf"]
    L_Etot = "------------" ; L_Esurf = "-----" ; L_F = "-----"
    filename = "Sum_Esurf"
    firstline = "dir  [R,W,N]  Etot[eV]  Force[eV/A, atom]  Esurf[eV] | "
    ##################################   INPUT ZONE   ##################################

    def conv_line(line):
        line = line.replace('\n'," ").replace('\r'," ").replace('\t'," ").replace('^M'," ")
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path_dir_sumEsurf = nowpath + "/sum_Esurf"
    if not os.path.exists(path_dir_sumEsurf): os.mkdir(path_dir_sumEsurf)
    sumEsurfs = sorted([x for x in os.listdir(nowpath) if filename in x])
    if sumEsurfs != 0:
        for x in sumEsurfs: shutil.move(nowpath + "/" + x, path_dir_sumEsurf)

    # read Esup
    path_Ebulk = nowpath + "/Ebulk.dat"
    with open(path_Ebulk) as feb: eblines = feb.readlines()
    Ebulk_atom = float([hl.conv_line(x) for x in eblines][1][0])

    ### Get total energies from each adsorption models ###
    dirs_slab = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x) and x not in list_rm and "_" in x])
    L = firstline + "  Ebulk_atom: " + str(Ebulk_atom) + "\n"

    for dir_slab in dirs_slab:
        print(dir_slab)
        path_slab = nowpath + "/" + dir_slab
        dirs_eachslab = sorted([x for x in os.listdir(path_slab) if os.path.isdir(path_slab + "/" + x) and x not in list_rm and "_" and x])

        ## Loop: adsorption models ##
        list_SumEn = []
        for dir_eachslab in dirs_eachslab:
            list_En = [] ; append_En = list_En.append ; append_En(dir_eachslab)
            if __name__ == "__main__": print(dir_eachslab)

            # Read vauto-input file
            path_dir = path_slab + "/" + dir_eachslab
            path_ilist = path_dir + "/vauto-input"
            if os.path.exists(path_ilist):
                with open(path_ilist) as fi: ilines = fi.readlines()
            else:
                if __name__ == "__main__":
                    print("vauto-input doesn't exist!\n"); continue

            path_calc = path_dir + "/calc"
            path_pos_in_calc = path_calc + "/POSCAR"
            with open(path_pos_in_calc) as fpc: poslines = fpc.readlines()
            sum_num_atoms = rp.get_sumofnumofel(poslines)
            mat_lat = rp.get_matrix(poslines)
            surface_area = rp.calc_surface_area(mat_lat)

            # Make vauto-input list
            clist = [conv_line(l) for l in ilines if len(conv_line(l)) > 0 and [v for v in conv_line(l) if v != "#"][0] in ["M","N","A"] and conv_line(l)[1] != "A"]
            clist = [[x for x in l if x != "#"] for l in clist]

            # Get energy from each accuracy calc. #
            list_tot = []
            for num_prec, x in enumerate (clist):
                pr = str(x[0]); cut = str(x[1])
                kx = str(x[2]); ky = str(x[3]); kz = str(x[4]); ed = str(x[7])

                if str(x[8]) == "F":
                    dirname = str(num_prec) + "_Prec" + pr + "cut" + cut + "k" + kx + "x" + ky + "x" + kz + "Ed" + ed
                elif str(x[8]) == "T":
                    dirname = str(num_prec) + "_Prec" + pr + "cut" + cut + "k" + kx + "x" + ky + "x" + kz + "Ed" + ed + "Fd" + str(x[9])

                print(dirname)
                # if __name__ == "__main__": print dir_eachslab + " " + dirname
                a = [] ; a.append(dirname) ; append_En(dirname)
                path_dir_in_re = path_dir + "/" + dirname
                path_stdout = path_dir_in_re + "/std.out"
                path_outcar = path_dir_in_re + "/OUTCAR"

                # read stdout
                #with open(path_stdout) as fs: stdline = fs.read()
                #if "VERY BAD NEWS! internal error in subroutine SGRCON:" in stdline:
                #    print(dirname); sys.exit()
                #    continue
                
                # Make MAXFORCE
                if os.path.exists(path_outcar) and os.path.exists(path_stdout): gm.get_maxforce(path_dir_in_re)
                path_maxforce = path_dir_in_re + "/MAXFORCE"

                if os.path.exists(path_maxforce):
                    with open(path_maxforce,"r") as fm: maxflines = fm.readlines()
                    if len(maxflines) == 0:
                        iter = 0
                        Etot_forout = L_Etot
                        F_forout = L_F
                        atom = "----"
                        Esurf_forout = L_Esurf
                    else:
                        iter = int(conv_line(maxflines[-1])[0])
                        Etot = float(conv_line(maxflines[-1])[1])
                        Etot_forout = "%.6f" % Etot

                        F = float(conv_line(maxflines[-1])[2])
                        F_forout = "%.3f" % F

                        Esurf = (Etot - sum_num_atoms * Ebulk_atom)/(2*surface_area) * 16.0217663400
                        Esurf_forout = "%.2f" % Esurf
                        atom = conv_line(maxflines[-1])[3]
                else:
                    iter = 0
                    Etot_forout = L_Etot
                    F_forout = L_F
                    atom = "----"
                    Esurf_forout = L_Esurf

                if not os.path.exists(path_stdout):
                    if __name__ == "__main__":
                        print(dirname + ": std.out does not exist!")
                    sign_iter = "Nn"
                else:
                    with open(path_stdout,"r") as fs: stdline = fs.read()
                    flag_req  = stdline.find("reached required accuracy - stopping structural energy minimisation")
                    flag_wav  = stdline.find("writing wavefunctions")
                    flag_bad1 = stdline.find("VERY BAD NEWS! internal error in subroutine SGRCON:")
                    flag_bad2 = stdline.find("VERY BAD NEWS! internal error in subroutine IBZKPT:")
                    flag_dav  = stdline.find("DAV:   1")

                    if flag_req >= 0 and iter > 0: sign_iter = "R" + str(iter)
                    elif flag_wav >= 0 and iter > 0: sign_iter = "W" + str(iter)
                    else:
                        if iter > 0: sign_iter = "N" + str(iter)
                        elif iter == 0 and flag_bad1 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                        elif iter == 0 and flag_bad2 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                        else: sign_iter = "N" + str(iter)

                list_En += [sign_iter, Etot_forout, F_forout, atom, Esurf_forout]
                a += [sign_iter, Etot_forout, F_forout, atom, Esurf_forout]
                list_tot.append(a)

            # Make TOTEN.dat in each adcalc-directory. #
            else:
                list_SumEn.append(list_En)
                Lf = ""
                for list_e in list_tot: Lf += "  ".join([str(x) for x in list_e]) + "\n"
                with open(path_dir + "/TotalEnergy.dat",'w') as f: f.write(Lf)
        ### End: Get total energies from each-adsorption models ###

        list_row = []
        if list_SumEn == []: continue
        max_list_Sum = max([len(x) for x in list_SumEn])

        for i in range(len(list_SumEn)):
            if len(list_SumEn[i]) < max_list_Sum:
                exlist = ["--" for val in range(max_list_Sum - len(list_SumEn[i]))]
                list_SumEn[i].extend(exlist)

        for i in range(max_list_Sum): list_row.append([row[i] for row in list_SumEn])
        for row in list_row:
            max_x = max([len(x) for x in row])
            i = 0
            for x in row:
                if len(x) < max_x:
                    while len(x) < max_x: x += " "
                row[i] = x ; i += 1

        ### Make Sum_Esurf.dat ###
        for i2 in range(len(list_row[0])):
            for i1 in range(len(list_row)): L += str(list_row[i1][i2]) + " "
            L += "\n"
        L += "\n"

    return L
# End: Definiition of fucntion


if __name__ == "__main__":
    time = "{0:%Y%m%d%H%M}".format(datetime.datetime.today())
    L = get_surfen(os.getcwd())
    with open(os.getcwd() + "/Sum_Esurf" + "_" + time + ".dat", "w") as fs: fs.write(L)
    print("\n" + L)
### End: Program ###
