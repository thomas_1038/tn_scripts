#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob
import addmol.helper as hl
import addmol.readpos as rp
import addmol.mk_poscar as mkp

class Get_pcs:
    def make_dir(self, path_pc):
        if not os.path.exists(path_pc): os.mkdir(path_pc)
        else:
            files_in_pcs = [x for x in os.listdir(path_pc)]
            for x in files_in_pcs: os.remove(path_pc + "/" + x)

    def get_pcs_from_dir(self, nowpath):
        stdout = "std.out"
        ind_iter = "F="
        ind_req = "reached required accuracy - stopping structural energy minimisation"
        ind_wav = "writing wavefunctions"

        path_poscars = nowpath + "/POSCARs"; path_contcars = nowpath + "/CONTCARs"
        self.make_dir(path_poscars); self.make_dir(path_contcars)

        ### Get total energies from each adsorption models ###
        list_exp = ["baders","sum_Eads","models","POSCARs","tmp","origin","CONTCARs","scripts","candidates","miss1","miss2","miss","PARCHGs"]
        direcs_adcalc = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" + x) and x not in list_exp])
        print("\nGet POSCAR/CONTCAR from directories!\n")

        list_pos_tot = []; list_cont_tot = []
        ## Loop: adsorption models ##
        for direc in direcs_adcalc:
            print(direc)
            path_dir = nowpath + "/" + direc
            path_calc = path_dir + "/calc"
            if not os.path.exists(path_calc):
                continue

            path_pos = path_dir + "/calc/POSCAR"
            path_pos_new = path_poscars + "/POSCAR_" + direc + ".vasp"
            shutil.copy(path_pos, path_pos_new)
            list_pos_tot.append(path_pos_new)

            # Read vauto-input file
            path_ilist = path_dir + "/vauto-input"
            if os.path.exists(path_ilist):
                with open(path_ilist) as fi: ilines = fi.readlines()
            else:
                if __name__ == "__main__": print("\nvauto-input doesn't exist!\n")
                continue

            # Make vauto-input list
            clist = [hl.conv_line(l) for l in ilines if len(hl.conv_line(l)) > 0 and [val for val in hl.conv_line(l) if val != "#"][0] in ["M","N","A"] and hl.conv_line(l)[1] != "A"]
            clist = [[x for x in l if x != "#"] for l in clist]
            list_con = []

            for num_prec, rd in enumerate(clist):
                if str(rd[8])=="F":
                    dirname = str(num_prec) + "_Prec"+str(rd[0])+"cut"+str(rd[1])+"k"+str(rd[2])+"x"+str(rd[3])+"x"+str(rd[4])+"Ed"+str(rd[7])
                elif str(rd[8])=="T":
                    dirname = str(num_prec) + "_Prec"+str(rd[0])+"cut"+str(rd[1])+"k"+str(rd[2])+"x"+str(rd[3])+"x"+str(rd[4])+"Ed"+ str(rd[7])+"Fd"+str(rd[9])
                path_dir_in_re = path_dir + "/" + dirname
                path_stdout = path_dir_in_re + "/" + stdout
                path_cont = path_dir_in_re + "/CONTCAR"

                # Read std.out #
                if os.path.exists(path_stdout):
                    with open(path_stdout) as fs: stdline = fs.read()
                    iter = stdline.count(ind_iter)
                    flag_req = stdline.find(ind_req); flag_wav = stdline.find(ind_wav)
                    if flag_req >= 0 and iter > 0: sign_re = "R" + str(iter); list_con.append([path_cont, dirname])
                    elif flag_wav >= 0 and iter > 0: sign_re = "W" + str(iter); list_con.append([path_cont, dirname])
                    elif iter > 0: sign_re = "N" + str(iter); list_con.append([path_cont, dirname])

            if len(list_con) > 0:
                path_cont_old = str(list_con[-1][0])
                name_cont_new = direc + "_" + list_con[-1][1] + "_" + sign_re +".vasp"
                path_cont_new = path_contcars + "/" + name_cont_new

                with open(path_cont_old) as fc: contlines = fc.readlines()
                labelofel = rp.get_labelofel(contlines)
                numofel = rp.get_numofel(contlines)
                sumofnumofel = rp.get_sumofnumofel(contlines)
                mat_lat = rp.get_matrix(contlines)
                eachlabels_el = rp.mk_labelofel(labelofel, numofel)[0]
                firstlines, flagS = rp.get_firstlines(contlines)
                coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumofnumofel, mat_lat)
                mkp.mk_pos(path_contcars, name_cont_new, firstlines, mat_lat, labelofel, numofel, eachlabels_el, flagS, coor_car, list_SD)
                list_cont_tot.append(path_cont_new)

        mkp.mk_poss(path_poscars); mkp.mk_poss(path_contcars)
        print("\nDONE!\n")
        return list_pos_tot, list_cont_tot

    def get_pcs_from_dirs(self, nowpath):
        path_poscars = nowpath + "/POSCARs"; path_contcars = nowpath + "/CONTCARs"
        self.make_dir(path_poscars); self.make_dir(path_contcars)

        list_exp = ["baders","sum_Eads","models","POSCARs","tmp","origin","CONTCARs","scripts","candidates","miss1","miss2","miss","PARCHGs"]
        dirs_ad = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" + x) and x not in list_exp])

        for dir_ad in dirs_ad:
            print("\n" + dir_ad)
            list_pospath, list_contpath = self.get_pcs_from_dir(nowpath + "/" + dir_ad)
            for p in list_pospath: shutil.copy(p, path_poscars)
            for c in list_contpath: shutil.copy(c, path_contcars)

        mkp.mk_poss(path_poscars); mkp.mk_poss(path_contcars)

if __name__ == "__main__":
    gp = Get_pcs()
    nowpath = os.getcwd()
    argv = sys.argv

    if len(argv) == 1: gp.get_pcs_from_dir(nowpath)
    elif len(argv) > 1 and argv[1] == "0": gp.get_pcs_from_dir(nowpath)
    elif len(argv) > 1 and argv[1] == "1": gp.get_pcs_from_dirs(nowpath)

    print("\nDONE!\n")
# END: Program