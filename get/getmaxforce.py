#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script

import os, sys, math
import addmol.helper as hl

def get_maxforce(nowpath, filename="MAXFORCE", stdout="std.out"):
    """
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line
    """

    ind_force ="TOTAL-FORCE"
    ind_en1 = "FREE ENERGIE OF THE ION-ELECTRON"
    ind_en2 = "energy without entropy"
    path_out = nowpath + "/OUTCAR"
    path_pos = nowpath + "/POSCAR"
    path_con = nowpath + "/CONTCAR"
    path_std1 = nowpath + "/" + stdout
    path_std2 = nowpath + "/stdout"

    if os.path.exists(path_std1):  path_std = path_std1
    elif os.path.exists(path_std2): path_std = path_std2
    else: print("There is no std.out! You should prepare it. BYE!"); sys.exit()

    if not os.path.exists(path_pos):
        if os.path.exists(path_con): path_pos = path_con
        else: print("POSCAR does not exists in " + str(nowpath)  + "! BYE!"); sys.exit()


    # read std.out
    with open(path_std) as fs: stdline = fs.read()
    if "VERY BAD NEWS! internal error in subroutine SGRCON:" in stdline:
        with open(nowpath + "/" + filename,'w') as fm: fm.write("")
        list_maxFtot = []

    # read POSCAR
    with open(path_pos) as fp: poslines = fp.readlines()
    if not poslines:
        with open(nowpath + "/" + filename,'w') as fm: fm.write("")
        list_maxFtot = []

    else:
        atomlabel = hl.conv_line(poslines[5]); atomnum = hl.conv_line(poslines[6]); sumatomnum = sum([int(x) for x in atomnum])
        na = 0; list_atomlabel = []
        while na < len(atomlabel):
            na2 = 0
            while na2 < int(atomnum[na]): list_atomlabel.append(atomlabel[na]+str(na2+1)); na2 += 1
            na += 1

        flagS = 0
        if "Selective" in poslines[7]: flagS = 1

        #make atom positon matrix
        num2 = 8 + flagS; atomposition = []
        while num2 < 8 + sumatomnum + flagS: atomposition.append(hl.conv_line(poslines[num2])); num2 += 1

        #make fix word matrix
        num3 = 0; fixword = []
        while num3 < len(atomposition):
            a = []
            if flagS == 1: a.append(str(atomposition[num3][3]))
            a.append(list_atomlabel[num3])
            fixword.append(a); num3 += 1
        # END: read POSCAR

        # read OUTCAR
        if not os.path.exists(path_out): print("OUTCAR doesn't exist! BYE!") ; sys.exit()
        with open(path_out,"r") as fo: outlines = fo.readlines()

        #Definition of the variable
        n1 = 0; num_f = 0; Etot = []; Ftot = []; Ftot_relax = []
        Etot_append = Etot.append; Ftot_append = Ftot.append; Ftot_relax_append = Ftot_relax.append
        #Start: while
        while n1 < len(outlines):
            if ind_en1 in outlines[n1]:
#                print(hl.conv_line(outlines[n1+4])); sys.exit()
                Etot_append(hl.conv_line(outlines[n1+4])[6])
                n1 += 4

            if ind_force not in outlines[n1]: n1 += 1
            else:
                num_f += 1; n1 += 2; fnum = int(n1) + int(sumatomnum)

                num_a = 0; list_Fi = []; list_Fi_relax = []
                list_Fi_append = list_Fi.append; list_Fi_relax_append = list_Fi_relax.append
                while n1 < fnum:
                    Fi_atom = outlines[n1].replace("\n","").split(" ")
                    while Fi_atom.count("") > 0: Fi_atom.remove("")
                    Fi_atom.pop(0); Fi_atom.pop(0); Fi_atom.pop(0)

                    ftot = math.sqrt(float(Fi_atom[0])**2+ float(Fi_atom[1])**2 + float(Fi_atom[2])**2)
                    Fi_atom += [str(ftot), str(fixword[num_a][0])]

                    if flagS == 1 :
                        Fi_atom.append(str(fixword[num_a][1]))
                        if str(fixword[num_a][0]) == "T": list_Fi_relax_append(ftot)
                    else: list_Fi_relax_append(ftot)

                    list_Fi_append(Fi_atom)
                    num_a += 1; n1 += 1

                Ftot_append(list_Fi); Ftot_relax_append(list_Fi_relax)

                if num_f == len(Etot): break
                else: continue

        #make maxforcelist
        list_num = [str(i) for i in range(1, len(Ftot)+1)]
        list_maxF = [str(max(x)) for x in Ftot_relax]

        #make list_totalmaxforce
        list_maxFlabel = []; list_maxFlabel_append = list_maxFlabel.append
        for n1, Fi in enumerate(Ftot):
            for Fatom in Fi:
                if Fatom[3].find(str(list_maxF[n1])) >= 0:
                    if flagS == 1: list_maxFlabel_append(Fatom[5])
                    else: list_maxFlabel_append(Fatom[4])
                    break

        list_maxFtot = [list_num, Etot, list_maxF, list_maxFlabel]
        list_for_output = [hl.adjust_len(x) for x in list_maxFtot]

        L = ""
        for i1 in range(0,len(list_for_output[0])):
            L += " " + "  ".join([str(list_for_output[i2][i1]) for i2 in range(0,len(list_for_output))]) + "\n"

        with open(nowpath + "/" + filename,'w') as fm: fm.write(L)

        #for x in list_maxFtot: print(len(x))
    return list_maxFtot

if __name__  == "__main__": get_maxforce(os.getcwd()); print("MAXFORCE has been made!")
# End: Program
