#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, datetime
#import subprocess as sub
from get import getmaxforce as gm

##################################   INPUT ZONE   ##################################
list_rm = ["models","models2","tmp","origin","POSCARs","CONTCARs","scripts","candidates","miss1","miss2","miss","PARCHGs","backup"]
L_Etot = "------------" ; L_Eads = "-----" ; L_F = "-----"
filename = "Sum_Eads"
firstline = "dir  [R,W,N]  Etot[eV]  Force[eV/A, atom]  Eads[eV] | "
##################################   INPUT ZONE   ##################################

ECF = input("Which do you use the EC-functional? [pbe/p, rpbe/r]: ")
if "d" == ECF:
    E_H2      = -.67727580E+01 ; E_N2      = -.16641889E+02 ; E_O2      = -.98650541E+01
    E_F2      = -.35625839E+01 ; E_CO      = -.14781615E+02 ; E_H2O     = -.14224623E+02
    E_CO2     = -.22963059E+02 ; E_C2H2    = -.22950427E+02 ; E_NH3     = -.19543029E+02
    E_CH4     = -.24045410E+02 ; E_C2H4    = -.31980343E+02 ; E_C2H6    = -.40404340E+02
    E_CH3OH   = -.30219328E+02 ; E_HCOOH   = -.29721737E+02 ; E_H2O2    = -.18146803E+02
    E_C6H6    = -.76023525E+02 ; E_C6H5OH  = -.82527370E+02 ; E_C6H5NO2 = -.91417363E+02
    E_C6H5NH2 = -.87886115E+02 ; E_N2H4    = -.30302714E+02

elif "p" == ECF or "pbe" == ECF or "PBE" == ECF:
    E_H2      = -.67727580E+01 ; E_N2      = -.16641889E+02 ; E_O2      = -.98650541E+01
    E_F2      = -.35625839E+01 ; E_CO      = -.14781615E+02 ; E_H2O     = -.14224623E+02
    E_CO2     = -.22963059E+02 ; E_C2H2    = -.22950427E+02 ; E_NH3     = -.19543029E+02
    E_CH4     = -.24045410E+02 ; E_C2H4    = -.31980343E+02 ; E_C2H6    = -.40404340E+02
    E_CH3OH   = -.30219328E+02 ; E_HCOOH   = -.29721737E+02 ; E_H2O2    = -.18146803E+02
    E_C6H6    = -.76023525E+02 ; E_C6H5OH  = -.82527370E+02 ; E_C6H5NO2 = -.91417363E+02
    E_C6H5NH2 = -.87886115E+02 ; E_N2H4    = -.30302714E+02

elif "r"== ECF or "rpbe" == ECF or "RPBE" == ECF:
    E_H2      = -.69888458E+01 ; E_N2      = -.16270362E+02; E_O2      = -.95723895E+01
    E_CO      = -.14423735E+02;  E_H2O     = -.14147657E+02

### Definition of functions ###
def conv_line(line):
    line = line.replace('\n'," ").replace('\r'," ").replace('\t'," ").replace('^M'," ")
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

def mkEmol(list_Esup):
    list_Emol = []; list_Lmol = []
    for i in list_Esup:
        Q_num_admol = input("\nPlease input the number of adsorption molecules, e.g, H2 & N2 =>2: ")
        num_admol = int(Q_num_admol)
        Emol = 0; L_admol = "Adsorbate: "

        for num in range(num_admol):
            Q_moltype = input("Please input the name of molecules, [H2, h2 / N2, n2 / O2, o2 / F2, f2 / CO, co / H2O, h2o / CO2, co2 / C2H2, c2h2 / NH3, nh3 / CH4, ch4 /C2H4, c2h4 / C2H6, c2h6 / CH3OH, ch3oh / HCOOH, hcooh / H2O2, h2o2 /  C6H6, c6h6 / C6H5OH, c6h5oh / C6H5NO2, c6h5no2 / C6H5NH2, c6h5nh2 / other, ot]: ")
            if Q_moltype in ["H2","h2"]:
                Q_1or2 = input("Do you use energy of adatom or molecule, [1 or 2]: ")
                if Q_1or2 == "1": Emol += float(E_H2)/2 ; L_admol += "H "
                elif Q_1or2 == "2": Emol += float(E_H2) ; L_admol += "H2 "
            elif Q_moltype in ["N2","n2"]:
                Q_1or2 = input("Do you use energy of adatom or molecule, [1 or 2]: ")
                if Q_1or2 == "1": Emol += float(E_N2)/2 ; L_admol += "N "
                elif Q_1or2 == "2": Emol += float(E_N2) ; L_admol += "N2 "
            elif Q_moltype in ["O2","o2"]:
                Q_1or2 = input("Do you use energy of adatom or molecule, [1 or 2]: ")
                if Q_1or2 == "1": Emol += float(E_O2)/2 ; L_admol += "O "
                elif Q_1or2 == "2": Emol += float(E_O2) ; L_admol += "O2 "
            elif Q_moltype in ["F2","f2"]:
                Q_1or2 = input("Do you use energy of adatom or molecule, [1 or 2]: ")
                if Q_1or2 == "1": Emol += float(E_F2)/2 ; L_admol += "F "
                elif Q_1or2 == "2": Emol += float(E_F2) ; L_admol += "F2 "
            elif Q_moltype in ["CO","co"]: Emol += float(E_CO) ; L_admol += "CO "
            elif Q_moltype in ["H2O","h2o"]: Emol += float(E_H2O) ; L_admol += "H2O "
            elif Q_moltype in ["CO2","co2"]: Emol += float(E_CO2) ; L_admol += "CO2 "
            elif Q_moltype in ["C2H2","c2h2"]: Emol += float(E_C2H2) ; L_admol += "C2H2 "
            elif Q_moltype in ["C2H4","c2h4"]: Emol += float(E_C2H4) ; L_admol += "C2H4 "
            elif Q_moltype in ["C2H6","c2h6"]: Emol += float(E_C2H6) ; L_admol += "C2H6 "
            elif Q_moltype in ["CH4","ch4"]: Emol += float(E_CH4) ; L_admol += "CH4 "
            elif Q_moltype in ["NH3","nh3"]: Emol += float(E_NH3) ; L_admol += "NH3 "
            elif Q_moltype in ["CH3OH,""ch3oh"]: Emol += float(E_CH3OH) ; L_admol += "CH3OH "
            elif Q_moltype in ["HCOOH","HCOOH"]: Emol += float(E_HCOOH) ; L_admol += "HCOOH "
            elif Q_moltype in ["H2O2","h2O2"]: Emol += float(E_H2O2) ; L_admol += "H2O2 "
            elif Q_moltype in ["C6H6","c6h6"]: Emol += float(E_C6H6) ; L_admol += "C6H6 "
            elif Q_moltype in ["C6H5OH","c6h5oh"]: Emol += float(E_C6H5OH) ; L_admol += "C6H5OH "
            elif Q_moltype in ["C6H5NO2","c6h5no2"]: Emol += float(E_C6H5NO2) ; L_admol += "C6H5NO2 "
            elif Q_moltype in ["C6H5NH2","c6h5nh2"]: Emol += float(E_C6H5NH2) ; L_admol += "C6H5NH2 "
            elif Q_moltype in ["other","ot"]:
                Q_Emol = input("Please input the value of Emol: ")
                Emol += float(Q_Emol)
                L_admol += input("Please input the name of adsorbate: ")
            Qpn = input("Do you consider the defect formation? [y/N]: ")
            if "y" in Qpn: Emol = -1 * Emol

        else: list_Emol.append(Emol); list_Lmol.append(L_admol)
    return list_Emol, list_Lmol
### End:  Definition of functions ###

# path
path = os.getcwd()

# read Esup
path_Esup = path + "/Esup.dat"
if os.path.exists(path_Esup):
    with open(path_Esup,"r") as fsup: esuplines = fsup.readlines()
    list_Esup = [float(conv_line(x)[0]) for x in esuplines if "#" not in x]
    list_Lsup = [conv_line(x)[1] for x in esuplines if "#" not in x]
else:
    Q_Esup = input("Please input the total energy of adsorbate: ")
    Q_Lsup = input("Please input the name of the substrate: ")
    list_Esup = [float(Q_Esup)]; list_Lsup = [str(Q_Esup)]
    with open(path_Esup, "w") as fs: fs.write(Q_Esup + " " + Q_Lsup + "\n")

Le = "Esup: "; i = 0
#Le = "Sup: " + str(list_Esup[1]) + "; Esup: " + str(list_Esup[0]) + " "
for x in list_Esup: Le += list_Lsup[i] + ": " + str(x) + " "; i += 1
print("\n" + Le)

# read Emol
path_Emol = path + "/Emol.dat"
if os.path.exists(path_Emol):
    with open(path_Emol, "r") as fsup: emollines = fsup.readlines()
    list_Emol = [float(conv_line(x)[0]) for x in emollines if "#" not in x]
    list_Lmol = [conv_line(x)[1] for x in emollines if "#" not in x]
    if len(list_Emol) != len(list_Esup): 
        print("\nYou should write the same number of Emol in Emol.dat! BYE!\n"); sys.exit()
else:
    list_Emol, list_Lmol = mkEmol(list_Esup)
    Lm = ""
    for i, emol in enumerate(list_Emol): Lm += str(emol) + " " + str(list_Lmol[i]) + "\n"
    with open(path_Emol, "w") as fm: fm.write(Lm)

Lm = "Emol: "; i = 0
for x in list_Esup: Lm += list_Lmol[i] + ": " + str(list_Emol[i]) + " "; i += 1
print(Lm + "\n")
### End: Question ###

def get_aden(Esup, Emol, Lsup, Lmol):
    ### Get total energies from each adsorption models ###
    direcs_adcalc = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and x not in list_rm]
    direcs_adcalc.sort()

    ## Loop: adsorption models ##
    list_SumEn = []

    for direc in direcs_adcalc:
        list_En = [] ; append_En = list_En.append ; append_En(direc)
        if __name__ == "__main__": print(direc)

        # Read vauto-input file
        path_dir = path + "/" + direc
        path_ilist = path_dir + "/vauto-input"
        if os.path.exists(path_ilist) == True:
            with open(path_ilist, "r") as fi: ilines = fi.readlines()
        else:
            if __name__ == "__main__": print("vauto-input doesn't exist!\n"); continue

        # Make vauto-input list
        clist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0 and [val for val in conv_line(line) if val != "#"][0] in ["M","N","A"] and conv_line(line)[1] != "A"]
        clist = [[x for x in l if x != "#"] for l in clist]

        # Get energy from each accuracy calc. #
        list_tot = []
        for num_prec,x in enumerate(clist):
            if str(x[8]) == "F":
                dirname = str(num_prec) + "_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])
            elif str(x[8]) == "T":
                dirname = str(num_prec) + "_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])+"Fd"+str(x[9])

            # if __name__ == "__main__": print direc + " " + dirname
            a = [] #; a.append(dirname) ; append_En(dirname)
            a.append(str(num_prec)) ; append_En(str(num_prec))
            path_dir_in_re = path_dir + "/" + dirname
            path_stdout = path_dir_in_re + "/std.out"
            path_outcar = path_dir_in_re + "/OUTCAR"

            # Make MAXFORCE
            #os.chdir(path_dir_in_re)
            if os.path.exists(path_outcar) == True and os.path.exists(path_stdout) == True: gm.get_maxforce(path_dir_in_re)
            path_maxforce = path_dir_in_re + "/MAXFORCE"

            if os.path.exists(path_maxforce):
                with open(path_maxforce,"r") as fm: maxflines = fm.readlines()
                if len(maxflines) == 0: iter = 0; Etot_forout = L_Etot; F_forout = L_F; atom = "----"; Eads_forout = L_Eads
                else:
                    iter = int(conv_line(maxflines[-1])[0])
                    Etot =  float(conv_line(maxflines[-1])[1]); Etot_forout = "%.6f" % Etot
                    F = float(conv_line(maxflines[-1])[2]);     F_forout = "%.3f" % F
                    Eads = Etot - Emol - Esup;                  Eads_forout = "%.2f" % Eads
                    atom = conv_line(maxflines[-1])[3]
            else: iter = 0; Etot_forout = L_Etot; F_forout = L_F; atom = "----"; Eads_forout = L_Eads

            if os.path.exists(path_stdout) == False:
                if __name__ == "__main__": print(dirname + ": std.out does not exist!")
                sign_iter = "Nn"
            else:
                with open(path_stdout,"r") as fs: stdline = fs.read()
                flag_req  = stdline.find("reached required accuracy - stopping structural energy minimisation")
                flag_wav  = stdline.find("writing wavefunctions")
                flag_bad1 = stdline.find("VERY BAD NEWS! internal error in subroutine SGRCON:")
                flag_bad2 = stdline.find("VERY BAD NEWS! internal error in subroutine IBZKPT:")
                flag_dav  = stdline.find("DAV:   1")

                if flag_req >= 0 and iter > 0: sign_iter = "R" + str(iter)
                elif flag_wav >= 0 and iter > 0: sign_iter = "W" + str(iter)
                else:
                    if iter > 0: sign_iter = "N" + str(iter)
                    elif iter == 0 and flag_bad1 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                    elif iter == 0 and flag_bad2 >= 0 and flag_dav >= 0: sign_iter = "IBE" + str(iter)
                    else: sign_iter = "N" + str(iter)

            list_En += [sign_iter, Etot_forout, F_forout, atom, Eads_forout]
            a += [sign_iter, Etot_forout, F_forout, atom, Eads_forout]
            list_tot.append(a)

        # Make TOTEN.dat in each adcalc-directory. #
        else:
            list_SumEn.append(list_En)
            Lf = ""
            for list_e in list_tot: Lf += "  ".join([str(x) for x in list_e]) + "\n"
            with open(path_dir + "/TotalEnergy.dat",'w') as f: f.write(Lf)
    ### End: Get total energies from each-adsorption models ###

    list_row = []
    max_list_Sum = max([len(x) for x in list_SumEn])
    #min_list_Sum = min([len(x) for x in list_SumEn])

    for i in range(len(list_SumEn)):
        if len(list_SumEn[i]) < max_list_Sum:
            exlist = ["--" for val in range(max_list_Sum - len(list_SumEn[i]))]
            list_SumEn[i].extend(exlist)

    for i in range(max_list_Sum): list_row.append([row[i] for row in list_SumEn])
    for row in list_row:
        max_x = max([len(x) for x in row])
        i = 0
        for x in row:
            if len(x) < max_x:
                while len(x) < max_x: x += " "
            row[i] = x ; i += 1

    ### Make Sum_Eads.dat ###
    L = firstline + Lmol + "  Emol: " + str(Emol) + "  Sup: " + Lsup + "  Esup: " + str(Esup) + "\n"
    for i2 in range(len(list_row[0])):
        for i1 in range(len(list_row)): L += str(list_row[i1][i2]) + " "
        else: L += "\n"
    return L

# End: Definiition of fucntion

num = 0 ; Lsum = ""
for Esup in list_Esup:
    Lsum += get_aden(Esup, list_Emol[num], list_Lsup[num], list_Lmol[num]) + "\n"
    num += 1
    if __name__ == "__main__": print("")

# move old Sum_Eads.dats to sum_Eads
#path_dir_sumEads = path + "/sum_Eads"
#if not os.path.exists(path_dir_sumEads): os.mkdir(path_dir_sumEads)
#sumEadss = sorted([x for x in os.listdir(path) if filename in x])
#if sumEadss != 0:
#    for x in sumEadss: shutil.move(path + "/" + x, path_dir_sumEads)

# make new Sum_Eads.dat
#time = "{0:%Y%m%d%H%M}".format(datetime.datetime.today())
with open(path + "/" + filename + ".dat", "w") as fs: fs.write(Lsum)
if __name__ == "__main__": print("\n" + Lsum)
### End: Program ###
