#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, re, sys, glob, shutil, threading
import subprocess
import numpy as np
import pandas as pd
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy import genfromtxt

path = os.getcwd()
argv = sys.argv

# ファイル読み込み
#d = genfromtxt(path+"/"+argv[1], delimiter=",")
d = pd.read_csv(path+"/"+argv[1], header=None, delimiter=r",", names=[0,1,2,3])
d0 = d.iloc[:,0].values
d1 = d.iloc[:,1].values
d2 = d.iloc[:,2].values
d3 = d.iloc[:,3].values

# グラフ作成
fig = pyplot.figure()
#ax = Axes3D(fig)
ax = fig.gca(projection='3d')

# 軸ラベルの設定
ax.set_xlabel("N-H ($\AA$)",fontsize=12)
ax.set_ylabel("N-H ($\AA$)",fontsize=12)
ax.set_zlabel("N-Ru ($\AA$)",fontsize=12)

# 表示範囲の設定
ax.set_xlim(0.5, 3.5) # N-H
ax.set_ylim(0.5, 3.5) # O-H
ax.set_zlim(0.5, 3.5) # N-O

# 抽出条件設定
#d1 = d[d[:,0] >= 7]
#d2 = d[(d[:,0] < 7) & ((d[:,1] > 3) & (d[:,1] <= 3.5))]
#d3 = d[(d[:,0] < 7) & ((d[:,1] <= 3) | (d[:,1] > 3.5))]

# グラフ描画
scatter = ax.scatter3D(d0, d1, d2,
                       s=100,                   # マーカーのサイズ
                       c=d3,                    # 色分けに使う数値（任意の数値を指定可）
                       cmap=pyplot.cm.viridis,
                       marker="o",
                       linewidths=0.5,
                       edgecolors="black",
                       depthshade=False)

#ax.plot(d[:,0], d[:,1], d[:,2], "bo", ms=8)
#ax.plot(d[:,0], d[:,1], d2[:,2], "o", color="#00cccc", ms=4, mew=0.5)
#ax.plot(d[:,0], d[:,1], d3[:,2], "o", color="#ff0000", ms=4, mew=0.5)
cbar = pyplot.colorbar(scatter)
cbar.ax.set_title("Energy (eV)",fontsize=10)
pyplot.rcParams["font.family"] = "Arial"
pyplot.tick_params(labelsize=10)
#pyplot.rcParams["font.size"] = 12
pyplot.rcParams['xtick.direction'] = 'in'
pyplot.rcParams['ytick.direction'] = 'in'

pyplot.show()
# END: Program
