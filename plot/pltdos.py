#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys
import os

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
argv = sys.argv
if len(argv) == 1:
    print("\n")
    print("######################################################")
    print("#  You should input DOS file after script name! BYE! #")
    print("######################################################")
    print("\n")
    sys.exit()

num = 0
for dosfile in argv:
    if num == 0:
        pass
    else:
        # Read DOS
        path_dos = path + "/" + str(dosfile)
        f = open(path_dos)
        doslines = f.readlines()
        f.close()

        list_dos = [conv_line(dosline) for dosline in doslines]
        legend = list_dos[0]

        del list_dos[0]
        energy = [line[0] for line in list_dos]

        num_p = 1
        while num_p < len(list_dos[0]):
            pdos = [line[num_p] for line in list_dos]
            plt.plot(energy, pdos, color=cm.spectral(float(num_p)/(len(list_dos[0])-1)), label=str(legend[num_p]), linewidth=2)
            num_p += 1
    num += 1

fermi = [0 for line in list_dos]
plt.plot(fermi, pdos, color="black", linewidth=2)
plt.xlabel('$\it{E}$ - $\it{E_{F}}$ (eV)',fontsize=16)
plt.ylabel('$\it{DOS}$',fontsize=16)
plt.grid(True)
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
plt.legend(loc="best",prop={'size':12})
plt.minorticks_on

plt.plot()
plt.show()

