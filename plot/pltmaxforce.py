#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import numpy as np
import os, sys

def plot_maxforce(nowpath):
    path_maxforce = nowpath + "/MAXFORCE"

    with open(path_maxforce) as fm: maxforcelines= fm.readlines()

    list_maxforce = []
    for line in maxforcelines:
        line = line.replace('\n','').replace('\r','').replace('\t',' ').split(" ")
        while line.count("") > 0: line.remove("")
        list_maxforce.append(line)

    x = []; y1_en = []; y2_fc = []; label = []
    e0 = float(list_maxforce[0][1])

    num = 0    
    for line in list_maxforce:
        iteration = list_maxforce[num][0]
        x.append(iteration)
        energy = float(list_maxforce[num][1])
        y1_en.append(energy - e0)
        force = float(list_maxforce[num][2])
        y2_fc.append(force)
        eachlabel = list_maxforce[num][3]
        label.append(eachlabel)
        num += 1

    fig = plt.figure()
    ax1 = fig.add_subplot(2,1,1)
    ax1.plot(x, y1_en, color="r")
    ax1.set_ylabel('$\it{E_{tot}}$ (eV)',fontsize=12)
    #ax1.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax1.xaxis.set_major_locator(ticker.AutoLocator())
    ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax1.yaxis.label.set_fontsize(10)

    ax2 = fig.add_subplot(2,1,2)
    ax2.plot(x, y2_fc, color="b")
    ax2.set_xlabel('Iteration',fontsize=12)
    ax2.set_ylabel('$\it{F}$ (eV/${\AA}$)',fontsize=12)
    ax2.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax2.xaxis.set_major_locator(ticker.AutoLocator())
    ax2.xaxis.set_minor_locator(ticker.AutoMinorLocator())
    ax2.yaxis.label.set_fontsize(10)
    ax2.yaxis.label.set_fontsize(10)

    plt.plot()
    plt.show()

if __name__ == "__main__": plot_maxforce(os.getcwd())
# END: script
