#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
#from scipy.interpolate import interp1d
import addmol.helper as hl

#import neb.nebgetef as ne

def plt_nebei(nowpath):
    argv = sys.argv
    nebEifiles = sorted([x for x in os.listdir(nowpath) if os.path.isfile(nowpath + "/" + x) and "nebEiter" in x])

    if len(argv) == 2: colums_count = int(argv[1])
    else: colums_count = int(input("Please input the number of the colums, e.g., 2: "))

    graphs_count = len(nebEifiles)
#    colums_count = 3
    if graphs_count % colums_count == 0: row_count = graphs_count / colums_count
    else: row_count = graphs_count // colums_count + 1

    fig = plt.figure()

    for i1, nebEi in enumerate(nebEifiles):
        path_nebei = nowpath + "/" + nebEi
        with open(path_nebei) as fn: eilines = fn.readlines()
        eilines = [hl.conv_line(l) for l in eilines]

        list_Ei = np.array(eilines).T.tolist(); del list_Ei[0]
        list_Ei = np.array(list_Ei).T.tolist()
        x = [i for i in range(0, len(list_Ei[0]))]

        list_maxE = []
        cmap = cm.get_cmap("Spectral")
        for i2, y in enumerate(list_Ei):
            plt.subplot(row_count, colums_count, i1+1)
#            axes.append(fig.add_subplot(row_count, colums_count, i1+1))

            yi = [float(e) for e in y]
            list_maxE.append(max(yi))
            #f_line = interp1d(x, yi, kind="cubic")
            #xnew = np.linspace(x[0], x[-1], num = 51)
            plt.plot(x,
                     yi,
                     marker = "o",
                     color = cmap(i2/len(list_Ei)),
                     markerfacecolor = cmap(i2/len(list_Ei)))

        plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))
#        plt.xlabel("iteration", fontsize=17)
#        plt.ylabel('Energy (eV)', fontsize=17)
        plt.grid(True)
        plt.xticks(x, fontsize=12)
        plt.yticks(fontsize=12)
        plt.title(nebEi.replace("nebEiter_","").replace(".dat",""))
        plt.text(0.0, max(list_maxE)*0.85, "NSW: "+str(len(list_Ei)), fontweight="bold", color="k")
        plt.text(0.0, max(list_maxE)*0.95, "$\it{E_{a}}$: "+str("{0:.2f}".format(list_maxE[-1])), fontweight="bold", color="r")

    fig.text(0.5, 0.04, "Iteration", ha="center", fontsize=15)
    fig.text(0.04, 0.5, "Enerygy", va="center", rotation="vertical", fontsize=15)
    fig.subplots_adjust(wspace=0.2, hspace=0.2)
    plt.show()

if __name__ == "__main__": plt_nebei(os.getcwd())
# END: script