#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker
import numpy as np
import sys

path = os.getcwd()
path_toten1 = path + "/Sum_TOTENandLATCON.dat"
path_toten2 = path + "/TotalEnergyandLatticeConst"

flag_toten1 = os.path.exists(path_toten1)
flag_toten2 = os.path.exists(path_toten2)
if flag_toten1 == True:
    path_toten = path_toten1
else:
    if flag_toten2 == True:
        path_toten = path_toten2
    else:
        print("Sum_TOTENandLATCON.dat or TotalEnergyandLatticeConst does not exists! BYE!")

f = open(path_toten)
totenlines= f.readlines()
f.close()

list_toten = []
for line in totenlines:
    line = line.replace('\n','')
    line = line.replace('\r','')
    line = line.replace('\t',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    list_toten.append(line)

del list_toten[0]
#print list_toten

list_cutoff = []
cutoff0 = int(list_toten[0][0])
listx = []
listy = []
listy_sum = []
listya = []
listya_sum = []
listyb = []
listyb_sum = []
listyc = []
listyc_sum = []

num = 0
#print len(list_toten)
for line in list_toten:
    #print num
    kmesh = list_toten[num][1]
    cutoff = int(list_toten[num][0])
    totenperatom = float(list_toten[num][4])
    latA = float(list_toten[num][11])
    latB = float(list_toten[num][12])
    latC = float(list_toten[num][13])

    diff_en = abs(cutoff - cutoff0)

    if diff_en == 0:
        if num == len(list_toten) -1:
            list_cutoff.append(cutoff0)
            listx.append(kmesh)
            listy.append(totenperatom)
            listy_sum.append(listy)
            listya.append(latA)
            listya_sum.append(listya)
            listyb.append(latB)
            listyb_sum.append(listyb)
            listyc.append(latC)
            listyc_sum.append(listyc)
    
        else:
            listx.append(kmesh)
            listy.append(totenperatom)
            listya.append(latA)
            listyb.append(latB)
            listyc.append(latC)
            num += 1
    else :
        list_cutoff.append(cutoff0)
        listy_sum.append(listy)
        listya_sum.append(listya)
        listyb_sum.append(listyb)
        listyc_sum.append(listyc)

        listx = []
        listy = []
        listya = []
        listyb = []
        listyc = []

        cutoff0 = cutoff
        listx.append(kmesh)
        listy.append(totenperatom)
        listya.append(latA)
        listyb.append(latB)
        listyc.append(latC)

        num += 1

x = []
numx = 1
for k in listx:
    x.append(numx)
    numx += 1
#print x

plt.subplot(221)
i = 0
for y in listy_sum:
    co = list_cutoff[i]
    p, = plt.plot(x, y, color=cm.viridis(float(i)/len(listy_sum)), marker="o", markersize=10, markerfacecolor=cm.viridis(float(i)/len(listy_sum)), label= str(co))
    #plt.title('Local Potential',fontsize=20)
    i += 1
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.4f'))
#plt.xlabel('$\it{k}$-mesh',fontsize=11)
plt.ylabel('$\it{E_{atom}}$ (eV)',fontsize=12)
plt.grid(True)
plt.xticks(x, listx, rotation=30,fontsize=11)
plt.yticks(fontsize = 11)
plt.legend(loc="best",prop={'size':9})
plt.minorticks_on

plt.subplot(222)
l = 0
for ya in listya_sum:
    co = list_cutoff[l]
    p, = plt.plot(x, ya, color=cm.viridis(float(l)/len(listya_sum)), marker="o", markersize=10, markerfacecolor=cm.viridis(float(l)/len(listya_sum)), label= str(co))
    l += 1
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
#plt.xlabel('$\it{k}$-mesh',fontsize=12)
plt.ylabel("$lc_a$ (%)",fontsize=12)
plt.grid(True)
plt.xticks(x, listx, rotation=30,fontsize=11)
plt.yticks(fontsize = 11)
#plt.legend(loc="best",prop={'size':9})
plt.minorticks_on

plt.subplot(223)
m = 0
for yb in listyb_sum:
    co = list_cutoff[m]
    p, = plt.plot(x, yb, color=cm.viridis(float(m)/len(listyb_sum)), marker="o", markersize=10, markerfacecolor=cm.viridis(float(m)/len(listyb_sum)), label= str(co))
    m += 1
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
#plt.xlabel('k-mesh',fontsize=10)
plt.ylabel("$lc_b$ (%)",fontsize=12)
plt.grid(True)
plt.xticks(x, listx, rotation=30,fontsize=11)
plt.yticks(fontsize = 11)
#plt.legend(loc="best",prop={'size':9})
plt.minorticks_on

plt.subplot(224)
n = 0
for yc in listyc_sum:
    co = list_cutoff[n]
    p, = plt.plot(x, yc, color=cm.viridis(float(n)/len(listyb_sum)), marker="o", markersize=10, markerfacecolor=cm.viridis(float(n)/len(listyc_sum)), label= str(co))
    n += 1
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
#plt.xlabel('k-mesh',fontsize=12)
plt.ylabel("$lc_c$ (%)",fontsize=12)
plt.grid(True)
plt.xticks(x, listx, rotation=30,fontsize=11)
plt.yticks(fontsize = 11)
#plt.legend(loc="best",prop={'size':7})
plt.minorticks_on

plt.plot()
plt.show()


