#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, re, sys, glob, shutil, threading
import subprocess
import numpy as np
import pandas as pd
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from scipy import genfromtxt

path = os.getcwd()
#argv = sys.argv
filename = input("Please input the file name: ")
label_xaxis = input("Please input the label of x-axis: ")
label_yaxis = input("Please input the label of y-axis: ")
fs = 15

# ファイル読み込み
#d = genfromtxt(path+"/"+filename, delimiter=",")
d = pd.read_csv(path+"/"+filename, header=None, delimiter=r",", names=[0,1,2,3])
d0 = d.iloc[:,0].values
d1 = d.iloc[:,1].values
d2 = d.iloc[:,3].values

# グラフ作成
fig = pyplot.figure()
#ax = Axes3D(fig)
ax = fig.gca()

# 軸ラベルの設定
ax.set_xlabel(label_xaxis + " ($\AA$)",fontsize=fs)
ax.set_ylabel(label_yaxis + " ($\AA$)",fontsize=fs)

# 表示範囲の設定
ax.set_xlim(0.5, 3.5) # N-H
ax.set_ylim(0.5, 3.5) # O-H

# 抽出条件設定
#d1 = d[d[:,0] >= 7]
#d2 = d[(d[:,0] < 7) & ((d[:,1] > 3) & (d[:,1] <= 3.5))]
#d3 = d[(d[:,0] < 7) & ((d[:,1] <= 3) | (d[:,1] > 3.5))]

# グラフ描画
scatter = ax.scatter(d0, d1,
                     s=200,                   # マーカーのサイズ
                     c=d2,                    # 色分けに使う数値（任意の数値を指定可）
                     cmap=pyplot.cm.viridis,
                     marker="o",
                     linewidths=0.5,
                     edgecolors="black")

#ax.plot(d[:,0], d[:,1], d[:,2], "bo", ms=8)
#ax.plot(d[:,0], d[:,1], d2[:,2], "o", color="#00cccc", ms=4, mew=0.5)
#ax.plot(d[:,0], d[:,1], d3[:,2], "o", color="#ff0000", ms=4, mew=0.5)
cbar = pyplot.colorbar(scatter)
cbar.ax.set_title("Energy (eV)",fontsize=12)
pyplot.rcParams["font.family"] = "Arial"
pyplot.rcParams['mathtext.rm'] = 'Arial'
pyplot.tick_params(labelsize=fs)
pyplot.rcParams['xtick.direction'] = 'in'
pyplot.rcParams['ytick.direction'] = 'in'

pyplot.show()
# END: Program
