#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, math, shutil
import numpy as np
import addmol.helper as hp
import addmol.readpos as rp
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d.axes3d import Axes3D

radi = [1.97, 1.43, 0.74, 0.74, 0.4]
el_color=["green","skyblue","red","blue","gray"]
width_vec_def = 1
radi_plot_def = 12
alpha = 0.6

mode = input("\nWhich do you use the sphere mode or plot mode, [s or p]: ")
if "p" in mode:
    Q_radiplot = input("Please input the radi of plot, default = 12: ")
    if Q_radiplot in ["d", "default"]: radi_plot = radi_plot_def
    else: radi_plot = radi_plot = float(Q_radiplot)

Q_width_vec = input("Please input the width vector to the saddle point, default = 1.0: ")
if Q_width_vec in ["d", "default"]: width_vec = width_vec_def
else: width_vec = float(Q_width_vec)

file1 = input("Please input the first file, i.e., [ POSCAR,p / CONTCAR,c / SPOSCAR, s] or FILENAME (e.g., POSCAR1): ")
if file1 in ["POSCAR", "p", "P"]: file1 = "POSCAR"
elif file1 in ["CONTCAR", "c", "C"]: file1 = "CONTCAR"
elif file1 in ["SPOSCAR", "s", "S"]: file1 = "SPOSCAR"

#file2 = input("Please input the first file, i.e., [ POSCAR,p / CONTCAR,c / SPOSCAR, s] or FILENAME (e.g., POSCAR2): ")
#if file2 in ["POSCAR", "p", "P"]: file2 = "POSCAR"
#elif file2 in ["CONTCAR", "c", "C"]: file2 = "CONTCAR"
#elif file2 in ["SPOSCAR", "s", "S"]: file2 = "SPOSCAR"
"""
argv = sys.argv
if len(argv) < 2:
    print("\n############################################")
    print("# You should input script POSCAR1 POSCAR2 #")
    print("############################################\n"); sys.exit()
"""

"""
elif len(argv) == 3:
    print "\n#####################################################"
    print "# You should input script scale of vector, e.g., 20 #"
    print "#####################################################\n"; sys.exit()
"""

#file1 = str(argv[1]); file2 = str(argv[2]) #; C = float(argv[3]); R = float(argv[4])
def read_pos(POS):
    with open(POS) as fp: contlines = fp.readlines()

    labelofel = rp.get_labelofel(contlines)
    numofel = rp.get_numofel(contlines)
    sumatomnum = rp.get_sumofnumofel(contlines)

    list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel,numofel)
    firstlines, flagS = rp.get_firstlines(contlines)
    mat_lat = rp.get_matrix(contlines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
    arr_coor_car = np.array(coor_car)

    coor_eachE = []; numT = 0
    for i1 in range(0,len(numofel)):
        list = []
        for i2 in range(0, int(numofel[i1])): list.append(coor_car[numT + i2])
        coor_eachE.append(list); numT += int(numofel[i1])

    return arr_coor_car, coor_eachE, list_labelofel, firstlines, mat_lat

path = os.getcwd()
coor1_car, coor1_eachE, list_labelofel, firstlines, mat_lat = read_pos(file1)
#coor2_car = read_pos(file2)[0]

lc_a = math.sqrt(float(mat_lat[0][0])**2 + float(mat_lat[0][1])**2 + float(mat_lat[0][2])**2)
lc_b = math.sqrt(float(mat_lat[1][0])**2 + float(mat_lat[1][1])**2 + float(mat_lat[1][2])**2)
lc_c = math.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)

# read MODECAR
path_modecar = path + "/MODECAR"
if not os.path.exists(path_modecar): print("\nYou should prepare MOODECAR! BYE!\n"); sys.exit()
with open(path_modecar) as fm: modelines = fm.readlines()
vecters_to_sp = [[float(x) for x in hp.conv_line(line)] for line in modelines]


"""
Lpos = str(firstlines[0]) + "\n1.0\n"
for i in range(2,7): Lpos += str(firstlines[i]) + "\n"
else: Lpos += "Cartesian\n"

diff_coor_tot = []; diff_ratio_tot = []; Lco = Lpos; Lra = Lpos
for i in range(0,len(coor1_car)):
    diff_coor = coor2_car[i] - coor1_car[i]; diff_coor_tot.append(diff_coor)
    diff_ratio = [diff_coor[0]/lc_a, diff_coor[1]/lc_b, diff_coor[2]/lc_c]
    diff_ratio_tot.append(diff_ratio)
    length = np.linalg.norm(diff_coor)
    list_diff = list(map(float, diff_coor.tolist()))

    Lco += str('%03.3f' % list_diff[0]) + "  "\
         + str('%03.3f' % list_diff[1]) + "  "\
         + str('%03.3f' % list_diff[2]) + "  "\
         + str('%03.3f' % length) + "  " + list_labelofel[i] + "\n"
    Lra += str('%03.3f' % diff_ratio[0]) + "  "\
          + str('%03.3f' % diff_ratio[1]) + "  "\
          + str('%03.3f' % diff_ratio[2]) + "  "\
          + str('%03.3f' % length) + "  " + list_labelofel[i] + "\n"

# Write CONTCAR_car
with open(str(file1).replace(".vasp","") + "_diff.vasp",'w') as fc: fc.write(Lco)
with open(str(file1).replace(".vasp","") + "_ratio.vasp",'w') as fr: fr.write(Lra)
"""

#plot
def det_coor(coor0):
    x = []; y = []; z = []
    for coor in coor0: x.append(float(coor[0])); y.append(float(coor[1])),z.append(float(coor[2]))
    return x, y, z

def det_diff_coor(diff0):
    u = []; v = []; w = []
    for d in diff0:
      dx = d[0]; dy = d[1]; dz = d[2]
      if d[0] < -0.9:  dx += 1
      elif d[0] > 0.9: dx -= 1
      if d[1] < -0.9:  dy += 1
      elif d[1] > 0.9: dy -= 1
      if d[2] < -0.9:  dz += 1
      elif d[2] > 0.9: dz -= 1
      u.append(dx*lc_a); v.append(dy*lc_b); w.append(dz*lc_c)
    return u, v, w

X, Y, Z = det_coor(coor1_car)
U, V, W = det_coor(vecters_to_sp)
#U, V, W = det_diff_coor(diff_ratio_tot)
#print U, V, W

LX_max = max([mat_lat[0][0], mat_lat[1][0], mat_lat[2][0]])
LX_min = min([mat_lat[0][0], mat_lat[1][0], mat_lat[2][0]])
LY_max = max([mat_lat[0][1], mat_lat[1][1], mat_lat[2][1]])
LY_min = min([mat_lat[0][1], mat_lat[1][1], mat_lat[2][1]])
LZ_max = max([mat_lat[0][2], mat_lat[1][2], mat_lat[2][2]])
LZ_min = min([mat_lat[0][2], mat_lat[1][2], mat_lat[2][2]])

fig = plt.figure()
#ax = fig.gca(projection='3d')
ax = Axes3D(fig)
ax.quiver(X, Y, Z, U, V, W, color='k', length=width_vec, arrow_length_ratio=0.25, pivot="tail",linewidths=2)

pi = np.pi; cos = np.cos; sin = np.sin
phi, theta = np.mgrid[0.0:pi:12j, 0.0:2.0*pi:12j]
x = sin(phi)*cos(theta); y = sin(phi)*sin(theta); z = cos(phi)

for i in range(0,len(coor1_eachE)):
    X1 = []; Y1 = []; Z1 = []
    for coor1_E in coor1_eachE[i]:
        x1 = float(coor1_E[0]); y1 = float(coor1_E[1]); z1 = float(coor1_E[2])
        if mode == "s":
            ax.plot_surface(x1 + x*radi[i], y1 + y*radi[i], z1 + z*radi[i], rstride=1, cstride=1, linewidth=2, color=el_color[i], alpha=alpha)
        elif mode == "p": X1.append(x1); Y1.append(y1); Z1.append(z1)
    if mode == "p":
        ax.plot(X1,Y1,Z1,"o", color=el_color[i], ms=int(radi_plot)*radi[i], linewidth=4, alpha=alpha)
        #ax.plot(X1,Y1,Z1,"o", color=cm.spectral(float(i)/len(coor1_eachE)), ms=int(R)*radi[i], alpha=0.9)

ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax), np.diag([1, 1, 1.5, 1]))
ax.set_xlabel("x-axis"); ax.set_ylabel("y-axis"); ax.set_zlabel("z-axis")
ax.set_xlim([LX_min-2, LX_max+2]); ax.set_ylim([LY_min-2, LY_max+2]); ax.set_zlim([LZ_min, LZ_max-5])
plt.show()
# END