#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#plot DOS 1 graph.
#import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy.interpolate import interp1d
import numpy as np
import sys
import os

path = os.getcwd()
argv = sys.argv
dat = path + "/neb.dat"
#dat = str(argv[1])
flag_nebdat = os.path.exists(dat)
if flag_nebdat == False:
    print("##########################################################")
    print("##  You should prepare neb.dat by using nebbarrier.pl!  ##")
    print("##########################################################")
    sys.exit()

data = np.loadtxt(dat,dtype=None)
x = data[:,1]
y = data[:,2]
f_line = interp1d(x,y,kind='cubic')
xnew = np.linspace(x[0],x[-1],num=51)

plt.plot(x,y,"bo")
plt.plot(xnew,f_line(xnew),"b-")
#plt.title('')
plt.xlabel("Reaction Coordinate ($\AA$)",fontsize=20)
plt.ylabel('Energy (eV)', fontsize=20)
plt.gca().xaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.grid(True)
plt.minorticks_on
plt.show()
