#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

path = os.getcwd()
path_toten1 = path + "/Sum_TOTENandLATCON.dat"
path_toten2 = path + "/TotalEnergyandLatticeConst"

if os.path.exists(path_toten1): path_toten = path_toten1
else:
    if os.path.exists(path_toten2): path_toten = path_toten2
    else: print("Sum_TOTENandLATCON.dat or TotalEnergyandLatticeConst does not exists! BYE!")

f = open(path_toten)
totenlines= f.readlines()
f.close()

list_toten = []
for line in totenlines:
    line = line.replace('\n','').replace('\r','').replace('\t',' ').split(" ")
    while line.count("") > 0: line.remove("")
    list_toten.append(line)
del list_toten[0]

list_cutoff = []; cutoff0 = int(list_toten[0][0])
listx = []; listy = []; listy_sum = []
listya = []; listya_sum = []
listyb = []; listyb_sum = []
listyc = []; listyc_sum = []

num = 0
#print len(list_toten)
for line in list_toten:
    #print num
    kmesh = list_toten[num][1]
    cutoff = int(list_toten[num][0])
    totenperatom = float(list_toten[num][4])
    latA = float(list_toten[num][11])
    latB = float(list_toten[num][12])
    latC = float(list_toten[num][13])

    diff_en = abs(cutoff - cutoff0)

    if diff_en == 0:
        if num == len(list_toten) -1:
            list_cutoff.append(cutoff0)
            listx.append(kmesh); listy.append(totenperatom); listy_sum.append(listy)
            listya.append(latA); listya_sum.append(listya)
            listyb.append(latB); listyb_sum.append(listyb)
            listyc.append(latC); listyc_sum.append(listyc)

        else:
            listx.append(kmesh); listy.append(totenperatom)
            listya.append(latA); listyb.append(latB); listyc.append(latC)
            num += 1

    else :
        list_cutoff.append(cutoff0); listy_sum.append(listy)
        listya_sum.append(listya); listyb_sum.append(listyb); listyc_sum.append(listyc)

        listx = []; listy = []
        listya = []; listyb = []; listyc = []

        cutoff0 = cutoff
        listx.append(kmesh); listy.append(totenperatom)
        listya.append(latA); listyb.append(latB); listyc.append(latC)

        num += 1

x = []; numx = 1
for k in listx: x.append(numx); numx += 1

i = 0
for y_val in listy_sum:
    print(y_val)
    co = list_cutoff[i]
    p, = plt.plot(x,
                  y_val,
                  marker = "o",
                  markersize = 10,
                  label= str(co),
                  color = cm.viridis(float(i)/len(listy_sum)),
                  markerfacecolor = cm.viridis(float(i)/len(listy_sum)),
                  )
    #plt.title('Local Potential',fontsize=20)
    i += 1
plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.4f'))
plt.xlabel('$\it{k}$-mesh',fontsize=12)
plt.ylabel('$\it{E_{atom}}$ (eV)',fontsize=12)
plt.grid(True)
plt.xticks(x, listx, rotation=45,fontsize=10)
plt.yticks(fontsize = 10)
plt.legend(loc="best",prop={'size':10})
#plt.minorticks_on

plt.plot()
plt.show()
# END: script