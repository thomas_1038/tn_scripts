#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
import numpy as np
import addmol.readpos as rp
from addmol import Frac2Cart
f2c = Frac2Cart()

tag_elems = ["Ca", "Al", "O"]
num_elems = [3, 2, 1]
masses = [15.9994, 26.9815386, 40.078]

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
path_pos = path + "/POSCAR"
with open(path_pos) as fp: poslines = fp.readlines()

files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

labelofel = rp.get_labelofel(poslines)
numofel = rp.get_numofel(poslines)
sumofatomofel = rp.get_sumofnumofel(poslines)
eachlabels_el_nn = rp.mk_labelofel(labelofel, numofel)[1]
mat_lat = rp.get_matrix(poslines)
coor_car = rp.get_coordinate(poslines, sumofatomofel, mat_lat)[0]
coor_eachel = rp.get_coordinate_eachel(coor_car, eachlabels_el_nn)[0]

ie = 1
L = "comment\n\n"
L += str(sumofatomofel) + " atoms\n"
L += str(len(numofel)) + " atom types\n"
L += "0.0000000000 " + str(mat_lat[0][0]) + " xlo xhi\n"
L += "0.0000000000 " + str(mat_lat[1][1]) + " ylo yhi\n"
L += "0.0000000000 " + str(mat_lat[2][2]) + " zlo zhi\n\n"
L += "Masses\n\n"
for i, ms in enumerate(masses):
    L += str(i+1) + " " + str(ms) + "\n"
L += "\nAtoms\n\n"
for i0, coor_el in enumerate(coor_eachel):
    for coor in coor_el:
        L += str(ie) + " " + str(num_elems[i0]) + " " + " ".join([str(x) for x in ["{:.10f}".format(y) for y in coor]]) + "\n"
        ie += 1

with open(path + "/LAMMPS.conf", "w") as fl: fl.write(L)
print("Done!")
