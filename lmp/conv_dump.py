#!/usr/bin/env python3
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def conv_dump(nowpath, filename="dump.xyz"):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path_file = nowpath + "/" + filename
    with open(path_file) as f: dumplines = f.readlines()
    num_loop = int(dumplines[0])

    n = 1; Lt = ""
    for i, dl in enumerate(dumplines):
        if i == num_loop * n + 1:
            n += 1
        else:
            l = conv_line(dl)
            if l[0] == "1": l[0] = "O"
            elif l[0] == "2": l[0] = "Al"
            elif l[0] == "3": l[0] = "Ca"
        dl = " ".join(l) + "\n"
        Lt += dl

    name_newfile = filename.replace(".xyz","_new") + ".xyz"
    with open(nowpath + "/" + name_newfile, "w") as fw: fw.write(Lt)
    print(name_newfile)

if __name__ == "__main__":
    conv_dump(os.getcwd())
    print("Done!")
