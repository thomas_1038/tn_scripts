#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

def get_data_from_lammpsfile(nowpath, filename):
    outputs = sorted([x for x in os.listdir(nowpath) if filename in x])
    print(" ".join(outputs))
    Lt = ""

    for f, output in enumerate(outputs):
        path_output = nowpath + "/" + output
        with open(path_output) as fo: outputlines = fo.readlines()

        ib = 0; ie = 0; flag_end = 0; flag_m1 = 0
        Lo = ""
        for a, ol in enumerate(outputlines):
            if "Per MPI rank memory allocation" in ol:
                if f == 0: ib = a + 1
                else: ib = a + 2
            elif "WARNINF" in ol:
                im1 = a; flag_m1 = 1

            elif "Loop time of " in ol:
                ie = a; flag_end = 1
                break

        if flag_m1 == 1:
            Lt += "".join(outputlines[ib:im1] + outputlines[im+1:ie])

        else:
            if flag_end == 1:
                #with open(nowpath + "/data.out", "w") as fw: fw.write("".join(outputlines[ib:ie]))
                Lt += "".join(outputlines[ib:ie])
            else:
                #with open(nowpath + "/data.out", "w") as fw: fw.write("".join(outputlines[ib:-2]))
                Lt += "".join(outputlines[ib:-2])

    with open(nowpath + "/data.out", "w") as fw: fw.write(Lt)
    #print(outputlines[ib])

def get_data_per_result(nowpath, n_count=100, temp=2500):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path_data = nowpath + "/data.out"
    with open(path_data) as fd: datalines = fd.readlines()

    n = 1; Lt = datalines[0]; n_first = int(conv_line(datalines[1])[0])
    Ltt = datalines[0]
    print(n_first)
    for i, dl in enumerate(datalines[1:]):
        #print(conv_line(dl)[0])
        if int(conv_line(dl)[0]) == n * n_count + n_first:
            Lt += dl; n += 1
            if float(conv_line(dl)[1]) >= 2500:
                Ltt += dl

    with open(nowpath + "/data_nc_" + str(n_count) + ".out", "w") as fw: fw.write(Lt)
    with open(nowpath + "/data_tmp_more_than_" + str(temp) + "K.out", "w") as fw: fw.write(Ltt)

def plt_lammps(nowpath, filename, nx, ny):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path_data = nowpath + "/" + filename
    with open(path_data) as fd: datalines = fd.readlines()

    x_data = [float(conv_line(l)[nx]) for l in datalines[1:]]
    y_data = [float(conv_line(l)[ny]) for l in datalines[1:]]

    plt.scatter(x_data, y_data, 3, "b", "o", alpha=0.9)
    #plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
    plt.locator_params(axis='x',nbins=6) #y軸，6個以内

    plt.xlabel(conv_line(datalines[0])[nx], fontsize=14)
    plt.ylabel(conv_line(datalines[0])[ny], fontsize=14)
    #plt.grid(True)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    plt.minorticks_on
    plt.plot()
    plt.show()

if __name__ == "__main__":
    Q1 = input("\nDo you want to make dataset at here? [y/N]: ")
    if Q1 in ["y", "Y"]:
        get_data_from_lammpsfile(os.getcwd(), "std.out")
        print("\ndata.out has been made!")
        n_count = 100
        get_data_per_result(os.getcwd(), n_count)
        print("data_nc_" + str(n_count) + ".out & data_tmp.out have been made!")

    Q2 = input("\nDo you want to plot? [y/N]: ")
    if Q2 in ["y", "Y"]:
        filename = input("Please input the filename: ")
        nx = input("Please input the number of x axis: ")
        ny = input("Please input the number of y axis: ")
        plt_lammps(os.getcwd(), filename, int(nx), int(ny))

    print("\nDone!\n")
# END: Program
