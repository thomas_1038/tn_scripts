#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np

def make_backup_for_lammps(nowpath):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    num = 1
    path_dirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" +x)])

    for x in path_dirs:
        if str(num) in x:
            num += 1
        else:
            break

    path_backup = nowpath + "/" + str(num)
    path_restart = path_backup + "/restart"
    os.mkdir(path_backup); os.mkdir(path_restart)

    files = [x for x in os.listdir(nowpath) if os.path.isfile(nowpath + "/" + x)]
    restart_files = [x for x in files if "restart" in x]
    lastnum_restart = sorted([int(x.replace("restart.","")) for x in restart_files])[-1]
    other_necessary_files = [x for x in files if "log." in x or ".conf" in x or ".out" in x or "dump" in x]
    input_file = [x for x in files if "in." in x][0]

    restart_files.remove("restart." + str(lastnum_restart))

    for rf in restart_files:
        shutil.move(nowpath + "/" + rf, path_restart)

    for nf in other_necessary_files:
        shutil.move(nowpath + "/" + nf, path_backup)
    shutil.copy(nowpath + "/" + input_file, path_backup)

    path_job = nowpath + "/job.sh"
    with open(path_job) as fj: joblines = fj.readlines()

    Lj = ""
    for jl in joblines:
        if "read_restart" in jl:
            jl = "read_restart      restart." + str(lastnum_restart) + "\n"
        Lj +=  jl

    with open(path_job, "w") as fjw: fjw.write(Lj)

if __name__ == "__main__":
    make_backup_for_lammps(os.getcwd())
    print("\nDone!\n")
# END: Program
