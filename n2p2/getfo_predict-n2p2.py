#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os, sys, shutil
import subprocess as sub
import numpy as np

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
ffiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "nnforces" in x and "tot" not in x])

Lx = ""; Ly = ""; Lz = ""
i = 0
for ff in ffiles:
    print(i+1)
    with open(path + "/" + ff) as f: flines = f.readlines()
    for fline in flines[18:]:
        l = conv_line(fline)
        Lx += str(i+1) + "  " + l[0] + "  " + l[3] + "  " + l[6] + "\n"
        Ly += str(i+1) + "  " + l[1] + "  " + l[4] + "  " + l[7] + "\n"
        Lz += str(i+1) + "  " + l[2] + "  " + l[5] + "  " + l[8] + "\n"
        i += 1

with open(path + "/forcex_tot.out", "w") as ffx: ffx.write(Lx)
with open(path + "/forcey_tot.out", "w") as ffy: ffy.write(Ly)
with open(path + "/forcez_tot.out", "w") as ffz: ffz.write(Lz)
print("Done!")
