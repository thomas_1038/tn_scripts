#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil

"""
def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line
"""

path = os.getcwd()

backupdirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) if "backup" in x]
if len(backupdirs) + 1 < 10: num_dir = "0" + str(len(backupdirs) + 1)
else: num_dir = str(len(backupdirs) + 1)

trainforces =  sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "trainforces." in x and ".out" in x])
trainpoints =  sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "trainpoints." in x and ".out" in x])
testforces =   sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "testforces." in x and ".out" in x])
testpoints =   sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "testpoints." in x and ".out" in x])
neuron_stats = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "neuron-stats." in x and ".out" in x])
weights =      sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if "weights." in x and ".out" in x])
logs =         sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) if ".log." in x])
last_num_w = weights[-1][-10:]
weights_last = sorted([x for x in weights if last_num_w in x])

for x in weights_last:
    shutil.copy(path + "/" + x, path + "/" + x.replace(last_num_w, "data"))

path_backup = path + "/backup" + num_dir
path_trainforces_dir = path_backup + "/trainforces"
path_trainpoints_dir = path_backup + "/trainpoints"
path_testforces_dir = path_backup + "/testforces"
path_testpoints_dir = path_backup + "/testpoints"
path_neuron_stats_dir = path_backup + "/neuron_stats"
path_weights_dir = path_backup + "/weights"
path_log_dir = path_backup + "/log"

if not os.path.exists(path_backup):
    os.mkdir(path_backup)
path_and_files = [[trainforces, path_trainforces_dir],
                  [trainpoints, path_trainpoints_dir],
                  [testforces, path_testforces_dir],
                  [testpoints, path_testpoints_dir],
                  [neuron_stats, path_neuron_stats_dir],
                  [weights, path_weights_dir],
                  [logs, path_log_dir]]

for x in path_and_files:
    print(x[1].replace(path_backup, "").replace("/",""))
    os.mkdir(x[1])
    for f in x[0]: 
        print(f)
        shutil.move(path + "/" + f, x[1])
    print("")

for x in ["learning-curve.out", "train.data", "test.data", "std.out", "train-log.out", "updater.000.out"]:
    if os.path.exists(path + "/" + x):
        print(x)
        shutil.move(path + "/" + x, path_backup)

print("\nDone!\n")
