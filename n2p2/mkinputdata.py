#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import pandas as pd
import addmol.readpos as rp
import addmol.helper as hl
from addmol import Frac2Cart
f2c = Frac2Cart()

def get_coor_from_xdat(nowpath, xdat):
    #name_path_dirs = nowpath.split("/")
    #path_dir_xdat = nowpath + "/xdat_" + name_path_dirs[-3] + "_" + name_path_dirs[-2]
    #if not os.path.exists(path_dir_xdat): os.mkdir(path_dir_xdat)

    path_xdat = nowpath + "/" + xdat
    if not os.path.exists(path_xdat): print("You should prepare XDATCAR! BYE!"); sys.exit()
    with open(path_xdat) as fx: xdatlines = fx.readlines()

    xdatlines_first = xdatlines[:7]
    num_it = 0; eachPOSCARs = [] #; nums_config = [];
    for i, l in enumerate(xdatlines):
        if i < 7: pass
        else:
            if "Direct configuration=" in l:
                if int(num_it) != 0: eachPOSCARs.append(xdatlines_first + list_eachpos)
                num_it = l.replace("Direct configuration=","").replace(" ","").replace("\n","")
                #nums_config.append(int(num_it))
                list_eachpos = ["Direct\n"]
            else: list_eachpos.append(l)

    else: eachPOSCARs.append(xdatlines_first + list_eachpos)

    latvec_config = []; coor_config = []; element_config = []
    for poslines in eachPOSCARs:
        labelofel = rp.get_labelofel(poslines)
        numofel = rp.get_numofel(poslines)
        sumatomnum = rp.get_sumofnumofel(poslines)
        eachlabels_el_nn = rp.mk_labelofel(labelofel, numofel)[1]

        #firstlines, flagS = rp.get_firstlines(poslines)
        mat_lat = rp.get_matrix(poslines)
        coor_car = rp.get_coordinate(poslines, sumatomnum, mat_lat)[0]
        #df_coor = pd.DataFrame(coor_car)
        #pd.options.display.float_format = '{:.10f}'.format
        latvec_config.append(mat_lat)
        coor_config.append(coor_car)
        #coor_config.append(df_coor.values.tolist())
        element_config.append(eachlabels_el_nn)

    df_latvec = pd.DataFrame(latvec_config); df_coor = pd.DataFrame(coor_config)
    pd.options.display.float_format = '{:.8f}'.format
    latvec_config = df_latvec.values.tolist(); coor_config = df_coor.values.tolist()
    return latvec_config, coor_config, element_config #, nums_config

def get_energy_force(nowpath, out, pos, cycle=10):
    ind_force ="TOTAL-FORCE"
    ind_en1 = "FREE ENERGIE OF THE ION-ELECTRON"
    #ind_en2 = "energy without entropy"
    path_out = nowpath + "/" + out
    path_pos = nowpath + "/" + pos
    path_con = nowpath + "/CONTCAR"
    #path_std1 = nowpath + "/" + stdout
    #path_std2 = nowpath + "/stdout"

    """
    if os.path.exists(path_std1):  path_std = path_std1
    elif os.path.exists(path_std2): path_std = path_std2
    else: print("There is no std.out! You should prepare it. BYE!"); sys.exit()
    # read std.out
    with open(path_std) as fs: stdline = fs.read()
    if "VERY BAD NEWS! internal error in subroutine SGRCON:" in stdline:
        with open(nowpath + "/" + filename,'w') as fm: fm.write("")
        list_maxFtot = []
    """

    if not os.path.exists(path_pos):
        if os.path.exists(path_con): path_pos = path_con
        else: print("POSCAR does not exists in " + str(nowpath)  + "! BYE!"); sys.exit()

    # read POSCAR
    with open(path_pos) as fp: poslines = fp.readlines()
    atomlabel = hl.conv_line(poslines[5]); atomnum = hl.conv_line(poslines[6]); sumatomnum = sum([int(x) for x in atomnum])

    na = 0; list_atomlabel = []
    while na < len(atomlabel):
        na2 = 0
        while na2 < int(atomnum[na]): list_atomlabel.append(atomlabel[na]+str(na2+1)); na2 += 1
        na += 1

    flagS = 0
    if "Selective" in poslines[7]: flagS = 1

    #make atom positon matrix
    num2 = 8 + flagS; atomposition = []
    while num2 < 8 + sumatomnum + flagS: atomposition.append(hl.conv_line(poslines[num2])); num2 += 1

    #make fix word matrix
    num3 = 0; fixword = []
    while num3 < len(atomposition):
        a = []
        if flagS == 1: a.append(str(atomposition[num3][3]))
        a.append(list_atomlabel[num3])
        fixword.append(a); num3 += 1
    # END: read POSCAR

    # read OUTCAR
    if not os.path.exists(path_out): print("OUTCAR doesn't exist! BYE!") ; sys.exit()
    with open(path_out,"r") as fo: outlines = fo.readlines()

    #Definition
    n1 = 0; num_f = 0; energy_config = []; force_config = [] #; force_config_relax = []
    num_cy_e = 1; num_cy_f = 1
    energy_config_append = energy_config.append; force_config_append = force_config.append
    #force_config_relax_append = force_config_relax.append

    #Start: while
    while n1 < len(outlines):
        if ind_en1 in outlines[n1]:
            #print(hl.conv_line(outlines[n1+4])); sys.exit()
            mod_e = num_cy_e % cycle
            if mod_e == 0: energy_config_append(hl.conv_line(outlines[n1+2])[4])
            n1 += 2; num_cy_e += 1

        if ind_force not in outlines[n1]:
            n1 += 1
        else:
            num_f += 1; n1 += 2; fnum = int(n1) + int(sumatomnum)
            list_Fi = [] #; list_Fi_relax = []
            list_Fi_append = list_Fi.append #; list_Fi_relax_append = list_Fi_relax.append
            while n1 < fnum:
                mod_f = num_cy_f % cycle
                if mod_f == 0:
                    Fi_atom = outlines[n1].replace("\n","").split(" ")
                    while Fi_atom.count("") > 0: Fi_atom.remove("")
                    Fi_atom.pop(0); Fi_atom.pop(0); Fi_atom.pop(0)
                    list_Fi_append([Fi_atom[0], Fi_atom[1], Fi_atom[2]])
                #force_config = math.sqrt(float(Fi_atom[0])**2+ float(Fi_atom[1])**2 + float(Fi_atom[2])**2)
                #Fi_atom += [str(force_config), str(fixword[num_a][0])]
                #if flagS == 1 :
                #    Fi_atom.append(str(fixword[num_a][1]))
                #    if str(fixword[num_a][0]) == "T": list_Fi_relax_append(force_config)
                #else: list_Fi_relax_append(force_config)
                #list_Fi_append(Fi_atom)
                n1 += 1

            mod_f = num_cy_e % cycle
            if mod_f == 0: force_config_append(list_Fi) #; force_config_relax_append(list_Fi_relax)
            num_cy_f += 1
            #if num_f == len(energy_config): break
            #else: continue

    df_energy = pd.DataFrame(energy_config); df_force = pd.DataFrame(force_config)
    pd.options.display.float_format = '{:.8f}'.format
    energy_config = df_energy.values.tolist(); force_config = df_force.values.tolist()
    return energy_config, force_config

def make_inputline(nowpath, energies, forces, latvecs, coors, elements, xdatfile, i0):
    dirname = nowpath.split("/")[-1].replace("vfiles_","")
    Ltot = ""; inputlines = []
    for i1, each_coors in enumerate(coors):
        L = ""
        L += "begin\ncomment #" + str(i0) + " " + dirname + " " + str(xdatfile) + " #" + str(i1+1) + "\n"
        L += "lattice " + " ".join(["{:.10f}".format(c) for c in latvecs[i1][0]]) + "\n"
        L += "lattice " + " ".join(["{:.10f}".format(c) for c in latvecs[i1][1]]) + "\n"
        L += "lattice " + " ".join(["{:.10f}".format(c) for c in latvecs[i1][2]]) + "\n"

        for i2, x in enumerate(each_coors):
            L += "atom " + " ".join(["{:.10f}".format(a) for a in x]) + " " + elements[i1][i2] + " 0.0 0.0 "\
                 + " ".join(["{:.8f}".format(float(b)) for b in forces[i1][i2]]) + "\n"

        L += "energy " + energies[i1][0] + "\n" + "charge 0.0\nend\n"
        Ltot += L
        i0 += 1

        inputlines.append(L)

    return Ltot, i0, inputlines

if __name__ == "__main__":
    nowpath = os.getcwd()

    xdatfiles = sorted([x for x in os.listdir(nowpath) if "XDATCAR" in x])
    posfiles  = sorted([x for x in os.listdir(nowpath) if "POSCAR" in x])
    outfiles  = sorted([x for x in os.listdir(nowpath) if "OUTCAR" in x])

    Ltot = ""; i0 = 1; inputlines_tot = []
    print("\nRead XDATCARs!\n")
    for i, xdatfile in enumerate(xdatfiles):
        print(xdatfile.replace("XDATCAR_",""))
        energy_config, force_config = get_energy_force(nowpath, outfiles[i], posfiles[i])
        latvec_config, coor_config, element_config = get_coor_from_xdat(nowpath, xdatfile)
        L, i0, inputlines = make_inputline(nowpath, energy_config, force_config, latvec_config, coor_config, element_config, xdatfile, i0)
        Ltot += L
        inputlines_tot += inputlines

    with open(nowpath + "/input.data","w") as fi: fi.write(Ltot)

    path_inputs = nowpath + "/inputs"
    if not os.path.exists(path_inputs): os.mkdir(path_inputs)

    print("\nMake each strucure data!\n")
    for n, li in enumerate(inputlines_tot):
        print(n+1)
        if len(str(n+1)) == 1: str_n = "000" + str(n+1)
        elif len(str(n+1)) == 2: str_n = "00" + str(n+1)
        elif len(str(n+1)) == 3: str_n = "0" + str(n+1)
        else: str_n = str(n+1)
        with open(path_inputs + "/structure" + str_n + ".data", "w") as fl: fl.write(li)

    for x in xdatfiles + posfiles + outfiles: os.remove(nowpath + "/" + x)

    print("\nDone!\n")

# END: script" + filenamecd C
