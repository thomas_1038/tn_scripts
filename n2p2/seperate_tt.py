#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, random
import subprocess as sub
import numpy as np

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def seperate_percent(nowpath, percent):
    path_inputs = nowpath + "/inputs"
    path_traindata = path_inputs + "/traindata"
    path_testdata = path_inputs + "/testdata"
    for p in [path_traindata, path_testdata]:
        if not os.path.exists(p): os.mkdir(p)

    structure_files = sorted([x for x in os.listdir(path_inputs) if "structure" in x and ".data" in x])
    train_files = random.sample(structure_files, round(len(structure_files) * percent/100))
    test_files = sorted([x for x in structure_files if not x in train_files])

    for tr_f in train_files:
        shutil.copy(path_inputs + "/" + tr_f, path_traindata)
    for te_f in test_files:
        shutil.copy(path_inputs + "/" + te_f, path_testdata)

    print("\nN(train): " + str(len(train_files)) + "\n" + "N(test): " + str(len(test_files)) + "\n")

def seperate_num(nowpath, num_ext):
    path_inputs = nowpath + "/inputs"
    path_traindata = path_inputs + "/traindata"
    path_testdata = path_inputs + "/testdata"
    for p in [path_traindata, path_testdata]:
        if not os.path.exists(p): os.mkdir(p)

    structure_files = sorted([x for x in os.listdir(path_inputs) if "structure" in x and ".data" in x])
    train_files = random.sample(structure_files, num_ext)
    test_files = sorted([x for x in structure_files if not x in train_files])

    for tr_f in train_files:
        shutil.copy(path_inputs + "/" + tr_f, path_traindata)
    for te_f in test_files:
        shutil.copy(path_inputs + "/" + te_f, path_testdata)

    print("\nN(train): " + str(len(train_files)) + "\n" + "N(test): " + str(len(test_files)) + "\n")

if __name__ == "__main__":
    nowpath = os.getcwd()
    args = sys.argv

    if len(args) == 1:
        Q = input("Which do you want to seperate data based on percentage or the number? [p/n]: ")
        if "p" in Q:
            percent = input("Please input the percent, e.g., 80: ")
            seperate_percent(nowpath, int(percent))
        else:
            num_ext = input("Please input the number you want to extranct: ")
            seperate_num(nowpath, int(num_ext))

    else:
        if args[1] == "-p": seperate_percent(nowpath, int(args[2]))
        if args[1] == "-n": seperate_num(nowpath, int(args[2]))
        else:
            print("Please input -p; percentage or -n; number! BYE!"); sys.exit()

    print("\nDone!\n")
