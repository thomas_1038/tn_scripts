#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

def plot_energy_and_force_n2p2(nowpath, filename, ef, Na):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    mpl.rcParams['agg.path.chunksize'] = 100000
    path_file = nowpath + "/" + filename
    with open(path_file) as f: enlines = f.readlines()
    input_data = [float(conv_line(l)[1]) for l in enlines]
    train_data = [float(conv_line(l)[2]) for l in enlines]
    if ef == "e":
        diff_data = [float(conv_line(l)[3])/Na for l in enlines]
    else:
        diff_data = [float(conv_line(l)[3]) for l in enlines]

    if ef == "e":
      label_x = "Energy_input (eV)"; label_y = "Energy_train (eV)"; label_x_hist = "Diff_energy_per_atom (eV)"; unit = "meV/atom"
    else:
      label_x = "Force_input (eV/Å)"; label_y = "Force_train (eV/Å)"; label_x_hist = "Diff_force (eV/Å)"; unit = "meV/Å"

    ave = np.average([abs(x) for x in diff_data])
    rmse = np.sqrt(mean_squared_error(input_data, train_data))
    if ef == "e":
        print("\nEave: " + str(round(ave*1000,2)) + " " + unit)
        print("RMSE: " + str(round(rmse/Na*1000, 2)) + " " + unit + "\n")
    else: 
        print("\nEave: " + str(round(ave*1000, 2)) + " " + unit)
        print("RMSE: " + str(round(rmse*1000, 2)) + " " + unit + "\n")

    plt.subplot(121)
    clf = linear_model.LinearRegression()
    df_x = pd.DataFrame(input_data)
    df_y = pd.DataFrame(train_data)
    clf.fit(df_x, df_y)
    plt.plot(df_x, clf.predict(df_x), "k", linewidth=0.5)
    r2 = clf.score(df_x, df_y)
    print("R2: "+ str(round(r2,4)))
    plt.text(len(input_data)*0.9, len(input_data)*0.1, 'R2=' + str(round(r2, 4)), size=12)

    plt.scatter(input_data, train_data, 3, "b", "o", alpha=0.9)
    #plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
    #plt.locator_params(axis='x',nbins=6) #y軸，6個以内
    plt.xlabel(label_x, fontsize=15)
    plt.ylabel(label_y, fontsize=15)
    #plt.grid(True)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.xlim(min(min(input_data),min(train_data)),max(max(input_data),max(train_data)))
    plt.ylim(min(min(input_data),min(train_data)),max(max(input_data),max(train_data)))
    #plt.xlim(-15,15)
    #plt.ylim(-15,15)
    #plt.legend(loc="best",prop={'size':7})
    plt.minorticks_on

    plt.subplot(122)
    if ef == "e":
        plt.hist(diff_data, 40, rwidth=0.8, range=(-2e-2, 2e-2))
    else:
        plt.hist(diff_data, 50, rwidth=0.8, range=(-0.5, 0.5))
    plt.xlabel(label_x_hist, fontsize=15)
    plt.ylabel("Freq.", fontsize=15)
    #plt.grid(True)
    plt.gca().xaxis.set_major_formatter(plt.FormatStrFormatter('%.3f'))
    plt.locator_params(axis='x',nbins=6)#y
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    #plt.legend(loc="best",prop={'size':7})
    plt.minorticks_on
    plt.plot()
    plt.show()

if __name__ == "__main__":
    ef = input("Do you want to plot energy or force data? [e/f]: ")
    filename = input("Please input the filename; energy, forcex, forcey, forcez, [e, fx, fy, fz]: ")
    if "e" == filename:
        filename = "energy_tot.out"
    elif "fx" == filename:
        filename = "forcex_tot.out"
    elif "fy" == filename:
        filename = "forcey_tot.out"
    elif "fz" == filename:
        filename = "forcez_tot.out"
    plot_energy_and_force_n2p2(os.getcwd(), filename, ef, 116)
