#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def make_inputdata_from_structures(nowpath):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    st_files = sorted([x for x in os.listdir(nowpath) if os.path.isfile(nowpath+"/"+x) and "structure" in x])

    L = ""
    for i, sf in enumerate(st_files):
        print(sf)
        with open(nowpath + "/" + sf) as f: sflines = f.readlines()

        for sl in sflines:
            if "comment" in sl:
                list_sl = conv_line(sl)
                list_sl[1] = "#" + str(i+1)
                sl = " ".join(list_sl) + "\n"
            L += sl

    with open(nowpath + "/input.data", "w") as fw: fw.write(L)

if __name__ == "__main__":
    make_inputdata_from_structures(os.getcwd())
    print("Done!")
