#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra

def seperate_inputdata(nowpath):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path_predict = nowpath + "/predict"
    if not os.path.exists(path_predict): os.mkdir(path_predict)
    path_inputs = path_predict + "/inputs"
    if not os.path.exists(path_inputs): os.mkdir(path_inputs)

    path_input = nowpath + "/input.data"
    with open(path_input) as f: inputdatalines = f.readlines()

    L = ""; i = 1
    for l in inputdatalines:
        L += l
        if "end" in l:
            if len(str(i)) == 1: str_i = "000" + str(i)
            elif len(str(i)) == 2: str_i = "00" + str(i)
            elif len(str(i)) == 3: str_i = "0" + str(i)
            else: str_i = str(i)

            with open(path_inputs + "/structure" + str_i + ".data", "w") as f: f.write(L)
            L = ""
            i += 1

if __name__ == "__main__":
    seperate_inputdata(os.getcwd())
    print("Done!")
