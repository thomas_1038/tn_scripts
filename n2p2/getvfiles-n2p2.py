#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess as sub
import numpy as np
import n2p2.mkinputdata as npmkid

def get_vaspfiles_from_eachcalc(path_dir, path_xdats):
    if not os.path.exists(path_xdats): os.mkdir(path_xdats)

    tempdirs = sorted([x for x in os.listdir(path_dir) if os.path.isdir(path_dir + "/" + x) and "k" in x and "tmp" not in x])
    for tdir in tempdirs:
        print(tdir)

        path_tdir_in_xdats = path_xdats + "/" + tdir
        if not os.path.exists(path_tdir_in_xdats):
            os.mkdir(path_tdir_in_xdats)

        path_tdir = path_dir + "/" + tdir
        backupdirs = sorted([x for x in os.listdir(path_tdir) if os.path.isdir(path_tdir + "/" + x) and "backup" in x])

        poscars = sorted([x for x in os.listdir(path_tdir) if os.path.isfile(path_tdir + "/" + x) and "POSCAR_" in x and ".vasp" in x])
        i = 1
        for pos in poscars:
            shutil.copy(path_tdir + "/" + pos, path_tdir_in_xdats + "/POSCAR_" + str(i) + "_" + tdir)
            i += 1
        shutil.copy(path_tdir + "/POSCAR", path_tdir_in_xdats + "/POSCAR_" + str(i) + "_" + tdir)

        i = 1
        for bdir in backupdirs:
            path_bdir = path_tdir + "/" + bdir
            shutil.copy(path_bdir + "/XDATCAR", path_tdir_in_xdats + "/XDATCAR_" + str(i) + "_" + tdir)
            shutil.copy(path_bdir + "/OUTCAR", path_tdir_in_xdats + "/OUTCAR_" + str(i) + "_" + tdir)
            i += 1
        shutil.copy(path_tdir + "/XDATCAR", path_tdir_in_xdats + "/XDATCAR_" + str(i) + "_" + tdir)
        shutil.copy(path_tdir + "/OUTCAR", path_tdir_in_xdats + "/OUTCAR_" + str(i) + "_" + tdir)

        # convert vasp format to n2p2 format
        xdatfiles = sorted([x for x in os.listdir(path_tdir_in_xdats) if "XDATCAR" in x])
        posfiles  = sorted([x for x in os.listdir(path_tdir_in_xdats) if "POSCAR" in x])
        outfiles  = sorted([x for x in os.listdir(path_tdir_in_xdats) if "OUTCAR" in x])

        Ltot = ""; i0 = 1; inputlines_tot = []
        print("\nRead XDATCARs!\n")
        for i, xdatfile in enumerate(xdatfiles):
            print(xdatfile.replace("XDATCAR_",""))
            energy_config, force_config = npmkid.get_energy_force(path_tdir_in_xdats, outfiles[i], posfiles[i])
            latvec_config, coor_config, element_config = npmkid.get_coor_from_xdat(path_tdir_in_xdats, xdatfile)
            L, i0, inputlines = npmkid.make_inputline(path_tdir_in_xdats, energy_config, force_config, latvec_config, coor_config, element_config, xdatfile, i0)
            Ltot += L
            inputlines_tot += inputlines

        with open(path_tdir_in_xdats + "/input.data","w") as fi: fi.write(Ltot)

        path_inputs = path_tdir_in_xdats + "/inputs"
        if not os.path.exists(path_inputs): os.mkdir(path_inputs)

        print("\nMake each strucure data!\n")
        for n, li in enumerate(inputlines_tot):
            print(n+1)
            if len(str(n+1)) == 1: str_n = "000" + str(n+1)
            elif len(str(n+1)) == 2: str_n = "00" + str(n+1)
            elif len(str(n+1)) == 3: str_n = "0" + str(n+1)
            else: str_n = str(n+1)
            with open(path_inputs + "/structure" + str_n + ".data", "w") as fl: fl.write(li)

        for x in xdatfiles + posfiles + outfiles:
            os.remove(path_tdir_in_xdats + "/" + x)

def get_files_for_n2p2_from_dir(path_dir, path_xdats):
    #nowpath = os.getcwd()
    names_path_dirs = path_dir.split("/")
    dirname = names_path_dirs[-1]

    if not os.path.exists(path_xdats): os.mkdir(path_xdats)

    xdats = sorted([x for x in os.listdir(path_dir) if os.path.isfile(path_dir + "/" + x) and "XDATCAR_" in x])
    outs = sorted([x for x in os.listdir(path_dir) if os.path.isfile(path_dir + "/" + x) and "OUTCAR_" in x])
    poss = sorted([x for x in os.listdir(path_dir) if os.path.isfile(path_dir + "/" + x) and "POSCAR_" in x])

    for pos in poss:
        shutil.copy(path_dir + "/" + pos, path_xdats + "/" + pos)
        if ".gz" in pos:
            sub.call("gunzip " + path_xdats + "/" + pos, shell=True)
        os.rename(path_xdats + "/" + pos.replace(".gz",""), path_xdats + "/" + pos.replace(".gz","") + "_" + dirname)

    for xdat in xdats:
        shutil.copy(path_dir + "/" + xdat, path_xdats + "/" + xdat)
        if ".gz" in xdat:
            sub.call("gunzip " + path_xdats + "/" + xdat, shell=True)
        os.rename(path_xdats + "/" + xdat.replace(".gz",""), path_xdats + "/" + xdat.replace(".gz","") + "_" + dirname)

    for out in outs:
        shutil.copy(path_dir + "/" + out, path_xdats + "/" + out)
        if ".gz" in out:
            sub.call("gunzip " + path_xdats + "/" + out, shell=True)
        os.rename(path_xdats + "/" + out.replace(".gz",""), path_xdats + "/" + out.replace(".gz","") + "_" + dirname)

if __name__ == "__main__":
    nowpath = os.getcwd()
    names_path_dirs = nowpath.split("/")
    path_xdats = nowpath + "/vfiles_" + names_path_dirs[-2] + "_" + names_path_dirs[-1]

    Q1 = input("Peter's or mine; [p, m]: ")
    if Q1 == "m":
        get_vaspfiles_from_eachcalc(nowpath, path_xdats)

    else:
        Q2 = input("Do you want to get files from 'ONE'directory or directorie'S'? [f, l]: ")
        if Q2 == "f":
            get_files_for_n2p2_from_dir(nowpath, path_xdats)
        else:
            dirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x) and "start" not in x and "xdats" not in x and "set_a=exp" not in x])
            for dr in dirs:
                print(dr)
                path_dir = nowpath + "/" + dr
                get_files_for_n2p2_from_dir(path_dir, path_xdats)

    print("\nDone!\n")
#End script
