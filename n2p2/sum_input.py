#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def get_train_and_test_files(nowpath):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    vdirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x) if "vfiles" in x])
    path_train_sum = nowpath + "/traindata_sum"
    path_test_sum = nowpath + "/testdata_sum"

    for p in [path_train_sum, path_test_sum]:
        if not os.path.exists(p): os.mkdir(p)

    i_tr = 1; i_te = 1
    for vdr in vdirs:
        print(vdr)
        path_inputs = nowpath + "/" + vdr + "/inputs"
        path_train = path_inputs + "/traindata"
        path_test = path_inputs + "/testdata"
        train_files = sorted([x for x in os.listdir(path_train)])
        test_files = sorted([x for x in os.listdir(path_test)])

        for trf in train_files:
            if   len(str(i_tr)) == 1: str_i_tr = "0000" + str(i_tr)
            elif len(str(i_tr)) == 2: str_i_tr = "000" + str(i_tr)
            elif len(str(i_tr)) == 3: str_i_tr = "00" + str(i_tr)
            elif len(str(i_tr)) == 4: str_i_tr = "0" + str(i_tr)
            else: str_i_tr = str(i_tr)

            shutil.copy(path_train + "/" + trf, path_train_sum + "/structure" + str_i_tr + ".data")
            i_tr += 1

        for tef in test_files:
            if   len(str(i_te)) == 1: str_i_te = "0000" + str(i_te)
            elif len(str(i_te)) == 2: str_i_te = "000" + str(i_te)
            elif len(str(i_te)) == 3: str_i_te = "00" + str(i_te)
            elif len(str(i_te)) == 4: str_i_te = "0" + str(i_te)
            else: str_i_te = str(i_te)

            shutil.copy(path_test + "/" + tef, path_test_sum + "/structure" + str_i_te + ".data")
            i_te += 1

if __name__ == "__main__":
    get_train_and_test_files(os.getcwd())
    print("Done!")
