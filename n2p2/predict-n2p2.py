#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np

predict = "/home/4/17D20121/build/n2p2_intel18_intelmpi18/bin/nnp-predict 1"

nowpath = os.getcwd()
path_predict = nowpath + "/predict"
path_inputs = path_predict + "/inputs"

# get weight files
weight_files = [x for x in os.listdir(nowpath) if "weights" in x and ".data" in x]
if len(weight_files) == 0:
    print("weights.XXX.data does not exist here! Please prepare, BYE!\n")
    sys.exit()
#else:
#    for wf in weight_files: shutil.copy(nowpath + "/" + wf, path_predict)

# get necessary files
necessary_files = ["input.nn", "scaling.data"]
for f in necessary_files:
    if not os.path.exists(nowpath + "/" + f):
        print(f + " does not exist here! Please prepare, BYE!\n")
        sys.exit()
    #else: shutil.copy(nowpath + "/" + f, path_inputs)

# definition of dirs
path_energies =   path_predict + "/energies"
path_nnatoms =    path_predict + "/nnatoms"
path_nnforces =   path_predict + "/nnforces"
path_outputs =    path_predict + "/outputs"
path_structures = path_predict + "/structrues"
path_calc = path_predict + "/calc"
for p in [path_energies, path_nnatoms, path_nnforces, path_outputs, path_structures, path_calc]:
    if not os.path.exists(p): os.mkdir(p)

L_Etot = ""
structure_files = [x for x in os.listdir(path_inputs) if "structure" in x and ".data" in x]
#os.chdir(path_inputs)
fet = open(path_predict + "/energy_tot.out", "w")

for i, sf in enumerate(structure_files):
    if len(str(i+1)) == 1: str_i = "0000" + str(i+1)
    elif len(str(i+1)) == 2: str_i = "000" + str(i+1)
    elif len(str(i+1)) == 3: str_i = "00" + str(i+1)
    elif len(str(i+1)) == 4: str_i = "0" + str(i+1)
    else: str_i = str(i+1)

    path_dir = path_calc + "/" + str_i
    if not os.path.exists(path_dir): os.mkdir(path_dir)

    #copy necessary files for calculation
    for wf in weight_files: shutil.copy(nowpath + "/" + wf, path_dir)
    for nf in necessary_files: shutil.copy(nowpath + "/" + nf, path_dir)

    path_sf = path_inputs + "/" + sf
    shutil.copy(path_sf, path_dir + "/input.data")

    #execute nnp-predict
    os.chdir(path_dir)
    sub.call(predict, shell=True)

    shutil.copy(path_dir + "/energy.out",    path_energies + "/energy" + str_i + ".out")
    shutil.copy(path_dir + "/nnatoms.out",   path_nnatoms + "/nnatoms" + str_i + ".out")
    shutil.copy(path_dir + "/nnforces.out",  path_nnforces + "/nnforces" + str_i + ".out")
    shutil.copy(path_dir + "/output.data",   path_outputs + "/output" + str_i + ".data")
    shutil.copy(path_dir + "/structure.out", path_structures + "/structure" + str_i + ".out")

    with open(path_dir + "/energy.out") as fe: energylines = fe.readlines()
    L = str(i+1) + " " + energylines[-1].replace("\n","") + "\n"
    fet.write(L)

# End: script
