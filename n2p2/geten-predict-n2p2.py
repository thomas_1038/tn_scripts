#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
efiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "energy" in x and "tot" not in x])

Ltot = ""
for i, ef in enumerate(efiles):
    print(i+1)
    with open(path + "/" + ef) as f: enlines = f.readlines()
    Ltot += str(i+1) + " " + enlines[-1].replace("\n","") + "\n"

with open(path + "/energy_tot.out", "w") as fet: fet.write(Ltot)    
print("Done!")
