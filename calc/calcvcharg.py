#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce
from addmol import readpos as rp

def calc_vchg():
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0: line.remove("")
        return line

    path = os.getcwd()
    path_pos = path + "/POSCAR"
    with open(path_pos) as fps: poslines = fps.readlines()
    numofel = rp.get_numofel(poslines)
    #print numofel

    path_pot = path + "/POTCAR"
    with open(path_pot) as fpt: potlines = fpt.readlines()

    ind = 0; num_el = 0; vchg_el = []
    for i in range(0,len(potlines)):
        if i == 0: num = 1; ind = 1
        elif "End of Dataset" in potlines[i]: num = 2; ind = 1; num_el += 1

        if num_el == len(numofel): break
        if ind == 1: ind = 0; vchg_el.append(conv_line(potlines[i+num])[0]); continue

    #print numofel, vchg_el
    chg_all = [float(numofel[i]) * float(vchg_el[i]) for i in range(0,len(numofel))]
    return str(int(sum(chg_all)))

#print calc_vchg()
if __name__ == "__main__": print("\n" + calc_vchg() + "\n")

#files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
#dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

