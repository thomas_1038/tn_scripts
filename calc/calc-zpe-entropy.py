#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import math

def calc_zpe(outlines):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    vibenergies = [float(conv_line(ol)[-2])/1000 for ol in outlines if "cm-1" in ol]
    zpe = round(sum(vibenergies) * 0.5, 5)

    return zpe

def calc_vib_entropy(outlines, temp_ini=0, temp_fin=1001, temp_grid=10):
    kB = 8.6173629250e-5

    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    vibenergies = [float(conv_line(ol)[-2])/1000 for ol in outlines if "cm-1" in ol]
    temperatures = list(range(temp_ini, temp_fin, temp_grid))

    entopy_x_temp = []
    for tc in temperatures:
        t = tc + 273.15
        kBt = kB * t

        #for vibe in vibenergies:
        #    print(math.exp(-1*vibe/kBt))
        #sys.exit()

        sigma_exp1 = sum([math.log(1.0 - math.exp(-1.0 * vibe/kBt)) for vibe in vibenergies])
        sigma_exp2 = sum([vibe * 1.0/(math.exp(vibe/kBt) -1.0) for vibe in vibenergies])

        TS =  sigma_exp2 - kBt * sigma_exp1
        entopy_x_temp.append(round(TS, 5))

    return temperatures, entopy_x_temp

if __name__ == "__main__":
    nowpath = os.getcwd()
    path_out = nowpath + "/OUTCAR"
    if not os.path.exists(path_out):
        print("Please prepare OUTCAR here! BYE!"); sys.exit()

    with open(path_out) as fo: outlines = fo.readlines()

    zpe = calc_zpe(outlines)
    with open(nowpath + "/zpe.dat", "w") as fz: fz.write("ZPE: " + str(zpe))

    temperatures, entopy_x_temp = calc_vib_entropy(outlines)
    L = ""
    for i, t in enumerate(temperatures):
        L += str(t) + "  " + str(entopy_x_temp[i]) + "\n"
    with open(nowpath + "/entropy.dat", "w") as fe: fe.write(L)

    print("\nDone!\n")
# END: Program