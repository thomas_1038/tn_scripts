#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, math, shutil
import numpy as np
import addmol.readpos as rp
import addmol.helper as hl

argv = sys.argv
range_min = float(input("Please input the minimum range, e.g., 0.40: "))
range_max = float(input("Please input the maximum range, e.g., 0.70: "))
#range_min = 0.40; range_max = 0.70

nowpath = os.getcwd()
path_pos = nowpath + "/POSCAR"
#path_vasprun = nowpath + "/vasprun.xml"
parchg_name = [x for x in os.listdir(nowpath) if "PARCHG" in x][0]
path_parchg = nowpath + "/" + parchg_name

if not os.path.exists(path_parchg):
    print("################################################################")
    print("#   PARCHG does not exist! You should prepare PARCHG. BYE!     #")
    print("################################################################")
    sys.exit()

if not os.path.exists(path_pos):
    print("################################################################")
    print("#  POSCAR does not exist! You should prepare path_pos_wl. BYE!   #")
    print("################################################################")
    sys.exit()

# Read CONTCAR
print("Read POSCAR ...")
with open(path_pos) as fp: poslines = fp.readlines()

sumatomnum = rp.get_sumofnumofel(poslines)
firstlines, flagS = rp.get_firstlines(poslines) #POSCARの始め数行(Direct/Cartesianまで)
mat_lat = rp.get_matrix(poslines) #格子ベクトル
coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
len_pos = len(firstlines) + len(coor_car)
if flagS == 1: len_pos -= 1

mat_c  = mat_lat[2]
lc_c_pos = math.sqrt(float(mat_c[0])**2 + float(mat_c[1])**2 + float(mat_c[2])**2)

# Make PARCHG line
print("Read PARCHG ...")
with open(path_parchg) as fl2: lpline = fl2.read()
lpline = lpline.split("\n")
firstline = "\n".join(lpline[0:len_pos+1])
del lpline[0:len_pos+1]

grid = hl.conv_line(lpline[0])
print("PARCHG grid are "+ str(grid[0]) + " x " + str(grid[1]) + " x " + str(grid[2]) + " ...")
del lpline[0]

# Split PARCHG line
print("Split PARCHG line ...")
lpline_new = ','.join(lpline)
lpline_new.replace(",","")
list_lpline = lpline_new.split(" ")
list_lpline = [s for s in list_lpline if s != '']
list_lpline = [s for s in list_lpline if s != ',']

# splist by ab-plane
print("Sum potential energies on same z ...")
group_by = int(grid[0])*int(grid[1])
list_parchg = [list_lpline[i:i + group_by] for i in range(0, len(list_lpline), group_by)]


nt = 0; list_parchg2 = []
for lst in list_parchg:
    print(str(nt+1) + "/" + str(grid[2]))
    list = []
    for val in lst: list.append(float(val.replace(",", "")))
    else: list_parchg2.append(list); nt += 1
    if nt == int(grid[2]): break

print("\nMake z axis ...")
list_z = [i/len(list_parchg2) for i in range(0, len(list_parchg2))]

for i, lz in enumerate(list_z):
    if range_min <= lz <= range_max: pass
    else: list_parchg2[i] = [0 for j in range(0, len(list_parchg2[0]))]

list_parchg_new = []
for l in list_parchg2: list_parchg_new += l
Ltot = firstline + "\n   " +  "  ".join(grid) + "\n"

i = 1
for pchg in list_parchg_new:
    if i < 10: Ltot += str("{:.4f}".format(pchg)) + "  "; i += 1
    elif i == 10: Ltot += str("{:.4f}".format(pchg)) + "\n"; i = 1

with open(nowpath+"/"+parchg_name.replace(".vasp","")+"_"+str(range_min)+"-"+str(range_max)+".vasp", "w") as ft: ft.write(Ltot)
print("\nDONE!\n")
# END: script