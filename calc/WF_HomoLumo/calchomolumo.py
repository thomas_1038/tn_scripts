#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys
import os

#### INPUT ########
MAX = 1
#### INPUT ########

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
path_DOStot = path + "/DOSCAR_tot"
flag_DOStot = os.path.exists(path_DOStot)
vasprun = "../vasprun.xml"

#argv = sys.argv
if flag_DOStot == False:
    print("\n")
    print("########################################################")
    print("#  You should input DOSCAR_tot after script name! BYE! #")
    print("########################################################")
    print("\n")
    sys.exit()

flag_vasprun = os.path.exists(vasprun)
if flag_vasprun == False:
    print("#######################################################################")
    print("#  vasprun.xml does not exist! You should prepare vasprun.xml. BYE!   #")
    print("#######################################################################")
    sys.exit()

# Read vasprun                                                                                                                                                                                                   
f_v = open(vasprun)
vrlines = f_v.readlines()
f_v.close()
fermi = [float(conv_line(line)[2]) for line in vrlines if line.find("efermi") >= 0][0]

# Read DOS
f = open(path_DOStot)
doslines = f.readlines()
f.close()

list_dos = [conv_line(dosline) for dosline in doslines]
legend = list_dos[0]
    
del list_dos[0]
energy = list(map(float,[line[0] for line in list_dos]))
#arr_energy = np.array(energy)
dos = list(map(float,[line[1] for line in list_dos]))

num = 0
dos_max = MAX
en_max = 0
while num < len(energy):
    if energy[num] < 0.1:
        pass
    else:
        if dos[num] == 0 and dos_max == 0:
            pass
        elif dos[num] > dos_max:
            print(energy[num])
            dos_max = dos[num]
            en_max = energy[num]
        elif dos[num] == 0 and dos_max != MAX:
            break
        else:
            pass
    num += 1

HOMO = fermi
LUMO= en_max + fermi
    
L =  "HOMO: " + str(round(HOMO,3)) + "\n"
L += "LUMO: " + str(round(LUMO,3)) + "\n\n"
path_hl = path + "/HOMOLUMO.dat"
f = open(path_hl, "w")
f.write(L)
f.close()
print("HOMOLUMO.dat has been made!") 
