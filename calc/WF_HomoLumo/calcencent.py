#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys
import os

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
argv = sys.argv
if len(argv) == 1:
    print("\n")
    print("######################################################")
    print("#  You should input DOS file after script name! BYE! #")
    print("######################################################")
    print("\n")
    sys.exit()

Q_range_en = input("Please input the range of initial energy you want to calculate, defalt value is -200, [d / num]: ") 
if "d" in Q_range_en:
    range_ini_en = -200
else:
    range_ini_en = float(Q_range_en)

num = 0
for dosfile in argv:
    if num == 0:
        pass
    else:
        # Read DOS
        path_dos = path + "/" + str(dosfile)
        f = open(path_dos)
        doslines = f.readlines()
        f.close()

        list_dos = [conv_line(dosline) for dosline in doslines]
        legend = list_dos[0]

        del list_dos[0]
        energy = list(map(float,[line[0] for line in list_dos]))
        #print energy
        #arr_energy = np.array(energy)

        num_p = 1
        list_Eexp = []
        list_BandWidth = []
        while num_p < len(list_dos[0]):
            list = []
            list_bw = []
            pdos = list(map(float,[line[num_p] for line in list_dos]))
            arr_pdos = np.array(pdos)
            sum_pdos_tofermi = 0
            sum_pdosxen_tofermi = 0
            sum_pdos = 0
            sum_pdosxen = 0
            bandwidth_min = 0
            bandwidth_max = 0

            num_d = 0
            for en in energy:
                if en < range_ini_en:
                    pass
                else:
                    if en < 0:
                        sum_pdos += pdos[num_d]
                        sum_pdosxen += en*pdos[num_d]
                        sum_pdos_tofermi += pdos[num_d]
                        sum_pdosxen_tofermi += en*pdos[num_d]
                    else:
                        sum_pdos += pdos[num_d]
                        sum_pdosxen += en*pdos[num_d]
                num_d += 1

            num_w = 0
            for en in energy:
                if en < range_ini_en:
                    pass
                else:
                    if pdos[num_w] == 0 and bandwidth_min == 0:
                        pass
                    elif pdos[num_w] > 0 and bandwidth_min == 0:
                        bandwidth_min = en
                    elif pdos[num_w] == 0 and bandwidth_min != 0 and bandwidth_max == 0:
                        bandwidth_max = energy[num_w -1]
                        break
                    else:
                        pass
                num_w += 1

            list_bw.append(legend[num_p])
            list_bw.append(bandwidth_min)
            list_bw.append(bandwidth_max)
            list_BandWidth.append(list_bw)

            if sum_pdosxen_tofermi == 0 or sum_pdos_tofermi == 0:
                Eexp_tofermi = 0
            elif sum_pdosxen_tofermi != 0 and sum_pdos_tofermi != 0:
                Eexp_tofermi = sum_pdosxen_tofermi/sum_pdos_tofermi

            if sum_pdosxen == 0 or sum_pdos == 0:
                Eexp = 0
            elif sum_pdosxen != 0 and sum_pdos != 0:
                Eexp = sum_pdosxen/sum_pdos

            list.append(legend[num_p])
            list.append(Eexp_tofermi)
            list.append(Eexp)
            list_Eexp.append(list)
            num_p += 1

        L_Eexp = ""
        for Lexp in list_Eexp:
            L_Eexp += str(Lexp[0]) + "  " + str(round(Lexp[1],3)) + "  " + str(round(Lexp[2],3)) + "\n"
        path_Eexp = path + "/Eexp_" + str(dosfile) + ".dat"
        f = open(path_Eexp, "w")
        f.write(L_Eexp)
        f.close()
        print("Eexp_" + str(dosfile) + ".dat has been made!") 

        L_BW = ""
        for Lbw in list_BandWidth:
            L_BW = str(Lbw[0]) + "  " + str(round(Lbw[1],3)) + "  " + str(round(Lbw[2],3)) + "\n"
        path_BW = path + "/BandWidth_" + str(dosfile) + ".dat"
        f = open(path_BW, "w")
        f.write(L_BW)
        f.close()
        print("BandWidth_" + str(dosfile) + ".dat has been made!")

    num += 1
