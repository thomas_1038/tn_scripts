#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, math, shutil
import numpy as np
import matplotlib.pyplot as plt
from addmol import readpos as rp

###################################################
eff_dig = 2
en_window = 0.3
###################################################

argv = sys.argv
diff_slab_bulk = 0

if len(argv) == 1:
    print("\nPlease input 0/1; 0: non-plot mode, 1: plot mode. BYE!\n"); sys.exit()
else: mode = int(argv[1])
if len(argv) == 3: diff_slab_bulk = float(argv[2])

def conv_line(line):
    line = line.replace('\n',' ').replace('\t',' ').replace('\r',' ').replace('^M',' ').split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
LOCPOT = path + "/LOCPOT"
POSCAR = path + "/POSCAR"
vasprun = path + "/vasprun.xml"

flag_locpot = os.path.exists(LOCPOT)
if flag_locpot == False:
    print("################################################################")
    print("#   LOCPOT does not exist! You should prepare LOCPOT. BYE!     #")
    print("################################################################")
    sys.exit()

flag_pos = os.path.exists(POSCAR)
if flag_pos == False:
    print("################################################################")
    print("#  POSCAR does not exist! You should prepare POSCAR_wl. BYE!   #")
    print("################################################################")
    sys.exit()

flag_vasprun = os.path.exists(vasprun)
if flag_vasprun == False:
    print("#######################################################################")
    print("#  vasprun.xml does not exist! You should prepare vasprun.xml. BYE!   #")
    print("#######################################################################")
    sys.exit()

# Read CONTCAR
print("Read POSCAR ...")
with open(POSCAR) as fp: poslines = fp.readlines()

sumatomnum = rp.get_sumofnumofel(poslines)
firstlines, flagS = rp.get_firstlines(poslines) #POSCARの始め数行(Direct/Cartesianまで)
mat_lat = rp.get_matrix(poslines) #格子ベクトル
coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
len_pos = len(firstlines) + len(coor_car)
if flagS == 1: len_pos -= 1

mat_c  = mat_lat[2]
lc_c_pos = math.sqrt(float(mat_c[0])**2 + float(mat_c[1])**2 + float(mat_c[2])**2)

# Read vasprun
print("Read vasprun.xml ...")
with open(vasprun) as fv: vrlines = fv.readlines()
fermi = [float(conv_line(line)[2]) for line in vrlines if line.find("efermi") >= 0][0] - diff_slab_bulk

# Make LOCPOT line
print("Read LOCPOT ...")
with open(LOCPOT) as fl2: lpline = fl2.read()
lpline = lpline.split("\n")
del lpline[0:len_pos+1]

grid = conv_line(lpline[0])
print("LOCPOT grid are "+ str(grid[0]) + " x " + str(grid[1]) + " x " + str(grid[2]) + " ...")
del lpline[0]

# Split LOCPOT line
print("Split LOCPOT line ...")
lpline_new = ','.join(lpline)
lpline_new.replace(",","")
list_lpline = lpline_new.split(" ")
list_lpline = [s for s in list_lpline if s != '']
list_lpline = [s for s in list_lpline if s != ',']

# splist by ab-plane
print("Sum potential energies on same z ...")
group_by = int(grid[0])*int(grid[1])
list_split = [list_lpline[i:i + group_by] for i in range(0, len(list_lpline), group_by)]
#list_split2 = map(float,[[val.replace(",", "") for val in lst] for lst in list_split])

nt = 0
list_split2 = []
for lst in list_split:
    print(str(nt+1) + "/" + str(grid[2]))
    list = [] #; ntt = 0
    for val in lst: list.append(float(val.replace(",", ""))) #; ntt += 1
    else: list_split2.append(list); nt += 1
    if nt == int(grid[2]): break

print("Calculate averege potentail energies on same z ...")
#pot_ave_xy = [np.round(np.average(np.array(lst)),eff_dig) for lst in list_split2]
pot_ave_xy = [np.average(np.array(l)) for l in list_split2]
pot_ave_xy_basedEf = [np.average(np.array(l)) - float(fermi) for l in list_split2]
#print len(pot_ave_xy)

print("Make z axis ...")
list_z = []; pot_xy = []; pot_xy_basedEf = []
list_z_app = list_z.append; pot_xy_app = pot_xy.append; pot_xy_basedEf_app = pot_xy_basedEf.append
for i in range(0,len(list_split2)):
    a = []; b = []
    val_z = lc_c_pos/len(list_split2) * i
    list_z_app(val_z); a.append(val_z); b.append(val_z)
    a.append(pot_ave_xy[i]); pot_xy_app(a)
    b.append(pot_ave_xy_basedEf[i]); pot_xy_basedEf_app(b)
#print len(list_z)
min_xrange = float(list_z[1])

# sort pot_xy
def sort_pot(pot):
    pot_srt = sorted(pot, key=lambda x:x[1], reverse=True)
    vacpot_highest = pot_srt[0][1]
    list_vacpot = [vp for vp in pot_srt if vacpot_highest - vp[1] < en_window]
    list_vacpot_srt = sorted(list_vacpot, key=lambda x:x[0])
    vx = [lst[0] for lst in list_vacpot_srt]
    vy = [lst[1] for lst in list_vacpot_srt]

    v_range = 0
    numx = 1
    while numx < len(vx):
        diffx = vx[numx]- vx[numx-1]
        if diffx < 2*min_xrange: v_range += float(diffx)
        else: pass
        numx += 1

    return pot_srt, vx, vy, v_range

def calc_amm(v):
    ave = np.average(np.array(v))
    max = np.max(np.array(v))
    min = np.min(np.array(v))
    return ave, max, min

pot_xy_srt, vx, vy, v_range = sort_pot(pot_xy)
pot_xy_srt_Ef, vx_Ef, vy_Ef, vr = sort_pot(pot_xy_basedEf)

vacpot_ave, vacpot_max, vacpot_min = calc_amm(vy)
vacpot_ave_Ef, vacpot_max_Ef, vacpot_min_Ef = calc_amm(vy_Ef)
std = np.std(vy); WF = vacpot_ave - fermi; N_plt = len(vy)

# Write LOCPOT_z.dat
Llp = ""
for  i in range(0,len(list_z)): Llp += str(list_z[i]) + "  " + str(pot_ave_xy[i]) + "\n"
print("Make LOCPOT_z_raw.dat ...")
with open(path + "/LOCPOT_z_raw.dat", "w") as fz: fz.write(Llp)

Llpf = ""
for  i in range(0,len(list_z)): Llpf += str(list_z[i]) + "  " + str(pot_ave_xy_basedEf[i]) + "\n"
print("Make LOCPOT_z_Ef.dat ...")
with open(path + "/LOCPOT_z_Ef.dat", "w") as fz: fz.write(Llpf)

# Write LOCPOT_z.dat
L = ""
for xy in pot_xy_srt_Ef: L += str(xy[0]) + "  " + str(xy[1]) + "\n"
print("Make LOCPOT_z_Ef_srt.dat ...")
with open(path + "/LOCPOT_z_Ef_srt.dat", "w") as fzs: fzs.write(L)

print("\n##############################################################################")
print("# Evac_ave " + str(round(vacpot_ave,3)) + " eV")
print("# std:     " + str(round(std,3)) + " eV")
print("# Efermi   " + str(round(fermi,3)) + " eV")
print("# WF:      " + str(round(WF,1)) + " eV")
print("# N_plt:   " + str(N_plt) + " points")
print("# v_range: " + str(round(v_range,1)) + " Ang")
print("##############################################################################\n")

L_wf =  "# Work Function & Evac, Efermi # \n"
L_wf += "WF:       " + str(round(WF, 1)) + " eV \n"
L_wf += "Evac_ave: " + str(round(vacpot_ave, 3)) + " eV \n"
L_wf += "std:      " + str(round(std,3)) + " eV \n"
L_wf += "Evac_max: " + str(round(vacpot_max, 3)) + " eV \n"
L_wf += "Evac_min: " + str(round(vacpot_min, 3)) + " eV \n"
L_wf += "Efermi:   " + str(round(fermi ,3)) + " eV \n\n"
L_wf += "# Conditions & Accuracies # \n"
L_wf += "Energy window:   " + str(en_window) + " eV \n"
L_wf += "number of plots: " + str(N_plt) + " points \n"
L_wf += "vaccum range:    " + str(round(v_range,1)) + " Ang"
with open(path + "/WF.dat", "w") as fw: fw.write(L_wf)

# plot
def plt_pot(pot, vx, vy, v_fermi, ave, max, min):
    fontsize = 12

    fermiline = [v_fermi for z in list_z]
    plt.subplot(211)
    x = list_z
    y = pot
    p, = plt.plot(x, y, color="blue", markersize=10)
    p, = plt.plot(x, fermiline, color="red",linestyle="dashed")
    plt.ylabel('Energy (eV)',fontsize=fontsize)
    plt.grid(True)
    plt.xticks(fontsize = fontsize); plt.yticks(fontsize = fontsize)

    plt.subplot(212)
    vave = [ave for x in vx]; vmax = [max for x in vx]; vmin = [min for x in vx]
    p, = plt.plot(vx, vy, "bo")
    #p, = plt.plot(vx, vave, color="red")
    #p, = plt.plot(vx, vmax, color="black")
    #p, = plt.plot(vx, vmin, color="black")
    plt.xlabel('z ($\AA$)',fontsize=fontsize)
    plt.ylabel('Energy (eV)',fontsize=fontsize)
    plt.grid(True)
    plt.xticks(fontsize = fontsize); plt.yticks(fontsize = fontsize)
    plt.plot()
    plt.show()

if mode == 1: plt_pot(pot_ave_xy_basedEf, vx_Ef, vy_Ef, 0, vacpot_ave_Ef, vacpot_max_Ef, vacpot_min_Ef)
# END
