#! /usr/bin/env python3                                                            
# -*- coding: utf-8 -*-                                                        

#2015/08/13 version 1.0
#This make Energy vs. iteration step file
#usage:script std.out

import os
import shutil
import numpy as np
import sys
import re
import math
import time

t1 = time.time()

#tolerance_f = 2.5
#mesh = 0.1
filename = input("Please input the file name: ")
tolerance_f = input("Please input tolerance factor., i.e., 2.5: ")
mesh = input("Please input mesh, i.e., 0.5: " )
range_lp = 1.6

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def det3(mat):
  return ((mat[0][0]*mat[1][1]*mat[2][2]) + (mat[0][1]*mat[1][2]*mat[2][0]) + (mat[0][2]*mat[1][0]*mat[2][1]) - (mat[0][2]*mat[1][1]*mat[2][0]) - (mat[0][1]*mat[1][0]*mat[2][2]) - (mat[0][0]*mat[1][2]*mat[2][1\
]))

def frac2cart(cellParam, fracCoords):
  cartCoords = []
  for i in fracCoords:
    xPos = i[0]*cellParam[0][0] + i[1]*cellParam[1][0] + i[2]*cellParam[2][0]
    yPos = i[0]*cellParam[0][1] + i[1]*cellParam[1][1] + i[2]*cellParam[2][1]
    zPos = i[0]*cellParam[0][2] + i[1]*cellParam[1][2] + i[2]*cellParam[2][2]
    cartCoords.append([xPos, yPos, zPos])
  return cartCoords

def cart2frac(cellParam, cartCoords):
  latCnt = [x[:] for x in [[None]*3]*3]
  for a in range(3):
    for b in range(3):
          latCnt[a][b] = cellParam[b][a]

  fracCoords = []
  detLatCnt = det3(latCnt)
  for i in cartCoords:
    aPos = (det3([[i[0], latCnt[0][1], latCnt[0][2]], [i[1], latCnt[1][1], latCnt[1][2]], [i[2], latCnt[2][1], latCnt[2][2]]])) / detLatCnt
    bPos = (det3([[latCnt[0][0], i[0], latCnt[0][2]], [latCnt[1][0], i[1], latCnt[1][2]], [latCnt[2][0], i[2], latCnt[2][2]]])) / detLatCnt
    cPos = (det3([[latCnt[0][0], latCnt[0][1], i[0]], [latCnt[1][0], latCnt[1][1], i[1]], [latCnt[2][0], latCnt[2][1], i[2]]])) / detLatCnt
    fracCoords.append([aPos, bPos, cPos])
  return fracCoords

path = os.getcwd()

#read POSCAR/CONTCAR/SPOSCAR
f = open(filename)
contlines = f.readlines()
f.close()

for line in contlines:
    flag_d1 = line.find("Direct")
    #flag_d2 = line.find("D")
    if flag_d1 >= 0: #or flag_d2 >= 0:
        print("You should convert this POS/CONTCAR by using chgdircar.py!")
        sys.exit()

#read Atom label
atomlabel = contlines[5]
atomlabel = conv_line(atomlabel)
#print atomlabel

#count Atom number
atomnum = contlines[6]
atomnum = conv_line(atomnum)
#print atomnum

#make atomlabellist
lastatom = 0
for num in atomnum:
    lastatom += int(num)

#If
flag_selective = contlines[7].find("Selective")
if flag_selective >= 0:
    #make upper line of POSCAR_car
    num0 = 0
    firstposcarlines = []
    while num0 < 8:
        line = contlines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)

    #make lattice vecor matrix
    num1 = 2
    mat_lat =[]
    while num1 < 5:
        line = contlines[num1]
        line = conv_line(line)
        line = list(map(float, line))

        mat_lat.append(line)
        num1 += 1

    arr_mat_lat = np.array(mat_lat)
    inv_mat_lat = np.linalg.inv(arr_mat_lat)

    #make atom position matrix
    num2 = 9
    lastnum = int(num2) + lastatom
    atomposition = []
    atomposition_forarr = []
    while num2 < lastnum:
        line = contlines[num2]
        line = conv_line(line)

        arr = np.array([line[0], line[1], line[2]])
        arr = list(map(float, arr))
        atomposition_forarr.append(arr)

        atomposition.append(line)
        num2 += 1

else:
    #make upper line of POSCAR_car
    num0 = 0
    firstposcarlines = []
    while num0 < 7:
        line = contlines[num0].replace('\n','')
        firstposcarlines.append(line)
        num0 += 1
    carline = "Cartesian"
    firstposcarlines.append(carline)

    #make lattice vecor matrix
    num1 = 2
    mat_lat =[]
    while num1 < 5:
        line = contlines[num1]
        line = conv_line(line)
        line = list(map(float, line))

        mat_lat.append(line)
        num1 += 1

    arr_mat_lat = np.array(mat_lat)
    inv_mat_lat = np.linalg.inv(arr_mat_lat)

    #make atom position matrix
    num2 = 8
    lastnum = int(num2) + lastatom
    atomposition = []
    atomposition_forarr = []
    while num2 < lastnum:
        line = contlines[num2]
        line = conv_line(line)

        arr = np.array([line[0], line[1], line[2]])
        arr = list(map(float, arr))
        atomposition_forarr.append(arr)

        atomposition.append(line)
        num2 += 1

arr_atomposition_car = np.array(atomposition_forarr)
atomposition_dir = cart2frac(mat_lat, atomposition_forarr)
arr_atomposition_dir = np.array(atomposition_dir)

###  make 3x3x3 coordinate of atoms in support (direct) ###
#A = np.mgrid[-0.2:0.3:.2,-0.2:0.3:.2,-0.2:0.3:.2].reshape(3,-1).T
A = np.mgrid[-1:1.1,-1:1.1,-1:1.1].reshape(3,-1).T

#atomposition3x3x3_dir = [arr_atomposition_dir + vec for vec in A]
atomposition3x3x3_dir = []
append_ad = atomposition3x3x3_dir.append
atomposition3x3x3_dir_test = []
append_ts = atomposition3x3x3_dir_test.append
for vec in A:
    arr_atomposition33_dir = arr_atomposition_dir + vec
    for each_atps_dir in arr_atomposition33_dir:
        append_ts(each_atps_dir)
        if abs(each_atps_dir[0]) < range_lp and abs(each_atps_dir[1]) < range_lp and abs(each_atps_dir[2]) < range_lp: 
            append_ad(each_atps_dir)

print("ALL " + str(len(atomposition3x3x3_dir_test)) + " 30% " + str(len(atomposition3x3x3_dir)))
arr_atomposition3x3x3_dir = np.array(atomposition3x3x3_dir)
lst_atomposition3x3x3_dir = arr_atomposition3x3x3_dir.tolist()
arr_atomposition3x3x3_car = np.array(frac2cart(mat_lat, lst_atomposition3x3x3_dir))

#print len(arr_atomposition3x3x3_car)
#print arr_atomposition3x3x3_car
### fin ###

lat_a = np.sqrt(float(mat_lat[0][0])**2 + float(mat_lat[0][1])**2 + float(mat_lat[0][2])**2)
lat_b = np.sqrt(float(mat_lat[1][0])**2 + float(mat_lat[1][1])**2 + float(mat_lat[1][2])**2)
lat_c = np.sqrt(float(mat_lat[2][0])**2 + float(mat_lat[2][1])**2 + float(mat_lat[2][2])**2)

#len_a = lat_a - float(mesh)
#len_b = lat_b - float(mesh)
#len_c = lat_c - float(mesh)

#lst_lp = np.mgrid[0:len_a:float(mesh), 0:len_b:float(mesh), 0:len_c:float(mesh)].reshape(3,-1).T
#print lst_lp
#print len(lst_lp)

mesh_a = float(mesh)/lat_a
mesh_b = float(mesh)/lat_b
mesh_c = float(mesh)/lat_c

len_a = 1 - mesh_a
len_b = 1 - mesh_b
len_c = 1 - mesh_c

lst_lp_dir = np.mgrid[0:len_a:mesh_a, 0:len_b:mesh_b, 0:len_c:mesh_c].reshape(3,-1).T
#print lst_lp_dir
#print len(lst_lp_dir)

lst_lp = np.array(frac2cart(mat_lat, lst_lp_dir))
#print lst_lp
#print len(lst_lp)

#total_coor = [lp.tolist() for lp in lst_lp if min(np.array([np.linalg.norm(lp - pos_at) for pos_at in arr_atomposition3x3x3_car])) > float(tolerance_f)]
total_coor = []
append_tot = total_coor.append                                                                                                                                                                                   
num_lp =0
for lp in lst_lp:
    print(str(num_lp) + " / " + str(len(lst_lp)))
    list_dis = np.array([np.linalg.norm(lp - pos_at) for pos_at in arr_atomposition3x3x3_car])
    if min(list_dis) > float(tolerance_f):
                append_tot(lp.tolist())
    num_lp += 1

#mesh_c = 0
#total_coor = []
#while mesh_c < lat_c - float(mesh):
#   mesh_b = 0
#   while mesh_b < lat_b - float(mesh):
#       mesh_a = 0
#       while mesh_a < lat_a - float(mesh):
#           print str(mesh_a) + " " + str(mesh_b) + " " + str(mesh_c) 
#           coor = np.array([mesh_a, mesh_b, mesh_c])
#           list_dis = []
#           for pos_at in arr_atomposition3x3x3_car:
#               dis_at = np.linalg.norm(coor - pos_at)
#               list_dis.append(dis_at)
#               
#           if min(list_dis) > float(tolerance_f):
#               total_coor.append(coor.tolist())
#           
#           mesh_a += float(mesh)
#       mesh_b += float(mesh)
#   mesh_c += float(mesh)

print(total_coor)
print(len(total_coor))

#total_coor_uniq = []
#for x1 in total_coor:
#    if x1 not in total_coor_uniq:
#        total_coor_uniq.append(x1)

L_forpos = ""
L_label = ""
for label_at in atomlabel:
     L_label += label_at + " "
L_label += "X"

L_num = ""
for num_at in atomnum:
    L_num += num_at + " "
L_num += str(len(total_coor))

L_forpos += L_label + " | tolerance factor:  " + str(tolerance_f) + "Ang | mesh: " + str(mesh) + " Ang \n"
L_forpos +=  "1.0" + "\n"
L_forpos += str(mat_lat[0][0]) + "  " + str(mat_lat[0][1]) + "  " + str(mat_lat[0][2]) + "\n"
L_forpos += str(mat_lat[1][0]) + "  " + str(mat_lat[1][1]) + "  " + str(mat_lat[1][2]) + "\n"
L_forpos += str(mat_lat[2][0]) + "  " + str(mat_lat[2][1]) + "  " + str(mat_lat[2][2]) + "\n"
L_forpos += L_label + "\n"
L_forpos += L_num + "\n"

if flag_selective >= 0:
    L_forpos += "Selective" + "\n"
L_forpos += "Cartesian" + "\n"

for atmposi in atomposition:
    line_atmps = ""
    for cmp_atmps in atmposi:
        line_atmps += str(cmp_atmps) + "  "
    line_atmps += "\n"
    L_forpos += line_atmps

num_X = 1
for coor_X in total_coor:
    line_coor = str(coor_X[0]) + "  " + str(coor_X[1]) + "  " + str(coor_X[2]) + "  !XX" + str(num_X) + "\n"
    L_forpos += line_coor
    num_X += 1

POSCAR_new = path + "/" + filename  + "_X.vasp" 
f_pos = open(POSCAR_new, "w")
f_pos.write(L_forpos)
f_pos.close()


L_lp = "lattice poinst " + filename + " | mesh: " + str(mesh) + " Ang \n"
L_lp +=  "1.0" + "\n"
L_lp += str(mat_lat[0][0]) + "  " + str(mat_lat[0][1]) + "  " + str(mat_lat[0][2]) + "\n"
L_lp += str(mat_lat[1][0]) + "  " + str(mat_lat[1][1]) + "  " + str(mat_lat[1][2]) + "\n"
L_lp += str(mat_lat[2][0]) + "  " + str(mat_lat[2][1]) + "  " + str(mat_lat[2][2]) + "\n"
L_lp += "X" + "\n"
L_lp += str(len(lst_lp)) + "\n" + "Cartesian" + "\n"

L_lp_coor = ""            
for line_lp in lst_lp:
    line_lp = line_lp.tolist()
    L_lp_coor += str(line_lp[0])+ " " + str(line_lp[1]) + " " + str(line_lp[2]) + "\n" 
    
path_lp = path + "/LP_" + filename + ".vasp"    
with open(path_lp, "w") as file:
    file.write(L_lp)
    file.write(L_lp_coor)

t2 = time.time()
elapsedtime = t2 -t1
print("\n")
print("elapsed time: " + str(elapsedtime))
print("\n")

