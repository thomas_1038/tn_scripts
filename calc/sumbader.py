#!/usr/bin/env python3                                                            
# -*- coding: utf-8 -*-                                                        
#2016/10/25 ver. 1.000

import os
import os.path
import sys
import shutil
import threading
import re

# Definition of path
path = os.getcwd()

# START:get number of Valence electron (NVE)
## PATH
POTCAR = "../POTCAR"

## Read POTCAR
flag_POTCAR = os.path.exists(POTCAR)
if flag_POTCAR == False:
    print("POTCAR doesn't exist!")
    sys.exit()

f = open(POTCAR)
lines_POTCAR = f.readlines()
f.close()

## Get NVE from POTCAR
num1 = 0
list_valence = []
while num1 < len(lines_POTCAR):
    if lines_POTCAR[num1].find("PAW_PBE") >= 0:
        line = lines_POTCAR[num1]
        line = line.replace('\n','')
        line = line.replace('\r','')
        line = line.replace('\t',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        list = []
        list.append(line)

        num1 += 1
        line = lines_POTCAR[num1]
        line = line.replace('\n','')
        line = line.replace('\r','')
        line = line.replace('\t',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        list.append(line)
        list_valence.append(list)

        num1 += 1
    else:
        num1 += 1

list_NVE = []
num2 =  0
while num2 < len(list_valence):
    list = []
    num_ve = list_valence[num2][1][0]
    list_NVE.append(num_ve)
    num2 += 2
# END:get VNE

# Path
POSCAR = "../POSCAR"

# Read POSCAR
f = open(POSCAR)
poslines = f.readlines()
f.close()

# Read Atom label
atomlabel = poslines[5].replace('\n','')
atomlabel = atomlabel.replace('^M','')
atomlabel = atomlabel.split(" ")
while atomlabel.count("") > 0:
    atomlabel.remove("")

# Count Atom number
atomnum = poslines[6].replace('\n','')
atomnum = atomnum.replace('^M','')
atomnum = atomnum.split(" ")
while atomnum.count("") > 0:
    atomnum.remove("")

lastatom = 0
for num in atomnum:
    lastatom += int(num)

# Make atom label list
num1 = 0
list_atomlabel = []
while num1 < len(atomlabel):
    num2 = 0
    while num2 < int(atomnum[num1]):
        num3 = num2 + 1
        L = atomlabel[num1] + str(num3)
        list_atomlabel.append(L)
        num2 += 1
    num1 += 1

# Make atom label list
num1 = 0
list_eachatomsofNVE = []
while num1 < len(list_NVE):
    num2 = 0
    while num2 < int(atomnum[num1]):
        NVE = str(list_NVE[num1])
        list_eachatomsofNVE.append(NVE)
        num2 += 1
    num1 += 1                                                         
#print list_eachatomsofNVE

# List of the directories
lists = os.listdir('./')
direcs = [x for x in lists if "LDOS-" in x]

# Read ACF.dat
ACFlist_tot = []
for direc in direcs:
    direcpath = path + "/" + direc

    ACF = direcpath + "/ACF.dat"
    flag_ACF = os.path.exists(ACF)
    if flag_ACF == True:
        f = open(ACF)
        lines_ACF = f.readlines()
        f.close()
    else:
        L = "ACF.dat is not located in "+ direcpath
        print(L)
        sys.exit()

    ACFlist = []
    for line in lines_ACF:
        line = line.replace('\n','')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        ACFlist.append(line)
    
    del ACFlist[0]
    del ACFlist[0]
    ACFlist.pop()
    ACFlist.pop()
    ACFlist.pop()
    ACFlist.pop()
    ACFlist_tot.append(ACFlist)

num_atom = len(ACFlist_tot[0])

#print num_atom

# Make Bader charge list
chglist_tot = []
num1 = 0
while num1 < num_atom:
    Baderchg = 0
    
    i = 0
    while i < len(direcs):
        Baderchg += float(ACFlist_tot[i][num1][4])
        i += 1

    list = []
    NBE = float(list_eachatomsofNVE[num1])
    Valency =  NBE - Baderchg
    
    Atomlabel = str(list_atomlabel[num1])
    list.append(Atomlabel)
    list.append(str(Valency))
    chglist_tot.append(list)    
    
    num1 += 1

#print chglist_tot

# Make BADERCHG file
BADERCHG = path + "/BADERCHG"
f = open(BADERCHG,'w')
num = 0
for value in chglist_tot:
    line = str(chglist_tot[num][0]) + "   " + str(chglist_tot[num][1]) + "\n"
    f.write(line)
    num += 1
f.close()

L = "BADERCHG has been made in " + path + "."
print(L)
