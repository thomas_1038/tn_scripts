#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

argv = sys.argv
path = os.getcwd()
path_img = path + "/imgs"
path_tmp = path + "/tmp"

if len(argv) == 1:
    print("Please input the command; -mk or -q.")
    sys.exit()

if argv[1] == "-mk": jobname = input("Please input the jobname: ")
vfiles = sorted([x for x in os.listdir(path_img) if os.path.isfile(path_img+"/"+x) and "vasp" in x])

for vf in vfiles:
    dirname = vf.replace(".vasp","")
    path_dir = path + "/" + dirname

    if argv[1] == "-mk":
        if not os.path.exists(path_dir): os.mkdir(path_dir)
        shutil.copy(path_tmp + "/KPOINTS", path_dir)
        shutil.copy(path_tmp + "/POTCAR", path_dir)
        shutil.copy(path_tmp + "/INCAR", path_dir)
        shutil.copy(path_img + "/" + vf, path_dir + "/POSCAR")

        with open(path_tmp + "/job.sh") as fj: joblines = fj.readlines()

        Lj = ""
        for jobline in joblines:
            if "#$ -N " in jobline:
                Lj += "#$ -N " + jobname + "_" + dirname + "\n"
            else: Lj += jobline
        with open(path_dir + "/job.sh", "w") as fjw: fjw.write(Lj)
        os.chmod(path_dir + "/job.sh", 0o755)

    elif argv[1] == "-q":
        os.chdir(path_dir)
        sub.call("qsub -g tga-MCES01 job.sh", shell=True)

print("Done!")
