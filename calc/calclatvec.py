#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import os.path
import sys
import shutil
import threading
import math

argv = sys.argv
if argv[1] == "p":
    pc = "POSCAR"
elif argv[1] == "c":
    pc = "CONTCAR"
elif argv[1] == "s":
    pc = "SPOSCAR"
else:
    print("Please input [p (POSCAR), c (CONTCAR, s (SPOSCAR)]")

path = os.getcwd()
path_pc = path + "/" + pc

#read POSCAR                                                                                             
flag_pc = os.path.exists(path_pc)
if flag_pc == False:
    Lp = pc + " does not exist. You should prepare " + pc + "!"
    print(Lp)
    sys.exit()

#read POSCAR
f = open(path_pc)
poslines= f.readlines()
f.close()

#caculate the lattice vector of POSCAR                                                                                                                                     
line_pa  = poslines[2]
line_pa = line_pa.replace('\n','')
line_pa = line_pa.replace('\r','')
line_pa = line_pa.replace('\t',' ')
line_pa = line_pa.split(" ")
while line_pa.count("") > 0:
    line_pa.remove("")

line_pb  = poslines[3]
line_pb = line_pb.replace('\n','')
line_pb = line_pb.replace('\r','')
line_pb = line_pb.replace('\t',' ')
line_pb = line_pb.split(" ")
while line_pb.count("") > 0:
    line_pb.remove("")

line_pc  = poslines[4]
line_pc = line_pc.replace('\n','')
line_pc = line_pc.replace('\r','')
line_pc = line_pc.replace('\t',' ')
line_pc = line_pc.split(" ")
while line_pc.count("") > 0:
    line_pc.remove("")

lc_a_pos = math.sqrt(float(line_pa[0])**2 + float(line_pa[1])**2 + float(line_pa[2])**2)
lc_b_pos = math.sqrt(float(line_pb[0])**2 + float(line_pb[1])**2 + float(line_pb[2])**2)
lc_c_pos = math.sqrt(float(line_pc[0])**2 + float(line_pc[1])**2 + float(line_pc[2])**2)

b_a = float(1/lc_a_pos)
b_b = float(1/lc_b_pos)
b_c = float(1/lc_c_pos)

L = "latticecnst_a  latticeconst_b  latticeconst_c" + "\n"
L += str(lc_a_pos) + "  " + str(lc_b_pos) + "  " + str(lc_c_pos) 

path_LC = path + "/LatticeConstant_"+ pc
f = open(path_LC,"w")
f.write(L)
f.close()

Lp = "LatticeConstant_"+ pc + "has been made!"
print(Lp)
