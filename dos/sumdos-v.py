#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/07/23 version 1.0
#2015/08/12 version 1.01
#This script deleate the error and output directories.
#usage: script

import os
import os.path
import shutil
import sys

argv = sys.argv
path = os.getcwd()

i = str(argv[1])
path_DOS = path + "/DOS" + str(i)
path_DOS_new = path + "/DOS" + str(i) + ".dat"

# Read DOS
f = open(path_DOS)
doslines = f.readlines()
f.close()

list_dos = []
for line in doslines:
    line = line.replace('\n','')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    list_dos.append(line)
del list_dos[0]

#print list_dos
#print list_dos[0][0]


list_dostot = []
L = "energy tot_up tot_down s_up s_down px_up px_down py_up py_down pz_up pz_down ptot_up ptot_down dxy_up dxy_down dyz_up dyz_down dzx_up dzx_down dz2_up dz2_downm dx2_up dx2_down dtot_up dtot_down"
list_dostot.append(L)

num = 0
while num < len(list_dos):
    energy = float(list_dos[num][0])
    s_up = float(list_dos[num][1])
    s_down = float(list_dos[num][2])
    px_up = float(list_dos[num][3])
    px_down = float(list_dos[num][4])
    py_up = float(list_dos[num][5])
    py_down = float(list_dos[num][6])
    pz_up = float(list_dos[num][7])
    pz_down = float(list_dos[num][8])
    dxy_up = float(list_dos[num][9])
    dxy_down = float(list_dos[num][10])
    dyz_up = float(list_dos[num][11])
    dyz_down = float(list_dos[num][12])
    dzx_up = float(list_dos[num][13])
    dzx_down = float(list_dos[num][14])
    dz2_up = float(list_dos[num][15])
    dz2_down = float(list_dos[num][16])
    dx2_up = float(list_dos[num][17])
    dx2_down = float(list_dos[num][18])

    ptot_up = px_up + py_up + pz_up
    dtot_up = dxy_up + dyz_up + dzx_up + dz2_up + dx2_up
    tot_up = s_up + ptot_up + dtot_up

    ptot_down = px_down + py_down + pz_down
    dtot_down = dxy_down + dyz_down + dzx_down+ dz2_down + dx2_down
    tot_down = s_down+ ptot_down + dtot_down

    Ltot = str(energy) + " " + str(tot_up) + " " + str(tot_down) + " "
    Ls = str(s_up) + " " + str(s_down) + " "
    Lp = str(px_up) + " " + str(px_down) + " " + str(py_up) + " " + str(py_down) + " " + str(pz_up) + " " + str(pz_down) + " " + str(ptot_up) + " " + str(ptot_down) + " "
    Ld = str(dxy_up) + " " + str(dxy_up) + " " + str(dyz_up) + " " + str(dyz_down) + " " + str(dzx_up) + " " + str(dzx_down) + " " + str(dz2_up) + " " + str(dz2_down)+ " " + str(dx2_up) + " " + str(dx2_down)+ " " + str(dtot_up) + " " + str(dtot_down) 
    L = Ltot + Ls + Lp + Ld
    list_dostot.append(L)

    num += 1

f = open(path_DOS_new,"w")
for line in list_dostot:
    L = line + "\n"
    f.write(L)
f.close()

L = path_DOS_new + " has been made!"
print(L)
