#!/bin/sh

set -u
#set -e

##Argument check 
if [ $# -eq 0 ]; then
  dirpath=`pwd`
elif [ -d $1 ]; then
  dirpath=`cd $1 && pwd`
fi

##Settings
file="DOSCAR"
xmlfile="vasprun.xml"
outfile="DOSwid.dat2"
outfile2="DOSwid.dat"
lmax=100000
fermi=0
value0="0.0000E+00"

#Check DOSCAR
if [ -e $dirpath/$file ]; then
  echo "$dirpath/$file file exists."
else
  echo "$dirpath/$file file doesnot exist. Stop this script."
  exit 1
fi

#Check xml file
if [ -e $dirpath/$xmlfile ]; then
  echo "$dirpath/$xmlfile file exists."
else
  echo "$dirpath/$xmlfile file doesnot exist. Stop this script."
  exit 1
fi

##Get Fermi Energy in vasprun.xml
if grep -a "efermi" $dirpath/$xmlfile > /dev/null; then
   fermi=`grep -a "efermi" $dirpath/$xmlfile | awk "NR==1" | sed -e "s/  */ /g" -e "s/^ *//g" -e "s/ *$//g"`
   fermi=`echo $fermi | sed -e 's/<i name="efermi"> //g' -e 's,</i>,,g'`
   echo "fermi=$fermi"
else
   echo "fermi could not find in vasprun.xml. Stop this script."
   exit 1
fi

##make DOSwid.dat2
echo "" | tr -d '\n' > $dirpath/$outfile
l=7
while :
do
   ar_01=($(sed -n `echo $l`p $dirpath/$file))
   ar_01[0]=$(echo "${ar_01[0]} - $fermi" | bc)
   res=$(echo "${ar_01[0]} >= 0.0" | bc)
   for i in 1 2
   do
      if [ ${ar_01[$i]} = "$value0" ]; then
         ar_01[$i]=0
      else
         ar_01[$i]=1
      fi
   done
   
   if [ $res -eq 1 ]; then
      ar_01[0]=0.0
      echo "${ar_01[@]}" >> $dirpath/$outfile
      echo "energy < fermi"
      echo "l=`expr $l`, data is `expr $l - 6`"
      break
   else
      echo "${ar_01[@]}" >> $dirpath/$outfile
   fi
   if [ $l -gt $lmax ]; then
      echo "i=$l, Stop this script."
      break
   fi

   l=`expr $l + 1`
 
done

##Check DOSwid.dat2 
if [ -e $dirpath/$outfile ]; then
   echo "$outfile exists."
else
   echo "$outfile file could not make.Stop this script."
   exit 1
fi

#Read outfile
l=4
energyf=-1
lmax=$(wc -l < $dirpath/$outfile); echo "lmax=$lmax"
echo "" | tr -d '\n' > $dirpath/$outfile2
echo "Made empty $outfile2."


while :
do

   if [ $l -gt $lmax ]; then
      echo "l=$l, Quit this loop."
      break
   fi

   ar_01=($(sed -n `expr $l - 3`p $dirpath/$outfile))
   ar_02=($(sed -n `expr $l - 2`p $dirpath/$outfile))
   ar_03=($(sed -n `expr $l - 1`p $dirpath/$outfile))
   ar_04=($(sed -n `echo $l`p $dirpath/$outfile))
   
   dos01=`echo "${ar_01[1]}${ar_01[2]}"`
   dos02=`echo "${ar_02[1]}${ar_02[2]}"`
   dos03=`echo "${ar_03[1]}${ar_03[2]}"`
   dos04=`echo "${ar_04[1]}${ar_04[2]}"`
   dos=`echo "$dos01$dos02$dos03$dos04"`
#   echo "$dos01 $dos02 $dos03 $dos04"
#   echo "dos=$dos"
  
   if [ $l -eq 4 ]; then
      ene00=${ar_01[0]}
      if [[ $dos =~ 1[01]0000[01][01] || $dos =~ [01]10000[01][01] ]]; then
         echo "EINT= ${ar_01[0]} ${ar_03[0]}" >> $dirpath/$outfile2
         l=`expr $l + 1`
         continue
      elif [[ $dos =~ 001[01]0000 || $dos =~ 00[01]10000 ]]; then
         echo "EINT= ${ar_01[0]} ${ar_04[0]}" >> $dirpath/$outfile2
         l=`expr $l + 1`
         continue
      fi
   fi 
   if [[ $dos =~ 00001[01][01][01] || $dos =~ 0000[01]1[01][01] ]]; then
      echo "EINT= ${ar_01[0]} " | tr -d '\n' >> $dirpath/$outfile2
   elif [[ $dos =~ [01][01]1[01]0000 || $dos =~ [01][01][01]10000 ]]; then
      echo "${ar_04[0]}" >> $dirpath/$outfile2
   fi
   if [ $l -eq $lmax ]; then
      if [[ $dos =~ [01][01]00001[01] || $dos =~ [01][01]0000[01]1 ]]; then
         echo "l=$l"
         echo "EINT= ${ar_02[0]} ${ar_04[0]}" >> $dirpath/$outfile2
         l=`expr $l + 1`
         continue
      else
         echo "${ar_04[0]}" >> $dirpath/$outfile2
         l=`expr $l + 1`
         continue
      fi
   fi

   l=`expr $l + 1`

done

## file last line check
if [ -e $dirpath/$outfile2 ]; then
   sed -i -e '/^0.0/d' $dirpath/$outfile2
fi 

##1st line check
resc=`grep "^EINT=$ene00 " $dirpath/$outfile2 | wc -l`
if [ $resc -eq 2 ] ; then
   sed -i -e "/^EINT= $ene00/d" $dirpath/$outfile2
   echo "Deleted first line."
fi

##Check Overlap
ar_ol1=(`cat $dirpath/$outfile2 | awk '{print $2}'`)
ar_ol2=(`cat $dirpath/$outfile2 | awk '{print $3}'`)
i=0
j=0
res=99
echo "Start to check overlap..."
cat $dirpath/$outfile2
for i in `seq 0 $(expr ${#ar_ol1[@]} - 2)`
do 
   j=`expr $i + 1` 
   #echo "i=$i, j=$j (${ar_ol1[$i]},${ar_ol2[$i]}) (${ar_ol1[$j]}, ${ar_ol2[$j]})"
   res=`echo "${ar_ol1[$j]} <= ${ar_ol2[$i]}" | bc -l`
   #echo "$res = ${ar_ol1[$j]} <= ${ar_ol2[$i]}"
   if [ $res -eq 1 ]; then
      echo "==Exchange ${ar_ol2[$i]}\$ >> ${ar_ol2[$j]}"
      sed -i -e "s/${ar_ol2[$i]}$/${ar_ol2[$j]}/g" $dirpath/$outfile2
      #cat $dirpath/$outfile2
      echo "==Delete EINT= ${ar_ol1[$j]} ${ar_ol2[$j]}"
      sed -i -e "s/^EINT= ${ar_ol1[$j]} ${ar_ol2[$j]}//g" $dirpath/$outfile2
      sed -i -e '/^$/d' $dirpath/$outfile2
      #cat $dirpath/$outfile2
   fi  
done
echo "Finish checing overlap."

##Show result 
echo "=================================="
cat $dirpath/$outfile2
echo "=================================="

echo "Finished doswid.sh."
#END
