#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, glob, re
import numpy as np
import addmol.helper as hl
import addmol.readpos as rp

#path
nowpath = os.getcwd()
path_incar = nowpath + "/INCAR"
with open(path_incar) as fi: incarlines = fi.readlines()

spinum = 1
for incarline in incarlines:
    if incarline.find("ISPIN =") >= 0: spin = hl.conv_line(incarline); ispin = int(spin[2]); break

#read DOSCAR
path_dos = nowpath + "/DOSCAR"
path_doslob = nowpath + "/DOSCAR.lobster"
if os.path.exists(path_doslob): dosfile = "DOSCAR.lobster"; path_dos = path_doslob
elif os.path.exists(path_dos): dosfile = "DOSCAR"
with open(path_dos) as fd: doslines = fd.readlines()

### Start: make the directory of DOSsplist ###
path_dosdirec = path_dos + "_split"
if not os.path.exists(path_dosdirec): os.mkdir(path_dosdirec)

### Start: get fermilevel from vasprun ###
#read vasprun.xml
path_vasprun = nowpath + "/vasprun.xml"
if not os.path.exists(path_vasprun): print("You should prepare the vasprun.xml!"); sys.exit()

with open(path_vasprun) as fv: vasprunlines = fv.readlines()
fermi = []
for line in vasprunlines:
    if line.find("efermi") >= 0: fermi.append(line.split(' ')) ; break
fermi = fermi[0]
while fermi.count("") > 0: fermi.remove("")
Efermi = float(fermi[2]) #; print(Efermi)

### Start: make the lables of atoms  ###
path_pos = nowpath + "/POSCAR"; path_cont = nowpath + "/CONTCAR"
if os.path.exists(path_pos):
    with open(path_pos) as fp: poslines = fp.readlines()
else:
    if os.path.exists(path_cont):
        with open(path_cont) as fc: poslines = fc.readlines()
    else: print("Please prepare POSCAR or CONTCAR.") ; sys.exit()

#read Atom label
atomlabel = hl.conv_line(poslines[5]); atomnum = hl.conv_line(poslines[6])
#make atomlist
num1 = 0; atomlabellist = []
while num1 < len(atomlabel):
    num2 = 0
    while num2 < int(atomnum[num1]):
        num3 = num2 + 1
        if num3 in [1,2,3,4,5,6,7,8,9]:  L = atomlabel[num1] + "0" + str(num3)
        else: L = atomlabel[num1] + str(num3)
        atomlabellist.append(L); num2 += 1
    num1 += 1

def adjust_len_in_list(listX):
    for i, l in enumerate(listX): listX[i] = [str("{:.5f}".format(x)) if type(x) is float else str(x) for x in l]

    listX_T = np.array(listX).T.tolist()
    for i, line in enumerate(listX_T): listX_T[i] = hl.adjust_len(line)
    listX = np.array(listX_T).T.tolist()
    return listX

def mk_sumpdos(pdos_for_sum):
    num_pdos = 0; p_sum = 0; d_sum = 0; f_sum = 0; sum_all = 0
    for dosVal in pdos_for_sum:
        sum_all += dosVal
        if len(pdos_for_sum) == 4:
            if num_pdos > 0: p_sum += float(dosVal)
        elif len(pdos_for_sum) == 9:
            if 0 < num_pdos  and num_pdos < 4: p_sum += float(dosVal)
            elif 3 < num_pdos: d_sum += float(dosVal)
        elif len(pdos_for_sum) == 16:
            if 0 < num_pdos  and num_pdos < 4: p_sum += float(dosVal)
            elif 3 < num_pdos and num_pdos < 9: d_sum += float(dosVal)
            elif 8 < num_pdos: f_sum += float(dosVal)
        num_pdos += 1
    return p_sum, d_sum, f_sum, sum_all

def mk_tdos(dosfile, doslines, N_dos, n_lp, firstline):
    list_tot = [firstline]
    for i in range(0, N_dos -1):
        line_dos = list(map(float,(hl.conv_line(doslines[n_lp]))))
        if dosfile == "DOSCAR": en = float(line_dos[0]) - Efermi
        elif dosfile == "DOSCAR.lobster": en = line_dos[0]

        list_d = [en]
        for n_dos in range(1, len(line_dos)): list_d.append(line_dos[n_dos])
        list_tot.append(list_d)
        n_lp += 1

    list_tot = adjust_len_in_list(list_tot)
    Ltot = ""
    for l in list_tot: Ltot += "  ".join(l) + "\n"
    with open(path_dosdirec + "/DOSCAR_tot.dat","w") as fd: fd.write(Ltot)

def mk_pdos_ispin1(dosfile, doslines, tag_pdos_tot, N_dos, n_lp, list_tot):
    list_pdos_tot = []
    len_linepdos = len(hl.conv_line(doslines[n_lp]))
    pdos_s = tag_pdos_tot[0]; pdos_p = tag_pdos_tot[1]; pdos_d = tag_pdos_tot[2]; pdos_f = tag_pdos_tot[3]

    if len_linepdos == len(pdos_s): list_tag = pdos_s[:2] + ["all"] + pdos_s[2:]
    elif len_linepdos == len(pdos_p): list_tag = pdos_p[:2] + ["p_sum","all"] + pdos_p[2:]
    elif len_linepdos == len(pdos_d): list_tag = pdos_d[:2] + ["p_sum","d_sum","all"] + pdos_d[2:]
    elif len_linepdos == len(pdos_f): list_tag = pdos_f[:2] + ["p_sum","d_sum","f_sum","all"] + pdos_f[2:]
    list_pdos_tot.append(list_tag)

    #LOOP for making dos line and sumation dos
    i = 0
    while i < N_dos -1:
        line_pdos        = list(map(float,hl.conv_line(doslines[n_lp])))
        line_pdos_forsum = list(map(float,hl.conv_line(doslines[n_lp]))); del line_pdos_forsum[0]

        # Change Energy ref fermi
        if dosfile == "DOSCAR": en = float(line_pdos[0]) - Efermi; line_pdos[0] = en
        elif dosfile == "DOSCAR.lobster": en = line_pdos[0]

        # Sum pdos
        p_sum, d_sum, f_sum, sum_all = mk_sumpdos(line_pdos_forsum)
        if len(line_pdos_forsum) == 4: line_pdos = line_pdos[:1] + [p_sum, sum_all] + line_pdos[1:]
        elif len(line_pdos_forsum) == 9: line_pdos = line_pdos[:1] + [p_sum, d_sum, sum_all] + line_pdos[1:]
        elif len(line_pdos_forsum) == 16: line_pdos = line_pdos[:1] + [p_sum, d_sum, f_sum, sum_all] + line_pdos[1:]
        i += 1 ; n_lp += 1

    # Make DOSCAR_X1
    list_pdos_tot_output = adjust_len_in_list(list_pdos_tot)
    L_dosX = ""
    for line in list_pdos_tot_output: L_dosX += "  ".join(line) + "\n"
    with open(path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1]) + ".dat", 'w') as fdi : fdi.write(L_dosX)
    print("DOSCAR_" + str(atomlabellist[n-1]) + " has been made!")

    # Make first line for DOSCAR_all
    firstline = []; firstline.append(str(atomlabellist[n-1])); numu_bar = 1
    while numu_bar < len(list_tag): firstline.append("---"); numu_bar += 1
    list_pdos_tot.insert(0, firstline); list_tot.append(list_pdos_tot)
    return list_tot

def mk_pdos_ispin2(dosfile, doslines, tag_pdos_tot, N_dos, n_lp, list_tot, list_tot_up, list_tot_down):
    pdos_s_up = tag_pdos_tot[0][0]; pdos_p_up = tag_pdos_tot[0][1]; pdos_d_up = tag_pdos_tot[0][2]; pdos_f_up = tag_pdos_tot[0][3]
    pdos_s_down = tag_pdos_tot[1][0]; pdos_p_down = tag_pdos_tot[1][1]; pdos_d_down = tag_pdos_tot[1][2]; pdos_f_down = tag_pdos_tot[1][3]

    len_linepdos = len(hl.conv_line(doslines[n_lp]))
    if len_linepdos == len(pdos_s_up) + len(pdos_s_down) + 1:
        list_tag_up = ["Energy"] + pdos_s_up[:1] + ["all_up"] + pdos_s_up[1:]
        list_tag_down = ["Energy"] + pdos_s_down[:1] + ["all_down"] + pdos_s_down[1:]
    elif len_linepdos == len(pdos_p_up) + len(pdos_p_down) + 1:
        list_tag_up = ["Energy"] + pdos_p_up[:1] + ["p_sum_up", "all_up"] + pdos_p_up[1:]
        list_tag_down = ["Energy"] + pdos_p_down[:1] + ["p_sum_down", "all_down"] + pdos_p_down[1:]
    elif len_linepdos == len(pdos_d_up) + len(pdos_d_down) + 1:
        list_tag_up = ["Energy"] + pdos_d_up[:1] + ["p_sum_up", "d_sum_up", "all_up"] + pdos_d_up[1:]
        list_tag_down = ["Energy"] + pdos_d_down[:1] + ["p_sum_down", "d_sum_down", "all_down"] + pdos_d_down[1:]
    elif len_linepdos == len(pdos_f_up) + len(pdos_f_down) + 1:
        list_tag_up = ["Energy"] + pdos_d_up[:1] + ["p_sum_up", "d_sum_up", "f_sum_up", "all_up"] + pdos_f_up[1:]
        list_tag_down = ["Energy"] + pdos_d_down[:1] + ["p_sum_down", "d_sum_down", "f_sum_down", "all_down"] + pdos_f_down[1:]
    list_tag = list_tag_up + list_tag_down

    list_pdos_tot = [list_tag]; list_pdos_tot_up = [list_tag_up];  list_pdos_tot_down = [list_tag_down]
    i = 0
    while i < N_dos -1:
        list_pdos_up = []; list_pdos_down = []
        line_pdos = list(map(float,hl.conv_line(doslines[n_lp])))
        # Add Energy
        if dosfile == "DOSCAR": en = float(line_pdos[0]) - Efermi
        elif dosfile == "DOSCAR.lobster": en = line_pdos[0]

        for a2 in range(1, len(line_pdos)):
            if a2 % 2 == 1: list_pdos_up.append(line_pdos[a2])
            else: list_pdos_down.append(line_pdos[a2])

        p_sum_up, d_sum_up, f_sum_up, sum_all_up = mk_sumpdos(list_pdos_up)
        p_sum_down, d_sum_down, f_sum_down, sum_all_down= mk_sumpdos(list_pdos_down)

        # Append p, d, f_sum
        if len(list_pdos_up) == 4:
            list_pdos_up = list_pdos_up[:1] + [p_sum_up, sum_all_up] + list_pdos_up[1:]
            list_pdos_down = list_pdos_down[:1] + [p_sum_down, sum_all_down] + list_pdos_down[1:]
        elif len(list_pdos_up) == 9:
            list_pdos_up = list_pdos_up[:1] + [p_sum_up, d_sum_up, sum_all_up] + list_pdos_up[1:]
            list_pdos_down = list_pdos_down[:1] + [p_sum_down, d_sum_down, sum_all_down] + list_pdos_down[1:]
        elif len(list_pdos_up) == 16:
            list_pdos_up = list_pdos_up[:1] + [p_sum_up, d_sum_up, f_sum_up, sum_all_up] + list_pdos_up[1:]
            list_pdos_down = list_pdos_down[:1] + [p_sum_down, d_sum_down, f_sum_down, sum_all_down] + list_pdos_down[1:]

        # Insert energy
        list_pdos_up.insert(0, en); list_pdos_down.insert(0, en)
        list_pdos_tot_up.append(list_pdos_up); list_pdos_tot_down.append(list_pdos_down)
        list_pdos = list_pdos_up + list_pdos_down ; list_pdos_tot.append(list_pdos)
        i += 1 ; n_lp += 1

    list_pdos_tot_output = adjust_len_in_list(list_pdos_tot)
    list_pdos_tot_up_output = adjust_len_in_list(list_pdos_tot_up)
    list_pdos_tot_down_output = adjust_len_in_list(list_pdos_tot_down)
    Ldtot = ""; Ldtotu = ""; Ldtotd = ""
    for line in list_pdos_tot_output: Ldtot += "  ".join(line) + "\n"
    for line in list_pdos_tot_up_output: Ldtotu += "  ".join(line) + "\n"
    for line in list_pdos_tot_down_output: Ldtotd += "  ".join(line) + "\n"
    with open(path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1]) + ".dat", "w") as ft: ft.write(Ldtot)
    #with open(path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1]) + "_up.dat","w") as fu: fu.write(Ldtotu)
    #with open(path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1])+ "_down.dat","w") as fd: fd.write(Ldtotd)

    list_firstu = [str(atomlabellist[n-1]) + " up"]; list_firstd = [str(atomlabellist[n-1]) + " down"]; n_bar = 1
    while n_bar < len(list_tag_up): list_firstu.append("---"); list_firstd.append("---"); n_bar += 1
    list_pdos_tot_up.insert(0, list_firstu); list_pdos_tot_down.insert(0, list_firstd)
    list_first_tot = list_firstu + list_firstd; list_pdos_tot.insert(0, list_first_tot)
    list_tot.append(list_pdos_tot); list_tot_up.append(list_pdos_tot_up); list_tot_down.append(list_pdos_tot_down)

    return list_tot, list_tot_up, list_tot_down

### Start: count dosdata of each DOS ###
dosline  = hl.conv_line(doslines[5]); N_dos = int(dosline[2])
n = 0; l = 0; Nloop = len(atomlabellist) + 1; N_dos = int(dosline[2]) + 1
if ispin == 1:
    list_tot = []
    firstline_tdos = ["Energy","DOS","iDOS"]
    tag_pdos_s = ["Energy","s"]
    tag_pdos_p = ["Energy","s","py","pz","px"]
    tag_pdos_d = ["Energy","s","py","pz","px","dxy","dyz","d3z2-1","dxz","dx2-y2"]
    tag_pdos_f = ["Energy","s","py","pz","px","dxy","dyz","d3z2-1","dxz","dx2-y2","f1","f2","f3","f4","f5","f6","f7"]
    tag_pdos_tot = [tag_pdos_s, tag_pdos_p, tag_pdos_d, tag_pdos_f]

    while n < Nloop:
        # Make DOSCAR_tot
        if n == 0: mk_tdos(dosfile, doslines, N_dos, 6 + n*N_dos, firstline_tdos)
        else: list_tot = mk_pdos_ispin1(dosfile,
                                        doslines,
                                        tag_pdos_tot,
                                        N_dos,
                                        6 + n*N_dos,
                                        list_tot)
        n += 1

elif ispin == 2:
    list_tot = []; list_tot_up = []; list_tot_down = []
    firstline_tdos = "Energy   DOS_up   DOS_down   iDOS_up   iDOS_down" + "\n"
    tag_pdos_s_up = ["s_up"]; tag_pdos_s_down = ["s_down"]
    tag_pdos_p_up =   tag_pdos_s_up   + ["py_up","pz_up","px_up"]
    tag_pdos_p_down = tag_pdos_s_down + ["py_down","pz_down","px_down"]
    tag_pdos_d_up =   tag_pdos_p_up   + ["dxy_up","dyz_up","d3z2-1_up","dxz_up","dx2-y2_up"]
    tag_pdos_d_down = tag_pdos_p_down + ["dxy_down","dyz_down","d3z2-1_down","dxz_down","dx2-y2_down"]
    tag_pdos_f_up =   tag_pdos_d_up   + ["f1_up","f2_up","f3_up","f4_up","f5_up","f6_up","f7_up"]
    tag_pdos_f_down = tag_pdos_d_down + ["f1_down","f2_down","f3_down","f4_down","f5_down","f6_down","f7_down"]
    tag_pdos_tot = [[tag_pdos_s_up,tag_pdos_p_up,tag_pdos_d_up,tag_pdos_f_up],
                    [tag_pdos_s_down, tag_pdos_p_down, tag_pdos_d_down, tag_pdos_f_down]]

    while n < Nloop:
        # Make DOSCAR_tot
        if n == 0: mk_tdos(dosfile, doslines, N_dos, 6 + n*N_dos, firstline_tdos)
        else:
            print(str(atomlabellist[n-1]))
            list_tot, list_tot_up, list_tot_down = mk_pdos_ispin2(dosfile,
                                                                  doslines,
                                                                  tag_pdos_tot,
                                                                  N_dos,
                                                                  6 + n*N_dos,
                                                                  list_tot,
                                                                  list_tot_up,
                                                                  list_tot_down)
        n += 1

#make DOS_all
def mkdosfile(DOSTOT, name):
    list_print = []; a = 0
    while a < len(DOSTOT[0]):
        DOSTOT_en = []; b = 0
        while b < len(DOSTOT): DOSTOT_en += DOSTOT[b][a]; b += 1
        list_print.append(DOSTOT_en); a += 1
    L = "" ; l = 0
    while l < len(list_print): L += "  ".join([str(x) for x in list_print[l]]) + "\n"; l += 1
    with open(path_dosdirec + "/" + name, 'w') as fp: fp.write(L)

def mkEachElementDOS(list_tot):
    list_EachElementDOS = []; n_e = 0; n_a = 0
    for Element in atomnum:
        list_dos_Esum = []; list_dos_Esum_append = list_dos_Esum.append; list_dos_Esum_append(list_tot[n_a][1])
        n_d = 0
        for dos in list_tot[0]:
            if n_d == 0 or n_d == 1: pass
            else:
                line_pdos = np.zeros(len(list_tot[n_a][1]))
                num_a = 0; en = list_tot[0][n_d][0]
                while num_a < int(atomnum[n_e]):
                    dos_array = list_tot[num_a + n_a][n_d]
                    line_pdos += np.array(list(map(float, dos_array)))
                    num_a += 1
                line_pdos_sum = line_pdos.tolist()
                line_pdos_sum.pop(0); line_pdos_sum.insert(0, en); list_dos_Esum_append(line_pdos_sum)
            n_d += 1
        list_EachElementDOS.append(list_dos_Esum); n_a += int(atomnum[n_e]); n_e += 1
    return list_EachElementDOS

mkdosfile(list_tot, "DOSCAR_all.dat"); print("\nDOSCAR_all.dat has been made!")
if ispin == 1: mkEachElementDOS(list_tot)
elif ispin == 2:
    list_EEdos_up = mkEachElementDOS(list_tot_up); list_EEdos_down = mkEachElementDOS(list_tot_down)
    n_l = 0
    for i, edos_up in enumerate(list_EEdos_up):
        list_EEdos = adjust_len_in_list(np.c_[np.array(list_EEdos_up[i]), np.array(list_EEdos_down[i])].tolist())
        L_dos = ""
        for l in list_EEdos: L_dos += "  ".join([str(x) for x in l]) + "\n"
        with open(path_dosdirec + "/DOSCAR_" +str(atomlabel[n_l]) + "_sum.dat","w") as ftot: ftot.write(L_dos)
        print("DOSCAR_" + str(atomlabel[n_l]) + "_sum.dat has been made!")
        n_l += 1
print("\nDone!\n")
#END: script