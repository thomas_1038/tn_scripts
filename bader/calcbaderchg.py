#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/05/01 ver1.2
#2016/06/19 ver1.3

import os, shutil, sys
import subprocess as sub
from addmol import readpos as rp

def calc_baderchg(nowpath):
    bader_files = ["ACF.dat","BCF.dat","AVF.dat"]
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def adjust_len(list_qd):
        list_new = [] ; list_new_ap = list_new.append ; max_len = max([len(x) for x in list_qd])
        for l in list_qd:
            str_num = max_len - len(l) ; n = 1
            while n < str_num +1: l += " " ; n += 1
            list_new_ap(l)
        return list_new

    #path = os.getcwd()
    POTCAR = nowpath + "/POTCAR"; POSCAR = nowpath + "/POSCAR"

    # Make and move baderfiles
    path_bader = nowpath + "/bader"
    if os.path.exists(path_bader) == False: os.mkdir(path_bader)

    for bf in bader_files:
        if os.path.exists(nowpath+"/"+bf): shutil.copy(nowpath+"/"+bf,path_bader); os.remove(nowpath+"/"+bf)

    # Read POSCAR
    if os.path.exists(POSCAR) == False: print("You should prepare POSCAR! BYE"); sys.exit()
    with open(POSCAR) as fp: poslines = fp.readlines()
    labelofel = rp.get_labelofel(poslines)
    numofel = rp.get_numofel(poslines)
    list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel,numofel)

    # Read and Get the number of valence electrons (nve) from POTCAR
    if os.path.exists(POTCAR) == False: sub.call("mkpot.py", shell=True)
    with open(POTCAR) as fpot: potlines = fpot.readlines()
    ind = 0; num_el = 0; nve_el = []
    for i in range(0,len(potlines)):
        if i == 0: num = 1; ind = 1
        elif "End of Dataset" in potlines[i]: num = 2; ind = 1; num_el += 1
        if num_el == len(numofel): break
        if ind == 1: ind = 0; nve_el.append(conv_line(potlines[i+num])[0]); continue

    nve_allatoms = []
    for i1 in range(0,len(nve_el)):
        for i2 in range(0,int(numofel[i1])): nve_allatoms.append(nve_el[i1])

    # Read ACF.dat and Get the number of bader electrons (NBE)
    ACF = path_bader + "/ACF.dat"
    if os.path.exists(ACF):
        with open(ACF) as fa: acflines = fa.readlines()
    else: print("ACF.dat is not located in bader! BYE!"); sys.exit()
    del acflines[:2]; del acflines[-4:]
    nbe_allatoms = [conv_line(line)[4] for line in acflines]

    list_labelofel = [x.replace("!","") for x in list_labelofel]
    list_chg = [str(round(float(nve_allatoms[i]) - float(nbe_allatoms[i]), 3)) for i in range(0, len(nbe_allatoms))]
    list_labelofel = adjust_len(list_labelofel)
    list_chg = adjust_len(list_chg)

    # Make BADERCHG in bader
    Lb = ""
    for i in range(0, len(list_chg)): Lb += str(list_labelofel[i]) +  "  " + str(list_chg[i]) + "\n"
    with open(path_bader + "/BADERCHG", "w") as fb: fb.write(Lb)

if __name__ == "__main__":
    calc_baderchg(os.getcwd()); print("\nBADERCHG has been made in bader!\n")
# END: Program
