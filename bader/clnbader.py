#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/04/10 version 1.0
#2015/08/05 version 1.1 #make backup
#2015/08/12 version 1.11
#This script make "bader" directory where bader analysis files are located.
#and move bader analysis files to bader directory.
#usase:script

import os
import os.path
import shutil

#judge whether bader directory exists.
path = os.getcwd()
pathbader = path + "/bader"

flag1 = os.path.exists(pathbader)
if flag1 == True:
    print("bader directory already exists.")

    acf1 = pathbader + "/ACF.dat"
    avf1 = pathbader + "/AVF.dat"
    bcf1 = pathbader + "/BCF.dat"
    flag2 = os.path.exists(acf1)

    if flag2 == True:
        i=1
        while True:
            baderbpname = pathbader + "_backup" + str(i)
            flag3 = os.path.exists(baderbpname)
            if flag3 == True:
                i += 1
            else:
                break
        os.rename(pathbader,baderbpname)
        os.mkdir(pathbader)
        print("New bader directory has been made!")

else:
    os.mkdir(pathbader)
    print("New bader directory has been made!")

#move bader analysis files to bader directory.
acf = path + "/ACF.dat"
avf = path + "/AVF.dat"
bcf = path + "/BCF.dat"
shutil.move(acf,pathbader)
shutil.move(avf,pathbader)
shutil.move(bcf,pathbader)

ale0 = path + "/AECCAR0"
ale1 = path + "/AECCAR1"
ale2 = path + "/AECCAR2"

flag_ALE0 = os.path.exists(ale0)
if flag_ALE0 == True:
    shutil.move(ale0,pathbader)

flag_ALE1 = os.path.exists(ale1)
if flag_ALE1 == True:
    shutil.move(ale1,pathbader)

flag_ALE2 = os.path.exists(ale2)
if flag_ALE2 == True:
    shutil.move(ale2,pathbader)

print("All bader files has been moved to directory!")
