#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys #, shutil, threading, glob, re
import subprocess as sub
from bader import calcbaderchg as cbc

def get_baderfds(nowpath, atomlabs, Lre=0):
    #mkbader = "calcbaderchg.py"
    chgsum = "/home/4/17D20121/script/vtstscripts-933/chgsum.pl AECCAR0 AECCAR2"
    bader = "/home/4/17D20121/script/tn_scripts_local/tbm/bader CHGCAR -ref CHGCAR_sum"
    calcbader = "calcbaderchg.py"
    baderdir = chgsum + " && " + bader + " && " + calcbader

    #baderdir = dir_vtst + "chgsum.pl AECCAR0 AECCAR2 && " + dir_vtst + "bader CHGCAR -ref CHGCAR_sum && calcbaderchg.py"
    firstline = "dir  [R,W,N]"

    def allbaderdir(path_dir):
        os.chdir(path_dir)
        sub.call(chgsum, shell=True)
        sub.call(bader, shell=True)
        sub.call(calcbader, shell=True)

    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    nowpath = os.getcwd()
    #files = [x for x in os.listdir(nowpath) if os.path.isfile(nowpath + "/" + x)]
    file_eads = [x for x in os.listdir(nowpath) if os.path.isfile(nowpath + "/" + x) and "Sum_Eads" in x][-1]
    with open(nowpath + "/" + file_eads) as fs: ealines = fs.readlines() ; del ealines[0]

    list_Eads = []
    for line in ealines:
        if firstline in line:
            break
        elif line != "\n":
            list_Eads.append(conv_line(line))

    dirs = []
    for l in list_Eads:
        dirs.append(str(l[0]))
        #prec = sorted([x for x in l if "Prec" in x])
        #conv = [l[i] for i in range(2,len(l),4)]

        #i = 0
        #for x in conv:
        #    if "R" in x or "W" in x: i += 1 ; continue
        #    elif "N" in x and prec[i] != prec[0]: dirs.append(str(l[0]) + "/" + str(prec[i-1])) ; break
        #else: dirs.append(str(l[0]) + "/" + str(prec[-1]))

    Ltot = ""; ind = 0
    for lab in atomlabs:
        print("\n" + lab)
        L = ""
        for d in dirs:
            print(d)
            path_dir = nowpath + "/" + d

            dir_scf = [l for l in os.listdir(path_dir) if "SCF_" in l]
            if len(dir_scf) == 0:
                continue
            else:
                path_dir_scf = path_dir + "/"+ dir_scf[0]

            if Lre == 1:
                if ind == 0:
                    allbaderdir(path_dir_scf)
                    #sub.run(baderdir, shell=True)
                else:
                    if os.path.exists(path_dir_scf + "/bader"):
                        cbc.calc_baderchg(path_dir_scf) #sub.call(mkbader, shell=True)
                    else:
                        allbaderdir(path_dir_scf)

            else:
                if os.path.exists(path_dir_scf + "/bader"):
                    cbc.calc_baderchg(path_dir_scf) #sub.call(mkbader, shell=True)
                else:
                    allbaderdir(path_dir_scf)
                    #sub.run(baderdir, shell=True)
            #else:
            #    print("You should input command after script name! BYE!"); sys.exit()

            with open(path_dir_scf + "/bader/BADERCHG") as fb: baderlines = fb.readlines()
            baderlines = [conv_line(l) for l in baderlines]
            chg_target = [x[-1] for x in baderlines if lab in x]

            if len(chg_target) == 0:
                chg_val = "---"
                L += str(d.replace("/","  ")) + "  SCF  " + lab + "  " + chg_val + "\n"
            else:
                chg_val = chg_target[-1]
                L += str(d.replace("/","  ")) + "  SCF  " + lab + "  " + chg_val + "\n"

        with open(nowpath + "/bader_tot_" + lab + ".dat","w") as fbt: fbt.write(L)
        Ltot += L + "\n"; ind += 1

if __name__ == "__main__":
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    argv = sys.argv
    if len(argv) == 1:
        Lre = 0
    elif argv[1] == "-re":
        Lre = 1
    else:
        Lre = 0

    Qatom = input("Please input the label of target atom, default, d = N1 N2 H1 H2 H3 H4 H5 H6: ")
    if Qatom in ["d", "default"]: atomlabs = conv_line("N1 N2 H1 H2 H3 H4 H5 H6")
    else: atomlabs = conv_line(Qatom)

    get_baderfds(os.getcwd(), atomlabs, Lre)
    print("\n" + "Done!\n")
# END Program
