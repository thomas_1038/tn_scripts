#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra

def mk_badertot():
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def adjust_len(list_qd):
        list_new = []; list_new_ap = list_new.append
        max_len = max([len(x) for x in list_qd])
        L = ""
        for i in range(0,max_len): L += "-"

        for l in list_qd:
            str_num = max_len - len(l); n = 1
            while n < str_num +1: l += " "; n += 1
            list_new_ap(l)
        return list_new

    path = os.getcwd()
    baderfiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "bader" in x and "bader_tot.dat" not in x])

    i = 0; eachbaders = []
    for bf in baderfiles:
        with open(path + "/" + bf) as fb: baderlines = fb.readlines()
        baders = [str("%.2f" % float(conv_line(x)[3])) if not conv_line(x)[3] == "---" else "0.00" for x in baderlines]
        baders.insert(0,str(conv_line(baderlines[0])[2])); baders = adjust_len(baders); eachbaders.append(baders)
        if i == 0: dirnames = [conv_line(x)[0] for x in baderlines]; dirnames.insert(0,"#"); dirnames = adjust_len(dirnames)
        i += 1

    eachbaders.insert(0, dirnames)
#    print(eachbaders); sys.exit()

    L = ""
    for i in range(0,len(eachbaders[0])):
        Q = 0; i1 = 0
        for line in eachbaders:
            L += line[i] + " "
            if i != 0 and i1 != 0: Q += float(line[i])
            i1 += 1
        else:
            if i != 0: L += str("%.2f" % Q) + " "
            else: L += "tot "
            L += "\n"
    with open(path + "/bader_tot.dat", "w") as fbt: fbt.write(L)

    path_baders = path + "/baders"
    if os.path.exists(path_baders):
        for f in os.listdir(path_baders): os.remove(path_baders + "/" + f)
    else: os.mkdir(path_baders)

    for baderfile in baderfiles: shutil.move(path + "/" + baderfile, path_baders)
    return L

if __name__ == "__main__": L = mk_badertot(); print("\n" + L)

# End
