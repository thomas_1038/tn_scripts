#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/05/01 ver1.2
#2016/06/19 ver1.3
#make LDOS at optional level

import os, shutil, sys
import subprocess as sub

#from make import mkpot as mp
mkpot = "mkpot.py"

# Definition of path
path = os.getcwd()
POTCAR = path + "/POTCAR"
POSCAR = path + "/POSCAR"

## Read POTCAR
if os.path.exists(POTCAR) == False: sub.call(mkpot, shell = True) #mp.mk_pot()
with open(POTCAR) as fpot:lines_POTCAR = fpot.readlines()

## Get NVE from POTCAR
num1 = 0; list_valence = []
while num1 < len(lines_POTCAR):
    if lines_POTCAR[num1].find("PAW_PBE") >= 0:
        list = []
        val1 = lines_POTCAR[num1].replace('\n','').replace('\r','').replace('\t',' ').split(" ")
        while val1.count("") > 0: val1.remove("")
        list.append(val1)

        num1 += 1
        val2 = lines_POTCAR[num1].replace('\n','').replace('\r','').replace('\t',' ').split(" ")
        while val2.count("") > 0: val2.remove("")
        list.append(val2); list_valence.append(list)

    num1 += 1

list_NVE = []; num2 =  0
while num2 < len(list_valence): list_NVE.append(list_valence[num2][1][0]); num2 += 2
# END:get VNE

# Read POSCAR
with open(POSCAR) as fp: poslines = fp.readlines()

# Read Atom label
atomlabel = poslines[5].replace('\n','').replace('^M','').split(" ")
while atomlabel.count("") > 0: atomlabel.remove("")

# Count Atom number
atomnum = poslines[6].replace('\n','').replace('^M','').split(" ")
while atomnum.count("") > 0: atomnum.remove("")

lastatom = 0
for num in atomnum: lastatom += int(num)

# Make atom label list
n1 = 0; list_atomlabel = []
while n1 < len(atomlabel):
    n2 = 0
    while n2 < int(atomnum[n1]):list_atomlabel.append(atomlabel[n1] + str(n2+1)); n2 += 1
    n1 += 1

# Make atom label list
n1 = 0; list_eachatomsofNVE = []
while n1 < len(list_NVE):
    n2 = 0
    while n2 < int(atomnum[n1]): list_eachatomsofNVE.append(str(list_NVE[n1])); n2 += 1
    n1 += 1

# Read ACF.dat
ACF = path + "/bader/ACF.dat"
if os.path.exists(ACF) == True:
    with open(ACF) as fa: lines_ACF = fa.readlines()
else: print("ACF.dat is not located in " + path); sys.exit()

ACFlist = []
for line in lines_ACF:
    line = line.replace('\n','').split(" ")
    while line.count("") > 0: line.remove("")
    ACFlist.append(line)

del ACFlist[0]; del ACFlist[0]
ACFlist.pop(); ACFlist.pop(); ACFlist.pop(); ACFlist.pop()

chglist_tot = []; num1 = 0
while num1 < len(ACFlist):
    Baderchg = float(ACFlist[num1][4])
    NBE = float(list_eachatomsofNVE[num1])
    Valency =  NBE - Baderchg; Atomlabel = str(list_atomlabel[num1])
    chglist_tot.append([Atomlabel,str(Valency)])
    num1 += 1

# Make BADERCHG file
Lbad = ""; num = 0
for value in chglist_tot: Lbad +=  str(chglist_tot[num][0]) + "   " + str(chglist_tot[num][1]) + "\n"; num += 1
with open(path + "/bader/BADERCHG", "w") as fb: fb.write(Lbad)

print("BADERCHG has been made in " + path + "!")
# END: Program
