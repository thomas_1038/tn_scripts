#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/04/10 ver1.5 
#This script make "bader" directory where bader analysis files iare located.
#and copy bader analysis files to "bader" directory. 

import glob
import os
import sys
import shutil

#valuable definition
argv = sys.argv
argc = len(argv)
path1 = os.getcwd()
acf0 = path1 + "/bader/ACF.dat"

#make bader directory.
pathbader = path1 + "/bader"
flug = os.path.exists(pathbader)

if flug == True:
    print("bader directory exists.")
else:
    os.mkdir(pathbader)
    print("bader directory has been made.")

#copy ACF.dat file to bader.
num1 = 1
while num1 < argc:
    acf = str(argv[num1]) + "/bader/ACF.dat"
    shutil.copy(acf,pathbader)
    name1 = path1 + "/bader/bader_" + str(argv[num1]) 
    os.rename(acf0,name1)
    num1 += 1

print("All bader analysis files are moved to bader!")
