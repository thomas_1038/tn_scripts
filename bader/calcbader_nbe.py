#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2016/10/25 ver1.00
#make LDOS at optional level

import os
import os.path
import shutil
import sys

os.system("eodel.py")

# Definition of path                          
path = os.getcwd()

# List of the directories
direcs = sys.argv
del direcs[0]
print(direcs)

# 
for direc in direcs:
    # Path
    direcpath = path + "/" + direc
    print(direcpath)
    
    # Make LDOS-directory
    LDOSpath = direcpath + "/LDOS"
    flag_LDOS = os.path.exists(LDOSpath)
    if flag_LDOS == False:
        os.mkdir(LDOSpath)
        L = "The directory named LDOS has been made in " + str(direcpath) + "."
        print(L)

    else:
        L = "The directory named LDOS has been in " + str(direcpath) + "."
        print(L)
        sys.exit()
        
    # List of the directories                                                                   
    lists = os.listdir(direcpath)
    ldosdirecs = [x for x in lists if "LDOS-" in x]

    for ldirec in ldosdirecs:
        ldosdirecpath = direcpath + "/" + ldirec
        shutil.move(ldosdirecpath,LDOSpath)

    L = "The LDOS directories in " + str(direcpath) + " have been moved to LDOS."
    print(L)

    for ldirec in ldosdirecs:
        ldosdirecpath = LDOSpath + "/" + ldirec
        os.chdir(ldosdirecpath)
        print(ldosdirecpath)
        
        # Calc bader charge using PARCHG
        ACF = ldosdirecpath + "/ACF"
        flag_ACF = os.path.exists(ACF)

        PARCHG = ldosdirecpath + "/PARCHG"
        flag_PARCHG = os.path.exists(PARCHG)

        if flag_PARCHG == True:
            if flag_ACF == False:
                os.system("bader PARCHG")
        else:
            L = "PARCHG is not located in " + direcpath
            print(L)
            sys.exit()
            
    os.chdir(LDOSpath)
    os.system("sumbader.py")
