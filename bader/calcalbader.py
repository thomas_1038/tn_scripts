#! /usr/bin/env python3
# -*- coding: utf-8 -*-  

import os
import os.path
import sys
import shutil
import threading

path = os.getcwd()
AECCAR0 = path + "/AECCAR0"
AECCAR2 = path + "/AECCAR2"
CHGCAR_sum = path + "/CHGCAR_sum"

flag0 = os.path.exists(AECCAR0)
flag2 = os.path.exists(AECCAR2)
flag_sum = os.path.exists(CHGCAR_sum)

if flag0 == False:
    print("AECCAR0 doesnt exist!")
    sys.exit()
if flag2 == False:
    print("AECCAR2 doesnt exist!")
    sys.exit()

if flag_sum == False:
    chgsum = "chgsum.pl AECCAR0 AECCAR2"
    os.system(chgsum)

else:
    print("CHGCAR_sum already exists!")
    
bader = "bader CHGCAR -ref CHGCAR_sum"
os.system(bader)
os.system("clnbader.py")
os.system("calcbader_direc.py")

