#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/05/01 ver1.2
#2016/06/19 ver1.3
#make LDOS at optional level

import os
import os.path
import shutil
import sys

os.system("eodel.py")

# Definition of path                          
path = os.getcwd()
POTCAR_bf = path + "/POTCAR"
KPOINTS_bf = path + "/KPOINTS"

# List of the directories
direcs = sys.argv
del direcs[0]
print(direcs)

# Make DOSwid.dat
for direc in direcs:
    direcpath = path + "/" + direc
    os.chdir(direcpath)

    DOSwid = path + "/" + str(direc) + "/DOSwid.dat"
    flag_DOSwid = os.path.exists(DOSwid)
    if flag_DOSwid == False:
        os.system("doswid.sh")

os.chdir(path)

# Read DOSwid.dat
for direc in direcs:
    direcpath = path + "/" + str(direc)
    DOSwid = path + "/" + str(direc) + "/DOSwid.dat"

    os.chdir(direcpath)
    os.system("eodel.py")

    # List of EINT
    f = open(DOSwid)
    DOSwidlist = f.readlines()
    f.close()

    numD = 1
    for eint in DOSwidlist:
        ldosdirecpath = direcpath + "/LDOS-" + str(numD)
        os.mkdir(ldosdirecpath)

        os.chdir(ldosdirecpath)

        # Debag 
        print(ldosdirecpath)
        
        # Copy files to LDOS
        CONTCAR_bf = direcpath + "/CONTCAR"
        WAVECAR_bf = direcpath + "/WAVECAR"
        INCAR_bf = direcpath + "/INCAR"
        POTCAR_bf = direcpath + "/POTCAR"
        KPOINTS_bf = direcpath + "/KPOINTS"

        shutil.copy(CONTCAR_bf,ldosdirecpath)
        #shutil.copy(INCAR,ldosdirecpath)
        shutil.copy(POTCAR_bf,ldosdirecpath)
        shutil.copy(WAVECAR_bf,ldosdirecpath)
        shutil.copy(KPOINTS_bf,ldosdirecpath)

        # New path
        INCAR_af = ldosdirecpath + "/INCAR"
        CONTCAR_af = ldosdirecpath + "/CONTCAR"
        POSCAR_af = ldosdirecpath + "/POSCAR"

        os.rename(CONTCAR_af,POSCAR_af)

        # Make sh file
        shname = str(direc) + "-LDOS" + str(numD)
        mksh = "mkdovasp.py " + shname
        os.system(mksh)
        
        # Make INCAR in LDOS
        incar = open(INCAR_bf)
        incarlines = incar.readlines()
        incar.close()

        for line in incarlines:
            line.replace("\r","")

        num = 0
        while num < len(incarlines):
            flag_LPARD = incarlines[num].find("!LPARD = ")
            flag_LWAVE = incarlines[num].find("!LWAVE = .FALSE.")
            flag_NBMOD = incarlines[num].find("!NBMOD = ")
            flag_EINT = incarlines[num].find("!EINT = ")
            flag_IBRION = incarlines[num].find("IBRION = ")
            flag_NSW = incarlines[num].find("NSW = ")
            flag_ISTART1 = incarlines[num].find("ISTART = 0 ; ICHARG = 2")
            flag_ISTART2 = incarlines[num].find("ICHARG = 1 ; ISTART = 1")
            flag_LVTOT = incarlines[num].find("LVTOT = ")
            flag_NEDOS = incarlines[num].find("NEDOS = ")
            flag_LAECHG = incarlines[num].find("LAECHG = ")

            if flag_LPARD >= 0:
                LPARD_tag = "LPARD = .TRUE." + "\n"
                incarlines[num] = LPARD_tag
                num += 1
            elif flag_LWAVE >= 0:
                LWAVE_tag = "LWAVE = .FALSE." + "\n"
                incarlines[num] = LWAVE_tag
                num += 1
            elif flag_NBMOD >= 0:
                NBMOD_tag = "NBMOD = -3" + "\n"
                incarlines[num] = NBMOD_tag
                num += 1
            elif flag_EINT >= 0:
                EINT_tag = eint + "\n"
                incarlines[num] = EINT_tag
                num += 1
            elif flag_IBRION >= 0:
                IBRION_tag = "IBRION = -1" + "\n"
                incarlines[num] = IBRION_tag
                num += 1
            elif flag_NSW >= 0:
                NSW_tag = "NSW = 0" + "\n"
                incarlines[num] = NSW_tag
                num += 1
            elif flag_ISTART1 >= 0:
                ISTART1_tag = "ISTART = 1" + "\n"
                incarlines[num] = ISTART1_tag
                num += 1
            elif flag_ISTART2 >= 0:
                ISTART2_tag = "ISTART = 1" + "\n"
                incarlines[num] = ISTART2_tag
                num += 1
            elif flag_LVTOT >= 0:
                LVTOT_tag = "!LVTOT = .TURE." + "\n"
                incarlines[num] = LVTOT_tag
                num += 1
            elif flag_NEDOS >= 0:
                NEDOS_tag = "!" + str(incarlines[num]) + "\n"
                incarlines[num] = NEDOS_tag
                num += 1
            elif flag_LAECHG >= 0:
                LAECHG_tag = "!LAECHG = .TRUE." + "\n"
                incarlines[num] = LAECHG_tag
                num += 1

            else:
                num += 1

        f = open(INCAR_af,'w')
        for line in incarlines:
            f.write(line)
        f.close()
            
        # Qsub 
        qsub = "qsub " + str(direc) + "-LDOS" + str(numD) + ".sh"
        os.system(qsub)

        Lprint = str(direc) + "/LDOS-" + str(numD) + " has been made!"
        print(Lprint)

        numD += 1
