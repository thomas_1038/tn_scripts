#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script

import os
import os.path
import shutil
import sys

path = os.getcwd()
Ldirec = input("Please input the directory name: ")

#read list_direcname
Listname = input("Please input the list name: ")
f = open(Listname)
lines_direc = f.readlines()
f.close()

list_direcname = []
for line in lines_direc:
    line = line.replace('\n','')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    list_direcname.append(line)

#START
list_MAIN = []
num = 0
while num < len(list_direcname):
    #PATH
    direcname = list_direcname[num][0]
    direcpath = str(path) + "/"+ Ldirec + "/" + str(direcname)
    direc_sto = str(direcpath) + "/stoichiometry"
    direc_def = str(direcpath) + "/del1H_1"

    #START:get number of Valence electron (NVE)
    ##PATH
    path_pot_sto = direc_sto + "/POTCAR"

    ##read POTCAR
    flag_pot_sto = os.path.exists(path_pot_sto)
    if flag_pot_sto == False:
        print("POTCAR doesn't exist!")
        num += 1

    f = open(path_pot_sto)
    lines_pot_sto = f.readlines()
    f.close()
	
    ##get NVE from POTCAR
    num1 = 0
    list_valence_sto = []
    while num1 < len(lines_pot_sto):
        if lines_pot_sto[num1].find("PAW_PBE") >= 0:
            line = lines_pot_sto[num1]
            line = line.replace('\n','')
            line = line.replace('\r','')
            line = line.replace('\t',' ')
            line = line.split(" ")
            while line.count("") > 0:
                line.remove("")
            list = []
            list.append(line)

            num1 += 1
            line = lines_pot_sto[num1]
            line = line.replace('\n','')
            line = line.replace('\r','')
            line = line.replace('\t',' ')
            line = line.split(" ")
            while line.count("") > 0:
                line.remove("")
            list.append(line)
            list_valence_sto.append(list)

            num1 += 1
        else:
            num1 += 1
    
    list_VE = []
    num2 =  0
    
    while num2 < len(list_valence_sto):
        list = []
        pot = list_valence_sto[num2][0][1]
        num_ve = list_valence_sto[num2][1][0]
        list.append(pot)
        list.append(num_ve)
        list_VE.append(list)
        num2 += 2
    #END:get VNE 
    
    #START:get bader charge (BC)
    ##PATH                                                                                                                
    path_acf_sto = direc_sto + "/bader/ACF.dat"
    path_acf_def = direc_def + "/bader/ACF.dat"

    ##read ACF.dat at stoichiometry            
    flag_acf_sto = os.path.exists(path_acf_sto)
    if flag_acf_sto == False:
        print("ACF.dat doesn't exist!")
        num += 1

    f = open(path_acf_sto)
    lines_acf_sto = f.readlines()
    f.close()

    ##necessary information
    Metal1 = str(list_VE[3][0]) + "1"
    Metal2 = str(list_VE[3][0]) + "2"
    H_ve = list_VE[2][1]
    M_ve = list_VE[3][1]
    list_num_sto = [[int(66),Metal1,M_ve],[int(67),Metal2,M_ve],[int(63),"Hads1",H_ve],[int(64),"Hads2",H_ve],[int(54),"Hbulk",H_ve]]

    ##START:make list_BC_sto
    list_BC_sto = []
    num1 = 0
    while num1 < len(list_num_sto):
        raw_number = int(list_num_sto[num1][0])
        line = lines_acf_sto[raw_number]
        line = line.replace('\n','')
        line = line.replace('\r','')
        line = line.replace('\t',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        list = []
        name_atom = str(list_num_sto[num1][1])
        list.append(name_atom)
        
        Atom_nbc = float(line[4])
        Atom_nvc = float(list_num_sto[num1][2])
        Atom_ne = -1*(Atom_nbc - Atom_nvc)
        list.append(str(Atom_ne))
        list.append(line[0])
        list_BC_sto.append(list)
        num1 += 1
    #END:make list_BC_sto
        
    ##read ACF.dat at del1H
    flag_acf_def = os.path.exists(path_acf_def)
    if flag_acf_def == False:
        print("ACF.dat doesn't exist!")
        num += 1

    f = open(path_acf_def)
    lines_acf_def = f.readlines()
    f.close()

    ##necessary information
    Metal1 = str(list_VE[3][0]) + "1"
    Metal2 = str(list_VE[3][0]) + "2"
    H_ve = list_VE[2][1]
    M_ve = list_VE[3][1]
    list_num_def = [[int(65),Metal1,M_ve],[int(66),Metal2,M_ve],[int(63),"Hads2",H_ve],[int(54),"Hbulk",H_ve]]

    ##START:make list_BC_sto
    list_BC_def = []
    num1 = 0
    while num1 < len(list_num_def):
        raw_number = int(list_num_def[num1][0])
        line = lines_acf_def[raw_number]
        line = line.replace('\n','')
        line = line.replace('\r','')
        line = line.replace('\t',' ')
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        list = []
        name_atom = str(list_num_def[num1][1])
        list.append(name_atom)

        Atom_nbc = float(line[4])
        Atom_nvc = float(list_num_def[num1][2])
        Atom_ne = -1*(Atom_nbc - Atom_nvc)
        list.append(str(Atom_ne))
        list.append(line[0])

        list_BC_def.append(list)
        num1 += 1
    ##END:make list_BC_sto         

    #make list_BC_diff    
    ne_diff_M1 = float(float(list_BC_def[0][1]) - float(list_BC_sto[0][1]))
    ne_diff_M2 = float(float(list_BC_def[1][1]) - float(list_BC_sto[1][1]))
    Metal1 = str(list_VE[3][0]) + "1"
    Metal2 = str(list_VE[3][0]) + "2"

    list1 = []
    list1.append(Metal1)
    list1.append(str(ne_diff_M1))
    list2 =[]
    list2.append(Metal2)
    list2.append(str(ne_diff_M2))

    list_BC_diff = []
    list_BC_diff.append(list1)
    list_BC_diff.append(list2)
    
    list = []
    list.append(list_BC_sto)
    list.append(list_BC_def)
    list.append(list_BC_diff)

    list_MAIN.append(list)
    num +=1
#END

os.chdir(path)
f = open("BADERCHARGE",'w')
num = 0

while num < len(list_MAIN):
        num1 = 0
        sto_ve = "sto    "
        def_ve = "del1H  "
        name = "         "
        while num1 < len(list_MAIN[num][0]):
            name += str(list_MAIN[num][0][num1][0]) + "       "
            sto_ve  += str(list_MAIN[num][0][num1][1]) + "  "
            num1 += 1

        name += "\n"
        sto_ve += "\n"
        
        num2 = 0
        def_ve = "del1H  "
        while num2 < len(list_MAIN[num][1]):
            if num2 == 1:
                def_ve += str(list_MAIN[num][1][num2][1]) + "       -      "
            else:
                def_ve += str(list_MAIN[num][1][num2][1]) + "  "
            num2 += 1
        def_ve += "\n"

        num3 = 0
        diff_ve = "diff   "
        while num3 < len(list_MAIN[num][2]):
            diff_ve += str(list_MAIN[num][2][num3][1]) + "   "
            num3 += 1
        diff_ve += "\n" + "\n"

        f.write(name)
        L = "-------------------------------------------------------------------" + "\n"
        f.write(L)
        f.write(sto_ve)
        f.write(def_ve)
        f.write(diff_ve)
        num += 1
f.close()

print("BADERCHARGE has been made!")
