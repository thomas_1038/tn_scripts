#!/usr/bin/env python3 
# -*- coding: utf-8 -*-                              

import os
import os.path
import shutil
import sys
import glob
import re

#path
path = os.getcwd()
path_pos = path + "/POSCAR"
path_cont = path + "/CONTCAR"

#read DOSCAR
dosfile = "DOSCAR.lobster"
path_dos = path + "/DOSCAR.lobster"
#dosfile = raw_input("Pleace input the DOS file name: ")
f = open(path_dos)
doslines = f.readlines()
f.close()

### Start: make the directory of DOSsplist ###
path_dosdirec = path_dos + "_split"
flag_dd = os.path.exists(path_dosdirec)
if flag_dd == False:
    os.mkdir(path_dosdirec)
### Finish: make the directory of DOSsplist ### 

### Start: get fermilevel from vasprun ###
#read vasprun.xml
path_vasprun = path + "/vasprun.xml"
flag_vasprun = os.path.exists(path_vasprun)
if flag_vasprun == False:
    print("You should prepare the vasprun.xml!")
    sys.exit()

ld = open("./vasprun.xml")
lines = ld.readlines()
ld.close()

#search "efermi" line from vasprun.xml
fl = []
for line in lines:
    if line.find("efermi") >= 0:
        list1 = line.split(' ')
        fl.append(list1)

#make efermi line list
fermi = []
list2 = fl[0]
num1 = 0
while num1 < len(list2):
    fermi.append(list2[num1])
    num1 += 1

#deleate " " from list
while fermi.count("") > 0:
    fermi.remove("")

#print fermi energy
Efermi =  float(fermi[2])
print(Efermi)
### Finish: get fermilevel from vasprun ###

### Start: make the lables of atoms  ###
#check the POSCAR or CONTCAR is in the directory
flag_pos = os.path.exists(path_pos)
if flag_pos == True:
    f = open(path_pos)
    poslines = f.readlines()
    f.close()

else:
    flag_cont = os.path.exists(path_cont)
    if flag_cont == True:
        f = open(path_cont)
        poslines = f.readlines()
        f.close()
    else:
        print("Please prepare POSCAR or CONTCAR.")
        sys.exit()

#read Atom label                                                                                    
atomlabel = poslines[5].replace('\n','')
atomlabel = atomlabel.replace('^M','')
atomlabel = atomlabel.split(" ")
while atomlabel.count("") > 0:
    atomlabel.remove("")

#count Atom number                                                                                  
atomnum = poslines[6].replace('\n','')
atomnum = atomnum.replace('^M','')
atomnum = atomnum.split(" ")
while atomnum.count("") > 0:
    atomnum.remove("")

#make atomlist        
num1 = 0
atomlabellist = []
while num1 < len(atomlabel):
    num2 = 0
    while num2 < int(atomnum[num1]):
        num3 = num2 + 1
        L = atomlabel[num1] + str(num3)
        atomlabellist.append(L)
        num2 += 1
    num1 += 1
### Finish: make the lables of atoms  ###

### Start: count dosdata of each DOS ###
dosline  = doslines[5]
dosline = dosline.replace('\n','')
dosline = dosline.replace('\r','')
dosline = dosline.replace('\t',' ')
dosline = dosline.split(" ")
while dosline.count("") > 0:
    dosline.remove("")

N_dos = int(dosline[2])
### Finish: count dosdata of each DOS ###

total_dos = "Energy   DOS_up   DOS_down   iDOS_up   iDOS_down" + "\n"
partial_dos = ["Energy","s_up","s_down","py_up","py_down","pz_up","pz_down","px_up","px_down","dxy_up","dxy_down","dyz_up","dyz_down","d3z2-1_up","d3z2-1_down","dxz_up","dxz_down","dx2-y2_up","dx2-y2_down"]


list_tot = []
list_totup = []
list_totdown = []

n = 0
l = 0
Nloop = len(atomlabellist) +1
N_dos = int(dosline[2])+1
while n < Nloop:
    if n == 0:
        path_dossplit = path_dosdirec + "/DOSCAR_tot" 
        f = open(path_dossplit, 'w')
        f.write(total_dos)

        m = 6 + n*N_dos
        i = 0
        fi = N_dos -1
        while i < N_dos:
            line_dos = doslines[m]
            line_dos = line_dos.replace('\n','')
            line_dos = line_dos.replace('\r','')
            line_dos = line_dos.replace('\t',' ')
            line_dos = line_dos.split(" ")
            while line_dos.count("") > 0:
                line_dos.remove("")

            if dosfile == "DOSCAR":
                    en = float(line_dos[0]) -Efermi
            elif dosfile == "DOSCAR.lobster":
                    en = line_dos[0]

            Ld = str(en) + "   "

            n_dos  = 1
            while n_dos  < len(line_dos):
                Ld += str(line_dos[n_dos]) + "   " 
                n_dos += 1
            Ld += "\n"
                
            f.write(Ld)

            i += 1
            m += 1

        f.close()
        print("DOSCAR_tot has been made!")

        n += 1

    else:
        path_dossplit = path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1])
        path_dossplit_up = path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1]) + "_up"
        path_dossplit_down = path_dosdirec + "/DOSCAR_" + str(atomlabellist[n-1])+ "_down"
        f = open(path_dossplit, 'w')
        fup = open(path_dossplit_up, 'w')
        fdown =open(path_dossplit_down, 'w')

        list_atomtotup = []
        list_atomtotdown = []
        list_atomtot = []

        m = 6 + n*N_dos
        i = 0
        fi = N_dos -1
        while i < fi:
            list_tagup = []
            list_tagdown = []
            list_dup = []
            list_ddown = []

            if i == 0:
                line_orb = doslines[m]
                line_orb = line_orb.replace('\n','')
                line_orb = line_orb.replace('\r','')
                line_orb = line_orb.replace('\t',' ')
                line_orb = line_orb.split(" ")
                while line_orb.count("") > 0:
                    line_orb.remove("")

                #add tag of Energy
                tagup = partial_dos[0] + "   "
                tagdown = partial_dos[0] + "   "
                list_tagup.append(partial_dos[0])
                list_tagdown.append(partial_dos[0])
                
                #add Energy
                if dosfile == "DOSCAR":
                    en = float(line_orb[0]) -Efermi
                elif dosfile == "DOSCAR.lobster":
                    en = line_orb[0]
                    
                Ldup = str(en) + "   "
                Lddown = str(en) + "   "
                list_dup.append(en)
                list_ddown.append(en)

                Nduptot = 0
                Nddowntot = 0

                n_orb  = 1
                while n_orb  < len(line_orb):
                    if n_orb % 2 == 1:
                        list_tagup.append(partial_dos[n_orb])
                        tagup += partial_dos[n_orb] + "   "

                        list_dup.append(line_orb[n_orb])
                        Ldup += line_orb[n_orb] + "   "
                        Nduptot += float(line_orb[n_orb])

                        n_orb += 1

                    else:
                        list_tagdown.append(partial_dos[n_orb])
                        tagdown += partial_dos[n_orb] + "   "

                        list_ddown.append(line_orb[n_orb])
                        Lddown += line_orb[n_orb] + "   "
                        Nddowntot += float(line_orb[n_orb])

                        n_orb += 1

                #add total-dostag of each atom
                tagup += "total_up"  
                tagdown += "total_down"
                Ltag = tagup + "  " + tagdown
                
                #write dostag of each atom
                Ltag += "\n"
                f.write(Ltag)
                tagup += "\n"
                fup.write(tagup)
                tagdown += "\n"
                fdown.write(tagdown)

                #add total-dos of each atom
                Ldup += str(Nduptot) 
                Lddown += str(Nddowntot)
                Ldtot = Ldup + "   " + Lddown

                #write dos of each atom
                Ldtot += "\n"
                f.write(Ldtot)
                Ldup += "\n"
                fup.write(Ldup)
                Lddown += "\n"
                fdown.write(Lddown)

                #make first line
                list_firstu = []
                numu_bar = 1
                fnumu_bar = len(list_tagup)
                while numu_bar < fnumu_bar:
                    list_firstu.append("---")
                    numu_bar += 1

                atomup = str(atomlabellist[n-1]) + "_up"
                list_firstu.append(atomup)

                list_firstd = []
                numd_bar = 1
                fnumd_bar = len(list_tagdown)
                while numd_bar < fnumd_bar:
                    list_firstd.append("---")
                    numd_bar += 1
                    
                atomdown = str(atomlabellist[n-1]) + "down"
                list_firstd.append(atomdown)

                #add list_atomtotup
                list_atomtotup.append(list_firstu)
                list_atomtotup.append(list_tagup)
                list_atomtotup.append(list_dup)
                
                #add list_atomtotdown
                list_atomtotdown.append(list_firstd)
                list_atomtotdown.append(list_tagdown)
                list_atomtotdown.append(list_ddown)
                
                #make first line of tot
                list_first_tot = list_firstu + list_firstd
                list_tagtot = list_tagup + list_tagdown

                #add list_atomtot
                list_atomtot.append(list_first_tot)
                list_atomtot.append(list_tagtot)

                list_dtot = list_dup + list_ddown
                list_atomtot.append(list_dtot)

            else:                
                line_orb = doslines[m]
                line_orb = line_orb.replace('\n','')
                line_orb = line_orb.replace('\r','')
                line_orb = line_orb.replace('\t',' ')
                line_orb = line_orb.split(" ")
                while line_orb.count("") > 0:
                    line_orb.remove("")
            
                #add Energy
                if dosfile == "DOSCAR":
                    en = float(line_orb[0]) -Efermi
                elif dosfile == "DOSCAR.lobster":
                    en = line_orb[0]

                Ldup = str(en) + "   "
                Lddown = str(en) + "   "
                list_dup.append(en)
                list_ddown.append(en)
                
                Nduptot = 0
                Nddowntot = 0

                n_orb = 1
                while n_orb < len(line_orb):
                    if n_orb % 2 == 1:
                        list_dup.append(line_orb[n_orb])
                        Ldup += line_orb[n_orb] + "   "
                        Nduptot += float(line_orb[n_orb])

                        n_orb += 1

                    else:
                        list_ddown.append(line_orb[n_orb])
                        Lddown += line_orb[n_orb] + "   "
                        Nddowntot += float(line_orb[n_orb])

                        n_orb += 1

                #add total-dos of each atom
                Ldup += str(Nduptot)
                Lddown += str(Nddowntot)
                Ldtot = Ldup + "   " + Lddown

                #write dos of each atom
                Ldtot += "\n"
                f.write(Ldtot)
                Ldup += "\n"
                fup.write(Ldup)
                Lddown += "\n"
                fdown.write(Lddown)

                #add
                list_atomtotup.append(list_dup)
                list_atomtotdown.append(list_ddown)

                list_dtot = list_dup + list_ddown
                list_atomtot.append(list_dtot)


            i += 1
            m += 1
    

        f.close()
        fup.close()
        fdown.close()

        print("DOSCAR_" + str(atomlabellist[n-1]) + " has been made!")
        print("DOSCAR_" + str(atomlabellist[n-1]) + "_up has been made!")
        print("DOSCAR_" + str(atomlabellist[n-1]) + "_down has been made!")

        list_tot.append(list_atomtot)
        list_totup.append(list_atomtotup)
        list_totdown.append(list_atomtotdown)

        n += 1

print("Done!")
#print list_tot[65]

#make DOS_all
list_tot_print = []
a = 0
while a < len(list_tot[0]):
    list_tot_en = []

    b = 0
    while b < len(list_tot):
        list_tot_en += list_tot[b][a]
        b += 1

    list_tot_print.append(list_tot_en)
    a += 1


path_dosall = path_dosdirec + "/DOSCAR_all"
fall = open(path_dosall, 'w')

l = 0
while l < len(list_tot_print):
    L = ""

    v = 0
    while v < len(list_tot_print[l]):
        L += str(list_tot_print[l][v]) + "   "
        v += 1

    L += "\n"
    fall.write(L)
    l += 1

print("DOSCAR_all has been made!")

#make DOS_all_up
list_totup_print = []
a = 0
while a < len(list_totup[0]):
    list_totup_en = []

    b = 0
    while b < len(list_totup):
        list_totup_en += list_totup[b][a]
        b += 1

    list_totup_print.append(list_totup_en)
    a += 1

path_dosallup = path_dosdirec + "/DOSCAR_all_up"
fallup = open(path_dosallup, 'w')
l = 0
while l < len(list_tot_print):
    L = ""

    v = 0
    while v < len(list_tot_print[l]):
        L += str(list_tot_print[l][v]) + "   "
        v += 1

    L += "\n"
    fallup.write(L)
    l += 1

print("DOSCAR_all_up has been made!")

#make DOS_all_down
list_totdown_print = []
a = 0
while a < len(list_totdown[0]):
    list_totdown_en = []

    b = 0
    while b < len(list_totdown):
        list_totdown_en += list_totdown[b][a]
        b += 1

    list_totdown_print.append(list_totdown_en)
    a += 1

path_dosalldown = path_dosdirec + "/DOSCAR_all_down"
falldown = open(path_dosalldown, 'w')
for line in list_totdown_print:
    L = ""

l = 0
while l < len(list_tot_print):
    L = ""

    v = 0
    while v < len(list_tot_print[l]):
        L += str(list_tot_print[l][v]) + "   "
        v += 1
    
    L += "\n"
    falldown.write(L)
    l += 1

print("DOSCAR_all_down has been made!")
