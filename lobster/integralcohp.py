#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/08/26 ver1.00

import os
import numpy as np
import sys

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

argv = sys.argv
path = os.getcwd()

filename = input("Please input the file name: ")
enmin = input("Please input the minimum energy: ")
enmax = input("Please input the maximum energy: ")

# read file
path_file = str(path+ "/" +filename)
f = open(path_file)
filelines = f.readlines()
f.close()

list_cohp = [conv_line(x) for x in filelines]
del list_cohp[0]

energy = [x[0] for x in list_cohp]
cohp_b = [float(x[1]) for x in list_cohp]
cohp_a = [float(x[2]) for x in list_cohp]

cohp_tot = np.array(np.array(cohp_b) + np.array(cohp_a)).tolist()

def integral(list):
     S = 0
     num_c1 = 0  
     num_c2 = 1

     while num_c2 < len(energy):
          x1 = float(energy[num_c1])
          x2 = float(energy[num_c2])
          y1 = float(list[num_c1])
          y2 = float(list[num_c2])
    
          if x1 < float(enmin): 
               S += 0
               num_c1 += 1
               num_c2 += 1
          elif x1 > float(enmin) and x2 <= float(enmax):
               s = 0.5*(x2-x1)*(y1+y2)
               S += s
          else:
               S += 0
          num_c1 += 1
          num_c2 += 1

     return S

Sb = integral(cohp_b)
Sa = integral(cohp_a)
Stot = integral(cohp_tot)
#Stot = Sa + Sb

print("iCOHP_bonding = " + str(Sb))
print("iCOHP_anti-bonding = " + str(Sa))
print("iCOHP_total = " + str(Stot))


