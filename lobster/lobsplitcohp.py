#!/usr/bin/env python3 
# -*- coding: utf-8 -*-                              

import os
import os.path
import shutil
import sys
import glob
import re

path = os.getcwd()

#argv = sys.argv
Q_cohp = input("Pleace input the COOP/COHP file name, [o,h]: ")
if Q_cohp == "h":
    cohpfile = "COHPCAR.lobster"
elif Q_cohp == "o":
    cohpfile = "COOPCAR.lobster"

#cohpfile = str(argv[2]) 
with open(cohpfile) as fc: lines = fc.readlines()

taglist = []; datalist0 = []
for line in lines:
    if line.find("No.") >= 0:
        taglist.append(line)
    elif line.replace(".","").isdigit() >= 0:
        datalist0.append(line)

#remove datalist0[0]~[2]                                                      
del datalist0[0]
del datalist0[0]
del datalist0[0]

#make tag list
taglist1 = []
for tag in taglist:
    tag = tag.replace('No.1:','total_')
    tag = tag.replace('No.2:','')
    tag = re.sub(r'\(.*?\)','',tag)
    tag = tag.replace('\n','')
    tag = tag.replace('>','')
    tag = tag.replace('[','_')
    tag = tag.replace(']','')
    tag = tag.replace('^','')
    taglist1.append(tag)

# print taglist1
# print len(taglist1)

line_tag_forgraph = "Energy[eV] " + str(taglist1[0]) + "_ave_bonding " + str(taglist1[0]) + "_ave_anti-bonding " + str(taglist1[0])+ "_1st_bonding " + str(taglist1[0])+ "_1st_anti-bonding "
tag = 1
f_tag = len(taglist1) -1
while tag < f_tag:
    line_tag_forgraph += str(taglist1[tag]) +  "_bonding " +  str(taglist1[tag]) +  "_anti-bonding "    
    tag += 1    
line_tag_forgraph += str(taglist1[tag]) +  "_bonding " +  str(taglist1[tag]) +  "_anti-bonding " + "----- Energy[eV] COHP_total_bonding COHP_total_anti-bonding" + "\n"

line_tag = "Energy[eV] " + str(taglist1[0]) + "_ave " + str(taglist1[0])+ "_1st " 
tag = 1
f_tag = len(taglist1) -1
while tag < f_tag:
    line_tag += str(taglist1[tag]) +  " " 
    tag += 1
line_tag += str(taglist1[tag]) +  " "  + " ----- Energy[eV] COHP_total" + "\n"
    
#make datalist
datanum = len(datalist0)
print(datanum)
i = 0
datalist1 = []
while i < datanum:
    splitline = datalist0[i].split(" ")
    while splitline.count("") > 0:
          splitline.remove("")
    datalist1.append(splitline)
    i += 1

j = 0
cohplist = []
while j < len(datalist1[0]):
    i = 0
    cohpval = []
    while i < len (datalist1):
        L = float(datalist1[i][j])
        cohpval.append(L)
        i += 1
    cohplist.append(cohpval)
    j += 1
#print cohplist

def makesplitfile_spin1():
    #make directory
    direc_path = path + "/split-" + str(cohpfile)
    os.mkdir(direc_path)

    L_1st = "Energy[eV] COHP_ave_bonding COHP_ave_anti-bonding iCOHP_ave COHP_1st_bonding COHP_1st_anti-bonding iCOHP_1st" + "\n"

    #definition of file name
    filename = direc_path + "/" + str(taglist1[0]) + ".dat"
    f = open(filename,'w')
    f.write(L_1st)

    cohp_ave = float(cohplist[1][num])
    cohp_1st = float(cohplist[3][num])

    num = 0
    while num < len(cohplist[0]):
        cohp_ave = float(cohplist[1][num])
        icohp_ave = float(cohplist[2][num])
        cohp_1st = float(cohplist[3][num])
        icohp_1st = float(cohplist[4][num])

        cohp_ave_b = 0
        cohp_ave_a = 0
        cohp_1st_b = 0
        cohp_1st_a = 0

        if cohp_ave < 0:
            cohp_ave_b = cohp_ave
            cohp_ave_a = 0
        elif cohp_ave > 0:
            cohp_ave_b = 0
            cohp_ave_a = cohp_ave
        elif cohp_ave == 0:
            cohp_ave_b = 0
            cohp_ave_a = 0
            
        if cohp_1st < 0:
            cohp_1st_b = cohp_1st
            cohp_1st_a = 0
        elif cohp_1st > 0:
            cohp_1st_b = 0
            cohp_1st_a = cohp_1st
        elif cohp_1st == 0:
            cohp_1st_b = 0
            cohp_1st_a = 0

        line = str(cohplist[0][num]) + "  " + str(cohp_ave_b) + "  " + str(cohp_ave_a) + "  " + str(icohp_ave) + "  " + str(cohp_1st_b) + "  " + str(cohp_1st_a) + "  " + str(icohp_1st) + "\n"
        f.write(line)
        num += 1
    f.close()

def makesplitfile_spin2():
    #make directory
    updirec_path = path + "/split-" + str(cohpfile) +"_up"
    os.mkdir(updirec_path)

    downdirec_path = path + "/split-" + str(cohpfile) +"_down"
    os.mkdir(downdirec_path)

    #definition of file name
    upfilename = updirec_path + "/" + str(taglist1[0]) + "_up.dat"
    downfilename = downdirec_path + "/" + str(taglist1[0]) + "_down.dat"
    fup = open(upfilename,'w')
    fdown = open(downfilename,'w')

    L_1st = "Energy[eV] COHP_ave_bonding COHP_ave_anti-bonding iCOHP_ave COHP_1st_bonding COHP_1st_anti-bonding iCOHP_1st" + "\n"
    fup.write(L_1st)
    fdown.write(L_1st)

    num = 0
    while num < len(cohplist[0]):
        cohpu_ave = float(cohplist[1][num])
        icohpu_ave = float(cohplist[2][num])
        cohpu_1st = float(cohplist[3][num])
        icohpu_1st = float(cohplist[4][num])

        cohpu_ave_b = 0
        cohpu_ave_a = 0
        cohpu_1st_b = 0
        cohpu_1st_a = 0

        if cohpu_ave < 0:
            cohpu_ave_b = cohpu_ave
            cohpu_ave_a = 0
        elif cohpu_ave > 0:
            cohpu_ave_b = 0
            cohpu_ave_a = cohpu_ave
        elif cohpu_ave == 0:
            cohpu_ave_b = 0
            cohpu_ave_a = 0

        if cohpu_1st < 0:
            cohpu_1st_b = cohpu_1st
            cohpu_1st_a = 0
        elif cohpu_1st > 0:
            cohpu_1st_b = 0
            cohpu_1st_a = cohpu_1st
        elif cohpu_1st == 0:
            cohpu_1st_b = 0
            cohpu_1st_a = 0

        upline = str(cohplist[0][num]) + " " + str(cohpu_ave_b) + " " + str(cohpu_ave_a) + " " + str(icohpu_ave) + " " + str(cohpu_1st_b) + " " + str(cohpu_1st_a) + " "  + str(icohpu_1st) + "\n"
        fup.write(upline)
        num += 1
    fup.close()

    num = 0
    while num < len(cohplist[0]):
        cohpd_ave = float(cohplist[5][num])
        icohpd_ave = float(cohplist[6][num])
        cohpd_1st = float(cohplist[7][num])
        icohpd_1st = float(cohplist[8][num])

        cohpd_ave_b = 0
        cohpd_ave_a = 0
        cohpd_1st_b = 0
        cohpd_1st_a = 0

        if cohpd_ave < 0:
            cohpd_ave_b = cohpd_ave
            cohpd_ave_a = 0
        elif cohpd_ave > 0:
            cohpd_ave_b = 0
            cohpd_ave_a = cohpd_ave
        elif cohpd_ave == 0:
            cohpd_ave_b = 0
            cohpd_ave_a = 0

        if cohpd_1st < 0:
            cohpd_1st_b = cohpd_1st
            cohpd_1st_a = 0
        elif cohp_1st > 0:
            cohpd_1st_b = 0
            cohpd_1st_a = cohpd_1st
        elif cohpd_1st == 0:
            cohpd_1st_b = 0
            cohpd_1st_a = 0

        downline = str(cohplist[0][num]) + "  " + str(cohpd_ave_b) + "  " + str(cohpd_ave_a) + "  " + str(icohpd_ave) + "  " + str(cohpd_1st_b) + "  " + str(cohpd_1st_a) + "  " + str(icohpd_1st) + "\n"
        fdown.write(downline)
        num += 1
    fdown.close()

def makesplitfile_orbital_spin1():
    #make directory
    direc_path = path + "/split-" + str(cohpfile) 
    os.mkdir(direc_path)

    #variable parameter
    k = 1
    m = 0
    orbnum = len(cohplist)
    orbnum1 = orbnum + 1

    energy = []
    cohp_all_a = []
    cohp_all_b = []
    icohp_all = []
    cohp_all = []

#split COHP data and make split file
    while k < len(cohplist):
        k1 = int(k+1)
        k2 = int(k+2)
        k3 = int(k+3)

        filename = direc_path + "/" + str(taglist1[m]) + ".dat"
        f = open(filename,'w')

        # make COHP_total
        if k == 1:
            list_cohpall = []
            L1_tag = str(taglist1[m]) + " --- --- --- --- --- ---"
            L_1st = "Energy[eV] COHP_ave_bonding COHP_ave_anti-bonding iCOHP_ave COHP_1st_bonding COHP_1st_anti-bonding iCOHP_1st"
            f.write(L_1st+"\n")
            
            list_cohpall.append(L1_tag)
            list_cohpall.append(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohp_ave = float(cohplist[k][num])
                icohp_ave = float(cohplist[k1][num])
                cohp_1st = float(cohplist[k2][num])
                icohp_1st = float(cohplist[k3][num])

                cohp_ave_b = 0
                cohp_ave_a = 0
                cohp_1st_b = 0
                cohp_1st_a = 0

                if cohp_ave < 0:
                    cohp_ave_b = cohp_ave
                    cohp_ave_a = 0
                elif cohp_ave > 0:
                    cohp_ave_b = 0
                    cohp_ave_a = cohp_ave
                elif cohp_ave == 0:
                    cohp_ave_b = 0
                    cohp_ave_a = 0

                if cohp_1st < 0:
                    cohp_1st_b = cohp_1st
                    cohp_1st_a = 0
                elif cohp_1st > 0:
                    cohp_1st_b = 0
                    cohp_1st_a = cohp_1st
                elif cohp_1st == 0:
                    cohp_1st_b = 0
                    cohp_1st_a = 0

                energy.append(cohplist[0][num])
                line = str(cohplist[0][num]) + " " + str(cohp_ave_b) + " " + str(cohp_ave_a) + " " + str(icohp_ave) + " " + str(cohp_1st_b) + " " + str(cohp_1st_a) + " " + str(icohp_1st)
                f.write(line+"\n")
                list_cohpall.append(line)
                num += 1

            f.close()
            cohp_all.append(list_cohpall)

            k += 4
            m += 1

        # make COHP_each_orbital
        elif  k >= 5 and k <= orbnum:
            list_cohp_b = []
            list_cohp_a = []
            list_icohp = []
            list_cohpall = []

            L1_tag += str(taglist1[m]) + " --- --- ---"
            L_1st = "Energy[eV] COHP_bonding COHP_anti-bonding iCOHP"
            f.write(L_1st+"\n")

            list_cohpall.append(L1_tag)
            list_cohpall.append(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohp = float(cohplist[k][num])
                icohp = float(cohplist[k1][num])
                
                cohp_b = 0
                cohp_a = 0
                if cohp < 0:
                    cohp_b = cohp
                    cohp_a = 0
                elif cohp > 0:
                    cohp_b = 0
                    cohp_a = cohp
                elif cohp == 0:
                    cohp_b = 0
                    cohp_a = 0

                list_cohp_b.append(cohp_b)
                list_cohp_a.append(cohp_a)
                list_icohp.append(icohp)

                line = str(cohplist[0][num]) + " " + str(cohp_b) + " " + str(cohp_a) + " " + str(icohp)
                f.write(line+"\n")
                list_cohpall.append(line)
                num += 1

            f.close()
            cohp_all_b.append(list_cohp_b)
            cohp_all_a.append(list_cohp_a)
            icohp_all.append(list_icohp)
            cohp_all.append(list_cohpall)

            k += 2
            m += 1

    # make list of COHP_sum_bonding
    def mklist_cohp_sum(list_all):
        n1 = 0
        cohp_sum = []
        while n1 < len(list_all[0]):
            n2 = 0
            cohp_sum_each = 0
            while n2 < len(list_all):
                cohp_sum_each += float(list_all[n2][n1])
                n2 += 1
            cohp_sum.append(cohp_sum_each)
            n1 += 1
    
        return cohp_sum
    
    cohp_sum_b = mklist_cohp_sum(cohp_all_b)
    cohp_sum_a = mklist_cohp_sum(cohp_all_a)
    icohp_sum =  mklist_cohp_sum(icohp_all)

    # make Total.dat
    num_s = 0
    L_sum = "Energy[eV] COHP_total_bonding COHP_total_anti-bonding iCOHP_total\n"
    while num_s < len(energy):
        cohp_sum_val = float(cohp_sum_b[num_s]) + float(cohp_sum_a[num_s])
        if cohp_sum_val < 0:
            cohp_sum_b_val = cohp_sum_val
            cohp_sum_a_val = 0

        elif cohp_sum_val > 0:
            cohp_sum_b_val = 0
            cohp_sum_a_val = cohp_sum_val

        elif cohp_sum_val == 0:
            cohp_sum_b_val = 0
            cohp_sum_a_val = 0

        L_sum += str(energy[num_s]) + " " + str(cohp_sum_b_val) + " " + str(cohp_sum_a_val) + " " + str(icohp_sum[num_s]) + "\n"
        num_s += 1

    name_cohp_sum = direc_path + "/Total.dat"
    f_sum = open(name_cohp_sum, "w")
    f_sum.write(L_sum)
    f_sum.close()
        
    # make All.dat
    n_a1 = 0
    L_all = ""
    while n_a1 < len(cohp_all[0]):
        n_a2 = 0
        while n_a2 < len(cohp_all):
            L_all += str(cohp_all[n_a2][n_a1]) + " "
            n_a2 += 1
        
        L_all += "\n"
        n_a1 += 1

    name_cohp_all = direc_path + "/All.dat"
    f_all = open(name_cohp_all, "w")
    f_all.write(L_all)
    f_all.close()
        
def makesplitfile_orbital_spin2():
    #make directory
    updirec_path = path + "/split-" + str(cohpfile) +"_up" 
    flag_u = os.path.exists(updirec_path)
    if flag_u == False:
        os.mkdir(updirec_path)
    else:
        Lp = str(cohpfile) +"_up" + "already exists!"
        print(Lp)

    downdirec_path = path + "/split-" + str(cohpfile) +"_down"
    flag_d = os.path.exists(downdirec_path)
    if flag_d == False:
        os.mkdir(downdirec_path)
    else:
        Lp = str(cohpfile) +"_down" + "already exists!"
        print(Lp)

    #varialble parameter
    k = 1
    m = 0
    orbnum = int((len(cohplist)-1)/2)
    orbnum1 = orbnum + 1
    
    #split COHP data and make split file
    while k < len(cohplist):
        k1 = int(k+1)
        k2 = int(k+2)
        k3 = int(k+3)
        if m <= int(len(taglist1) - 1):
            filename = updirec_path + "/" + str(taglist1[m]) + "_up.dat"
        elif m >= len(taglist1):
            n = m - len(taglist1) 
            filename = downdirec_path + "/" + str(taglist1[n]) + "_down.dat"

        #file name
        f = open(filename,'w')
        
        if k == 1:
            L_1st = "Energy[eV]  COHP_ave_bonding   COHP_ave_anti-bonding   iCOHP_ave  COHP_1st_bonding    COHP_1st_anti-bonding   iCOHP_1st" + "\n"
            f.write(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohpu_ave = float(cohplist[k][num])
                icohpu_ave = float(cohplist[k1][num])
                cohpu_1st = float(cohplist[k2][num])
                icohpu_1st = float(cohplist[k3][num])

                cohpu_ave_b = 0
                cohpu_ave_a = 0
                cohpu_1st_b = 0
                cohpu_1st_a = 0
                
                if cohpu_ave < 0:
                    cohpu_ave_b = cohpu_ave
                    cohpu_ave_a = 0
                elif cohpu_ave > 0:
                    cohpu_ave_b = 0
                    cohpu_ave_a = cohpu_ave
                elif cohpu_ave == 0:
                    cohpu_ave_b = 0
                    cohpu_ave_a = 0

                if cohpu_1st < 0:
                    cohpu_1st_b = cohpu_1st
                    cohpu_1st_a = 0
                elif cohpu_1st > 0:
                    cohpu_1st_b = 0
                    cohpu_1st_a = cohpu_1st
                elif cohpu_1st == 0:
                    cohpu_1st_b = 0
                    cohpu_1st_a = 0

                line = str(cohplist[0][num]) + " " + str(cohpu_ave_b) + " " + str(cohpu_ave_a) + " " + str(icohpu_ave) + " " + str(cohpu_1st_b) + " " + str(cohpu_1st_a) + " "  + str(icohpu_1st) + "\n"
                f.write(line)
                num += 1
            f.close()
            k += 4
            m += 1

        elif  k >= 5 and k <= orbnum:
            L_1st = "Energy[eV]  COHP_ave_bonding   COHP_ave_anti-bonding   iCOHP_ave" + "\n"
            f.write(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohpu = float(cohplist[k][num])
                icohpu = float(cohplist[k1][num])

                cohpu_b = 0
                cohpu_a = 0

                if cohpu < 0:
                    cohpu_b = cohpu
                    cohpu_a = 0
                elif cohpu > 0:
                    cohpu_b = 0
                    cohpu_a = cohpu
                elif cohpu == 0:
                    cohpu_b = 0
                    cohpu_a = 0

                line = str(cohplist[0][num]) + " " + str(cohpu_b) + " " + str(cohpu_a) + " " + str(icohpu) + "\n"
                f.write(line)
                num += 1
            f.close()
            k += 2
            m += 1

        elif k == orbnum1:
            L_1st = "Energy[eV] COHP_ave_bonding COHP_ave_anti-bonding iCOHP_ave COHP_1st_bonding COHP_1st_anti-bonding iCOHP_1st\n"
            f.write(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohpd_ave = float(cohplist[k][num])
                icohpd_ave = float(cohplist[k1][num])
                cohpd_1st = float(cohplist[k2][num])
                icohpd_1st = float(cohplist[k3][num])

                cohpd_ave_b = 0
                cohpd_ave_a = 0
                cohpd_1st_b = 0
                cohpd_1st_a = 0

                if cohpd_ave < 0:
                    cohpd_ave_b = cohpd_ave
                    cohpd_ave_a = 0
                elif cohpd_ave > 0:
                    cohpd_ave_b = 0
                    cohpd_ave_a = cohpd_ave
                elif cohpd_ave == 0:
                    cohpd_ave_b = 0
                    cohpd_ave_a = 0

                if cohpd_1st < 0:
                    cohpd_1st_b = cohpd_1st
                    cohpd_1st_a = 0
                elif cohpd_1st > 0:
                    cohpd_1st_b = 0
                    cohpd_1st_a = cohpd_1st
                elif cohpd_1st == 0:
                    cohpd_1st_b = 0
                    cohpd_1st_a = 0

                line = str(cohplist[0][num]) + " " + str(cohpd_ave_b) + " " + str(cohpd_ave_a) + " " + str(icohpd_ave) + " " + str(cohpd_1st_b) + " " + str(cohpd_1st_a) + " " + str(icohpd_1st) + "\n"
                f.write(line)
                num += 1
            f.close()
            k += 4
            m += 1

        elif k >= int(orbnum1+4):
            L_1st = "Energy[eV] COHP_ave_bonding COHP_ave_anti-bonding iCOHP_ave\n"
            f.write(L_1st)

            num = 0
            while num < len(cohplist[0]):
                cohpd = float(cohplist[k][num])
                icohpd = float(cohplist[k1][num])

                cohpd_b = 0
                cohpd_a = 0

                if cohpd < 0:
                    cohpd_b = cohpd
                    cohpd_a = 0
                elif cohpd > 0:
                    cohpd_b = 0
                    cohpd_a = cohpd
                elif cohpd == 0:
                    cohpd_b = 0
                    cohpd_a = 0

                line = str(cohplist[0][num]) + " " + str(cohpd_b) + " " + str(cohpd_a) + " " + str(icohpd) + "\n"
                f.write(line)
                num += 1
            f.close()
            k += 2
            m += 1
        #END: LOOP

    #START LOOP
    n = 0
    orbnum = int((len(cohplist)-1)/2)
    orbnumb = orbnum -1
    orbnuma = orbnum +1

    list_cohpu = []
    list_cohpu_forgraph = []
    list_icohpu = []

    list_cohpd = []
    list_cohpd_forgraph = []
    list_icohpd = []

    list_cohputot = []
    list_cohpdtot = []
    list_cohputot_forgraph = []
    list_cohpdtot_forgraph = []

    list_cohpu.append(line_tag)
    list_cohpu_forgraph.append(line_tag_forgraph)
    list_icohpu.append(line_tag)

    list_cohpd.append(line_tag)
    list_cohpd_forgraph.append(line_tag_forgraph)
    list_icohpd.append(line_tag)

    list_cohputot.append("Energy[eV] COHP_total\n")
    list_cohpdtot.append("Energy[eV] COHP_total\n")
    list_cohputot_forgraph.append("Energy[eV] COHP_total_bonding COHP_total_anti-bonding\n")
    list_cohpdtot_forgraph.append("Energy[eV] COHP_total_bonding COHP_total_anti-bonding\n")

    #up_spin
    while n < len(cohplist[0]):
        line_cohpu = ""
        line_cohpu_forgraph =""
        line_icohpu = ""

        line_cohpd = ""
        line_cohpd_forgraph =""
        line_icohpd = ""

        k = 1
        cohputot = 0
        cohpdtot = 0

        while k <= orbnum:
            k1 = int(k+1)
            k2 = int(k+2)
            k3 = int(k+3)

            if k == 1:
               # print k
                cohpu_ave = float(cohplist[k][n])
                cohpu_1st = float(cohplist[k2][n])
                icohpu_ave = float(cohplist[k1][n])
                icohpu_1st = float(cohplist[k3][n])

                cohpu_ave_b = 0
                cohpu_ave_a = 0
                cohpu_1st_b = 0
                cohpu_1st_a = 0

                if cohpu_ave < 0:
                    cohpu_ave_b = cohpu_ave
                    cohpu_ave_a = 0
                elif cohpu_ave > 0:
                    cohpu_ave_b = 0
                    cohpu_ave_a = cohpu_ave
                elif cohpu_ave == 0:
                    cohpu_ave_b = 0
                    cohpu_ave_a = 0

                if cohpu_1st < 0:
                    cohpu_1st_b = cohpu_1st
                    cohpu_1st_a = 0
                elif cohpu_1st > 0:
                    cohpu_1st_b = 0
                    cohpu_1st_a = cohpu_1st
                elif cohpu_1st == 0:
                    cohpu_1st_b = 0
                    cohpu_1st_a = 0

                line_cohpu += str(cohplist[0][n]) + " " + str(cohpu_ave) +  " " + str(cohpu_1st) + " " 
                line_cohpu_forgraph += str(cohplist[0][n]) + " " + str(cohpu_ave_b) +  " " + str(cohpu_ave_a) +  " " + str(cohpu_1st_b) + " " + str(cohpu_1st_a) + " "
                line_icohpu += str(cohplist[0][n]) +  " " + str(icohpu_ave) + " " + str(icohpu_1st) + " "
                k += 4

            elif  k >= 5 and k < orbnumb:
               # print k
                cohpu = float(cohplist[k][n])
                icohpu = float(cohplist[k1][n])

                cohpu_b = 0
                cohpu_a = 0

                if cohpu < 0:
                    cohpu_b = cohpu
                    cohpu_a = 0
                elif cohpu > 0:
                    cohpu_b = 0
                    cohpu_a = cohpu
                elif cohpu == 0:
                    cohpu_b = 0
                    cohpu_a = 0

                line_cohpu += str(cohpu) + " "
                line_cohpu_forgraph += str(cohpu_b) +  " " + str(cohpu_a) +  " "
                line_icohpu += str(icohpu) + " "
                cohputot += cohpu_b + cohpu_a
                k += 2
                
            elif k == orbnumb:
                cohpu = float(cohplist[k][n])
                icohpu = float(cohplist[k1][n])
                
                cohpu_b = 0
                cohpu_a = 0

                if cohpu < 0:
                    cohpu_b = cohpu
                    cohpu_a = 0
                elif cohpu > 0:
                    cohpu_b = 0
                    cohpu_a = cohpu
                elif cohpu == 0:
                    cohpu_b = 0
                    cohpu_a = 0

                cohputot += cohpu_b + cohpu_a
                cohputot_a = 0
                cohputot_b = 0
                if cohputot < 0:
                    cohputot_b = cohputot
                    cohputot_a = 0
                elif cohputot > 0:
                    cohputot_b = 0
                    cohputot_a = cohputot
                elif cohputot == 0:
                    cohputot_a = 0
                    cohputot_b = 0

                line_cohpu +=  str(cohpu) + "   -----   " + str(cohplist[0][n]) + " " + str(cohputot) + "\n"
                line_cohpu_forgraph +=  str(cohpu_b) + " " +  str(cohpu_a) + "   -----   " + str(cohplist[0][n]) + " " + str(cohputot_b) + " " + str(cohputot_a) + "\n"
                line_icohpu += str(icohpu) + "\n"
                line_cohputot = str(cohplist[0][n]) + " " + str(cohputot) + "\n"
                line_cohputot_forgraph = str(cohplist[0][n]) + " " + str(cohputot_b) + " " + str(cohputot_a) + "\n"
                k += 2
                
        list_cohpu.append(line_cohpu)
        list_cohpu_forgraph.append(line_cohpu_forgraph)
        list_icohpu.append(line_icohpu)
        list_cohputot.append(line_cohputot)
        list_cohputot_forgraph.append(line_cohputot_forgraph)

        #down_spin
        while k < len(cohplist):
            k1 = int(k+1)
            k2 = int(k+2)
            k3 = int(k+3)

            if k == orbnuma:
               # print k
                cohpd_ave = float(cohplist[k][n])
                cohpd_1st = float(cohplist[k2][n])
                icohpd_ave = float(cohplist[k1][n])
                icohpd_1st = float(cohplist[k3][n])

                cohpd_ave_b = 0
                cohpd_ave_a = 0
                cohpd_1st_b = 0
                cohpd_1st_a = 0

                if cohpd_ave < 0:
                    cohpd_ave_b = cohpd_ave
                    cohpd_ave_a = 0
                elif cohpd_ave > 0:
                    cohpd_ave_b = 0
                    cohpd_ave_a = cohpd_ave
                elif cohpd_ave == 0:
                    cohpd_ave_b = 0
                    cohpd_ave_a = 0

                if cohpd_1st < 0:
                    cohpd_1st_b = cohpd_1st
                    cohpd_1st_a = 0
                elif cohpd_1st > 0:
                    cohpd_1st_b = 0
                    cohpd_1st_a = cohpd_1st
                elif cohpd_1st == 0:
                    cohpd_1st_b = 0
                    cohpd_1st_a = 0

                line_cohpd += str(cohplist[0][n]) + " " + str(cohpd_ave) +  " " + str(cohpd_1st) + " " 
                line_cohpd_forgraph += str(cohplist[0][n]) + " " + str(cohpd_ave_b) +  " " + str(cohpd_ave_a) +  " " + str(cohpd_1st_b) + " " + str(cohpd_1st_a) + " "
                line_icohpd += str(cohplist[0][n]) +  " " + str(icohpd_ave) + " " + str(icohpd_1st) + " "
                k += 4

            elif  k >= int(orbnum1+4) and k < int(len(cohplist)-2):
               # print k
                cohpd = float(cohplist[k][n])
                icohpd = float(cohplist[k1][n])

                cohpd_b = 0
                cohpd_a = 0

                if cohpd < 0:
                    cohpd_b = cohpd
                    cohpd_a = 0
                elif cohpd > 0:
                    cohpd_b = 0
                    cohpd_a = cohpd
                elif cohpd == 0:
                    cohpd_b = 0
                    cohpd_a = 0

                line_cohpd += str(cohpd) +  " " 
                line_cohpd_forgraph += str(cohpd_b) +  " " + str(cohpd_a) +  " "
                line_icohpd += str(icohpd) + " "
                cohpdtot += cohpd_b + cohpd_a
                k += 2

            elif k == int(len(cohplist)-2):
                cohpd = float(cohplist[k][n])
                icohpd = float(cohplist[k1][n])

                cohpd_b = 0
                cohpd_a = 0

                if cohpd < 0:
                    cohpd_b = cohpd
                    cohpd_a = 0
                elif cohpd > 0:
                    cohpd_b = 0
                    cohpd_a = cohpd
                elif cohpd == 0:
                    cohpd_b = 0
                    cohpd_a = 0

                cohpdtot += cohpd_b + cohpd_a
                cohpdtot_a = 0
                cohpdtot_b = 0
                if cohpdtot < 0:
                    cohpdtot_b = cohputot
                    cohpdtot_a = 0
                elif cohpdtot > 0:
                    cohpdtot_b = 0
                    cohpdtot_a = cohputot
                elif cohpdtot == 0:
                    cohpdtot_a = 0
                    cohpdtot_b = 0

                line_cohpd +=  str(cohpd) + "   -----   " + str(cohplist[0][n]) + " " + str(cohpdtot) +  "\n"
                line_cohpd_forgraph +=  str(cohpd_b) + " " +  str(cohpd_a) + "   -----   " + str(cohplist[0][n]) + " " + str(cohpdtot_b) + " " + str(cohpdtot_a) + "\n"
                line_icohpd += str(icohpd) + "\n"
                line_cohpdtot = str(cohplist[0][n]) + " " + str(cohpdtot) + "\n"
                line_cohpdtot_forgraph = str(cohplist[0][n]) + " " + str(cohpdtot_b) + " " + str(cohpdtot_a) + "\n"
                k += 2

        list_cohpd.append(line_cohpd)
        list_cohpd_forgraph.append(line_cohpd_forgraph)
        list_icohpd.append(line_icohpd)
        list_cohpdtot.append(line_cohpdtot)
        list_cohpdtot_forgraph.append(line_cohpdtot_forgraph)

        n += 1
        #END: LOOP
        
    cohpup_name = updirec_path + "/All_up.dat"
    with  open(cohpup_name,'w') as cohpu:
        cohpu.writelines(list_cohpu)

    cohpupforgraph_name = updirec_path + "/All_up_forgraph.dat"
    with  open(cohpupforgraph_name,'w') as cohpufg:
        cohpufg.writelines(list_cohpu_forgraph)

    icohpup_name = updirec_path + "/iAll_up.dat"
    with  open(icohpup_name,'w') as icohpu:
        icohpu.writelines(list_icohpu)

    cohpuptot_name = updirec_path + "/Total_up.dat"
    with  open(cohpuptot_name,'w') as cohputot:
        cohputot.writelines(list_cohputot)

    cohpuptotforgraph_name = updirec_path + "/Total_up_forgraph.dat"
    with  open(cohpuptotforgraph_name,'w') as cohputotfg:
        cohputotfg.writelines(list_cohputot_forgraph)

    cohpdown_name = downdirec_path + "/All_down.dat"
    with  open(cohpdown_name,'w') as cohpd:
        cohpd.writelines(list_cohpd)

    cohpdownforgraph_name = downdirec_path + "/All_down_forgraph.dat"
    with  open(cohpdownforgraph_name,'w') as cohpdfg:
        cohpdfg.writelines(list_cohpd_forgraph)
        
    icohpdown_name = downdirec_path + "/iAll_down.dat"
    with  open(icohpdown_name,'w') as icohpd:
        icohpd.writelines(list_icohpd)

    cohpdowntot_name = downdirec_path + "/Total_down.dat"
    with  open(cohpdowntot_name,'w') as cohpdtot:
        cohpdtot.writelines(list_cohpdtot)
        
    cohpdowntotforgraph_name = downdirec_path + "/Total_down_forgraph.dat"
    with  open(cohpdowntotforgraph_name,'w') as cohpdtotfg:
        cohpdtotfg.writelines(list_cohpdtot_forgraph)


Q_atom_or_orbital = input("Did you calculate COHP/COOP between Atoms only or between Atoms including orbital? [a/o]: ")
Q_spin = input("Please input the number of spin, 1 or 2: ")
if Q_atom_or_orbital in ['a', 'A']:
    if Q_spin in ['1']: makesplitfile_spin1()
    else: makesplitfile_spin2()

else:
    if Q_spin in ['1']:
        makesplitfile_orbital_spin1()
    else:
        makesplitfile_orbital_spin2()

L =  str(cohpfile) + " have been splitted!"
print(L)


#judge = str(argv[1])
#if judge == "a":
#    makesplitfile()
#elif judge == "o":
#    makesplitfile_orbital()
#else:
#    print 'You should input "a" or "o" after script name!'
#    sys.exit()
