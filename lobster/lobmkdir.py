#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/09/13 
#usage:script
#This script make directory which include necessary files for lobster calculation.

import os, sys, shutil, glob
from stat import *

path = os.getcwd()
choice = input("Please input lobster version. [1/2/3]: ")
if choice in ['1']:
    ver_lob = "lobster-1.2.0"
elif choice in ['2']:
    ver_lob = "lobster-2.1.0"
elif choice in ['3']:
    ver_lob = "lobster-3.2.0"

machine = input("Please input the machine name. [tsubame/ accel/ ties]: ")
if machine in ["tsubame", "tbm"]:
    path_lob = "/home/4/17D20121/" + ver_lob
elif machine in ["accel", "a"]:
    path_lob = "/home/nakao/src/" + ver_lob 
elif machine in ["ties", "ti"]:
    path_lob = "/home/nakao/src/" + ver_lob 

dolob = path_lob + "/" + ver_lob

# make lobster directory
num = 1
while True:
    path_lobaf = path + "/lobster" + str(num)
    flag = os.path.exists(path_lobaf)
    if flag == True:
        num += 1
    else:
        break
os.mkdir(path_lobaf)

# copy need files
path_inc   = path + "/INCAR"
path_kp    = path + "/KPOINTS"
path_pot   = path + "/POTCAR"
path_cont  = path + "/CONTCAR"
path_dos   = path + "/DOSCAR"
path_eigen = path + "/EIGENVAL"
path_out   = path + "/OUTCAR"
path_wave  = path + "/WAVECAR"
path_vasp  = path + "/vasprun.xml"

shutil.copy(path_inc, path_lobaf)
shutil.copy(path_kp, path_lobaf)
shutil.copy(path_dos ,path_lobaf)
shutil.copy(path_pot ,path_lobaf)
shutil.copy(path_cont ,path_lobaf)
shutil.copy(path_dos ,path_lobaf)
shutil.copy(path_eigen ,path_lobaf)
shutil.copy(path_out, path_lobaf)
shutil.copy(path_wave, path_lobaf)
shutil.copy(path_vasp, path_lobaf)

# make lobster input file
lobsterin_before = path_lob + "/lobsterin.example"
lobsterin_after = path_lobaf + "/lobsterin"
shutil.copy(lobsterin_before,lobsterin_after)

# make .sh file
L = "#!/bin/sh\n"
L += "#PBS -l nodes=1:ppn=1\n"
L += "cd $PBS_O_WORKDIR\n"
L += "NPROCS=`wc -l <$PBS_NODEFILE`\n"
L += "export MKL_NUM_THREADS=1\n\n"
L += dolob + " > std.out\n"

path_job = path_lobaf + "/dolobster.sh"
f = open(path_job,"w")
f.write(L)
f.close()

os.chmod(path_job, S_IXUSR | S_IRUSR | S_IWUSR )

print("\nLobster directory has been made!")

# END
