#! /usr/bin/env python3                                                            
# -*- coding: utf-8 -*-                                                        
#2017/10/27 version 1.00
#2017/10/28 version 1.10
#This make Energy vs. iteration step file
#usage:script std.out

import os
import shutil
import sys
import subprocess

argv = sys.argv
path = os.getcwd()

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def print_pot():
    print("\n")
    print("##########################################")
    print("#  You should input the name of POTCAR!  #")
    print("##########################################")
    print("\n")
    sys.exit()
    
if len(argv) > 1:
    if "--h" in argv:
        print("\n")
        print("--h: help")
        print("--pot: input the name of POTCARs")
        print("--m: input the name of job")
        print("\n")
        sys.exit()

    elif "--pot" in argv and "--m" in argv:
        if len(argv) == 2:
            print_pot()

        else:
            index_m = argv.index("--m")
            index_p = argv.index("--pot")
            name_mat = str(argv[index_m +1])

            if index_m < index_p:
                num_ind = index_p + 1
                mkpot_E = ""
                while num_ind < len(argv):
                    mkpot_E += str(argv[num_ind]) + " "
                    num_ind += 1

            else:
                num_ind = index_p + 1
                mkpot_E = ""
                while num_ind < index_m:
                    mkpot_E += str(argv[num_ind]) + " "
                    num_ind += 1

    elif "--pot" in argv and "--m" not in argv:
        if len(argv) == 2:
            print_pot()

        else:
            del argv[0]
            del argv[0]
            mkpot_E = ""
            for pot in argv:
                mkpot_E += str(pot) + " "
    
            name_mat = input("Please input NAME of Material, i.e., NaCl: ")

    elif "--m" in argv and "--pot" not in argv:
        index_m = argv.index("--m")
        name_mat = str(argv[index_m +1])
        mkpot_E = input("Please input NAME of POTCAR, i.e., Na_sv Cl: ")

elif len(argv) == 1:
    mkpot_E = input("Please input NAME of POTCAR, i.e., Na_sv Cl: ")
    name_mat = input("Please input NAME of Material, i.e., NaCl: ")

path_poscar = path + "/POSCAR"
flag_pos = os.path.exists(path_poscar)
if flag_pos == False:
    print("\n")
    print("##########################################")
    print("   You should prepare POSCAR here. Bye!   ")
    print("##########################################")
    print("\n")
    sys.exit()

path_cutkp_1p = path + "/cutkp-1p"
path_cutkp_re = path + "/cutkp-re"
path_cutkp_1p_calc = path_cutkp_1p + "/calc"
path_cutkp_re_calc = path_cutkp_re + "/calc"
path_script = "/home/nakao/script/nakao_script/calc/calccutkp.py"
path_cutoff = "/home/nakao/script/nakao_script/calc/cutoff"
path_cutoff_re = "/home/nakao/script/nakao_script/calc/cutoff_re"

list_path = []
list_cutkp1 = []
list_cutkp2 = []

list_cutkp1.append(path_cutkp_1p)
list_cutkp1.append(path_cutkp_1p_calc)
list_cutkp2.append(path_cutkp_re)
list_cutkp2.append(path_cutkp_re_calc)
list_path.append(list_cutkp1)
list_path.append(list_cutkp2)

#make POTCAR
#mkpot_E = raw_input("Please input NAME of POTCAR, i.e., Na_sv Cl: ")
mkpot = "mkpot.py " + mkpot_E + " 4"
os.system(mkpot)
path_potcar = path + "/POTCAR"

num = 0
for path_calc in list_path:
    os.mkdir(path_calc[0])
    os.mkdir(path_calc[1])

    os.chdir(path_calc[1])
    shutil.copy(path_poscar,path_calc[1])
    shutil.copy(path_potcar,path_calc[1])

    os.system("mkdovasp2.py --d")
    if num == 0:
        os.system("mkincar.py -ck1")
    else:
        os.system("mkincar.py -ckr")
    os.system("mkkplist.py")
    
    path_kplist = path_calc[1] + "/kplist"
    shutil.move(path_kplist, path_calc[0])
    shutil.copy(path_script, path_calc[0])
    if num == 0:
        shutil.copy(path_cutoff, path_calc[0])
    else:
        path_cutoff_re_new = path_calc[0] + "/cutoff" 
        shutil.copy(path_cutoff_re, path_cutoff_re_new)

    os.chdir(path_calc[0])
    if num == 0:
        calccutkp = "./calccutkp.py --m " + name_mat
        print(calccutkp)
        #os.system(calccutkp)

    num += 1

path_kplist_cutkp1p = path_cutkp_1p + "/kplist"
f_kp = open(path_kplist_cutkp1p)
kplines = f_kp.readlines()
f_kp.close()

del kplines[0]
kplist = [conv_line(line) for line in kplines if len(conv_line(line)) > 0 and "#" not in conv_line(line)]

#make directory for relax
path_relax = path + "/cut700k" + str(kplist[-1][0]) + "x" + str(kplist[-1][1]) + "x" + str(kplist[-1][2])
os.mkdir(path_relax)

#move POSCAR and POTCAR
shutil.move(path_potcar, path_relax)
shutil.move(path_poscar, path_relax)

#make INCAR, KPOINTS and .sh
os.chdir(path_relax)

job = name_mat + "_cut700k" + str(kplist[-1][0]) + "x" + str(kplist[-1][1]) + "x" + str(kplist[-1][2])
mkdv = "mkdovasp2.py --n " + job
os.system(mkdv)
os.system("mkincar.py -are")
mkkp = "mkkp.py " + str(kplist[-1][0]) + " " + str(kplist[-1][1]) + " " + str(kplist[-1][2])
os.system(mkkp)

jobname = job + ".sh"
qsub = "qsub " + jobname
print(qsub)
#os.system(qsub)

print("cutkp-1p & cutkp-re have been made!" + "\n")
