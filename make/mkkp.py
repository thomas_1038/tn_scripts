#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make KPOINTS file

import os, sys, shutil

def mk_kp(nowpath, k1, k2, k3, ind):
	#judge KPOINTS exisit
	if os.path.exists(nowpath + "/KPOINTS"):
		os.rename(nowpath + "/KPOINTS", nowpath + "/KPOINTS_pre")
		print("Previous KPOINTS changed to KPOINTS_before and New KPOINTS has benn made!")
	else: print("New KPOINTS has been made!")

	#file content
	Lkp = "Automatic mesh\n0.0\n"
	if ind == "g" or ind == "G": Lkp += "Gamma-Center\n"
	else: Lkp += "Monkhorst-pack\n"
	Lkp += str(k1) + " " + str(k2) + " " + str(k3) + "\n0 0 0\n"
	with open(nowpath + "/KPOINTS", "w") as fk: fk.write(Lkp)

	return Lkp

if __name__ == "__main__":
	argv = sys.argv
	Lkp = mk_kp(os.getcwd(), argv[1], argv[2], argv[3], argv[4])
	print("\n"+Lkp+"\n")
# END