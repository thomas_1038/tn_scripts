#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#make STOPCAR file
#2015/05/15 ver1.0
#2015/09/01 ver1.001
#2018/06/21 ver1.10

import sys

argv = sys.argv
if len(argv) == 1:
    Q_sa = input("Which do you use tag in STOPCAR, LSTOP or LABORT [s/a]: ")
    if "s" in Q_sa: numJ = 0
    elif "a" in Q_sa: numJ = 1
    else: print("You should type [s/a] only! BYE!"); sys.exit()
else:
    if "-e" in Q_sa: numJ = 0
    elif "-a" in Q_sa: numJ = 1

if numJ == 1: Ltag = "!LSTOP = T\n" + "LABORT = T"
else: Ltag = "LSTOP = T\n" + "!LABORT = T"

#file content
Ls =  "!LSTOP:stop VASP calculation at the next ionic step.\n"
Ls += "!LABORT:stop VASP calculation at the next electronic step.\n"
Ls += Ltag

#write content
with open("STOPCAR","w") as f: f.write(Ls)
print("STOPCAR has been made in current directory!")
