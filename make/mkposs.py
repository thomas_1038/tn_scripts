#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import addmol.mk_poscar as mkp

def make_poss(path_dir): 
    mkp.mk_poss(path_dir)

if __name__ == "__main__":
    make_poss(os.getcwd())
    print("Done!")
