#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess
import numpy as np

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def det3(mat):
  return ((mat[0][0]*mat[1][1]*mat[2][2]) + (mat[0][1]*mat[1][2]*mat[2][0]) + (mat[0][2]*mat[1][0]*mat[2][1]) - (mat[0][2]*mat[1][1]*mat[2][0]) - (mat[0][1]*mat[1][0]*mat[2][2]) - (mat[0][0]*mat[1][2]*mat[2][1]))

def frac2cart(cellParam, fracCoords):
  cartCoords = []
  for i in fracCoords:
    xPos = i[0]*cellParam[0][0] + i[1]*cellParam[1][0] + i[2]*cellParam[2][0]
    yPos = i[0]*cellParam[0][1] + i[1]*cellParam[1][1] + i[2]*cellParam[2][1]
    zPos = i[0]*cellParam[0][2] + i[1]*cellParam[1][2] + i[2]*cellParam[2][2]
    cartCoords.append([xPos, yPos, zPos])
  return cartCoords

def cart2frac(cellParam, cartCoords):
  latCnt = [x[:] for x in [[None]*3]*3]
  for a in range(3):
    for b in range(3):
          latCnt[a][b] = cellParam[b][a]

  fracCoords = []
  detLatCnt = det3(latCnt)
  for i in cartCoords:
    aPos = (det3([[i[0], latCnt[0][1], latCnt[0][2]], [i[1], latCnt[1][1], latCnt[1][2]], [i[2], latCnt[2][1], latCnt[2][2]]])) / detLatCnt
    bPos = (det3([[latCnt[0][0], i[0], latCnt[0][2]], [latCnt[1][0], i[1], latCnt[1][2]], [latCnt[2][0], i[2], latCnt[2][2]]])) / detLatCnt
    cPos = (det3([[latCnt[0][0], latCnt[0][1], i[0]], [latCnt[1][0], latCnt[1][1], i[1]], [latCnt[2][0], latCnt[2][1], i[2]]])) / detLatCnt
    fracCoords.append([aPos, bPos, cPos])
  return fracCoords

def readpos(path_pos):
    f = open(path_pos)
    poslines = f.readlines()
    f.close()

    atomlabel = conv_line(poslines[5])
    atomnum = conv_line(poslines[6])
    sumatomnum = 0
    for num in atomnum:
        sumatomnum += int(num)

    # make lattice vector matrix
    mat_lat = []
    i = 2
    while i < 5:
        mat_lat.append(list(map(float, conv_line(poslines[i]))))
        i += 1

    # get flag of "Selective dynamics" "Direct" and "Cartesian"
    flag_S = 0
    flag_D = 0
    flag_C = 0
    line_dc = ""
    
    if "S" in conv_line(poslines[7])[0]:
        flag_S = 1
        if "D" in conv_line(poslines[8])[0]:
            flag_D = 1
            line_dc = "Direct"
        elif "C" in conv_line(poslines[8])[0]:
            flag_C = 1
            line_dc = "Cartesian"
    else:
        if "D" in conv_line(poslines[7])[0]:
            flag_D = 1
            line_dc = "Direct"
        elif "C" in conv_line(poslines[7])[0]:
            flag_C = 1
            line_dc= "Cartesian"

    if flag_S == 1:
        num_f0 = 8
        num1 = 9
    elif flag_S == 0:
        num_f0 = 7
        num1 = 8

    # get first lines of POSCAR
    num0 = 0
    firstposlines = []
    while num0 < num_f0:
        firstposlines.append(poslines[num0].replace('\n',''))
        num0 += 1
    #firstposlines.append(line_dc)

    # make atom position matrix
    Coordinate_atom = []
    Coordinate_lines = []
    list_SD = []
    while num1 < len(poslines):
        line = conv_line(poslines[num1])
        Coordinate_lines.append(line)
        Coordinate_atom.append(list(map(float, np.array([line[0], line[1], line[2]]))))
        if flag_S == 1:
            list_SD.append( [line[3], line[4], line[5]])
        num1 += 1
        
    if flag_C == 1:
        Coordinate_atom_frac = cart2frac(mat_lat, Coordinate_atom)
        Coordinate_atom_cart = Coordinate_atom

    elif flag_D == 1:
        Coordinate_atom_frac = Coordinate_atom
        Coordinate_atom_cart = frac2cart(mat_lat, Coordinate_atom)

    return [mat_lat, Coordinate_atom_frac, Coordinate_atom_cart, Coordinate_lines, list_SD, atomlabel, atomnum, firstposlines]

Q_file = input("Please input the file name the cartesian of model is written, e.g., Cu10.vasp: ")
# Q_file = "Cu10.vasp"
Q_basis  =input("Please input the number of the basis coordinate of model, e.g., 1: ")
# Q_basis = 0
path = os.getcwd()
poscars = [x for x in os.listdir(path) if "POSCAR" in x]
#dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) == True]

path_model = path + "/" + str(Q_file)
Coor_model_cart = readpos(path_model)[2]
Coor_model_cart_diff = np.array(Coor_model_cart) - np.array(Coor_model_cart[int(Q_basis)])
atomlabel_model = readpos(path_model)[5]
atomnum_model = readpos(path_model)[6]

path_dir_model = path + "/models_" + str(Q_file.replace(".vasp",""))
if os.path.exists(path_dir_model) == False:
    os.mkdir(path_dir_model)

path_dir_pos = path + "/models"
if os.path.exists(path_dir_pos) == False:
    os.mkdir(path_dir_pos)
                                      
i = 0
name_ad = ""
while i < len(atomlabel_model):
    name_ad += str(atomlabel_model[i]) + str(atomnum_model[i]) 
    i += 1

for pos in poscars:
    path_p = path + "/" + pos
    mat_lat_pos = readpos(path_p)[0]
    Coor_pos_frac = readpos(path_p)[1]
    Coor_pos_cart = readpos(path_p)[2]

    atomlabel = readpos(path_p)[5]
    atomnum = readpos(path_p)[6]
    num_l = 0
    while num_l < len(readpos(path_model)[5]):
        atomlabel.pop(-1)
        atomnum.pop(-1)
        num_l += 1

    atomlabel = atomlabel + readpos(path_model)[5]
    atomnum = atomnum + readpos(path_model)[6]
    sdtag = readpos(path_p)[4]

    Coor_model_frac = cart2frac(mat_lat_pos, Coor_model_cart_diff)
    Coor_model_add = np.array(Coor_model_frac) + np.array(Coor_pos_frac[-1])
    # Coor_model_add = np.array(Coor_model_cart) + np.array(Coor_pos_cart[-1])

    # make vasp file of models #
    modellines = mat_lat_pos + [readpos(path_model)[5]] + [readpos(path_model)[6]] + [["Direct"]] + Coor_model_add.tolist()
    # modellines = mat_lat_pos + [readpos(path_model)[5]] + [readpos(path_model)[6]] + [["Cartesian"]] + Coor_model_add.tolist()
    Lm = str(Q_file) + "\n1.0\n"
    for linelist in modellines:
        for val in linelist:
          Lm += str(val) + " "
        else:
            Lm += "\n"

    path_mfile = path_dir_model + "/" +  str(Q_file).replace(".vasp","") + "_based_" + pos.replace(".vasp","") + ".vasp"
    fm = open(path_mfile,"w")
    fm.write(Lm)
    fm.close()
    # End #

    # make vasp file of POSCAR + model #
    if len(readpos(path_p)[4]) > 0:
        Coor_pos_frac_sd = []
        # Coor_pos_cart_sd = []
        s = 0
        # for coor in Coor_pos_cart:
        for coor in Coor_pos_frac:
            Coor_pos_frac_sd.append(coor + sdtag[s])
            # Coor_pos_cart_sd.append(coor + sdtag[s])
            s += 1

        Coor_model_add_sd = []
        for coor in Coor_model_add:
            Coor_model_add_sd.append(coor.tolist() + ["T","T","T"])
            
        del Coor_pos_frac_sd[-1]
        print(len(Coor_pos_frac_sd))
        poslines_new = mat_lat_pos + [atomlabel] + [atomnum] + [["Selective dynamics"],["Direct"]] + Coor_pos_frac_sd + Coor_model_add_sd
        # poslines_new = mat_lat_pos + [atomlabel] + [atomnum] + [["Selective dynamics"],["Cartesian"]] + Coor_pos_cart_sd + Coor_model_add_sd

    else:
        del Coor_pos_frac[-1]
        poslines_new = mat_lat_pos + [atomlabel] + [atomnum] + [["Direct"]] + Coor_pos_frac + Coor_model_add.tolist()
        # poslines_new = mat_lat_pos + [atomlabel] + [atomnum] + [["Cartesian"]] + Coor_pos_cart + Coor_model_add.tolist()

    L = str(pos) + "\n1.0\n" 
    for linelist in poslines_new:
        for val in linelist:
          L += str(val) + " "   
        else:
            L += "\n"

    path_file = path + "/" + pos.replace(".vasp","") + "_ad-" + name_ad + ".vasp"
    f = open(path_file,"w")
    f.write(L)
    f.close()
    # End #

    shutil.move(path_p, path_dir_pos)

print("Done!")


