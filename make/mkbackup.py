#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/08/12 version 0.0
#2015/11/25 version 0.1
#This script make backup directory. #usage:script

import os, sys, glob, shutil
import subprocess as sub
import others.contpos as cp

backup_files = ["INCAR","OUTCAR","OSZICAR","XDATCAR","CONTCAR","std.out","stdout","neb.dat","force.dat","date.txt","DIMCAR","CENTCAR","MODECAR","NEWMODECAR"]

def mk_backup(nowpath):
    #make directory
    num = 1
    while True:
        if num in range(1,10): path_backup = nowpath + "/backup0" + str(num)
        else: path_backup = nowpath + "/backup" + str(num)

        if os.path.exists(path_backup): num += 1
        else: break


    os.mkdir(path_backup)
    files = [x for x in os.listdir(nowpath) if os.path.isfile(nowpath + "/" + x)]
#    print(files)
    for file in files:
        if file in backup_files: shutil.copy(file, path_backup)

    print("\n" + "\n".join([str(x) for x in os.listdir(path_backup)]) + "\n")

    path_pos = nowpath + "/POSCAR"
    path_cent = nowpath + "/CENTCAR"
    path_mode = nowpath + "/MODECAR"
    path_newmode = nowpath + "/NEWMODECAR"

#    if os.path.exists(path_cent):
#        shutil.copy(path_cent, path_pos)
#        print("CENTCAR has changed to POSCAR!")
    cp.chg_contpos(nowpath)
    if os.path.exists(path_newmode):
        shutil.copy(path_newmode, path_mode)
        print("NEWMODECAR has changed to MODECAR!")
#    if not os.path.exists(path_cent):
#        cp.chg_contpos(nowpath)
    print("\nbackup directory has been made!\n")

if __name__ == "__main__": mk_backup(os.getcwd())
# END: Program
