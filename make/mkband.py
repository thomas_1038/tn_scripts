#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015.08.20 ver1.001
#This script make BAND_X directory.
#usage

import os
import shutil
import sys

argv = sys.argv

path = os.getcwd()
path_band = path + "/band"
INCAR = path + "/INCAR"
KPOINTS = path + "/KPOINTS"
CONTCAR = path + "/CONTCAR"
POTCAR = path + "/POTCAR"
CHGCAR = path + "/CHGCAR"

# Make directory
flag_band = os.path.exists(path_band)
if flag_band == False:
    os.mkdir(path_band)

# Pick up .sh
os.system("eodel.py")
files = os.listdir('./')
shfiles = [x for x in files if ".sh" in x]
shfile = str(shfiles[0])

# Make INCAR in BAND
incar = open(INCAR)
incarlines = incar.readlines()
incar.close()

num = 0
while num < len(incarlines):
    flag_ICHARG = incarlines[num].find("ICHARG =")
    flag_LORBIT = incarlines[num].find("LORBIT =")
    flag_LWAVE = incarlines[num].find("LWAVE =")
    flag_LCHRAG = incarlines[num].find("LCHRAG =")
    flag_NSW = incarlines[num].find("NSW =")
    flag_IBRION = incarlines[num].find("IBRION =")
    flag_NELMIN = incarlines[num].find("NELMIN =")
    flag_NPAR = incarlines[num].find("NPAR =")
    flag_RWIGS = incarlines[num].find("RWIGS =")

    if flag_ICHARG >= 0:
        value = "ICHARG = 11 \n"
        incarlines[num] = value
    elif flag_LORBIT >= 0:
        value= "LORBIT = 11 \n"
        incarlines[num] = value
    elif flag_LWAVE >= 0:
        value= "LWAVE = TRUE \n"
        incarlines[num] = value
    elif flag_LCHRAG >= 0:
        value= "LCHRAG = TRUE \n"
        incarlines[num] = value
    elif flag_NSW >= 0:
        value = "NSW = 0 \n"
        incarlines[num] = value
    elif flag_IBRION >= 0:
        value= "IBRION = -1 \n"
        incarlines[num] = value
    elif flag_NELMIN >= 0:
        value= "NELMIN = 0 \n"
        incarlines[num] = value
    elif flag_LORBIT >= 0:
        value= "LORBIT = 11 \n"
        incarlines[num] = value
    elif flag_IBRION >= 0:
        value= "IBRION = -1 \n"
        incarlines[num] = value
    #elif flag_NPAR >= 0:
    #    value= "NPAR = 0 \n"
    #    incarlines[num] = value
    num += 1

# Move files to BAND
shutil.copy(CONTCAR, path_band)
shutil.copy(POTCAR, path_band)
shutil.copy(KPOINTS, path_band)
shutil.copy(CHGCAR, path_band)
shutil.copy(shfile, path_band)

# Move to BAND
os.chdir(path_band)
CONTCAR_band = path_band + "/CONTCAR"
POSCAR_band = path_band + "/POSCAR"
INCAR_band = path_band + "/INCAR"

os.rename(CONTCAR_band, POSCAR_band)
incar_b = open(INCAR_band, "w")
for line in incarlines:
    incar_b.write(str(line))

print("\n")
print("#######################################")
print("#         BAND has beend made!        #")
print("#######################################")
print("\n")

#END
