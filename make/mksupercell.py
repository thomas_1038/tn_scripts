#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script

import os
import os.path
import shutil

Cellsize = input("Please input supercell size. For example '3 3 1': ")
L = 'phonopy -d --dim="' + Cellsize + '" -c POSCAR'
os.system(L)
