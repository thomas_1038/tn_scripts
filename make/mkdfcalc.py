#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import sys
import os
import shutil

path = os.getcwd()
path_calc = path + "/calc"
flag_calc = os.path.exists(path_calc)
if flag_calc == False:
    os.mkdir(path_calc)
os.chdir(path_calc)

path_pos = path + "/POSCAR"
flag_pos = os.path.exists(path_pos)
if flag_pos == False:
    print("\n")
    print("################################################")
    print("#          POSCAR does not exist!              #")
    print("################################################")
    print("\n")

else:
    shutil.move(path_pos, path_calc)

# Make KPOINTS
kp = input("Please input kpoints. For example '3 3 1': ")
mkkp = str("mkkp.py " + kp)
os.system(mkkp)

# Make POTCAR
pot = input("Please input kpoints. For example 'Ca_sv N H': ")
mkpot = str("mkpot.py " + pot + " 4")
print(mkpot)
os.system(mkpot)

# Make dovasp
mksh = "mkdovasp2.py --d" 
os.system(mksh)

# Make INCAR
os.system("mkincar.py")

print("\n Done!")
