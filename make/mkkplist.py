#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, math, shutil

def mk_kplist(nowpath, surf_mode="F", thr=0.34, lm_dense_kp=1.8, num_inc=0.1, fm=int(2000)):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0: line.remove("")
        return line

    # read POSCAR
    POSCAR = nowpath + "/POSCAR"
    if not os.path.exists(POSCAR): print("You should prepare POSCAR file here! BYE!"); sys.exit()
    with open(POSCAR) as f: poslines= f.readlines()

    # caculate the lattice vector of POSCAR
    line_pa  = conv_line(poslines[2]); line_pb  = conv_line(poslines[3]); line_pc  = conv_line(poslines[4])
    lc_a_pos = math.sqrt(float(line_pa[0])**2 + float(line_pa[1])**2 + float(line_pa[2])**2)
    lc_b_pos = math.sqrt(float(line_pb[0])**2 + float(line_pb[1])**2 + float(line_pb[2])**2)
    lc_c_pos = math.sqrt(float(line_pc[0])**2 + float(line_pc[1])**2 + float(line_pc[2])**2)
    #list_lc = []
    #list_lc.append(lc_a_pos); list_lc.append(lc_b_pos); list_lc.append(lc_c_pos)

    b_a = float(1/lc_a_pos); b_b = float(1/lc_b_pos); b_c = float(1/lc_c_pos)

    list_tot = []; m = 1
    while m < fm:
        list_mb = []; list_rb = []; list_lk = []; list = []

        mb_a = float(float(m)*b_a)
        round_mb_a = round(mb_a)
        diff_rmb_mb_a = abs(float(round_mb_a - mb_a))

        mb_b = float(float(m)*b_b)
        round_mb_b = round(mb_b)
        diff_rmb_mb_b = abs(float(round_mb_b - mb_b))

        mb_c = float(float(m)*b_c)
        round_mb_c = round(mb_c)
        diff_rmb_mb_c = abs(float(round_mb_c - mb_c))

        if surf_mode == "F":
            if diff_rmb_mb_a <= thr and diff_rmb_mb_b <= thr and diff_rmb_mb_c <= thr:
                list_rb.append(int(round_mb_a)); list_rb.append(int(round_mb_b)); list_rb.append(int(round_mb_c))
                list_mb.append(mb_a); list_mb.append(mb_b); list_mb.append(mb_c)

                if round_mb_a > 0 and round_mb_b > 0 and round_mb_c > 0:
                    #lk_a = b_a/float(round_mb_a)*100
                    #lk_b = b_b/float(round_mb_a)*100
                    #lk_c = b_c/float(round_mb_a)*100
                    kpda = round(lc_a_pos * int(round_mb_a))
                    kpdb = round(lc_b_pos * int(round_mb_b))
                    kpdc = round(lc_c_pos * int(round_mb_c))

                    if kpda >= 50 and kpdb >= 50 and kpdc >= 50 : break

                    list_lk.append(kpda)
                    list_lk.append(kpdb)
                    list_lk.append(kpdc)

                    list.append(list_mb); list.append(list_rb); list.append(list_lk)
                    list_tot.append(list)
                    m += num_inc

                else: m += num_inc; continue

            m += num_inc

        elif surf_mode == "T":
            if diff_rmb_mb_a <= thr and diff_rmb_mb_b <= thr:
                list_rb.append(int(round_mb_a)); list_rb.append(int(round_mb_b)); list_rb.append(1)
                list_mb.append(mb_a); list_mb.append(mb_b); list_mb.append(mb_c)

                if round_mb_a > 0 and round_mb_b > 0:
                    #lk_a = b_a/float(round_mb_a)*100
                    #lk_b = b_b/float(round_mb_a)*100
                    #lk_c = b_c/float(round_mb_a)*100
                    kpda = round(lc_a_pos * int(round_mb_a))
                    kpdb = round(lc_b_pos * int(round_mb_b))

                    #if lk_a < lm_dense_kp and lk_b < lm_dense_kp: break
                    if kpda >= 50 and kpdb >= 50 : break

                    list_lk.append(kpda)
                    list_lk.append(kpdb)
                    list_lk.append(1)

                    list.append(list_mb)
                    list.append(list_rb)
                    list.append(list_lk)
                    list_tot.append(list)
                    m += num_inc

                else: m += num_inc; continue

            m += num_inc

    num = 1; kplist = []
    Ltot = "kx ky kz lkx lky lkz [/A] | threshold: " + str(thr) + "\n"
    while num < len(list_tot):
        sp = "  "
        ka = str(list_tot[num][1][0])
        kb = str(list_tot[num][1][1])
        kc = str(list_tot[num][1][2])
        ka_bf = str(list_tot[num-1][1][0])
        kb_bf = str(list_tot[num-1][1][1])
        kc_bf = str(list_tot[num-1][1][2])
        lka = str(list_tot[num][2][0])
        lkb = str(list_tot[num][2][1])
        lkc = str(list_tot[num][2][2])

        Lk = ka + sp + kb + sp + kc
        Lk_bf = ka_bf + sp+ kb_bf + sp + kc_bf
        Llk = lka + sp + lkb + sp + lkc
        kp = [int(x) for x in [ka, kb, kc]]
        lk = [int(x) for x in [lka, lkb, lkc]]

        if ka == "0" or kb == "0" or kc == "0" or Lk == Lk_bf:
            pass
        else:
            Ltot += Lk + sp + Llk + "\n"
            kplist.append([kp, lk])
        num += 1

    with open(nowpath + "/kplist", 'w') as f: f.write(Ltot)
    return Ltot, kplist

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 1:
        L = mk_kplist(os.getcwd())[0]

    else:
        if "--h" in argv or "-help" in argv:
            print("\n")
            print("--dk: limit of dense of k-points. default value is 1.9.")
            print("--ni: the loop number. default value is 1.")
            print("--s: surf mode. default is F.")
            print("\n")
            sys.exit()

        if "--s" in argv: surf_mode = "T"
        else: surf_mode = "F"

        if "--thr" in argv: thr = float(argv[int(argv.index("--thr")) +1])
        else: thr = 0.34

        if "--dk" in argv: lm_dense_kp = float(argv[int(argv.index("--dk")) +1])
        else: lm_dense_kp = 1.8

        if "--ni" in argv: num_inc = float(argv[int(argv.index("--ni")) +1])
        else: num_inc = 0.1

        L = mk_kplist(os.getcwd(), surf_mode, thr, lm_dense_kp, num_inc)[0]

    print(L)
# END