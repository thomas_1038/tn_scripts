#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#make IR INCAR file

import os
import os.path
import shutil
import sys

flug = os.path.exists("INCAR")
if flug == True:
       	print("INCAR exists.")
else:
	print("INCAR doesnt exisits.")
	sys.exit()

#irspectra
ir0 = "\n" + "!IRspectra" + "\n"
ir1 = "ICHARG = 1" + "\n"
ir2 = "ISTART = 1" + "\n"
ir3 = "IBRION = 7" + "\n"
ir4 = "EDIFF = 1e-8" +  "\n"
ir5 = "LEPSILON = .True." + "\n"
ir6 = "LREAL = .False." + "\n"
ir7 = "ISYM = 0" + "\n"
ir8 = "NSW = 1" + "\n"
ir9 = "NWRITE =3" + "\n"
ir10 = "NELM = 120" + "\n"
ir11 = "NELMIN = 10" + "\n"
ir = [ir0,ir1,ir2,ir3,ir4,ir5,ir6,ir7,ir8,ir9,ir10,ir11]

incarlines = ir

incar = open("INCAR","a+")

for line in incarlines:
	incar.write(str(line))
