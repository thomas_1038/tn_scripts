#! /usr/bin/env python3 
# -*- coding: utf-8 -*-
#make STOPCAR file
#2015/07/17 ver1.0

import os
import os.path
import shutil
import sys

argv = sys.argv

#file content                                                                                                
a1 = "!LSTOP:stop VASP calculation at the next ionic step." + "\n"
a2 = "LSTOP = .True."+"\n"
stoplines = [a1,a2]

#make STOPCAR in 0X directories
path = os.getcwd()

stoppath0 = path + "/STOPCAR"
stop0 = open(stoppath0,"w")
for line in stoplines:
    stop0.write(str(line))
print("STOPCAR has been made in current directory!")

i = 1
while i <= int(argv[1]):
    stoppath = path +"/0" + str(i) + "/STOPCAR"
    stop = open(stoppath,"w")
    for line in stoplines:
        stop.write(str(line))
    L = "STOPCAR has been made "+  "in 0" + str(i) +"!" 
    print(L)
    i += 1

print("STOPCAR has been made in all 0X directories!")
#End
