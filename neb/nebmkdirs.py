#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
argv = sys.argv; del argv[0]

path_tot = []
i = 0
for dr in argv:
    dr = dr.replace("/","")
    path_dir = path + "/"+ dr
    dirs_in_dir = sorted([x for x in os.listdir(path_dir) if os.path.isdir(path_dir+"/"+x) and len(x) == 2])
    if i == 0:
        for d in dirs_in_dir: path_tot.append(path_dir + "/" + d + "/POSCAR")
    else:
        del dirs_in_dir[0]
        for d in dirs_in_dir: path_tot.append(path_dir + "/" + d + "/POSCAR")
    i += 1

for i in range(len(path_tot)):
    if i in range(0,10): dirname = "0" + str(i)
    else: dirname = str(i)
    path_imgdir = path + "/" + dirname
    if os.path.exists(path_imgdir) == False: os.mkdir(path_imgdir)
    shutil.copy(path_tot[i], path_imgdir)

print("Done!")
