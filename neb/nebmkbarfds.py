#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
import numpy as np
import addmol.helper as hl
import addmol.mk_poscar as mp

def neb_mkbarfds(nowpath, nebbar="nebbarrier.pl"):
    def read_nebbar(path_bar):
        with open(path_bar) as fb: barlines = fb.readlines()
        barlines = np.array([hl.conv_line(l) for l in barlines]).T.tolist()
        barlines = [[float(x) for x in l] for l in barlines]
        return barlines

    name_all = input("Please input the name of \"all\" directory, e.g., all0: " )
    name_part = input("Please input the name of \"part\" directory, e.g., part1_0002_a0: " )
    ini_part = int(input("Please input the start directory in \"part\" directory, e.g, 4: "))
    fin_part = int(input("Please input the end directory in \"part\" directory, e.g, 8: "))
    img_part = [int(i) for i in range(ini_part, fin_part)]

    path_all = nowpath + "/" + name_all
    path_part = nowpath + "/" + name_part

    os.chdir(path_all); sub.call(nebbar, shell=True)
    os.chdir(path_part); sub.call(nebbar, shell=True); os.chdir(nowpath)

    nebbar_all = read_nebbar(path_all + "/neb.dat")
    nebbar_part = read_nebbar(path_part + "/neb.dat")

    img_nebbar_all = [int(i) for i in nebbar_all[0]]
    coor_nebbar_all = np.array(nebbar_all[1])
    en_nebbar_all = np.array(nebbar_all[2])
    img_nebbar_part = [int(i) for i in nebbar_part[0]]
    coor_nebbar_part = np.array(nebbar_part[1]) + float(nebbar_all[1][ini_part])
    en_nebbar_part = np.array(nebbar_part[2]) + float(nebbar_all[2][ini_part])

    coor_nebbar_all = coor_nebbar_all[:ini_part].tolist() + coor_nebbar_part.tolist() + coor_nebbar_all[fin_part+1:].tolist()
    en_nebbar_all = en_nebbar_all[:ini_part].tolist() + en_nebbar_part.tolist() + en_nebbar_all[fin_part+1:].tolist()

    img_all = [i for i in range(0,len(coor_nebbar_all))]
    coor_nebbar_all = ["{0:.5f}".format(x) for x in coor_nebbar_all]
    en_nebbar_all = ["{0:.5f}".format(x) for x in en_nebbar_all]

    newnebdat = [img_all, coor_nebbar_all, en_nebbar_all]
    list_for_output = np.array([hl.adjust_len([str(x) for x in l]) for l in newnebdat]).T.tolist()

    L = ""
    for l in list_for_output: L += " " + "  ".join(l) + "\n"
    with open(nowpath + "/neb_" + name_all + "-" + name_part + ".dat", "w") as fnn: fnn.write(L)

    path_imgdir_all = [path_all + "/" + "0" + str(i) if len(str(i)) == 1 else path_all + "/" + str(i) for i in img_nebbar_all]
    path_imgdir_part = [path_part + "/" + "0" + str(i) if len(str(i)) == 1 else path_part + "/" + str(i) for i in img_nebbar_part]
    path_imgdir_tot = path_imgdir_all[:ini_part] + path_imgdir_part + path_imgdir_all[fin_part+1:]

    # get images in all and part
    path_imgs_tot = nowpath + "/imgs_" + name_all + "-" + name_part
    if not os.path.exists(path_imgs_tot): os.mkdir(path_imgs_tot)

    for i, path_img in enumerate(path_imgdir_tot):
        print(path_img)
        path_cont = path_img + "/CONTCAR"
        path_pos = path_img + "/POSCAR"

        if os.path.exists(path_cont):
            with open(path_cont) as fc: contlines = fc.readlines()
            if len(contlines) != 0: path_pc = path_pos
        elif os.path.exists(path_pos): path_pc = path_pos
        else: print("\nYou should prepare POSCAR/CONTCAR in " + path_img + "! BYE!\n"); sys.exit()

        if i in [n for n in range(10)]: name = "0" + str(i) + ".vasp"
        else: name = str(i) + ".vasp"
        shutil.copy(path_pc, path_imgs_tot + "/" + name)

    mp.mk_poss(path_imgs_tot)
    return L

if __name__ == "__main__": print("\n" + neb_mkbarfds(os.getcwd()))
# END: Program