#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, re
import subprocess as sub

def neb_calcbader(nowpath, Qall, Qcalc):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    mkpot = "mkpot.py"
    calcbader_dir = "calcbader_direc.py"
    calcbader_all = "~/script/vtstscripts-892/bader CHGCAR && clnbader.py && calcbader_direc.py"
    calcallbader = "chgsum.pl AECCAR0 AECCAR2 && ~/script/vtstscripts-892/bader CHGCAR -ref CHGCAR_sum && clnbader.py && calcbader_direc.py"

    def calcbader_eachimgs(q_atom, Ltot):
        dirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x) and re.match(r"\d+",x) and len(x) == 2])
        L = ""
        for d in dirs:
            print("\n" + d)
            path_dir = nowpath + "/" + d
            path_pot = path_dir + "/POTCAR"

            #if os.path.exists(path_dir + "/bader/BADERCHG"):
                #os.chdir(path_dir); sub.call(calcbader_dir, shell=True)

            if not os.path.exists(path_dir + "/bader/BADERCHG"):
                if not os.path.exists(path_pot):
                    os.chdir(path_dir); sub.call(mkpot, shell=True)

                if "y" in Qall:
                    if not os.path.exists(path_dir + "/AECCAR2"): print("You should prepare AECCAR2 in " + d + "!\n"); sys.exit()
                    os.chdir(path_dir); sub.call(calcallbader, shell=True)

                elif "y" not in Qall and "y" in Qcalc:
                    if not os.path.exists(path_dir + "/CHGCAR"): print("You should prepare CHGCAR in " + d + "!\n"); sys.exit()
                    os.chdir(path_dir); sub.call(calcbader_all, shell=True)

            with open(path_dir+"/bader/BADERCHG") as fb: baderlines = fb.readlines()
            chg = [x.replace("\n","") for x in baderlines if q_atom == conv_line(x)[0]][-1]
            L += str(d) + "  ---   " + chg + "\n"

        with open(nowpath + "/bader_tot_" + q_atom + ".dat", "w") as fbt: fbt.write(L)
        Ltot += "\n" + L + "\n"
        return Ltot

    Qatom = input("Please input the label of target atom, e.g., N1 N2 H1 H2 H3 H4 H5 H6: ")
    list_Qatoms = Qatom.split(" ")

    Ltot = ""
    for q in list_Qatoms:
        print("\n" + q); Ltot = calcbader_eachimgs(q, Ltot)

    return Ltot

if __name__ == "__main__":
    Qall = input("Do you want to calculate all-bader charges in each directory or not? [y/N]: ")
    Qcalc = input("Do you want to calculate bader charges in each directory or not? [y/N]: ")
    print("\n" + neb_calcbader(os.getcwd(), Qall, Qcalc) + "\nDone!\n")
# End: Program
