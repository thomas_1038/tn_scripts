#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, re #, sys, glob, shutil, threading
#import subprocess
import numpy as np
import addmol.readpos as rp
from addmol import Frac2Cart
f2c = Frac2Cart()

range_lp = 1.3
tolerance_dis = 0

path = os.getcwd()
vaspfiles = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "vasp" in x]
dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and re.match(r"\d+",x) and len(x) == 2])
Qlab = input("Please input label of atom you want to check, i.e., N1: ")
N_list = input("Please input the number of atoms you want: ")

dis_tot = []
label_tot = []
for d in dirs:
    path_dir = path + "/" + d
    if d == "00" or "0" + str(len(dirs)-1): path_pos = path_dir + "/POSCAR"
    else: path_pos = path_dir + "/CONTCAR"
    with open(path_pos) as fp: contlines = fp.readlines()

    labelofel = rp.get_labelofel(contlines)
    numofel = rp.get_numofel(contlines)
    sumatomnum = rp.get_sumofnumofel(contlines)
    list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel,numofel)

    firstlines, flagS = rp.get_firstlines(contlines) #POSCARの始め数行(Direct/Cartesianまで)
    mat_lat = rp.get_matrix(contlines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
    arr_coor_car = np.array(coor_car)
    arr_coor_dir = np.array(coor_dir)
    list_num_SD = rp.mk_listnumSD(list_SD)

    A = np.mgrid[-1:1.1, -1:1.1, -1:1.1].reshape(3, -1).T
    coor_3x3x3_dir = []; coor_3x3x3_dir_ap = coor_3x3x3_dir.append
    for vec in A:
        coor_3x3_dir = arr_coor_dir + vec
        for p in coor_3x3_dir: coor_3x3x3_dir_ap(p)

    arr_coor_3x3x3_dir = np.array(coor_3x3x3_dir)
    coor_3x3x3_dir = arr_coor_3x3x3_dir.tolist()
    arr_coor_3x3x3_car = np.array(f2c.frac2cart(mat_lat, coor_3x3x3_dir))
    coor_3x3x3_car = arr_coor_3x3x3_car.tolist()

    pos_basis = coor_car[list_labelofel.index("!"+Qlab)]
    distance = [np.linalg.norm(pos_basis - p) for p in arr_coor_3x3x3_car]

    index_dis_sort = np.array(distance).argsort()
    dis_sort = sorted(distance)
    dis_sort_N = []; dis_sort_N_ap = dis_sort_N.append
    index_dis_sort_N = []; index_dis_sort_N_ap = index_dis_sort_N.append
    label_dis_sort_N = []; label_dis_sort_N_ap = label_dis_sort_N.append

    i = 0
    for d in dis_sort:
        if d > tolerance_dis and len(dis_sort_N) < int(N_list):
            dis_sort_N_ap(d)
            ind = np.mod(index_dis_sort[i],int(sumatomnum))
            label_dis_sort_N_ap(list_labelofel[ind].replace("!",""))
        elif len(dis_sort_N) > int(N_list): break
        i += 1

    dis_tot.append(dis_sort_N); label_tot.append(label_dis_sort_N)

i1 = 0; L = ""
while i1 < len(dirs):
    """
    if i1 == 0:
        L += Qlab + " "
        for a in range(0, len(label_tot[i1])): L += "atom" + str(a+1) + " " + "d_" + Qlab + "-a" + str(a+1) + " "
        else: L += "\n"

    if len(str(i1)) == 1: L += "0" + str(i1) + " "
    else: L += str(i1) + " "
    """

    i2 = 0
    for i2 in range(0, len(label_tot[i1])): 
        L += str(label_tot[i1][i2]) + " " + str("%04.4f" % dis_tot[i1][i2]) + "\n"
    else: L += "\n"
    i1 += 1

with  open(path+"/dis_"+Qlab+".dat","w") as f: f.write(L)
print("\n" + L + "Done!")
# END: Program