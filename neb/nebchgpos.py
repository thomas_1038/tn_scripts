#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys #, sys, shutil, threading, glob, re
import numpy as np
from addmol import calc
from addmol import readpos as rp
from addmol.f2cc2f import Frac2Cart
from addmol.screening import Screening
f2c = Frac2Cart(); scr = Screening()

tolerance_dis = 0
N_list = 7

list_el = ["Ca", "Al"]
dict_el = {"Ca":1.97, "Al":1.43}

def nebchg_pos_forsphere(filename, Ltot, Qlab, Qmol, disAtS):
    def mk_listtot(coor, mat_lat, list_SD, list_labelofel, flagS, ind):
        if ind == 0: arr_coor = np.array(f2c.cart2frac(mat_lat, coor))
        elif ind == 1: arr_coor = np.array(coor)
        if flagS == 1:
            list_tot = np.concatenate([arr_coor, np.array(list_SD)], axis=1)
            list_tot = np.concatenate([list_tot, np.array([[str(x)] for x in list_labelofel])], axis=1)
        else: list_tot = np.concatenate([arr_coor, np.array([[str(x)] for x in list_labelofel])], axis=1)
        return list_tot

    path = os.getcwd()
    list_mol = Qmol.split(" ")

    # read POSCAR
    path_pos = path + "/" + filename
    with open(path_pos) as fp: poslines = fp.readlines()
    labelofel = rp.get_labelofel(poslines); numofel = rp.get_numofel(poslines); sumatomnum = rp.get_sumofnumofel(poslines)
    list_labelofel = rp.mk_labelofel(labelofel, numofel)[0]
    firstlines, flagS = rp.get_firstlines(poslines)
    mat_lat = rp.get_matrix(poslines)
    coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumatomnum, mat_lat)
    arr_coor_car = np.array(coor_car) #arr_coor_dir = np.array(coor_dir)

    if "D" in firstlines[-1]: flagFC = 0
    else: flagFC = 1

    # make 3x3x3
    coor_3x3x3_dir = []
    for vec in np.mgrid[-1:1.1, -1:1.1, -1:1.1].reshape(3, -1).T:
        coor_3x3_dir = coor_dir + vec
        for p in coor_3x3_dir: coor_3x3x3_dir.append(p)

    coor_3x3x3_car = f2c.frac2cart(mat_lat, coor_3x3x3_dir)
    arr_coor_3x3x3_car = np.array(coor_3x3x3_car) #arr_coor_3x3x3_dir = np.array(coor_3x3x3_dir)

    indexs_lab_in = []
    for i, x in enumerate(list_labelofel):
        if "!" + Qlab in x: indexs_lab_in.append(i)
    index_lab_in = indexs_lab_in[-1]

    print(index_lab_in)
    pos = coor_car[index_lab_in]
    distance = [np.linalg.norm(pos - p) for p in arr_coor_3x3x3_car]
    index_dis_sort = np.array(distance).argsort(); dis_sort = sorted(distance)
    dis_sort_N = []; label_dis_sort_N = []

    i = 0
    for d in dis_sort:
        if d > tolerance_dis and len(dis_sort_N) < N_list:
            dis_sort_N.append(d)
            ind = np.mod(index_dis_sort[i],int(sumatomnum))
            label_dis_sort_N.append(list_labelofel[ind].replace("!",""))
        elif len(dis_sort_N) > N_list: break
        i += 1

    i = 0
    for m in list_mol:
        if m in label_dis_sort_N:
            del dis_sort_N[label_dis_sort_N.index(m)]
            del label_dis_sort_N[label_dis_sort_N.index(m)]

    if disAtS > dis_sort_N[0]:
        coor_adatom = coor_car[index_lab_in]
        coor_sup = coor_car[list_labelofel.index("!"+label_dis_sort_N[0])]
        vec_adtosup = np.array(coor_adatom) - np.array(coor_sup)
        len_vec_adtosup = np.linalg.norm(vec_adtosup)
        unit_vec_adtosup = calc.uni_vec(vec_adtosup)
        vec_add = (disAtS - len_vec_adtosup) * unit_vec_adtosup

        for m in list_mol:
            coor_new = arr_coor_car[list_labelofel.index("!"+m)] + vec_add
            coor_car[list_labelofel.index("!"+m)] = coor_new.tolist()
        coor_dir = f2c.cart2frac(mat_lat, coor_car)

    if flagFC == 0: list_tot = mk_listtot(coor_dir, mat_lat, list_SD, list_labelofel, flagS, 1)
    elif flagFC == 1: list_tot = mk_listtot(coor_car, mat_lat, list_SD, list_labelofel, flagS, 1)

    L_forpos = "\n".join(firstlines) + "\n"
    for line in list_tot: L_forpos += " ".join(line) + "\n"
    Ltot += L_forpos

    if disAtS > dis_sort_N[0]:
        with open(path_pos, "w") as fp: fp.write(L_forpos)

    return Ltot

if __name__ == "__main__":
    filename = input("Please input the filename, [ p, POSCAR/  c, CONTCAR / s, SPOSCAR ] or the other name: ")
    if filename in ["p", "POSCAR"]:    filename = "POSCAR"
    elif filename in ["c", "CONTCAR"]: filename = "CONTCAR"
    elif filename in ["s", "SPOSCAR"]: filename = "SPOSCAR"
    else: filename = filename

    Qlab = input("Please input label of atom you want to move, i.e., N1: ")
    Qmol = input("Please input lable of atoms of molecule you want to move, i.e., N1 H1 H2 H3: ")
    Qdis = input("Do you use distance between adatom and suppport from dictionary  [y/N]: ")
    if Qdis in ["y", "Y"]:
        for el in list_el:
            if el in label_dis_sort_N[0]: key = el
        disAts = float(dict_el[key])
    elif Qdis in ["n", "N"]: disAtS = float(input("Please input the distance between adatom to support: "))

    nebchg_pos_forsphere(filename, "", Qlab, Qmol, disAtS)
    print("Done!")
# END: Program