#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make INCAR file for NEB

import os, sys, glob, shutil
import subprocess as sub

ENCUT = 400
#NPAR = 8

def nebmk_incar():
    path = os.getcwd()
    #argv = sys.argv

    if os.path.exists(path + "/INCAR"):
    	os.rename('INCAR','INCAR_pre')
    	print("Previous INCAR changed to INCAR_before and New INCAR has benn made!")
    else: print("New INCAR has been made!")

    #count 0X files
    #encut = input("Please input ENCUT value, i.e., 400: ")
    encut = ENCUT
    npar = input("Please input NPAR value, i.e., 4: ")
    nsim = input("Please input NSIM value, i.e., 12: ")

    num = len(glob.glob("[0-9][0-9]")) - 2

    #System
    d = "SYSTEM = neb" + "\n"
    d += "ISTART = 0 ; ICHARG = 2\n"
    d += "!ISTART = 1 ; ICHARG = 1\n"
    d += "LWAVE = T" + "\n"
    d += "LCHARG = T" + "\n\n"

    #algorithm
    al  = "!Algorithm" + "\n"
    al += "!ALGO and IALGO determines how the wavefunctions are optimized."+"\n"
    al += "!IALGO = 38,ALGO = N:defolt,Davidson block" + "\n"
    al += "!ALGO = Fast:Davidson is used for the first a few steps, and then VASP switches to RMM-DIIS." + "\n"
    al += "!IALGO = 48,ALGO = VeryFast:RMM-DIIS" + "\n"
    al += "ALGO = N" + "\n"
    al += "!IALGO = 48" + "\n\n"

    #accuracy
    a  = "!Accuracy" + "\n"
    a += "ENCUT = " + str(encut) + "\n"
    a += "!PPEC:High|Normal|Medium|Low" + "\n"
    a += "PREC = N" + "\n"
    a += "!LREAL:.Ture.|.False.|Auto|O" + "\n"
    a += "LREAL = A" + "\n\n"

    #NEB
    n  = "!NEB" + "\n"
    n += "!The spring constant, in eV/Ang^2 between the images; negative value turns on nudging." + "\n"
    n += "SPRING = -5" + "\n"
    n += "IMAGES = " + str(num) + "\n"
    n += "LscaLAPACK = F" + "\n"
    n += "LCLIMB = T" + "\n\n"

    #electronic relaxation
    e  = "!Electronic Relaxation" + "\n"
    e += "EDIFF = 1.0e-4" + "\n"
    e += "!ISPIN|1:non spin polarized|2:spin polarized" + "\n"
    e += "ISPIN = 2" +"\n"
    e += "!NELM:number of electronic step" +"\n"
    e += "NELM = 40" + "\n"
    e += "NELMIN = 4" + "\n"
    e += "!NELECT:number of electron" + "\n"
    e += "!NELECT =" + "\n"
    e += "!GGA|PB:Perdew-Becke|PW:Perdew-Wang 86|LM:Langreth-Mehl-Hu|" +"\n"
    e += "!91:Perdew-Wang 91|PE:Perdew-Bruke-Ernzehof|RP:revised PBE|" + "\n"
    e += "!AM:AM05|PS:PBE revised for solids|" + "\n"
    e += "!GGA = " + "\n\n"

    #ionic relaxation
    i  = "!ionic relaxation" + "\n"
    i += "EDIFFG = -2.0e-2" + "\n"
    i += "!IBRION|-1:Ion position fixed|0:MD|1:Quasi-Newton|" +"\n"
    i += "!3:Damped MD,use dumping factor(SMASS,POTIM)|" + "\n"
    i += "!When NEB calculation,You should use IBRION = 1 or 3" + "\n"
    i += "!If you use IOPT-tag,You should use IBRION = 3 & POTIM = 0" + "\n"
    i += "IBRION = 1" + "\n"
    i += "POTIM = 0.1" + "\n"
    i += "!NFREE:the number of vector s kept in the iterarion history." + "\n"
    i += "!complex model:10~20|simple model:the number of dgrees of freedom|"+ "\n"
    i += "NFREE = 2" + "\n"
    i += "!SMASS|-3:micro canonical|-2:initial velocity are kept const|"  + "\n"
    i += "!-1:continuous increase or decrease the kinetic energy|>=0:canonical|" + "\n"
    i += "!SMASS = 0.04" + "\n"
    i += "!ISIF|0:Ion relax only|2:Cell fixed|3:Cell & Volume relax|" + "\n"
    i += "ISIF = 2" + "\n"
    i += "NSW = 10" + "\n"
    i += "!ISYM|0:Break symmetry|1:Keep symmetry(USPP)|2:Keep symmetry(PAW)|" + "\n"
    i += "ISYM = 0" + "\n\n"

    #parallelisation
    pr  = "!parallelisation" + "\n"
    pr += "NPAR = " + str(npar) + "\n"
    pr += "KPAR = 1" + "\n"
    pr += "NSIM = " + str(nsim) + "\n"
    pr += "LPLANE = T" + "\n\n"

    #mixing parameter
    m  = "!mixing parameter" + "\n"
    m += "!AMIX = 0.2" + "\n"
    m += "!BMIX = 0.0001" + "\n"
    m += "!AMIX_MAG = 0.8" + "\n"
    m += "!BMIX_MAG = 0.0001" + "\n\n"

    #NEB
    nn  = "!NEB expation" + "\n"
    nn += "!IOPT|0:Use VASP optimizers specified from IBRION (default)|1:LBFGS|2:CG|3:QM|4:SD|7:FIRE|" + "\n"
    nn += "!IOPT = 0" + "\n\n"

    nn += "!LTANGENTOLD = .TRUE." + "\n"
    nn += "!LDNEB = .TRUE." + " !doubly NEB" + "\n"
    nn += "!LNEBCELL = .TRUE." + " !ss-NEB.Used ISIF=3 & IOPT=3." + "\n" + "\n"
    nn += "!LBFGS Parameters" + "\n"
    nn += "!MAXMOVE:Maximum allowed step size for translation" + "\n"
    nn += "!MAXMOVE = 0.2 " + "!defolt" +  "\n"
    nn += "!ILBFGSMEM:Number of steps saved when building the inverse Hessian matrix" + "\n"
    nn += "!ILBFGSMEM = 20 " + "!defolt" + "\n"
    nn += "!LGLOBAL:Optimize the NEB globally instead of image-by-image" + "\n"
    nn += "!LGLOBAL = .TRUE. " + "!defolt" + "\n"
    nn += "!LAUTOSCALE:Automatically determines INVCURV" + "\n"
    nn += "!LAUTOSCALE = .TRUE. " + "!defolt" + "\n"
    nn += "!INICURV:Initial inverse curvature, used to construct the inverse Hessian matrix" + "\n"
    nn += "!INVCURV = 0.01 "  + "!defolt"+ "\n"
    nn += "!LLINEOPT:Use a force based line minimizer for translation" + "\n"
    nn += "!LLINEOPT = .FALSE. " + "!defolt" + "\n"
    nn += "!FDSTEP:Finite difference step size for line optimizer" + "\n"
    nn += "!FDSTEP = 5e-3 " + "!defolt" + "\n" + "\n"

    nn += "!CG Parameters" + "\n"
    nn += "!MAXMOVE = 0.2 " + "!defolt" +  "\n"
    nn += "!FDSTEP = 5e-3 " + "!defolt" + "\n" + "\n"

    nn += "!QM Paremeters" + "\n"
    nn += "!MAXMOVE = 0.2 " + "!defolt" +  "\n"
    nn += "!TIMESTEP:Dynamical time step" + "\n"
    nn += "!TIMESTEP = 0.1 " + "!defolt" +  "\n" + "\n"

    nn += "!FIRE Paremeters" + "\n"
    nn += "!MAXMOVE = 0.2 " + "!defolt" +  "\n"
    nn += "!TIMESTEP = 0.1 " + "!defolt" +  "\n"
    nn += "!FTIMEMAX:Maximum dynamical time step allowed" + "\n"
    nn += "!FTIMEMAX = 1.0 " + "!defolt" +  "\n"
    nn += "!FTIMEDEC:Factor to decrease dt" + "\n"
    nn += "!FTIMEDEC = 0.5 " + "!defolt" +  "\n"
    nn += "!FTIMEINC:Factor to increase dt" +"\n"
    nn += "!FTIMEINC = 1.1 " + "!defolt" +  "\n"
    nn += "!FALPHA:Parameter that controls velocity damping" + "\n"
    nn += "!FALPHA = 0.1 " + "!defolt" +  "\n"
    nn += "!FNIN:Minimum number of iterations before adjusting alpha and dt" + "\n"
    nn += "!FNIN = 5 " + "!defolt" +  "\n\n"

    #DOS related values
    pd  = "!DOS related Values" + "\n"
    pd += "!RWIGS = " + "\n"
    pd += "ISMEAR = 1" + "\n"
    pd += "SIGMA = 0.05" + "\n\n"

    L = d + al + a + n + e + i + pr + m + pd + nn
    with open(path+"/INCAR","w") as f: f.write(L)

if __name__ == "__main__": nebmk_incar(); print("\n"); sub.call("grep -v ! INCAR",shell=True)
# End: Program
