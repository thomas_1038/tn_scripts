#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, glob, sys #, datetime #,sys
import numpy as np
import addmol.helper as hl
import get.getmaxforce as gm

def neb_getef(nowpath):
    imgdirs = sorted(glob.glob(nowpath+"/[0-9][0-9]"))

    list_Eeachimgs = []
    for i, imgdir in enumerate(imgdirs):
        #path_imgdir = nowpath + "/" + imgdir
        if i == 0 or i == len(imgdirs)-1: continue
        else:
            list_ef = gm.get_maxforce(imgdir)
            list_Eeachimgs.append(list_ef[1])

    min_nsw = min([len(l) for l in list_Eeachimgs])
    for i, l in enumerate(list_Eeachimgs):
        if min_nsw == len(l): continue
        elif len(l) > min_nsw: del l[-1]; list_Eeachimgs[i] = l

#    for l in list_Eeachimgs: print(len(l))
#    sys.exit()

    Eini_last = gm.get_maxforce(imgdirs[0])[1][-1]
    Efin_last = gm.get_maxforce(imgdirs[-1])[1][-1]
    list_Eeachimgs.insert(0,[Eini_last for i in list_Eeachimgs[0]])
    list_Eeachimgs.append([Efin_last for i in list_Eeachimgs[0]])

    list_Etot_eachstep = []; list_Etot_eachstep_append = list_Etot_eachstep.append
    for i, nsw in enumerate(list_Eeachimgs[0]):
        list_Etot_eachstep_append([str(float(E[i]) - float(Eini_last)) for E in list_Eeachimgs])

    list_for_output = [[str(i) for i in range(1, len(list_Eeachimgs[0])+1)]] + np.array(list_Etot_eachstep).T.tolist()
    list_for_output = [hl.adjust_len(l) for l in list_for_output]

    L = ""
    for i1 in range(0,len(list_for_output[0])):
        L += " " + " ".join([str(list_for_output[i2][i1]) for i2 in range(0,len(list_for_output))]) + "\n"

    current_dir = nowpath.split("/")[-2] + "_" + nowpath.split("/")[-1]
    #time = "{0:%Y%m%d%H%M}".format(datetime.datetime.today())
    with open(nowpath +"/nebEiter_" + current_dir + ".dat","w") as fn: fn.write(L)

    return list_Etot_eachstep, L

if __name__ == "__main__": print(neb_getef(os.getcwd())[1])
# END: script