#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/08/12 version 1.0
#2015/10/04 version 1.1
#This script makes POSCARfile POSCAR_ini or POSCAR_before and CONTCAR POSCAR in 0X directories.
#usage:script

import glob
import os

os.system('eoclean.py')

#count "0X" directories.
path = os.getcwd()
pathincar = path + "/INCAR"

incar = open(pathincar)
incarlines = incar.readlines()
incar.close()

for line in incarlines:
    if line.find("IMAGES") >= 0:
        line = line.replace('\n','')
        imagelist = line.split(' ')

#change CONTCAR to POSCAR in 0X.
num1 = 1
num2 = int(imagelist[2]) + 1

while num1 < num2:
    if num1 <= 9:
        path0X = path +"/0"+str(num1)
        os.chdir(path0X)
        L = "In " + "0" + str(num1)
        print(L)
        os.system("contpos.py")
        num1 += 1
    else:
        pathX = path + "/" + str(num1)
        os.chdir(pathX)
        L = "In " + str(num1)
        print(L)
        os.system("contpos.py")
        num1 += 1
print("Each CONCAR has been renamed to POSCAR!")
