#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make INCAR file for DIMMER MOTHEOD

import os, sys, glob, shutil
import subprocess as sub

#NPAR = 8
ENCUT = 400

def dimmk_incar():
    path = os.getcwd()
    #argv = sys.argv

    if os.path.exists(path + "/INCAR"):
    	os.rename('INCAR','INCAR_pre')
    	print("Previous INCAR changed to INCAR_before and New INCAR has benn made!")
    else: print("New INCAR has been made!")

    #count 0X files
#    encut = input("Please input ENCUT value, i.e., 400: ")
    encut = ENCUT
    Qediff = input("Please input EDIFF value, default: 1.0e-8: ")
    if Qediff == "d": ediff = "1.0e-8"
    else: ediff = Qediff
    Qnpar = input("Please input NPAR value, default: 3: ")
    if Qnpar == "d": npar = "3"
    else: npar = Qnpar
    Qnsim = input("Please input NSIM value, default: 12: ")
    if Qnsim == "d": nsim = "12"
    else: nsim = Qnsim
    num = len(glob.glob("[0-9][0-9]")) - 2

    #System
    d = "SYSTEM = dim" + "\n"
    d += "ICHARG = 2 ; ISTART = 0" + "\n"
    d += "!ICHARG = 1 ; ISTART = 1" + "\n"
    d += "LWAVE = T\n"
    d += "LCHARG = T\n\n"

    #algorithm
    al  = "!Algorithm" + "\n"
    al += "!ALGO and IALGO determines how the wavefunctions are optimized."+"\n"
    al += "!IALGO = 38,ALGO = N:defolt,Davidson block" + "\n"
    al += "!ALGO = Fast:Davidson is used for the first a few steps, and then VASP switches to RMM-DIIS." + "\n"
    al += "!IALGO = 48,ALGO = VeryFast:RMM-DIIS" + "\n"
    al += "ALGO = N" + "\n"
    al += "!IALGO = 48" + "\n\n"

    #accuracy
    a  = "!Accuracy" + "\n"
    a += "ENCUT = " + str(encut) + "\n"
    a += "!PPEC:High|Normal|Medium|Low" + "\n"
    a += "PREC = N" + "\n"
    a += "!LREAL:.Ture.|.False.|Auto|O" + "\n"
    a += "LREAL = A" + "\n\n"

    #NEB
#    n  = "!NEB" + "\n"
#    n += "!The spring constant, in eV/Ang^2 between the images; negative value turns on nudging." + "\n"
#    n += "SPRING = -5" + "\n"
#    n += "IMAGES = " + str(num) + "\n"
#    n += "LscaLAPACK = F" + "\n"
#    n += "LCLIMB = T" + "\n\n"

    # DIM
    dim = "ICHAIN = 2" + "\n"
    dim += "IOPT = 2" + "\n"
    dim += "DdR = 0.005" + "\n"
    dim += "DRotMax = 4" + "\n"
    dim += "DFNMin = 0.01" + "\n"
    dim += "DFNMax = 1.0" + "\n\n"

    # electronic relaxation
    e  = "!Electronic Relaxation" + "\n"
    e += "EDIFF = " + str(ediff) + "\n"
    e += "!ISPIN|1:non spin polarized|2:spin polarized" + "\n"
    e += "ISPIN = 2" +"\n"
    e += "!NELM:number of electronic step" +"\n"
    e += "NELM = 70" + "\n"
    e += "NELMIN = 4" + "\n"
    e += "!NELECT:number of electron" + "\n"
    e += "!NELECT =" + "\n"
    e += "!GGA|PB:Perdew-Becke|PW:Perdew-Wang 86|LM:Langreth-Mehl-Hu|" +"\n"
    e += "!91:Perdew-Wang 91|PE:Perdew-Bruke-Ernzehof|RP:revised PBE|" + "\n"
    e += "!AM:AM05|PS:PBE revised for solids|" + "\n"
    e += "!GGA = " + "\n\n"

    #ionic relaxation
    i  = "!ionic relaxation" + "\n"
    i += "EDIFFG = -1.0e-2" + "\n"
    i += "IBRION = 3" + "\n"
    i += "POTIM = 0.0" + "\n"
    i += "!ISIF|0:Ion relax only|2:Cell fixed|3:Cell & Volume relax|" + "\n"
    i += "ISIF = 2" + "\n"
    i += "NSW = 100" + "\n"
    i += "!ISYM|0:Break symmetry|1:Keep symmetry(USPP)|2:Keep symmetry(PAW)|" + "\n"
    i += "ISYM = 0" + "\n\n"

    #parallelisation
    pr  = "!parallelisation" + "\n"
    pr += "NPAR = " + str(npar) + "\n"
    pr += "KPAR = 1" + "\n"
    pr += "NSIM = " + str(nsim) + "\n"
    pr += "LPLANE = T" + "\n\n"

    #mixing parameter
    m  = "!mixing parameter" + "\n"
    m += "!AMIX = 0.2" + "\n"
    m += "!BMIX = 0.0001" + "\n"
    m += "!AMIX_MAG = 0.8" + "\n"
    m += "!BMIX_MAG = 0.0001" + "\n\n"

    #DOS related values
    pd  = "!DOS related Values" + "\n"
    pd += "!RWIGS = " + "\n"
    pd += "ISMEAR = 1" + "\n"
    pd += "SIGMA = 0.05" + "\n\n"

    L = d + al + a + dim + e + i + pr + m + pd 
    with open(path+"/INCAR","w") as f: f.write(L)

if __name__ == "__main__": dimmk_incar(); print("\n"); sub.call("grep -v ! INCAR",shell=True)
# End: Program
