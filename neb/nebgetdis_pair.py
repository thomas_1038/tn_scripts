#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, re, sys, glob, shutil, threading
import subprocess
import numpy as np
from addmol import readpos as rp

range_lp = 1.3
tolerance_dis = 0

def det3(mat):
    return ((mat[0][0]*mat[1][1]*mat[2][2]) + (mat[0][1]*mat[1][2]*mat[2][0]) + (mat[0][2]*mat[1][0]*mat[2][1]) - (mat[0][2]*mat[1][1]*mat[2][0]) - (mat[0][1]*mat[1][0]*mat[2][2]) - (mat[0][0]*mat[1][2]*mat[2][1]))

def frac2cart(cellParam, fracCoords):
    cartCoords = []
    for i in fracCoords:
        xPos = i[0]*cellParam[0][0] + i[1]*cellParam[1][0] + i[2]*cellParam[2][0]
        yPos = i[0]*cellParam[0][1] + i[1]*cellParam[1][1] + i[2]*cellParam[2][1]
        zPos = i[0]*cellParam[0][2] + i[1]*cellParam[1][2] + i[2]*cellParam[2][2]
        cartCoords.append([xPos, yPos, zPos])
    return cartCoords

def cart2frac(cellParam, cartCoords):
    latCnt = [x[:] for x in [[None]*3]*3]
    for a in range(3):
        for b in range(3):
            latCnt[a][b] = cellParam[b][a]

    fracCoords = []
    detLatCnt = det3(latCnt)
    for i in cartCoords:
        aPos = (det3([[i[0], latCnt[0][1], latCnt[0][2]], [i[1], latCnt[1][1], latCnt[1][2]], [i[2], latCnt[2][1], latCnt[2][2]]])) / detLatCnt
        bPos = (det3([[latCnt[0][0], i[0], latCnt[0][2]], [latCnt[1][0], i[1], latCnt[1][2]], [latCnt[2][0], i[2], latCnt[2][2]]])) / detLatCnt
        cPos = (det3([[latCnt[0][0], latCnt[0][1], i[0]], [latCnt[1][0], latCnt[1][1], i[1]], [latCnt[2][0], latCnt[2][1], i[2]]])) / detLatCnt
        fracCoords.append([aPos, bPos, cPos])
    return fracCoords

path = os.getcwd()
vaspfiles = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "vasp" in x]
dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and re.match(r"\d+",x) and len(x) == 2])
Qlab = input("Please input label of atom2 you want to check, i.e., N1 Ca39: ")
#N_list = raw_input("Please input the number of atoms you want: ")
Qlab1 = Qlab.split(" ")[0]
Qlab2 = Qlab.split(" ")[1]
print(Qlab1, Qlab2)

dis_tot = []
label_tot = []
for d in dirs:
    path_dir = path + "/" + d
    if d == "00" or "0" + str(len(dirs)-1): path_pos = path_dir + "/POSCAR"
    else: path_pos = path_dir + "/CONTCAR"
    with open(path_pos) as fp: contlines = fp.readlines()

    labelofel = rp.get_labelofel(contlines)
    numofel = rp.get_numofel(contlines)
    sumatomnum = rp.get_sumofnumofel(contlines)
    list_labelofel, list_labelofel2, dict_labelofel = rp.mk_labelofel(labelofel,numofel)

    firstlines, flagS = rp.get_firstlines(contlines) #POSCARの始め数行(Direct/Cartesianまで)
    mat_lat = rp.get_matrix(contlines) #格子ベクトル
    coor_car, coor_dir, list_SD = rp.get_coordinate(contlines, sumatomnum, mat_lat)
    arr_coor_car = np.array(coor_car)
    arr_coor_dir = np.array(coor_dir)
    list_num_SD = rp.mk_listnumSD(list_SD)

    A = np.mgrid[-1:1.1,-1:1.1,-1:1.1].reshape(3,-1).T
    coor_3x3x3_dir = []; coor_3x3x3_dir_ap = coor_3x3x3_dir.append

    for vec in A:
        coor_3x3_dir = arr_coor_dir + vec
        for p in coor_3x3_dir: coor_3x3x3_dir_ap(p)

    arr_coor_3x3x3_dir = np.array(coor_3x3x3_dir)
    coor_3x3x3_dir = arr_coor_3x3x3_dir.tolist()
    arr_coor_3x3x3_car = np.array(frac2cart(mat_lat, coor_3x3x3_dir))
    coor_3x3x3_car = arr_coor_3x3x3_car.tolist()

    pos_basis = coor_car[list_labelofel.index("!"+Qlab1)]
    distance = [np.linalg.norm(pos_basis - p) for p in arr_coor_3x3x3_car]

    index_dis_sort = np.array(distance).argsort()
    dis_sort = sorted(distance)

    dir_sort_1x1x1 = dis_sort[:int(sumatomnum)]
    index_dis_sort_1x1x1 = index_dis_sort[:int(sumatomnum)]
    label_dis_sort_1x1x1 = [list_labelofel[np.mod(int(i),int(sumatomnum))].replace("!","") for i in index_dis_sort_1x1x1]

    dis_tot.append(dir_sort_1x1x1[label_dis_sort_1x1x1.index(Qlab2)])

L = "img " + Qlab1 + "-" + Qlab2 + "\n"
for i1 in range(0, len(dirs)):
    if len(str(i1)) == 1: L += "0" + str(i1) + " "
    else: L += str(i1) + " "
    L +=  str("%04.4f" % dis_tot[i1]) + "\n"

with  open(path+"/dis_"+Qlab1+"-"+Qlab2+".dat","w") as f: f.write(L)
print("\n" + L + "\n")
# END: Program

