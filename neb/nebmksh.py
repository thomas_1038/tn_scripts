#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import shutil
import sys

argv = sys.argv
N = str(argv[2])
NPN = str(int(argv[2]) * 16)

a1 = "#!/bin/sh" + "\n"
a2 = "#PBS -l nodes="  + N + ":ppn=16" + "\n"
a3 = "cd $PBS_O_WORKDIR"+"\n"
a4 = "NPROCS=`wc -l <$PBS_NODEFILE`" + "\n"
a5 = "export MKL_NUM_THREADS=1" + "\n"
a6 = "\n"
a7 = "\n"
a8 = "mpiexec -np " + NPN + " /home/nakao/vasp/vasp_src/vasp.5.2_neb/vasp.5.2/vasp > std.out"

shlines = [a1,a2,a3,a4,a5,a6,a7,a8]

shname = str(argv[1]) + ".sh"
sh = open(shname,"w")
for line in shlines:
	sh.write(str(line))

#change the right
import os
from stat import *

os.chmod(shname, S_IXUSR | S_IRUSR | S_IWUSR )
