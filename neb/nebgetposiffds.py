#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, glob
import subprocess as sub
import numpy as np
import addmol.helper as hl

def neb_getposif(nowpath):
    maindirs = [x for x in os.listdir(nowpath) if os.path.isdir(nowpath+"/"+x) if "N2H" in x or "NH2" in x]
    path_ifPOSCARs = nowpath + "/ifPOSCARs"

    if not os.path.exists(path_ifPOSCARs): os.mkdir(path_ifPOSCARs)

    for maindir in maindirs:
        path_maindir = nowpath + "/" + maindir
        subdirs1 = [x for x in os.listdir(path_maindir) if os.path.isdir(path_maindir+"/"+x) if "cc" in x]

        print("")
        print(path_maindir)
        for subdir1 in subdirs1:
            path_subdir1 = path_maindir + "/" + subdir1
            path_subdir1_all0 = path_subdir1 + "/all0"

            imgdirs = sorted([x for x in os.listdir(path_subdir1_all0) if len(x) == 2])
            path_pos_ini = path_subdir1_all0 + "/" + imgdirs[0] + "/POSCAR"
            path_pos_fin = path_subdir1_all0 + "/" + imgdirs[-1] + "/POSCAR"

            shutil.copy(path_pos_ini, path_ifPOSCARs + "/" + maindir + "_" + subdir1 + "_all0_ini.vasp")
            shutil.copy(path_pos_fin, path_ifPOSCARs + "/" + maindir + "_" + subdir1 + "_all0_fin.vasp")

if __name__ == "__main__": neb_getposif(os.getcwd()); print("Done!")
# END: script