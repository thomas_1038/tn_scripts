#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/08/12 version 1.0
#2015/10/04 version 1.1 : Algorythm which count "0X" directory was changed.
#2015/12/08 version 1.2 : 
#This script make baclup directories in 0X directories.
#usage:script

import shutil
import glob
import os
import subprocess as sub
import threading
import re

path = os.getcwd()

images = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and re.match(r"\d+",x) and len(x) == 2 and "00" not in x]
images.sort()
del images[-1]

print("\nMake backup directories in each image directories!")
for i in images:
    print("\n" + str(i))
    path_dir = path + "/" + i
    os.chdir(path_dir)
    sub.call("mkbackup.py",shell=True)
print("\nEach backup directories have been made in image directories!") 

# make backup directory in current directory.
os.chdir(path)
sub.call("nebbarrier.pl",shell=True)
sub.call("nebef.pl > force.dat",shell=True)

dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and "backup" in x]
dirs.sort()

if os.path.exists(path+"/backup01") == False:
    dirname = "backup01"
else:
    if int(dirs[-1].replace("backup","")) in [1,2,3,4,5,6,7,8]:
        dirname = "backup0" + str(int(dirs[-1].replace("backup","")) + 1)
    else:
        dirname = "backup" + str(int(dirs[-1].replace("backup","")) + 1)

path_bdir = path+"/"+dirname
os.mkdir(path_bdir)

files = ["INCAR","neb.dat","force.dat","date.txt","std.out"]
for f in files:
    shutil.copy(path+"/"+f, path_bdir)

print("\nDone!\n")
# End Program
    
    
