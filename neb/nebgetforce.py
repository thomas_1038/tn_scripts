#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/12/10 version 1.00
#2017/10/10 version 1.10
#usage: script

import os, shutil, sys, math

def get_nebforce(nowpath, filename="NEBFORCE", stdout="stdout"):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    ind_force ="CHAIN + TOTAL"
    path_out = nowpath + "/OUTCAR"
    path_pos = nowpath + "/POSCAR"
    path_std = nowpath + "/" + stdout

    # read POSCAR
    if not os.path.exists(path_pos): 
        print("POSCAR does not exists! BYE!"); sys.exit()
    with open(path_pos,"r") as fp: poslines = fp.readlines()

    atomlabel = conv_line(poslines[5]); atomnum = conv_line(poslines[6]); sumatomnum = sum([int(x) for x in atomnum])
    na = 0; list_atomlabel = []
    while na < len(atomlabel):
        na2 = 0
        while na2 < int(atomnum[na]): list_atomlabel.append(atomlabel[na]+str(na2+1)); na2 += 1
        na += 1

    flagS = 0
    if "Selective" in poslines[7]: flagS = 1

    #make atom positon matrix
    num2 = 8 + flagS; atomposition = []
    while num2 < 8 + sumatomnum + flagS: atomposition.append(conv_line(poslines[num2])); num2 += 1

    #make fix word matrix
    num3 = 0; fixword = []
    while num3 < len(atomposition):
        list = []
        if flagS == 1: list.append(str(atomposition[num3][3]))
        list.append(list_atomlabel[num3])
        fixword.append(list); num3 += 1
    # END: read POSCAR

    # read stdout
    if not os.path.exists(path_std): stdout + "doesn't exist! BYE!"; sys.exit()
    with open(path_std) as fs: stdlines = fs.readlines()
    Etot = [str(conv_line(x)[4]) for x in stdlines if "F=" in x]

    # read OUTCAR
    if not os.path.exists(path_out): print("OUTCAR doesn't exist! BYE!") ; sys.exit()
    with open(path_out,"r") as fo: outlines = fo.readlines()

    #Definition of the variable
    n1 = 0; num_f = 0; Ftot = []; Ftot_relax = []
    Ftot_append = Ftot.append; Ftot_relax_append = Ftot_relax.append
    #Start: while
    while n1 < len(outlines):
        if ind_force not in outlines[n1]: n1 += 1
        else:
            num_f += 1; n1 += 2; fnum = int(n1) + int(sumatomnum)

            num_a = 0; list_Fi = []; list_Fi_relax = []
            list_Fi_append = list_Fi.append; list_Fi_relax_append = list_Fi_relax.append
            while n1 < fnum:
                Fi_atom = outlines[n1].replace("\n","").split(" ")
                while Fi_atom.count("") > 0: Fi_atom.remove("")
                #Fi_atom.pop(0); Fi_atom.pop(0); Fi_atom.pop(0)

                ftot = math.sqrt(float(Fi_atom[0])**2+ float(Fi_atom[1])**2 + float(Fi_atom[2])**2)
                Fi_atom += [str(ftot), str(fixword[num_a][0])]

                if flagS == 1 :
                    Fi_atom.append(str(fixword[num_a][1]))
                    if str(fixword[num_a][0]) == "T": list_Fi_relax_append(ftot)
                else: list_Fi_relax_append(ftot)

                list_Fi_append(Fi_atom)
                num_a += 1; n1 += 1

            Ftot_append(list_Fi)
            Ftot_relax_append(list_Fi_relax)

            if num_f == len(Etot): break
            else: continue

    print(Ftot_relax)
    #make maxforcelist
    list_maxF = [str(max(x)) for x in Ftot_relax]
    print(list_maxF)

    #make list_totalmaxforce
    spc = "  ";  n1 = 0; list_maxFtot = []
    for Fi in Ftot:
        for Fatom in Fi:
            if Fatom[3].find(str(list_maxF[n1])) >= 0:
                if flagS == 1: L = str(n1+1) + spc + Etot[n1] + spc + Fatom[3] + spc + Fatom[5]
                else: L = str(n1+1) + spc + Etot[n1] + spc + Fatom[3] + spc + Fatom[4]
                list_maxFtot.append(L)
        n1 += 1

    with open(nowpath + "/" + filename,'w') as fm: fm.write("\n".join(list_maxFtot))

if __name__  == "__main__": 
    get_nebforce(os.getcwd()); print("NEBFORCE has been made!")
# End: Program
