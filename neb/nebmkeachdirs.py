#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, shutil #, threading, glob, re
import subprocess as sub
#import numpy as np
nebmake = "nebmake.pl"

def conv_line(line):
    line = line.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').replace('^M', ' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
vfiles = sorted([x.replace("POSCAR", "").replace(".vasp", "") for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "POSCARm" in x])
img_signs = ["i"] + vfiles + ["f"]

Ldirs = ""
for i in range(0, len(img_signs)-1):
    name_imgdir = "img_" + img_signs[i] + img_signs[i+1]
    Ldirs += " " + name_imgdir

    path_dir = path + "/" + name_imgdir
    if not os.path.exists(path_dir): os.mkdir(path_dir)

    name_pos1 = "POSCAR" + img_signs[i] + ".vasp"
    name_pos2 = "POSCAR" + img_signs[i+1] + ".vasp"
    path_pos1 = path + "/" + name_pos1; path_pos2 = path + "/" + name_pos2
    shutil.copy(path_pos1, path_dir); shutil.copy(path_pos2, path_dir)

    print("\nimg_" + img_signs[i] + img_signs[i+1])
    num_img = input("Please input the number of images: ")
    os.chdir(path_dir)
    sub.call(nebmake + " " + name_pos1 + " " + name_pos2 + " " + num_img, shell=True)

os.chdir(path)
sub.call("nebmkdirs.py " + Ldirs, shell=True); sub.call("nebmkpcs.py p", shell=True)

imgdirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and "img_" in x]
vfiles_new = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and ".vasp" in x]

path_imgdirs = path + "/imgdirs"
if not os.path.exists(path_imgdirs): os.mkdir(path_imgdirs)
for x in imgdirs: shutil.move(path + "/" + x, path_imgdirs)

path_models = path + "/models"
if not os.path.exists(path_models): os.mkdir(path_models)
for x in vfiles_new: shutil.move(path + "/" + x, path_models)

print("\nDone!\n")
# END