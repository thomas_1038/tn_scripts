#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
#import numpy as np
import fractions as fra
from functools import reduce

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
argv = sys.argv

fi = open(path+"/INCAR")
inlines = fi.readlines()
fi.close()

POTIM = [x for x in inlines if "POTIM = " in x and "!" not in x][0]
chgwav1 = [x for x in inlines if "ISTART = 1" in x][0]
chgwav0 = [x for x in inlines if "ISTART = 0" in x][0]

i = argv[1]
L = ""
for line in inlines:
    if POTIM in line:
        line = "POTIM = " + str(float(i)*float(argv[2])) + "\n"
    if chgwav1 in line:
        line = "ICHARG = 1 ; ISTART = 1\n"
    if chgwav0 in line:
        line = "!ICHARG = 2 ; ISTART = 0\n"
    L += line

fi = open(path+"/INCAR"+str(i),"w")
fi.write(L)
fi.close()

print("Done!")
