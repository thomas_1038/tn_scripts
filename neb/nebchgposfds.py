#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, glob #, sys, shutil, threading, re
from neb import nebchgpos as ncp

tolerance_dis = 0
N_list = 7

list_el = ["Ca", "Al"]
dict_el = {"Ca":1.97, "Al":1.43}

def nebchg_pos_for_images():
    path = os.getcwd()
    img_dirs = glob.glob("[0-9][0-9]"); img_dirs.sort()
    Qlab = input("Please input label of atom you want to move, i.e., N1: ")
    Qmol = input("Please input lable of atoms of molecule you want to move, i.e., N1 H1 H2 H3: ")
    #list_mol = Qmol.split(" ")
    Qdis = input("Do you use distance between adatom and suppport from dictionary  [y/N]: ")
    if Qdis in ["y", "Y"]:
        for el in list_el:
            if el in dict_el: key = el
        disAts = float(dict_el[key])
    elif Qdis in ["n","N"]: disAtS = float(input("Please input the distance between adatom to support: "))

    Ltot = ""
    for d in img_dirs[1:-1]:
        print(d)
        os.chdir(path + "/" + d)
        Ltot += ncp.nebchg_pos_forsphere("POSCAR", Ltot, Qlab, Qmol, disAtS)
    with open(path + "/POSCARS", "w") as fps: fps.write(Ltot)

if __name__ == "__main__": nebchg_pos_for_images(); print("Done!")
# END: Program
