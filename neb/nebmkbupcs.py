#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
from addmol import readpos as rp

def nebmkpcs():
    argv = sys.argv

    """
    if len(argv) > 1: Lj = str(argv[1])
    else:
        Qj = input("Please input [ p,POSCAR / c,CONTCAR]: ")
        if Qj in ["p","POSCAR"]: Lj = "p"; nametag = "nebp"
        elif Qj in ["c","CONTCAR"]: Lj = "c"; nametag = "nebc"
        else: print("Please input Please input [ p,POSCAR / c,CONTCAR] only! BYE!"); sys.exit()
    """

    num_bakcup = input("Please input the number of backup directory, e.g., 04 or 11: ")

    path = os.getcwd()
    name_curdir = path.split("/")[-2] + path.split("/")[-1]
    dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and re.match(r"\d+",x) and len(x) == 2])
    vfiles = sorted([x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "vasp" in x])

    path_CONTCARs = path + "/eachCONTCARs"
    if os.path.isdir(path_CONTCARs)== False: os.mkdir(path_CONTCARs)

#    if Lj == "p":  path_dir_pc = path_POSCARs
    path_dir_pc = path_CONTCARs

    def read_write_pos(pclines, d, L):
        listofel = rp.get_labelofel(pclines)
        numofel = rp.get_numofel(pclines)
        sum_num_atoms = rp.get_sumofnumofel(pclines)
        list_numofel= rp.mk_labelofel(listofel,numofel)[0]
        firstlines, flagS = rp.get_firstlines(pclines) ; firstlines[0] = d
        mat_lat = rp.get_matrix(pclines)
        coor_car, coor_dir, list_SD = rp.get_coordinate(pclines, sum_num_atoms, mat_lat)

        Lpos = "\n".join(firstlines) + "\n"
        num_d = 0
        for x in coor_car:
            Lpos += "  ".join([str(i) for i in x])
            if flagS == 1: Lpos += "  " + " ".join([str(i) for i in list_SD[num_d]])
            Lpos += "  " + str(list_numofel[num_d]) + "\n"
            num_d += 1
        L += Lpos
        return L, Lpos

    Ltot = ""
    if len(dirs) == 0:
        for vf in vfiles:
            d = vf.replace(".vasp","")
            print(d)
            with open(path + "/" + vf) as fpc: pclines = fpc.readlines()
            Ltot, Lpos = read_write_pos(pclines, d, Ltot)

    else:
       for d in dirs:
            print(d)
            path_dir =  path + "/" + d
            path_dir_bu = path_dir + "/backup" + num_bakcup
            if d == dirs[0] or d == dirs[-1]: fp = open(path_dir + "/POSCAR")
            else: fp = open(path_dir_bu + "/CONTCAR")
#           else: print("Please input p/c after scritpt name! BYE!"); sys.exit()
            pclines = fp.readlines() ; fp.close()
            Ltot, Lpos = read_write_pos(pclines, d, Ltot)
#            with open(path_dir_pc+"/"+str(d)+".vasp","w") as fc: fc.write(Lpos)

    name_poscars = name_curdir + "b" + num_bakcup  + "_nebcPOSCARS"
    with open(path_dir_pc+ "/" + name_poscars, "w") as fps: fps.write(Ltot)

if __name__ == "__main__": nebmkpcs() ; print("\nDone!\n")
